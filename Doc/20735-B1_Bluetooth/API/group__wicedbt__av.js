var group__wicedbt__av =
[
    [ "Advanced Audio (A2DP)", "group__wicedbt__a2dp.html", "group__wicedbt__a2dp" ],
    [ "WICED Audio Connectionless Slave Broadcast (ACSB)", "group__wicedbt__av__ascb.html", "group__wicedbt__av__ascb" ],
    [ "Audio / Video Distribution (AVDTP)", "group__wicedbt__avdt.html", "group__wicedbt__avdt" ],
    [ "Audio / Video Remote Control (AVRCP)", "group__wicedbt__avrc.html", "group__wicedbt__avrc" ]
];