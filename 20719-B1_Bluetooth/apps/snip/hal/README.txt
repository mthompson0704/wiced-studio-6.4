--------------------------------------------------------------
HAL (Hardware Adaptation Layer) snippets (under apps\snip\hal)
--------------------------------------------------------------

ADC application
    This snippet demonstrates how to configure and use ADC in WICED boards to
    measure DC voltage on various DC input channels. See
    hal_adc\hal_adc_app.c.

Display application
    This implements a sample display application that configures and uses a
    128x128 LCD display to draw bitmaps and vector graphics using the u8lib
    graphics library and the on-chip DBI Type C interface. See
    demo/display_demo.c.
    
GPIO application
    This snippet demonstrates how to use WICED GPIO APIs to configure GPIOs as
    Input (with interrupt enabled) or Output pins.  See
    hal_gpio\hal_gpio_app.c.

PUART application
    This snippet demonstrates how to use the WICED PUART driver interface to
    send and receive bytes or a stream of bytes over the UART hardware.  See
    hal_puart\hal_puart_app.c.

PWM application
    This snippet demonstrates how to configure and use PWM in WICED boards to
    control the LED flashing.  See hal_pwm\hal_pwm_app.c.

SPI Master application
    This snippet demonstrates how to use the WICED SPI master driver interface
    to send and receive data over SPI interface. See spi_master\hal_spi_master.c

SPI Slave application
    This snippet demonstrates how to use the WICED SPI slave driver interface
    to send and receive data over SPI interface. See spi_slave\hal_spi_slave.c

I2C Master application
    This snippet demonstrates how to use the WICED I2C master driver interface
    to send and receive data over I2C interface. See i2c_master\hal_i2c_master.c
