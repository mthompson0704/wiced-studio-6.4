/*
********************************************************************
* THIS INFORMATION IS PROPRIETARY TO
* Cypress Semiconductor.
*-------------------------------------------------------------------
*                                                                        
*           Copyright (c) 2014 Cypress Semiconductor.
*                      ALL RIGHTS RESERVED                              
*                                                                       
********************************************************************/
#ifndef __HIDD_PMU_PRE_LPO_HOOK_H__
#define __HIDD_PMU_PRE_LPO_HOOK_H__

#ifdef __cplusplus
extern "C" {
#endif

void handleLhlMiaPreSleepProcessing(void);
void handleLhlMiaPostSleepProcessing(void);

#ifdef __cplusplus
}
#endif

#endif


