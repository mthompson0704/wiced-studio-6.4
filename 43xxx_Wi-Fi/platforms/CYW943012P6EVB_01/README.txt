------------------------
CYW943012P6EVB_01 - README
------------------------

Provider    : Cypress
Website     : www.cypress.com
Description : PSoC� 6 Pioneer Kit with ARM� Cortex�-M architecture (M4 and M0+) and 43012.
Schematics & Photos : /platforms/CYW943012P6EVB_01/schematics/

EVB Features
  Please refer to the documentation
