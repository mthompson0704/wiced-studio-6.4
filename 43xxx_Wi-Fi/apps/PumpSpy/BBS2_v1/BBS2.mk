#
# Copyright 2016, PumpSpy
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of PumpSpy.
#

NAME := BBS2

$(NAME)_SOURCES    := main.c \
					  ../common/globals.c \
					  ../common/initialization.c \
					  ../common/dct.c \
					  ../common/config_button_handler.c \
					  ../common/dns_lookup.c \
					  ../common/control_leds.c \
					  ../common/get_deviceid.c \
					  ../common/restful_methods.c \
					  ../common/timers.c \
					  ../common/access_points.c \
					  ../common/new_firmware.c \
					  isI2c/isI2c.c
#					  ../common/set_bor.c \

$(NAME)_INCLUDES := . \
                    ../common


$(NAME)_COMPONENTS := protocols/HTTP \
					  daemons/pumpspy_configuration \
					  daemons/ota_server 

GLOBAL_DEFINES += APPLICATION_STACK_SIZE=10000 
GLOBAL_DEFINES += APPLICATION_WATCHDOG_TIMEOUT_SECONDS=15
GLOBAL_DEFINES += REMOTEDEBUG=1
#GLOBAL_DEFINES += LOCAL_SERVER=1
#GLOBAL_DEFINES += TEST_SERVER=1
#GLOBAL_DEFINES += DEV_SERVER=1
#GLOBAL_DEFINES += API2_SERVER=1
GLOBAL_DEFINES += NO_LEDS=1
GLOBAL_DEFINES += PRODUCTION_SERVER=1
GLOBAL_DEFINES += BBS_SYSTEM
#GLOBAL_DEFINES += STOP_APP_HERE=1

APPLICATION_DCT := ../common/config_mode_dct.c

#NETWORK_CONFIG_DCT_H := network_config_dct.h

#WIFI_CONFIG_DCT_H := wifi_config_dct.h

#Set factory reset application to be this same application.
#FR_APP    := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
#DCT_IMAGE := $(OUTPUT_DIR)/DCT.stripped.elf
