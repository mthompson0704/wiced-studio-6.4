/*
 * get_deviceid.h
 *
 *  Created on: Dec 21, 2017
 *      Author: mthom
 */

#ifndef APPS_PUMPSPY_PUMPOUTLET_NEWCURRENT_GET_DEVICEID_H_
#define APPS_PUMPSPY_PUMPOUTLET_NEWCURRENT_GET_DEVICEID_H_

extern uint64_t get_deviceid();





#endif /* APPS_PUMPSPY_PUMPOUTLET_NEWCURRENT_GET_DEVICEID_H_ */
