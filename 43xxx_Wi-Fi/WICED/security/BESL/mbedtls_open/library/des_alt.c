/*
 *  FIPS-46-3 compliant Triple-DES implementation
 *
 *  Copyright (C) 2006-2015, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */
/*
 *  DES, on which TDES is based, was originally designed by Horst Feistel
 *  at IBM in 1974, and was adopted as a standard by NIST (formerly NBS).
 *
 *  http://csrc.nist.gov/publications/fips/fips46-3/fips46-3.pdf
 */

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#include <stdio.h>
#define mbedtls_printf printf

#if defined(MBEDTLS_DES_C)

#include "mbedtls/des_alt.h"

#include <string.h>

#if defined(MBEDTLS_DES_ALT)
#include "platform_crypto.h"

void mbedtls_des_init( mbedtls_des_context *ctx )
{
    ctx->mbedtls_des_hw_ctx = malloc ( sizeof( wiced_hw_des_context ));
    if ( ctx->mbedtls_des_hw_ctx == NULL )
    {
        return;
    }

    memset (ctx->mbedtls_des_hw_ctx, 0, sizeof(wiced_hw_des_context));
    wiced_hw_des_init((wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx );
}

void mbedtls_des_free( mbedtls_des_context *ctx )
{
    if ( ctx->mbedtls_des_hw_ctx != NULL )
    {
        wiced_hw_des_free((wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx );
        free ( ctx->mbedtls_des_hw_ctx );
        ctx->mbedtls_des_hw_ctx = NULL;
    }
}

void mbedtls_des3_init( mbedtls_des3_context *ctx )
{
    ctx->mbedtls_des3_hw_ctx = malloc ( sizeof( wiced_hw_des_context ));
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return;
    }

    memset (ctx->mbedtls_des3_hw_ctx, 0, sizeof(wiced_hw_des_context));
    wiced_hw_des3_init((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx );
}

void mbedtls_des3_free( mbedtls_des3_context *ctx )
{
    if ( ctx->mbedtls_des3_hw_ctx != NULL )
    {
        wiced_hw_des3_free((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx );
        free ( ctx->mbedtls_des3_hw_ctx );
        ctx->mbedtls_des3_hw_ctx = NULL;
    }
}

/*
 * DES key schedule (56-bit, encryption)
 */
int mbedtls_des_setkey_enc( mbedtls_des_context *ctx, const unsigned char key[MBEDTLS_DES_KEY_SIZE] )
{
    if ( ctx->mbedtls_des_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des_setkey((wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx, key );
}

/*
 * DES key schedule (56-bit, decryption)
 */
int mbedtls_des_setkey_dec( mbedtls_des_context *ctx, const unsigned char key[MBEDTLS_DES_KEY_SIZE] )
{
    if ( ctx->mbedtls_des_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des_setkey((wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx, key );
}

/*
 * Triple-DES key schedule (112-bit, encryption)
 */
int mbedtls_des3_set2key_enc( mbedtls_des3_context *ctx,
                      const unsigned char key[MBEDTLS_DES_KEY_SIZE * 2] )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des3_set2key((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx, key );
}

/*
 * Triple-DES key schedule (112-bit, decryption)
 */
int mbedtls_des3_set2key_dec( mbedtls_des3_context *ctx,
                      const unsigned char key[MBEDTLS_DES_KEY_SIZE * 2] )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des3_set2key((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx, key );
}

/*
 * Triple-DES key schedule (168-bit, encryption)
 */
int mbedtls_des3_set3key_enc( mbedtls_des3_context *ctx,
                      const unsigned char key[MBEDTLS_DES_KEY_SIZE * 3] )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des3_set3key((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx, key );
}

/*
 * Triple-DES key schedule (168-bit, decryption)
 */
int mbedtls_des3_set3key_dec( mbedtls_des3_context *ctx,
                      const unsigned char key[MBEDTLS_DES_KEY_SIZE * 3] )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des3_set3key((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx, key );
}

/*
 * DES-ECB block encryption/decryption
 */
int mbedtls_des_crypt_ecb( mbedtls_des_context *ctx,
                    const unsigned char input[8],
                    unsigned char output[8] )
{
    if ( ctx->mbedtls_des_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des_crypt_ecb( (wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx, input, output );
}

/*
 * DES-CBC buffer encryption/decryption
 */
int mbedtls_des_crypt_cbc( mbedtls_des_context *ctx,
                    int mode,
                    size_t length,
                    unsigned char iv[8],
                    const unsigned char *input,
                    unsigned char *output )
{
    if ( ctx->mbedtls_des_hw_ctx == NULL )
    {
        return -1;
    }

    if( length % 8 )
    {
        return( MBEDTLS_ERR_DES_INVALID_INPUT_LENGTH );
    }

    return wiced_hw_des_crypt_cbc((wiced_hw_des_context*) ctx->mbedtls_des_hw_ctx, mode, length, iv, input, output );
}

/*
 * 3DES-ECB block encryption/decryption
 */
int mbedtls_des3_crypt_ecb( mbedtls_des3_context *ctx,
                     const unsigned char input[8],
                     unsigned char output[8] )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    return wiced_hw_des3_crypt_ecb((wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx, input, output );
}

/*
 * 3DES-CBC buffer encryption/decryption
 */
int mbedtls_des3_crypt_cbc( mbedtls_des3_context *ctx,
                     int mode,
                     size_t length,
                     unsigned char iv[8],
                     const unsigned char *input,
                     unsigned char *output )
{
    if ( ctx->mbedtls_des3_hw_ctx == NULL )
    {
        return -1;
    }

    if( length % 8 )
    {
        return( MBEDTLS_ERR_DES_INVALID_INPUT_LENGTH );
    }

    return wiced_hw_des3_crypt_cbc( (wiced_hw_des_context*) ctx->mbedtls_des3_hw_ctx,mode,length,iv, input, output );
}

/* WICED_MBEDTLS Start */
int mbedtls_des_encrypt_ecb( mbedtls_des_context *des_ctx,
                    const unsigned char input[8],
                    unsigned char output[8] )

{
    if ( des_ctx->mbedtls_des_hw_ctx == NULL )
    {
        return -1;
    }

    wiced_hw_des_context* des_hw_ctx = (wiced_hw_des_context*) des_ctx->mbedtls_des_hw_ctx;
    des_hw_ctx->direction = MBEDTLS_DES_ENCRYPT;
    return mbedtls_des_crypt_ecb(des_ctx, input, output);
}
/* WICED_MBEDTLS End */

#if defined(MBEDTLS_SELF_TEST)
/*
 * DES and 3DES test vectors from:
 *
 * http://csrc.nist.gov/groups/STM/cavp/documents/des/tripledes-vectors.zip
 */
static const unsigned char des3_test_keys[24] =
{
    0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
    0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01,
    0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23
};

static const unsigned char des3_test_buf[8] =
{
    0x4E, 0x6F, 0x77, 0x20, 0x69, 0x73, 0x20, 0x74
};

static const unsigned char des3_test_ecb_dec[3][8] =
{
    { 0xCD, 0xD6, 0x4F, 0x2F, 0x94, 0x27, 0xC1, 0x5D },
    { 0x69, 0x96, 0xC8, 0xFA, 0x47, 0xA2, 0xAB, 0xEB },
    { 0x83, 0x25, 0x39, 0x76, 0x44, 0x09, 0x1A, 0x0A }
};

static const unsigned char des3_test_ecb_enc[3][8] =
{
    { 0x6A, 0x2A, 0x19, 0xF4, 0x1E, 0xCA, 0x85, 0x4B },
    { 0x03, 0xE6, 0x9F, 0x5B, 0xFA, 0x58, 0xEB, 0x42 },
    { 0xDD, 0x17, 0xE8, 0xB8, 0xB4, 0x37, 0xD2, 0x32 }
};

#if defined(MBEDTLS_CIPHER_MODE_CBC)
static const unsigned char des3_test_iv[8] =
{
    0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF,
};

static const unsigned char des3_test_cbc_dec[3][8] =
{
    { 0x12, 0x9F, 0x40, 0xB9, 0xD2, 0x00, 0x56, 0xB3 },
    { 0x47, 0x0E, 0xFC, 0x9A, 0x6B, 0x8E, 0xE3, 0x93 },
    { 0xC5, 0xCE, 0xCF, 0x63, 0xEC, 0xEC, 0x51, 0x4C }
};

static const unsigned char des3_test_cbc_enc[3][8] =
{
    { 0x54, 0xF1, 0x5A, 0xF6, 0xEB, 0xE3, 0xA4, 0xB4 },
    { 0x35, 0x76, 0x11, 0x56, 0x5F, 0xA1, 0x8E, 0x4D },
    { 0xCB, 0x19, 0x1F, 0x85, 0xD1, 0xED, 0x84, 0x39 }
};
#endif /* MBEDTLS_CIPHER_MODE_CBC */

/*
 * Checkup routine
 */
int mbedtls_des_self_test( int verbose )
{
    int i, j, u, v, ret = 0;
    mbedtls_des_context ctx;
    mbedtls_des3_context ctx3;
    unsigned char buf[8];
#if defined(MBEDTLS_CIPHER_MODE_CBC)
    unsigned char prv[8];
    unsigned char iv[8];
#endif

    mbedtls_des_init( &ctx );
    mbedtls_des3_init( &ctx3 );
    /*
     * ECB mode
     */
    for( i = 0; i < 6; i++ )
    {
        u = i >> 1;
        v = i  & 1;

        if( verbose != 0 )
            mbedtls_printf( "  DES%c-ECB-%3d (%s): ",
                             ( u == 0 ) ? ' ' : '3', 56 + u * 56,
                             ( v == MBEDTLS_DES_DECRYPT ) ? "dec" : "enc" );

        memcpy( buf, des3_test_buf, 8 );

        switch( i )
        {
        case 0:
            mbedtls_des_setkey_dec( &ctx, des3_test_keys );
            break;

        case 1:
            mbedtls_des_setkey_enc( &ctx, des3_test_keys );
            break;

        case 2:
            mbedtls_des3_set2key_dec( &ctx3, des3_test_keys );
            break;

        case 3:
            mbedtls_des3_set2key_enc( &ctx3, des3_test_keys );
            break;

        case 4:
            mbedtls_des3_set3key_dec( &ctx3, des3_test_keys );
            break;

        case 5:
            mbedtls_des3_set3key_enc( &ctx3, des3_test_keys );
            break;

        default:
            return( 1 );
        }

        for ( j = 0; j < 10000; j++ )
        {
            if ( u == 0 )
            {
                if ( v == DES_DECRYPT )
                {
                    hw_des_context_t* hw_ctx = (hw_des_context_t*) ctx.mbedtls_des_hw_ctx;
                    hw_ctx->direction = DES_DECRYPT;

                    mbedtls_des_crypt_ecb( &ctx, buf, buf );
                }
                else
                {
                    hw_des_context_t* hw_ctx = (hw_des_context_t*) ctx.mbedtls_des_hw_ctx;
                    hw_ctx->direction = DES_ENCRYPT;
                    mbedtls_des_crypt_ecb( &ctx, buf, buf );
                }
            }
            else
            {
                if ( v == DES_DECRYPT )
                {

                    hw_des_context_t* hw_ctx = (hw_des_context_t*) ctx3.mbedtls_des3_hw_ctx;
                    hw_ctx->direction = DES_DECRYPT;

                    mbedtls_des3_crypt_ecb( &ctx3, buf, buf );
                }
                else
                {
                    hw_des_context_t* hw_ctx = (hw_des_context_t*) ctx3.mbedtls_des3_hw_ctx;
                    hw_ctx->direction = DES_ENCRYPT;
                    mbedtls_des3_crypt_ecb( &ctx3, buf, buf );
                }
            }
        }

        if( ( v == MBEDTLS_DES_DECRYPT &&
                memcmp( buf, des3_test_ecb_dec[u], 8 ) != 0 ) ||
            ( v != MBEDTLS_DES_DECRYPT &&
                memcmp( buf, des3_test_ecb_enc[u], 8 ) != 0 ) )
        {
            if( verbose != 0 )
                mbedtls_printf( "failed\n" );

            ret = 1;
            goto exit;
        }

        if( verbose != 0 )
            mbedtls_printf( "passed\n" );
    }

    if( verbose != 0 )
        mbedtls_printf( "\n" );

#if defined(MBEDTLS_CIPHER_MODE_CBC)
    /*
     * CBC mode
     */
    for( i = 0; i < 6; i++ )
    {
        u = i >> 1;
        v = i  & 1;

        if( verbose != 0 )
            mbedtls_printf( "  DES%c-CBC-%3d (%s): ",
                             ( u == 0 ) ? ' ' : '3', 56 + u * 56,
                             ( v == MBEDTLS_DES_DECRYPT ) ? "dec" : "enc" );

        memcpy( iv,  des3_test_iv,  8 );
        memcpy( prv, des3_test_iv,  8 );
        memcpy( buf, des3_test_buf, 8 );

        switch( i )
        {
        case 0:
            mbedtls_des_setkey_dec( &ctx, des3_test_keys );
            break;

        case 1:
            mbedtls_des_setkey_enc( &ctx, des3_test_keys );
            break;

        case 2:
            mbedtls_des3_set2key_dec( &ctx3, des3_test_keys );
            break;

        case 3:
            mbedtls_des3_set2key_enc( &ctx3, des3_test_keys );
            break;

        case 4:
            mbedtls_des3_set3key_dec( &ctx3, des3_test_keys );
            break;

        case 5:
            mbedtls_des3_set3key_enc( &ctx3, des3_test_keys );
            break;

        default:
            return( 1 );
        }

        if( v == MBEDTLS_DES_DECRYPT )
        {
            for( j = 0; j < 10000; j++ )
            {
                if( u == 0 )
                    mbedtls_des_crypt_cbc( &ctx, v, 8, iv, buf, buf );
                else
                    mbedtls_des3_crypt_cbc( &ctx3, v, 8, iv, buf, buf );
            }
        }
        else
        {
            for( j = 0; j < 10000; j++ )
            {
                unsigned char tmp[8];

                if( u == 0 )
                    mbedtls_des_crypt_cbc( &ctx, v, 8, iv, buf, buf );
                else
                    mbedtls_des3_crypt_cbc( &ctx3, v, 8, iv, buf, buf );

                memcpy( tmp, prv, 8 );
                memcpy( prv, buf, 8 );
                memcpy( buf, tmp, 8 );
            }

            memcpy( buf, prv, 8 );
        }

        if( ( v == MBEDTLS_DES_DECRYPT &&
                memcmp( buf, des3_test_cbc_dec[u], 8 ) != 0 ) ||
            ( v != MBEDTLS_DES_DECRYPT &&
                memcmp( buf, des3_test_cbc_enc[u], 8 ) != 0 ) )
        {
            if( verbose != 0 )
                mbedtls_printf( "failed\n" );

            ret = 1;
            goto exit;
        }
        if ( verbose != 0 )
            mbedtls_printf( "passed\n" );

    }
#endif /* MBEDTLS_CIPHER_MODE_CBC */

    if( verbose != 0 )
        mbedtls_printf( "\n" );

exit:
    mbedtls_des_free( &ctx );
    mbedtls_des3_free( &ctx3 );

    return( ret );
}

#endif /* MBEDTLS_SELF_TEST */

#endif /* MBEDTLS_DES_ALT */
#endif /* MBEDTLS_DES_C */
