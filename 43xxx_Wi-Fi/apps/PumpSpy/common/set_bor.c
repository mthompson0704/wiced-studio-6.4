#include <stdlib.h>
#include "wiced.h"

//#define OB_BOR_LEVEL3          ((uint8_t)0x00)  /*!< Supply voltage ranges from 2.70 to 3.60 V */
//#define OB_BOR_LEVEL2          ((uint8_t)0x04)  /*!< Supply voltage ranges from 2.40 to 2.70 V */
//#define OB_BOR_LEVEL1          ((uint8_t)0x08)  /*!< Supply voltage ranges from 2.10 to 2.40 V */
//#define OB_BOR_OFF             ((uint8_t)0x0C)  /*!< Supply voltage ranges from 1.62 to 2.10 V */

void set_bor(void)
{
    //
    // configure the brown out reset
    //
    WPRINT_APP_INFO( ( "\r\nCalling FLASH_OB_UnLock\r\n" ) );
	FLASH_OB_Unlock();
	uint8_t ret_status = FLASH_OB_GetBOR();
    WPRINT_APP_INFO( ( "FLASH_OB_GetBOR %d\r\n", ret_status ) );
    WPRINT_APP_INFO( ( "calling FLASH_OB_BORConfig %d\r\n", OB_BOR_LEVEL1) );
	FLASH_OB_BORConfig(OB_BOR_LEVEL1);
//  FLASH_OB_BORConfig(OB_BOR_LEVEL2);
//	FLASH_OB_BORConfig(OB_BOR_LEVEL3);
    WPRINT_APP_INFO( ( "Calling FLASH_OB_Launch\r\n" ) );
	FLASH_Status fs = FLASH_OB_Launch();
    WPRINT_APP_INFO( ( "FLASH_Stats fs %d\r\n", (int)fs) );
    WPRINT_APP_INFO( ( "Calling FLASH_OB_Lock\r\n" ) );
	FLASH_OB_Lock();
	ret_status = FLASH_OB_GetBOR();
    WPRINT_APP_INFO( ( "reconfigured FLASH_OB_GetBOR %d\r\n", ret_status ) );

}