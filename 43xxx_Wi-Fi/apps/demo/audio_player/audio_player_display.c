/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 */

#include "wiced.h"
#include "wiced_log.h"

#include "audio_player_display.h"



/******************************************************
 *                     Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
#define TITLE_CLEAN_MAX_LEN                    (28)
#define TITLE_CLEAN_EXTENSION_LEN               (4)

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
#ifndef AUDIO_PLAYER_ENABLE_DISPLAY
wiced_result_t audio_player_display_start(audio_player_display_t* display, char* area, char* group, char* device, wiced_bool_t use_wifi_area)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_display_stop(audio_player_display_t* display)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_display_title(audio_player_display_t* display, char* title, char* info)
{
    return WICED_SUCCESS;
}

#else
wiced_result_t audio_player_display_start(audio_player_display_t *display, char* area, char* group, char* device, wiced_bool_t use_wifi_area)
{
    if ((NULL == display) )
    {
        return WICED_ERROR;
    }

    /* reset basic info */
    memset(&display->info, 0, sizeof(display->info));

    /* management thread */
    if (use_wifi_area)
    {
        audio_display_create_management_thread(&display->thread, WICED_AUDIO_DISPLAY_ENABLE_WIFI);
    }
    else
    {
        audio_display_create_management_thread(&display->thread, WICED_AUDIO_DISPLAY_ENABLE_DEFAULT);
    }

    sprintf(display->info, "%s/%s/%s", area, group, device);

    /* bus serialization */
    wiced_rtos_init_mutex(&display->i2c_bus_mtx);
    audio_display_set_bus_mtx(&display->i2c_bus_mtx);

    /* basic info */
    audio_display_header_update_options(BATTERY_ICON_IS_VISIBLE | BATTERY_ICON_SHOW_PERCENT | SIGNAL_STRENGTH_IS_VISIBLE);

    audio_display_footer_update_song_info(display->name, display->info);

    audio_display_footer_update_options(FOOTER_IS_VISIBLE | FOOTER_CENTER_ALIGN);

    /* mark as ready */
    display->is_ready = WICED_TRUE;

    return WICED_SUCCESS;

}

wiced_result_t audio_player_display_stop(audio_player_display_t* display)
{
    if (NULL == display)
    {
        return WICED_ERROR;
    }

    if (WICED_TRUE == display->is_ready)
    {
        wiced_rtos_delete_thread(&display->thread);

        wiced_rtos_deinit_mutex(&display->i2c_bus_mtx);

        display->is_ready = WICED_FALSE;
    }

    return WICED_SUCCESS;
}

wiced_result_t audio_player_display_title(audio_player_display_t* display, char* title, char* info)
{
    char* title_begin = title;
    char* title_slash = title;

    char title_clean[TITLE_CLEAN_MAX_LEN+1] = "\0";
    char title_ascii[TITLE_CLEAN_MAX_LEN+1] = "\0";

    uint8_t title_clean_idx = 0;
    uint8_t ch2cpy = 0;


    if ((NULL == display) || (WICED_TRUE != display->is_ready))
    {
        return WICED_ERROR;
    }

    /* cleanup special chars && find the last title name in a link */
    while (*title_slash != '\0')
    {
        /* last slash */
        if (*title_slash == '/')
        {
            title_begin = title_slash + 1;
        }

        title_slash++;
    }

    /* search&replace ANSI %20 with a space */
    title_clean_idx = 0;
    title_slash     = title_begin;

    while ((title_clean_idx < TITLE_CLEAN_MAX_LEN) && (*title_slash != '\0'))
    {
        if (title_clean_idx < TITLE_CLEAN_MAX_LEN - 3)
        {
            if ( (*(title_slash + 0) == '%') &&
                 (*(title_slash + 1) == '2') &&
                 (*(title_slash + 2) == '0') )
            {
                ch2cpy = (title_slash - title_begin);
                ch2cpy = ((title_clean_idx + ch2cpy) >= TITLE_CLEAN_MAX_LEN) ? (TITLE_CLEAN_MAX_LEN - (title_clean_idx + 1)) : ch2cpy;

                strncpy((title_clean + title_clean_idx), title_begin, ch2cpy);

                title_clean_idx += ch2cpy ;          /* add one space */
                title_clean[title_clean_idx] = ' ';  /* space char */
                title_clean_idx++;

                /* skip %20 code */
                title_slash += 3;

                title_begin = title_slash;
            }
        }

        title_slash++;
    }

    /* copy last part */
    if (title_clean[strlen(title_clean) - TITLE_CLEAN_EXTENSION_LEN + 1] == '.')
    {
        ch2cpy = (title_slash - title_begin) - TITLE_CLEAN_EXTENSION_LEN;
    }
    else
    {
        ch2cpy = (title_slash - title_begin);
    }

    ch2cpy = ((title_clean_idx + ch2cpy) >= TITLE_CLEAN_MAX_LEN) ? (TITLE_CLEAN_MAX_LEN - (title_clean_idx)) : ch2cpy;

    strncpy((title_clean+title_clean_idx), title_begin, ch2cpy);

    /* UTF-8 removal (need a proper UTF-8_to_ASCII converter) */
    ch2cpy          = 0;
    title_clean_idx = 0;
    title_begin     = title_clean;
    title_slash     = title_clean;

    while ((title_clean_idx < TITLE_CLEAN_MAX_LEN) && (*title_slash != '\0'))
    {
        ch2cpy = 0;

        /* clean upper ascii */
        if (*title_slash < 0)
        {
            ch2cpy = title_slash - title_begin;
            ch2cpy = ((title_clean_idx + ch2cpy) >= TITLE_CLEAN_MAX_LEN) ? (TITLE_CLEAN_MAX_LEN - title_clean_idx) : ch2cpy;

            while (*title_slash < 0)
            {
                title_slash++;
            }

            strncpy((title_ascii + title_clean_idx), title_begin, ch2cpy);
            title_clean_idx += ch2cpy;
            title_begin = title_slash;
        }

        title_slash++;
    }

    /* last chunk */
    ch2cpy = title_slash - title_begin;
    ch2cpy = ((title_clean_idx + ch2cpy) >= TITLE_CLEAN_MAX_LEN) ? (TITLE_CLEAN_MAX_LEN - title_clean_idx) : ch2cpy;

    strncpy((title_ascii + title_clean_idx), title_begin, ch2cpy);

    /* display update */
    if (NULL == info)
    {
        audio_display_footer_update_song_info(title_ascii, display->info);
    }
    else
    {
        audio_display_footer_update_song_info(title_ascii, info);
    }

    return WICED_SUCCESS;
}

#endif
