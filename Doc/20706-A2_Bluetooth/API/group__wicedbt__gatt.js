var group__wicedbt__gatt =
[
    [ "Server", "group__gatt__server__api__functions.html", "group__gatt__server__api__functions" ],
    [ "Client", "group__gatt__client__api__functions.html", "group__gatt__client__api__functions" ],
    [ "Common", "group__gatt__common__api__functions.html", "group__gatt__common__api__functions" ],
    [ "GATT Database", "group__gattdb__api__functions.html", "group__gattdb__api__functions" ],
    [ "GATT Multi-Profile (GATT_MP) library", "group__wiced__bt__gatt__mp__api__functions.html", "group__wiced__bt__gatt__mp__api__functions" ],
    [ "GATT Utilities", "group__wicedbt__gatt__utils.html", "group__wicedbt__gatt__utils" ]
];