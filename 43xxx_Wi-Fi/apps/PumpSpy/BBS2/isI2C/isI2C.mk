#
# Copyright 2018, Inventek Systems, LLC
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Inventek Systems, LLC;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Inventek Systems, LLC..
#

NAME := isLib_isI2C

$(NAME)_SOURCES := isI2C.c

GLOBAL_INCLUDES := .

