/*
 * Copyright 2018, Inventek Systems, LLC
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Inventek Systems;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Inventek Systems.
 */

#ifndef ISI2C_H_
#define ISI2C_H_

#include "wiced_result.h"

/******************************************************
 *                    Constants
 ******************************************************/


/**********************************************************************************************************************
 * MACROS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * ENUMS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * API Prototypes
 *********************************************************************************************************************/
wiced_result_t ism_i2c_init( wiced_bool_t use_reset_device );
wiced_result_t ism_i2c_deinit(  wiced_bool_t use_reset_device );
wiced_result_t ism_i2c_write( wiced_bool_t use_reset_device, const uint8_t* data_out, uint32_t size );
wiced_result_t ism_i2c_read( uint8_t* data_in, uint32_t size, uint32_t timeout_ms );


#endif /* ISI2C_H_ */
