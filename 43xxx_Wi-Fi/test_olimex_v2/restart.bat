@echo off
setlocal enabledelayedexpansion

echo --------------------------
echo Resetting target
echo --------------------------
if "%1" == "OCD" (
echo Using ARM-USB-OCD
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-OCD.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown > openocd_log.txt 2>&1 && echo Target running
) else (
echo Using ARM-USB-TINY-H
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown > openocd_log.txt 2>&1 && echo Target running
)