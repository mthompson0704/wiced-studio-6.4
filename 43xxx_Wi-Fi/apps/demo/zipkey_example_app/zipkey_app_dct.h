/*
 * Copyright (C) 2018, Cirrent Inc
 *
 * All use of this software must be covered by a license agreement signed by Cirrent Inc.
 *
 * DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
 * ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE.
 */

/*
 * The example DCT structure definition for Cirrent Agent
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "cirrent.h"

typedef struct
{
    CA_DCT_CONFIG_T ca_config;

    /*
     * Put your data here
     */
} APP_DCT_T;

#ifdef __cplusplus
} /* extern "C"*/
#endif
