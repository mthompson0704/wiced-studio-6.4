=============================================================================== 
MQTT+Audio_Player HowTo:
===============================================================================

Audio_player is part of WICED SDK demo applications. 
The Audio_player application allows to play a file from a remote webserver 
and it is controlled via a uart interface on CYW943907-WAE4 or 
CYW943907-AEVAL1F boards.

This HowTo is about the extension of audio_player with the support for MQTT 
and SNTP protocols to allow a�remote control of�multiple�heterogeneous 
audio boards from a single command.

===============================================================================

1. Use Case
2. Streaming Architecture
3. General Setup 
    3.1 Three Layer multi-room
4 Server Setup 
    4.1 Raspbian install
    4.2 Hostname change
    4.3 Http server install
    4.4 MQTT broker install
    4.5 NTP server install
5 Client Setup 
    5.1 Code compilation
    5.2 Voice audio files
    5.3 Command line config
6 Remote Control 
    6.1 MQTT_player script
    6.2 Audio PLL tuning 
    6.3 Volume control
7 Advanced Remote Control 
    7.1 Beet file manager 
    7.1.1 Install Beet
    7.1.2 Use Beet
    7.1.3 Import audio library
    7.1.4 Search library: list
    7.1.5 Mqtt_player + Beet


    
1. Use Case
===============================================================================

The typical use cases for an MQTT controlled audio_player have 
the following requirements:

* wide area to be covered by a wired/wireless network,
* not a strong requirement on audio synchronization between multiple rooms
* each room has only one audio-player device (L+R playback from the same board)
* remote interface from a single point of control, 
  either the media server or a mobile application
* standard protocols for time distribution (NTP)
* standard protocols for multimedia streaming (HTTP)

For example some practical use cases can be:
* public announcement systems (hotels, offices, public areas)
* home multi-room audio
* offline streaming of pre-recorded audio (music or voice)
* streaming for native compressed format (audio files)
* streaming from remote sources (web radio) in native compressed format



2. Streaming Architecture
===============================================================================

We will focus on the home environment use case.
* The house has an internet router providing local DHCP networking 
  (wired or wireless) and NTP time sync to the whole home network.
* There is a single music storage (NAS) with a webserver on it (lighttpd) 
  providing audio files in HTTP streaming for playback.
* There are multiple playback devices in different rooms, but each room has 
  one (only one) playback device controlled by MQTT
  
  
  
3. General Setup
===============================================================================
You will need few items for the demo setup:
1. A WiFi router providing coverage for the whole area. 
   It is preferred to have 5G band. Using 2.4G will work thanks to HTTP streaming
   playback but for very congested environments it might cause delays on MQTT
   delivery and time difference in playback between boards.
2. One or more NTP server. 
   If your WiFi router is also providing internet connection you might use
   remote NTP servers from pool.ntp.org but it won't be optimal for playback
   timing accuracy and once again can cause small time differences between boards
   due to NTP jitter over the internet. 
   If your router also provides local NTP relay it is STRONGLY SUGGESTED 
   to use the local NTP server (the router) for NTP distribution.
   In this setup we use a local NTP server from the NAS itself.
3. One music file storage (NAS) providing http_streaming to the audio files, 
   a Raspberry-Pi board will be used for this setup.
5. Multiple WAE4 boards, one for each room you want to cover. You can also use
   AEVAL1F boards with audio shield.
   
   
3.1 Three Layer multi-room
==========================
The application has been designed to subscribe to a specific MQTT topic formed 
by a tree level of "grouping" of each device: 

AREA_NAME/GROUP_NAME/DEVICE_NAME

This is useful to control the playback of a single room (for example device01, 
device02 ... device##) or a group of rooms (for example group01) or a wider area.

If we focus on a home environment with multiple rooms we can think to a list
of playback devices grouped and named like this:

AREA : cyhome
   +--- GROUP: day
          +--- DEVICE: living
          +--- DEVICE: kitchen
   +--- GROUP: night
          +--- DEVICE: master
          +--- DEVICE: kids
          +--- DEVICE: guest
   +--- GROUP: outside
          +--- DEVICE: patio
          +--- DEVICE: garden
          +--- DEVICE: backyard
          
NOTE: the device is precisely the device name (i.e. the name of the board) 
and it should be used also for network indentification for the DHCP request.

IMPORTANT:
The rationale behind this three-layer design is to provide a simple 
but effective way to easily configure what music should to be played 
in which zone of the home.

For example we want to send one single command controlling music playback in
one subset of the playback devices like this:

* play "bohemian rapsody" in the outside area of my home, or
* play "van helen" in the whole home area (everywhere!), or
* play "beetoven" only in the night time area of the home, or
* play "walt disney" song in the kids room only (!)



4. Server Setup
===============================================================================
The system requires to have one MQTT broker and one HTTP streaming server 
into the same network where all the audio_player devices are connected.
(i.e. same WiFi SSID)

These two servers can be separated on two different entities 
(for example the MQTT broker could easily be placed on the router itself) 
but for this demo setup it is wise to place both on the same entity: 
in this demo we use a Raspberry-Pi board connected (wired) to the WiFi router.


4.1 Raspbian install
====================
Install "Raspbian Stretch" on a Raspberry-Pi card and make sure you know 
the IP address of the Raspberry itself by looking to the DHCP table of your 
router or by connecting an HDMI monitor and console.

You can download the latest image from 

https://www.raspberrypi.org/downloads/raspbian/ 

and use Win32 Disk Imager (https://sourceforge.net/projects/win32diskimager/) 
to flash a brand new sdcard. We won't cover all the steps to install Raspbian 
since they are fully covered from the official site: 

https://www.raspberrypi.org/documentation/installation/installing-images/
�
Notes:
1) please use a LARGE sdcard so you can store your music files on it. 
   16GB is the starting point, 32GB is better 
   (audio player will support WAV, FLAC, MP3, and AAC)
2) make sure you can access the board using SSH if you do not plan to 
   use a keyboard/monitor for it (simply place an empty file into the "boot" 
   partition and call it "ssh" with no extension)
   
   
4.2 Hostname change
===================
It is suggested to use a proper hostname different from 
the default "raspberrypi". We will use "audionas".

From console type:

sudo raspi-config

and select NETWORK-OPTIONS -> HOSTNAME. Type "audionas" save and select finish.
Issue a REBOOT to allow the hostname refresh in your DHCP server.


4.3 HTTP server install
===================
The audio player will play a song with an MQTT command carrying one 
HTTP link into the payload.

It is important to share the music storage folder with the correct permissions 
so that the installed webserver will be able to access your music storage folder.

For this demo we will assume that your music files root folder is:
/home/pi/music

The webserver to be used is LightHTTPD and you can install it on your rasberrypi
by typing the commands:

sudo apt-get update
sudo apt-get install lighttpd

Now make sure your music folder has the correct permissions, type the commands:

cd /home/pi
mkdir music
chown -R pi:www-data home/pi/music

Place your music files into the music folder, 
later we will use an external tool to import them and search into the 
advanced section of this HowTo.

Once you have placed your files into the /home/pi/music folder you have to 
make sure the permissions are set, and this must be done each time you copy 
files on your folder. Type and execute these commands on your raspberry console:

chown -R pi:www-data /home/pi/music
find /home/pi/music -type d -exec chmod 777 {} \;
find /home/pi/music -type f -exec chmod 666 {} \;

Now the last step is to link the music repository into the web root 
(this needs to be done only once)

cd /var/www/html
sudo ln -s /home/pi/music/ ./music

Your music files are now ready to be played by the mqtt+audio_player 
but we need the MQTT broker installed on our NAS (i.e. the raspberry board)


4.4 MQTT broker install
=======================

As an MQTT broker for the raspbian distribution We will use the Mosquitto server:

sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients

your NAS basic setup is now ready, take a note of your NAS ipaddress�by typing:


pi@audionas:~ $ ifconfig

eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.1  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::bf73:412d:e6b5:8642  prefixlen 64  scopeid 0x20<link>
        ether b8:27:eb:ff:c8:fb  txqueuelen 1000  (Ethernet)
        RX packets 187  bytes 24996 (24.4 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 147  bytes 19951 (19.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wlan0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        ether b8:27:eb:aa:9d:ae  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


NOTE: Since the raspberry is connected with a eth wire we do care about 
the "wired" ipaddress only.


4.5 NTP server install
======================
If you do not have a local NTP server on your home router you can set the 
Raspberry-Pi to be the local NTP server for your media streaming over your
local WLAN.

Having a LOCAL NTP server instead of a server in the cloud is important 
for time accuracy and has few advantages:

* works with minimal latency
* minimize the response time to all the mqtt_player clients
* works behind strong firewalls

It is common for home routers to also act as NTP servers for the local network, 
usually there is an option for "distribute time to lan subnet".

If you want to use raspberrypi as the local NTP server keep a note of the 
Raspberry-Pi IP address so that you can point each audio_player device to it.

On your raspberry console install the NTP package by typing the commands:

sudo apt-get update
sudo apt-get install ntp

NOTE:
The newer version of Raspbian have preinstalled�timesyncd service. 
It can be used for the same purpose, but you can not have running both 
of them at the same time. To fix this you can stop and disable timesyncd 
service using these commands.

sudo systemctl stop systemd-timesyncd
sudo systemctl disable systemd-timesyncd
sudo /etc/init.d/ntp stop
sudo /etc/init.d/ntp start

Now we need to set the NTP server as the local NTP provider for the subnet 
of your lan. You need to edit the file /etc/ntp.conf and set/add the lines:

restrict 192.168.1.0 mask 255.255.255.0
broadcast 192.168.1.255
broadcast 224.0.1.1

IMPORTANT:
Make sure you use the same subnet of your real network configuration and not 
the one used here as an example.
 
Now that the NTP parameters are set you need to restart the service with:

sudo /etc/init.d/ntp restart

In the next session you will point your audio_clients to the local 
Raspberry-Pi address (192.168.1.1 in this example)



5. Client Setup
================================================================================
The client application should be compiled for a CYW43907-WAE4 board or for 
a CYW43907AEVAL1F/CYW54907AEVAL1F.

Note that the AEVAL1F needs an external audio shield to play audio.


5.1 code compilation
====================
The code is placed in the WICED_SDK under the folder "apps/demo/audio_player"

Edit the makefile "audio_player.mk" and turn on the defines for 
- USE_MQTT
- USE_VOICE

If your board is a WAE4 or you have added an I2C oled display (SSD1306) 
you can also enable the USE_DISPLAY define

[SNIP]
#
# MQTT frontend for multi-room audio player deployement
# enable this feature to control multiple audio_player devices
# from a single MQTT broker command
USE_MQTT := 1
ifdef USE_MQTT
GLOBAL_DEFINES     += AUDIO_PLAYER_ENABLE_MQTT
endif

#
# FILE PLAYBACK support for voice feedback from SDCARD
# enable this feature only if your device has an sdcard
# and filesystem support with pre-stored voice messages
USE_VOICE :=1
ifdef USE_VOICE
GLOBAL_DEFINES     += AUDIO_PLAYER_ENABLE_VOICE
WICED_SDMMC_SUPPORT := yes
endif

#
# OLED DISPLAY (i2c multi-lines) enable this define
# only if your board has an SSD1306 oled display
# on the i2c bus.
USE_DISPLAY := 1
ifdef USE_DISPLAY
GLOBAL_DEFINES     += AUDIO_PLAYER_ENABLE_DISPLAY
endif
[/SNIP]


Set your wifi credentials and wifi network type into the file "wifi_config_dct.h" 


#define CLIENT_AP_SSID       "YOUR_SSID_HERE"
#define CLIENT_AP_PASSPHRASE "YOUR_PASSWORD_HERE"
#define CLIENT_AP_BSS_TYPE   WICED_BSS_TYPE_INFRASTRUCTURE
#define CLIENT_AP_SECURITY   WICED_SECURITY_WPA2_MIXED_PSK
#define CLIENT_AP_CHANNEL    11
#define CLIENT_AP_BAND       WICED_802_11_BAND_2_4GHZ

Set your MQTT parameters topic parameters into the file "audio_player_mqtt.h", 
specifically you want to set:

* the MQTT broker address (i.e. the MQTT server)
* the area name you want this device to subscribe
* the group name you want this device to subscribe
* the device name itself

In our example:

#define AUDIO_PLAYER_MQTT_BROKER_IPV4_DEFAULT      MAKE_IPV4_ADDRESS(192, 168, 1, 1)

/* audio player MQTT topic uses the structure AREA/GROUP/DEVICE */
#define AUDIO_PLAYER_MQTT_TOPIC_AREA_DEFAULT       "cyhome"
#define AUDIO_PLAYER_MQTT_TOPIC_GROUP_DEFAULT      "day"
#define AUDIO_PLAYER_MQTT_TOPIC_DEVICE_DEFAULT     "living"

For a more precise audio playback we will need to tune the PLL values 
(see later in this HowTo) and for this reason we need a proper time from 
the network NTP server.

Set your SNTP parameters into the file "audio_player_sntp.h".

It is suggested to use local NTP servers when possible, 
or leave them to ZERO (0.0.0.0) when you want to use a remote pool.ntp.org server
(NOTE: in this case your router must provide internet connection)

#define AUDIO_PLAYER_SNTP_SERVER_PRIMARY_IPV4_DEFAULT     MAKE_IPV4_ADDRESS(192, 168, 1, 1)
#define AUDIO_PLAYER_SNTP_SERVER_SECONDARY_IPV4_DEFAULT   MAKE_IPV4_ADDRESS(192, 168, 1, 1)

Once you have set these parameters you can compile the application and 
download on your board with the proper Make Target, for example:

demo.audio_player-CYW943907WAE4-release download run


5.2 voice audio files
=====================
If you have enabled the USE_VOICE define you need to place a micro-sd card into 
the slot of the WAE4 board with the following folder structure:

sdcard_root/
   |
   +--- audio_player/
          |
          +- audio_player_cannot_connect.mp3
          +- audio_player_cannot_play_link.mp3
          +- audio_player_clock_needs_tuning.mp3
          +- audio_player_clock_tuned.mp3
          +- audio_player_connected.mp3
          +- audio_player_disconnected.mp3
          +- audio_player_is_ready.mp3
          +- audio_player_time_tuning_cy.mp3
          +- audio_player_wrong_command.mp3

These files are used by the application for "feedback" on playback status 
but MOST IMPORTANT there is one which is used for a precise calibration 
of the onboard PLL.

The goal of the PLL calibration is to make sure that each board is playing files 
at the same time speed (i.e. a 3min song is really three minutes in playback time)

You can locate the files in the WICED-SDK under the folder 

Wiced-SDK/resources/apps/audio_player. 

Simply uncompress the "audio_player_voice.tar" file in your sdcard to obtain
the folder structure above. You do not need a large sdcard for this purpose,
a 2G microsd is ok.


5.3 Command line config
=======================
When the audio_player is compiled with MQTT frontend support you have all 
the config command built into the console interface on serial port.

On console (TeraTerm) use the command "config" to show the current configuration:

> config

Config Info: * = dirty
   Audio App DCT:
              Volume: 70
     HTTP buffer num: 200
 HTTP buffer preroll: 195
     Disable preroll: 1
    Audio buffer num: 80
   Audio buffer size: 2048
   Audio period size: 0
 HTTP threshold high: 195
  HTTP threshold low: 10
   HTTP read inhibit: 0
        App playback: 0
    Audio Output Device(s):
       0x102 ak4961_dac  4 conductor 3.5mm @ J3
  Network DCT:
           Interface: STA
            Hostname: WICED IP
             IP addr: 192.168.1.168
  WiFi DCT:
     WICED MAC (STA): 00:a0:50:45:53:23
                 MAC: 00:a0:50:23:45:53
                SSID: LeonCavalloCR2
            Security: wpa2_mix
          Passphrase: 0f0sf0r0
             Channel: 11
                Band: 2.4GHz
 Current:
             Channel: 11
                Band: 2.4GHz
 MQTT:
          Broker IP : 192.168.1.1
               Area : cyhome
              Group : day
             Device : living
                Qos : 0
       PLL adj init : -93 (ppm)
 SNTP:
         Sync Cycle : 60000 (ms)
         1st IP     : 192.168.1.1
         2nd IP     : 192.168.1.1

         
Use "config help" to show the command lines to change the MQTT/SNTP parameters:


> config help
Config commands:
    config                              : output current config
    config <?|help>                     : show this list
    config volume <0-100>               : set the playback volume
    config vol <0-100>
    config audio_out <0-?>              : audio output device select
    .
    .
    .
    config mqtt_broker_ip <x.x.x.x>     : IPAddr of the MQTT broker for remote playback control
    config mqtt_topic_area <string>     : MQTT topic area name
    config mqtt_topic_group <string>    : MQTT topic area group
    config mqtt_topic_device <string>   : MQTT topic area device
    config mqtt_qos <num>               : MQTT QOS (0,1,2)
    config pll_ppm_init <num>           : playback clock adjustment in ppm
    config sntp_sync_cycle <num>        : SNTP periodical refresh (in ms)
    config sntp_server_ip1 <x.x.x.x>    : SNTP primary server
    config sntp_server_ip2 <x.x.x.x>    : SNTP secondary server
    config save                         : save data to flash NOTE: Changes not
                                        : automatically saved to flash!

For example if you want to change the ipaddress of your MQTT broker 
(i.e. the mqtt server) use the command:

config mqtt_broker_ip <YOUR_RASPBERRY_IPADDR_HERE>
config save
�
Once all the parameters are set you can reboot the board and let it connect 
to the broker.

You will hear a voice telling you that the audio player is ready 
(but the clock playback is not set...) when the app is up&running and now 
you can play music!


6. Remote Control
================================================================================
Now that server and client are set we can control ALL the clients with just 
one MQTT command from the console of the broker.

We will assume that you have a song called "song.mp3" placed into your music folder.
�
The MQTT payload has a specific CSV (comma separated value) syntax which is
PRESENTATION_TIMESTAMP, PLAYBACK_CMD, cmd_option,
�
IMPORTANT NOTE: the payload must be terminated with a comma

IMPORTANT NOTE: the song name cannot contain "commas" or it will conflict 
with the CSV syntax (see advanced section)


Connect to the Raspberry-Pi board and issue the command:

mosquitto_pub --quiet -d -q 1 -t cyhome 1542654336000,play,http://192.168.1.1/music/song.mp3


This command is sending a message to ALL the connected clients telling them :

* if you belong to the cyhome area
* at absolute time equal to 1542654336000 milliseconds
* play the link http://192.168.1.1/music/Luka.mp3
�
NOTE: the "play at time" is not yet fully implemented and the devices 
will execute the command as soon it arrives, but it is required to provide
an absolute time (in ms) with the MQTT payload command.


6.1 MQTT_player script
======================
A script is provided to make it easier to send play/stop commands over MQTT.
This is done by the "mqtt_player.sh" script available under 

WICED_SDK/resources/apps/audio_player/mqtt_player.sh

This script can be placed into you home folder on the raspberry pi 
(use scp to copy the file)

Edit the mqtt_player.sh script and set the HTTP_PREFIX, LOCAL_FOLDER1, 
and LOCAL_FOLDER2 definitions to match your environment.

This script takes care of all the timing and file location, 
the syntax is the following:

mqtt_player.sh mqtt_topic play/stop-commands time_delay music_file

The delay is simply the delay to apply (referred to the current time on the broker) 
to execute the command on the clients.

For example you can say:

A) ~/mqtt_player.sh cyhome play 0 "song.mp3"  
which immediately plays the file song.mp3 located under /home/pi/music 
on ALL the devices of the home.

B) ~/mqtt_player.sh cyhome/day play 0 "song.mp3" 
which immediately plays the file song.mp3 located under /home/pi/music 
on the devices of the "day" group

C) ~/mqtt_player.sh cyhome/day/living play 0 "song.mp3"  
which immediately plays the file Luka.mp3 located under /home/pi/music but ONLY 
on one room device (living device)


IMPORTANT: do not forget the double quotes around the filename since 
they will take care of spaces/UTF8. 
A better solution is presented in the advanced section.
�
You can also stop playback with a command with empty payload

 ~/mqtt_player.sh cyhome stop 0

6.2 Audio PLL tuning
====================
Each HW board has a little difference on the I2S clock so that the same 
file will be played with a different timing.

Even a small difference into the PLL clock for I2S might create a few 
millisecond difference over a 3-4 min song and the user will get strong echoes.

NOTE: The same problem (echoes) can happen if the boards receive the MQTT 
command with a different time due to MQTT control 
(especially with a lot of devices). This is currenlty an expected behavior.

In this section it is presented a solution to align all the PLL clocks 
of all the devices in a "offline playback" using a file stored on the sdcard 
for this specific purpose, the file is: "audio_player_time_tuning_cy.mp3".

This is a file of precisely 3.00 min length and it will be used to play and 
measure the playback time on the device itself.

At the end of playback the devices computes the offset to be used to adjust 
the clock and saves the result to the DCT (pll init value)

6.2.1 how to tune PLL
=====================
Bring up your MQTT broker (raspberry) and turn on ALL your devices.

On your raspberry console issue the command:
 ~/mqtt_player.sh cyhome play_voice 0
�
This will cause all the devices to play the time tuning file and adjust their PLL.
�
This needs to be done only once since the result is permanently stored into 
the DCT (unless the board goes under a full re-flash from the WICED SDK).
�
IMPORTANT NOTE: since this is an approximation playback it is suggested 
to issue the command at least two times.

The first play will adjust the clock in a COARSE way, while the second playback 
session will refine the adjustment itself for optimal playback 
(if there is a need for adjustment)

�
6.3 Volume control
==================
You can also control the playback volume by area/group/device with the 
mqtt_player script, for example:

~/mqtt_player.sh cyhome/day volume 0 50

Will send out an immediate (delay==0) volume change to volume=50 to ALL the WAE4 
devices in the cyhome/day group.

You could set the volume for the WHOLE home with the command:

~/mqtt_player.sh cyhome volume 0 50

�or to just one device with:
~/mqtt_player.sh cyhome/day/kitchen volume 0 50
�
NOTE: if you are using an AEVAL1F board with the RASPIAUDIO shields there is no 
support for software volume control. At the moment only WAE4 boards have SW
volume control.



7. Advanced Remote Control
================================================================================
This section is about some avanced configurations on your NAS server which allow
easier search & play on your music file library:


7.1 Beet file manager
=====================
This application is open source and it is available here: http://beets.io/

From the Beet website it is listed what the "beet" tool does as:
<<<
The purpose of beets is to get your music collection right once and for all. 
It catalogs your collection, automatically improving its metadata as it goes 
using the MusicBrainz database. Then it provides a bouquet of tools for 
manipulating and accessing your music.

Because beets is designed as a library, it can do almost anything you can 
imagine for your music collection. Via plugins, beets becomes a panacea:
* Fetch or calculate all the metadata you could possibly need: 
  album art, lyrics, genres, tempos, ReplayGain levels, or acoustic fingerprints.
* Get metadata from MusicBrainz, Discogs, or Beatport. Or guess metadata using 
  songs� filenames or their acoustic fingerprints.
* Transcode audio to any format you like.
* Check your library for duplicate tracks and albums or for albums that are 
  missing tracks.
* Browse your music library graphically through a Web browser and play it in 
  any browser that supports HTML5 Audio.
>>>


7.2 Install Beet
================
On your Raspberry-Pi you can install beet with the command:

 sudo apt-get install beets beets-doc

Once the tool is installed you can check the installation with

pi@audionas:~ $ beet
Usage:
  beet COMMAND [ARGS...]
  beet help COMMAND

Options:
  --format-item=FORMAT_ITEM
                        print with custom format
  --format-album=FORMAT_ALBUM
                        print with custom format
  -l LIBRARY, --library=LIBRARY
                        library database file to use
  -d DIRECTORY, --directory=DIRECTORY
                        destination music directory
  -v, --verbose         log more details (use twice for even more)
  -c CONFIG, --config=CONFIG
                        path to configuration file
  -h, --help            show this help message and exit

Commands:
  config            show or edit the user configuration
  fields            show fields available for queries and format strings
  help (?)          give detailed help on a specific sub-command
  import (imp, im)  import new music
  list (ls)         query the library
  modify (mod)      change metadata fields
  move (mv)         move or copy items
  remove (rm)       remove matching items from the library
  stats             show statistics about the library or a query
  update (upd, up)  update the library
  version           output version information
  write             write tag information to files

  
7.3 use Beet
========
Beet works few external plugins that are needed to organize and rename all 
the files from a generic collections of music files.

To properly configure beet it is important to create a config files with 
few precise options.

The Beet documentation is exhaustive on all the options 
and can be found here : https://beets.readthedocs.io
�
For this demo we provide a pre-tested config file to be placed on your 
raspberry into "/home/pi/.config/beets/" with name "config.yaml" : config.yaml

you can edit the file with:

sudo  nano /home/pi/.config/beets/config.yaml

The file content is the following:

[FILE-BEGIN]
directory: ~/music

library: ~/music_db/musiclibrary.db

plugins: web fetchart info

web:
    host: 0.0.0.0

aciify_paths: yes

replace:
    '[\\/]': _
    '^\.': _
    '[\x00-\x1f]': _
    '[<>:"\?\*\|]': _
    '\.$': _
    '\s+$': ''
    '^\s+': ''
    ',': ''

fetchart:
    cautious: true
    cover_names: front back
    sources: amazon *
    art_filename: cover.png

ui:
    color: yes
    colors:
        text_success: green
        text_warning: yellow
        text_error: red
        text_highlight: red
        text_highlight_minor: lightgray
        action_default: turquoise
        action: blue
[FILE-END]


These options have been carefully selected for:

* name disambiguation
* avoid commas in file names
* retrieve song covers
* provide colors for the command line UI


7.4 Import existing library
===========================
Once you have installed and configured beet on your Raspberry-Pi you are ready 
to import all your music files. Suppose you have mounted a USB key under 
/media/music with a bunch of mp3 files in it, you will use the command

beet import /media/music/

this will import (copy) all those files from the USB pen into your 
/home/pi/music folder and while doing that it will rename, search for duplicates, 
download covers etc. etc.

NOTE: once the import is completed remember to allow permissions with:

chown -R pi:www-data /home/pi/music
find /home/pi/music -type d -exec chmod 777 {} \;
find /home/pi/music -type f -exec chmod 666 {} \;


Now you can search the library from command line.

search library: list

There are many, many ways to search the libary for a title or a author, 
you just have to provide the keywords and use the subcommand "list".

For example: do you want to know what songs from Bon Jovi you have in your library?

beet list artist:bon

Do you also want to specify some part of the specific song title?

beet list artist:bon road

Do you want to list the file path? use the option "-p"

beet list -p artist:bon road

�
7.5 mqtt_player + Beet
======================
The "mqtt_player.sh" script shown above can be used in combination with 
beet from command line.

The script will also allow the use to specify a list of results and select 
the specific song to play.

The IMPORTANT thing to remember is to use doublequotes+singlequote to include 
the beet search command.
�
For example: suppose you want to play a song from your library of Bon Jovi on 
the cyhome/day group"

~/mqtt_player.sh cyhome/day play 0 "`beet list -p Bon`"
�
This will produce the whole list of matching files and ask for a list number to play

pi@audionas:~ $ ~/mqtt_player.sh cyhome play 0 "`beet list -p Bon`"

Multiple file matching
=============================================================
0=>/home/pi/music/Bon Jovi/7800� Fahrenheit/01 In and Out of Love.mp3
1=>/home/pi/music/Bon Jovi/7800� Fahrenheit/02 The Price of Love.mp3
2=>/home/pi/music/Bon Jovi/7800� Fahrenheit/03 Only Lonely.mp3
[...]
6=>/home/pi/music/Bon Jovi/7800� Fahrenheit/07 The Hardest Part Is the Night.mp3
7=>/home/pi/music/Bon Jovi/7800� Fahrenheit/08 Always Run to You.mp3
8=>/home/pi/music/Bon Jovi/7800� Fahrenheit/09 To the Fire.mp3
=============================================================
type selection and press [ENTER] :



Now select the list number and press ENTER, the script will send the appropriate
MQTT command to your "cyhome" and all the devices in that area will start to 
play music.
�

When you want to stop the music just send the command:
 ~/mqtt_player.sh cyhome stop 0

