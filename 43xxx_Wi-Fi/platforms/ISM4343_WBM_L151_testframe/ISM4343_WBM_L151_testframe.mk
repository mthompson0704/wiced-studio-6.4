#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 # Cypress Semiconductor Corporation. All Rights Reserved.
 # This software, including source code, documentation and related
 # materials ("Software"), is owned by Cypress Semiconductor Corporation
 # or one of its subsidiaries ("Cypress") and is protected by and subject to
 # worldwide patent protection (United States and foreign),
 # United States copyright laws and international treaty provisions.
 # Therefore, you may use this Software only as provided in the license
 # agreement accompanying the software package from which you
 # obtained this Software ("EULA").
 # If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 # non-transferable license to copy, modify, and compile the Software
 # source code solely for use in connection with Cypress's
 # integrated circuit products. Any reproduction, modification, translation,
 # compilation, or representation of this Software except as specified
 # above is prohibited without the express written permission of Cypress.
 #
 # Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 # EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 # WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 # reserves the right to make changes to the Software without notice. Cypress
 # does not assume any liability arising out of the application or use of the
 # Software or any product or circuit described in the Software. Cypress does
 # not authorize its products for use in any products where a malfunction or
 # failure of the Cypress product may reasonably be expected to result in
 # significant property damage, injury or death ("High Risk Product"). By
 # including Cypress's product in a High Risk Product, the manufacturer
 # of such system or application assumes all risk of such use and in doing
 # so agrees to indemnify Cypress against all liability.
#

NAME := Platform_ISM4343_WBM_L151_testframe
WLAN_CHIP            := 4343W
WLAN_CHIP_REVISION   := A1
WLAN_CHIP_FAMILY     := 4343x

ISM_CORE:=412
#This is Inventek Systems internal use only
ifeq ($(ISM_CORE),205)
$(info [ISM4343_WBM_L151.mk] STM32F205 Core selected)
HOST_MCU_FAMILY      := STM32F2xx
HOST_MCU_VARIANT     := STM32F2x5
# 1MB Flash/128KB SRAM
HOST_MCU_PART_NUMBER := STM32F205RGT6
# HSE_VALUE = STM32 crystal frequency = 16MHz (needed to make UART work correctly)
GLOBAL_DEFINES += HSE_VALUE=16000000
GLOBAL_DEFINES += CORE_RESET_DELAY=3000
GLOBAL_DEFINES += WPRINT_ENABLE_WICED_INFO
GLOBAL_DEFINES += WPRINT_ENABLE_PLATFORM_INFO
GLOBAL_DEFINES += WPRINT_ENABLE_LIB_INFO
GLOBAL_DEFINES += WPRINT_ENABLE_LIB_ERROR
GLOBAL_DEFINES += WPRINT_ENABLE_NETWORK_INFO
GLOBAL_DEFINES += WPRINT_ENABLE_NETWORK_ERROR
GLOBAL_DEFINES += WPRINT_ENABLE_WWD_ERROR
endif
#***************************************
ifeq ($(ISM_CORE),412)
$(info [ISM4343_WBM_L151.mk] STM32F412 Core selected)
HOST_MCU_FAMILY      := STM32F4xx
HOST_MCU_VARIANT     := STM32F412
# 1MB Flash/256KB SRAM
HOST_MCU_PART_NUMBER := STM32F412VEH6
# HSE_VALUE = STM32 crystal frequency = 26MHz (needed to make UART work correctly)
GLOBAL_DEFINES += HSE_VALUE=26000000

#SYSCLK :=  USE_48MHZ_CLK
ifneq ($(SYSCLK),)
GLOBAL_DEFINES += $(SYSCLK)
ifeq ($(SYSCLK),USE_24MHZ_CLK)
GLOBAL_DEFINES += DBG_WATCHDOG_TIMEOUT_MULTIPLIER=(3600*4)
endif #USE_24MHZ_CLK
ifeq ($(SYSCLK),USE_48MHZ_CLK)
GLOBAL_DEFINES += DBG_WATCHDOG_TIMEOUT_MULTIPLIER=(3600*2)
endif #USE_48MHZ_CLK
$(info [ISM4343_WBM_L151.mk] $(SYSCLK) selected)
else
$(info [ISM4343_WBM_L151.mk] USE_96MHZ_CLK selected)
endif #SYSCLK

endif #412

BT_CHIP          := 43438
BT_CHIP_REVISION := A1
BT_CHIP_XTAL_FREQUENCY := 37_4MHz
PLATFORM_SUPPORTS_BUTTONS := 1

#Power Save
POWER_SAVE := 0
ifeq ($(POWER_SAVE),1)
$(info [ISM4343_WBM_L151.mk] Power Save Enabled)
GLOBAL_DEFINES   += ENABLE_APP_POWERSAVE
GLOBAL_DEFINES   += PLATFORM_POWERSAVE_DEFAULT=1
GLOBAL_DEFINES   += BT_POWER_SAVE
endif

ifndef BUS
BUS := SDIO
endif

EXTRA_TARGET_MAKEFILES +=  $(MAKEFILES_PATH)/standard_platform_targets.mk

GLOBAL_DEFINES += SLOW_SDIO_CLOCK

VALID_BUSES := SDIO SPI

WIFI_FIRMWARE_CLM_BLOB  := ../platforms/$(PLATFORM)/clm/43430_inventek_ism4343_v2_190318.clm_blob
ifneq ($(WIFI_FIRMWARE_CLM_BLOB),)
$(info [ISM4343_WBM_L151.mk] ISM PRODUCTION BUILD CLM: $(WIFI_FIRMWARE_CLM_BLOB) )
endif #WIFI_FIRMWARE_CLM_BLOB

RESLOC := 1
ifeq ($(RESLOC),1)
RESOURCES_LOCATION := RESOURCES_IN_DIRECT_RESOURCES
endif

# For apps without wifi firmware, NO_WIFI_FIRMWARE := YES and
# resources will be in DIRECT_RESOURCE for SDIO bus
ifneq ($(NO_WIFI_FIRMWARE),)
ifeq ($(BUS),SDIO)
RESOURCES_LOCATION := RESOURCES_IN_DIRECT_RESOURCES
endif
endif

# WIFI_FIRMWARE and WIFI_FIRMWARE_CLM_BLOB are now included into resources
# RESOURCES_LOCATION default to RESOURCES_IN_WICEDFS. But can be optionally config to RESOURCES_IN_DIRECT_RESOURCES
# WARNING: Config RESOURCES_LOCATION to RESOURCES_IN_DIRECT_RESOURCES will build firmware and blob to into main application
# and may cause internal flash to overflow
RESOURCES_LOCATION ?= RESOURCES_IN_WICEDFS

ifeq ($(RESOURCES_LOCATION), RESOURCES_IN_DIRECT_RESOURCES)
INTERNAL_MEMORY_RESOURCES = $(ALL_RESOURCES)
GLOBAL_DEFINES += WWD_DIRECT_RESOURCES
endif
$(info [ISM4343_WBM_L151.mk] RESOURCES_LOCATION = $(RESOURCES_LOCATION) )


# Global includes
GLOBAL_INCLUDES  := .
GLOBAL_INCLUDES  += $(SOURCE_ROOT)libraries/inputs/gpio_button

# Global defines
#GLOBAL_DEFINES += $$(if $$(NO_CRLF_STDIO_REPLACEMENT),,CRLF_STDIO_REPLACEMENT)

GLOBAL_DEFINES += WICED_DCT_INCLUDE_BT_CONFIG

# Components
$(NAME)_COMPONENTS += drivers/spi_flash \
                      inputs/gpio_button

# Source files
$(NAME)_SOURCES := platform.c

# WICED APPS
# APP0 and FILESYSTEM_IMAGE are reserved main app and resources file system
# FR_APP := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
# DCT_IMAGE :=
# OTA_APP :=
# FILESYSTEM_IMAGE :=
# WIFI_FIRMWARE :=
# APP0 := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
# APP1 :=
# APP2 :=

# WICED APPS LOOKUP TABLE
APPS_LUT_HEADER_LOC := 0x0000
APPS_START_SECTOR := 1

ifneq ($(APP),bootloader)
ifneq ($(MAIN_COMPONENT_PROCESSING),1)
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | IMPORTANT NOTES                                                                                     | )
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | Wi-Fi MAC Address                                                                                   | )
$(info |    The target Wi-Fi MAC address is defined in <WICED-SDK>/generated_mac_address.txt                 | )
$(info |    Ensure each target device has a unique address.                                                  | )
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | MCU & Wi-Fi Power Save                                                                              | )
$(info |    It is *critical* that applications using WICED Powersave API functions connect an accurate 32kHz | )
$(info |    reference clock to the sleep clock input pin of the WLAN chip. Please read the WICED Powersave   | )
$(info |    Application Note located in the documentation directory if you plan to use powersave features.   | )
$(info +-----------------------------------------------------------------------------------------------------+ )
endif
endif


#***** Inventek Systems *****
VALID_PLATFORMS += ISM4343*
$(info [ISM4343_WBM_L151.mk] $(VALID_PLATFORMS) )

#This is Inventek Systems internal use only
#WICED-6.2.1 Has the PatchRAM buf (BT/BLE Firmware) stored in RAM verse Flash.
#Define adds 'const' to 'unsigned char brcm_patchram_buf[]' in bt_firmware_controller.c
# for the 43438A1 (bt_chip above) firmware.
#Comment out the define to set default state.
GLOBAL_DEFINES += PATCHRAM_IN_FLASH
ifneq ($(wildcard ./platforms/$(PLATFORM)/iwin/iwin.mk),)
include ./platforms/$(PLATFORM)/iwin/iwin.mk
endif #./platforms/$(PLATFORM)/iwin/iwin.mk
#****************************

