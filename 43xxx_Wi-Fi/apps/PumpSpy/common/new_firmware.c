
#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "restful_methods.h"
#include "waf_platform.h"

#define NEW_FW_DOWNLOAD_ERROR_LIMIT 1000

wiced_result_t get_new_firmware()
{
    char *p;
//    char *p2;

    new_firmware_approved = WICED_FALSE;
    new_firmware_downloaded = WICED_FALSE;
    new_firmware_available = WICED_FALSE;
    clear_post_buffers();
    WPRINT_APP_INFO(("GET /new_firmware \r\n"));
    sprintf(post, GET_new_firmware, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
        	return WICED_ERROR;
        //
        // parse the approved byte here
        /*
        {
        "idnew_firmware": 3916606,
        "deviceid": 216051098966138,
        "new_firmware_version": 2.71,
        "new_firmware_approved": 1,
        "new_firmware_file": "'C:\NEW\'",
        "new_firmware_downloaded": 0,
        "new_firmware_avaialble": 1
        }
        ]
        */
        //
        //    scan buffer for new_firmware_approved
        //
        char* pos = strstr((char*)buffer, "\"new_firmware_approved\":") +  24;
        if (pos == NULL)
        	return WICED_ERROR;
        p = pos;
//        WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
        int i = 0;
        i  = strtol (pos, &p, 10);
        if (i == 0)
        	new_firmware_approved = WICED_FALSE;
        else
        	new_firmware_approved = WICED_TRUE;
//        WPRINT_APP_INFO( ( "new_firmware_approved |%d| \r\n", new_firmware_approved ) );
        //
        //    scan buffer for new_firmware_available
        //
        pos = strstr((char*)buffer, "\"new_firmware_available\":") +  25;
        if (pos == NULL)
        	return WICED_ERROR;
        p = pos;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        i = 0;
        i  = strtol (pos, &p, 10);
        if (i == 0)
        	new_firmware_available = WICED_FALSE;
        else
        	new_firmware_available = WICED_TRUE;
//        WPRINT_APP_INFO( ( "new_firmware_available |%d| \r\n", new_firmware_available ) );
        //
        //    scan buffer for new_firmware_downloaded
        //
        pos = strstr((char*)buffer, "\"new_firmware_downloaded\":") +  26;
        p = pos;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        i = 0;
        i  = strtol (pos, &p, 10);
        if (i == 0)
        	new_firmware_downloaded = WICED_FALSE;
        else
        	new_firmware_downloaded = WICED_TRUE;
//        WPRINT_APP_INFO( ( "new_firmware_downloaded |%d| \r\n", new_firmware_downloaded ) );
        //
		// return success
        //
        return WICED_SUCCESS;
    }
    else
    {
        WPRINT_APP_INFO( ( "GET /new_firmware failed: %u\r\n", result ) );
        return WICED_ERROR;
    }

    return result;
}

wiced_result_t put_new_firmware(uint16_t new_firmware_downloaded, uint64_t utcunixtime, uint16_t new_firmware_download_status_id)
{
    clear_post_buffers();
    WPRINT_APP_INFO( ( "POST /new_firmware -  new_firmware_downloaded %d \r\n", new_firmware_downloaded ) );
    sprintf(json, JSON_post_new_firmware, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), new_firmware_downloaded, (long unsigned int)(utcunixtime/1000), new_firmware_download_status_id);
//    WPRINT_APP_INFO(("json |%s| len=%d\r\n", json, strlen(json)));
    sprintf(post, PUT_STRING, "/new_firmware", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
//    WPRINT_APP_INFO(("sprintf done\r\n"));
//    WPRINT_APP_INFO(( post )); //This result looks correct
    result = post_the_post();
//    WPRINT_APP_INFO( ( "POST /new_firmware reuslt = %d Server returned \r\n%s\r\n", result, buffer ) );
    if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
    	return WICED_ERROR;
    else
    	return WICED_SUCCESS;
}

/*
wiced_result_t get_new_firmware_download_package()
{
//    char *p;
//    char *p2;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /new_firmware \r\n"));
    sprintf(post, GET_new_firmware_download_package, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), bearer_token, strlen(GET_tm));
    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );

    }
    else
    {
        WPRINT_APP_INFO( ( "GET /new_firmware failed: %u\r\n", result ) );
    }
    //
    // ***TODO make these dynamic
    // malloc the buffer based on block size
    //
    new_firmware_size_in_bytes = 559956;
    new_firmware_size_in_blocks = 2187;
    new_firmware_block_size = 256;
    // malloc (uint8_t new_firmware_block[], new_firmware_size_in_blocks);
    return result;
}
*/

wiced_result_t get_new_firmware_download_block(long int block_number)
{
    char *p;
//    char *p2;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /new_firmware \r\n"));
    sprintf(post, GET_new_firmware_download_block, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), block_number, dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
        	return WICED_ERROR;
        //
        // TODO - convert the byte array to binary bytes
        //
        /*
        {
        "deviceid": 216051098966138,
        "block_number": 64,
        "packet": "E8D100�",
        "crc": 4660
        }
    	*/
        //
        // scan buffer for the block_size
        //
        char *pos = strstr((char*)buffer, "\"block_size\":") +  13;
        p = pos;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        new_firmware_block_size = strtol (pos, &p, 10);
//        new_firmware_block_size = 256;
//        WPRINT_APP_INFO( ( "new block size |%ld| \r\n", new_firmware_block_size ) );
        //
        //    scan buffer for the start of the packet string
        //
        pos = strstr((char*)buffer, "\"packet\":") +  10;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        memset(new_firmware_block, 0, 256);
//        WPRINT_APP_INFO( ( "%08X : ", block_number) );
        for(int i = 0; i < new_firmware_block_size; i++) {
            sscanf(pos, "%2hhx", &new_firmware_block[i]);
            pos += 2;
//            WPRINT_APP_INFO( ( "%02X", new_firmware_block[i] ) );
        }
//        WPRINT_APP_INFO( ( "\r\n" ) );
        //
        // scan buffer for the size_in_bytes
        //
        pos = strstr((char*)buffer, "\"size_in_bytes\":") +  16;
        p = pos;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        new_firmware_size_in_bytes = strtol (pos, &p, 10);
//        new_firmware_size_in_bytes = 559956;
//        WPRINT_APP_INFO( ( "new size_in_bytes |%ld| \r\n", new_firmware_size_in_bytes ) );
        //
        // scan buffer for the size_in_bytes
        //
        pos = strstr((char*)buffer, "\"size_in_blocks\":") +  17;
        p = pos;
//        WPRINT_APP_INFO( ( "pos points to |%s| \r\n", pos ) );
        new_firmware_size_in_blocks = strtol (pos, &p, 10);
//        new_firmware_size_in_blocks = 2187;
//        WPRINT_APP_INFO( ( "new size_in_blocks |%ld| \r\n", new_firmware_size_in_blocks ) );
        //
        // return success
        //
        return WICED_SUCCESS;
    }
    else
    {
        WPRINT_APP_INFO( ( "GET /new_firmware failed: %u\r\n", result ) );
        return WICED_ERROR;
    }
    //
    // return
    //
    return result;
}


int close_firmware_app()
{
	printf("Closeing app0\r\n");
	if (wiced_framework_app_close( &app ) != WICED_SUCCESS)
		return -1;
	else
		return WICED_SUCCESS;
}

int process_new_firmware_block( uint8_t* new_firmware_block, uint32_t new_firmware_size_in_bytes, uint32_t new_firmware_size_in_blocks, uint new_firmware_block_size, uint32_t new_firmware_block_number)
{
//	int              i           = 0;
    uint32_t         offset      = 0;
//    uint32_t         file_size   = 0;
//    static uint32_t  chunk_count = 0;
//    static uint32_t  expected_offset = 0;
//    uint32_t         received_offset = 0;
//    char             offset_string[13];

    WPRINT_APP_INFO( ( "begin firmware block write block %ld block_size %ld blocks %ld bytes %ld \r\n", (unsigned long)new_firmware_block_number, (unsigned long)new_firmware_block_size, (unsigned long)new_firmware_size_in_blocks, (unsigned long)new_firmware_size_in_bytes ) );

/*
    offset    = atoi( strstr((char*)request->query_ptr, "offset=") + strlen("offset=") );
    file_size = atoi(strstr((char*)request->query_ptr, "filesize=") + strlen("filesize=") );
    received_offset = offset;

    if( expected_offset != offset )
    {
        memset(offset_string, 0x00, sizeof(offset_string));
        sprintf(offset_string, "%lu", expected_offset);
        wiced_tcp_stream_write( stream, offset_string, strlen(offset_string));
        return 0;
    }
*/

    //
    // on the first block, erase the file segment
    //
    if (new_firmware_block_number == 0)
    {
        WPRINT_APP_INFO( ( "erasing flash \r\n" ) );
        uint32_t    current_size;
        if (wiced_framework_app_open( DCT_APP0_INDEX, &app ) != WICED_SUCCESS)
        {
            return -1;
        }
        if (wiced_framework_app_get_size( &app, &current_size ) != WICED_SUCCESS)
        {
            return -1;
        }
        printf("current size %ld\r\n", current_size);
        wiced_framework_app_erase( &app );
        if (wiced_framework_app_erase( &app ) != WICED_SUCCESS)
        {
        	printf("erase failed \r\n");
            return -1;
        }
        if (wiced_framework_app_get_size( &app, &current_size ) != WICED_SUCCESS)
        {
            return -1;
        }
        printf("erased size %ld\r\n", current_size);
//        if (current_size != 0)
//        {
//        	printf("erased size not 0 \r\n");
//        	return -1;
//        }
        wiced_framework_app_set_size( &app, new_firmware_size_in_bytes );
        //TODO [IS] To fix WICED OTA, determine why need to write twice to work.
        if (wiced_framework_app_set_size( &app, new_firmware_size_in_bytes) != WICED_SUCCESS)
        {
            return -1;
        }
        if (wiced_framework_app_get_size( &app, &current_size ) != WICED_SUCCESS)
        {
            return -1;
        }
        if ( current_size < new_firmware_size_in_bytes )
        {
            printf("Error setting application size!! current_size %lu new_firmware_size_in_bytes %lu\n", current_size, new_firmware_size_in_bytes);
            return -1;
        }
        printf("erased successfully \r\n");
    }
    else
    {
    	printf("app->app_id %d\r\n",  app.app_id);
    	printf("app->offset %ld\r\n", app.offset);
    	printf("app->last_erased_sector %ld\r\n", app.last_erased_sector);
    	printf("app->app_header_location.id %ld\r\n", (long int)app.app_header_location.id);
//    	printf("app->app_header_location.id %ld\r\n", (int)app.app_header_location.detail);
//    	wiced_rtos_delay_milliseconds(2500);
    }

    //
    // write the block to flash.
    //
	printf("Writing block %lu of size %ld first byte %d \r\n", new_firmware_block_number, (unsigned long)new_firmware_block_size, new_firmware_block[0]);
//    for (i=0; i<256;i++)
//    {
//    	printf("%02X", new_firmware_block[i]);
//    }
//    printf("\r\n");
	wiced_framework_app_write_chunk( &app, new_firmware_block, new_firmware_block_size);
	offset += new_firmware_block_size;
	printf ("done writing block %lu \r\n", new_firmware_block_number);
	//
	// last block
	//

	if( new_firmware_block_number == new_firmware_size_in_blocks)
    {
        printf("Uploaded file size = %lu\r\n", new_firmware_size_in_bytes);
        wiced_framework_app_close( &app );
//        wiced_framework_set_boot ( DCT_APP0_INDEX, 0x01); // TODO - figure this constant out - PLATFORM_DEFAULT_LOAD - set to WICED_FRAMEWORK_LOAD_ONCE );
        wiced_framework_set_boot ( DCT_APP0_INDEX, PLATFORM_DEFAULT_LOAD); // TODO - figure this constant out - PLATFORM_DEFAULT_LOAD - set to WICED_FRAMEWORK_LOAD_ONCE );
        /*
        //
        // read back the memory
        //
        if (wiced_framework_app_open( DCT_APP0_INDEX, &app ) != WICED_SUCCESS)
        {
        	printf("error opening APP0 after it's written\r\n");
        	while (1) { wiced_rtos_delay_milliseconds(500);}
        }
        offset = 0;
        memset(new_firmware_block, 0, 256);
        if (wiced_framework_app_read_chunk( &app, offset, new_firmware_block, 256 ) != WICED_SUCCESS)
        {
        	printf("error reading block 0\r\n");
        	while (1) { wiced_rtos_delay_milliseconds(500);}
        }
        printf ("00000000 : ");
        for (i=0; i<256;i++)
        {
        	printf("%02X", new_firmware_block[i]);
        }
        printf("\r\n");
        wiced_framework_app_close( &app );
        */
    }
//    else
//    {
//        expected_offset = received_offset + new_firmware_block_size;
//    }

	printf ("returning \r\n");
    return 0;
}

void process_ota_firmware()
{
	//
	// New Firmware Timer  - 2 minutes
	//
	if ((server_time - new_firmware_timer) > NEW_FIRMWARE_TIMER_LIMIT)
	{
		//
		// check if there is a download in progress
		//
		if (new_firmware_download_in_progress == WICED_FALSE)
		{
			//
			// GET /new_firmware/{deviceid}
			//
			new_firmware_timer = server_time;
			result = get_new_firmware();
			if (result == WICED_SUCCESS)
			{
				if ((new_firmware_available == WICED_TRUE) && (new_firmware_approved == WICED_TRUE) && (new_firmware_downloaded == WICED_FALSE))
				{
					WPRINT_APP_INFO(( "new firmware is available and approved and not downloaded approved  %d\r\n", new_firmware_approved));
					new_firmware_download_in_progress = WICED_TRUE;
					new_firmware_block_number = 0;
					result = put_new_firmware(1, server_time, DOWNLOAD_STARTED);
                    new_firmware_donwload_block_error_count = 0;
				}
			}
			else
			{
				WPRINT_APP_INFO(( "no new firmware shown on server\r\n"));
			}
		}
	}
	//
	// download if if it's in progress
	//
	if (new_firmware_download_in_progress == WICED_TRUE)
	{
		result = get_new_firmware_download_block(new_firmware_block_number);
		if (result == WICED_SUCCESS)
		{
			result = process_new_firmware_block(new_firmware_block, new_firmware_size_in_bytes, new_firmware_size_in_blocks, new_firmware_block_size, new_firmware_block_number);
			if (result == WICED_SUCCESS)
			{
				if (new_firmware_block_number == new_firmware_size_in_blocks)
				{
					wiced_time_get_utc_time_ms  (&server_time);
					result = put_new_firmware(1, server_time, COMPLETE_AND_REBOOTING);
					if (result == WICED_SUCCESS)
						WPRINT_APP_INFO(( "/new_firmware posted r\n"));
					else
						WPRINT_APP_INFO(( "/new_firmware put failed r\n"));
					//
					// restarting
					//
					WPRINT_APP_INFO(("Restarting.. \r\n"));
					wiced_framework_reboot();

				}
				else
				{
					new_firmware_block_number++;
				}
			}
			else
			{
				result = put_new_firmware(1, server_time, FLASH_MEMORY_ERROR);
				close_firmware_app();
				new_firmware_download_in_progress = WICED_FALSE;
				new_firmware_block_number = 0;
			}
		}
		else
		{
            new_firmware_donwload_block_error_count++;
			WPRINT_APP_INFO(( "new_firmware donwload block error count = %d\r\n", new_firmware_donwload_block_error_count));
            if (new_firmware_donwload_block_error_count > NEW_FW_DOWNLOAD_ERROR_LIMIT )
            {
                result = put_new_firmware(1, server_time, DOWNLOAD_FAILED);
                close_firmware_app();
                new_firmware_download_in_progress = WICED_FALSE;
                new_firmware_block_number = 0;
            }
		}
		//
		// check for the change in donwload status
		//
		if ((new_firmware_block_number%100) == 0)
		{
			result = get_new_firmware();
			if (result == WICED_SUCCESS)
			{
				if ((new_firmware_available == WICED_FALSE) || (new_firmware_approved == WICED_FALSE))
				{
					result = put_new_firmware(1, server_time, DOWNLOAD_HALTED);
					WPRINT_APP_INFO(( "new firmware download cancelled %d\r\n", new_firmware_approved));
					close_firmware_app();
					new_firmware_download_in_progress = WICED_FALSE;
					new_firmware_block_number = 0;
				}
			}
		}
	}
}

