/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 */

#include "wiced.h"

#include "audio_player.h"
#include "wiced_log.h"

/******************************************************
 *                     Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define WICED_MQTT_DELAY_IN_MILLISECONDS         (20)
#define WICED_MQTT_KEEPALIVE_IN_MILLISECONDS     (10)

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

#ifdef AUDIO_PLAYER_ENABLE_MQTT
/*
 * Call back function to handle connection events.
 */
static wiced_result_t mqtt_connection_event_cb(wiced_mqtt_object_t mqtt_obj, wiced_mqtt_event_info_t* event)
{
    char* msg_params[32];
    uint8_t msg_params_num = 2;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    switch (event->type)
    {
        case WICED_MQTT_EVENT_TYPE_CONNECT_REQ_STATUS:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "WICED_MQTT_EVENT_TYPE_CONNECT_REQ_STATUS\n\n");
            break;

        case WICED_MQTT_EVENT_TYPE_DISCONNECTED:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "WICED_MQTT_EVENT_TYPE_DISCONNECTED\n\n");
            break;

        case WICED_MQTT_EVENT_TYPE_PUBLISHED:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "MESSAGE [ID: %u] published\n\n",event->data.msgid);
            break;

        case WICED_MQTT_EVENT_TYPE_SUBSCRIBED:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "WICED_MQTT_EVENT_TYPE_SUBSCRIBED\n\n");
            break;

        case WICED_MQTT_EVENT_TYPE_UNSUBSCRIBED:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "WICED_MQTT_EVENT_TYPE_UNSUBSCRIBED\n\n");
            break;

        case WICED_MQTT_EVENT_TYPE_PUBLISH_MSG_RECEIVED:
        {
            wiced_time_t msg_time;
            wiced_time_get_utc_time( &msg_time );

            wiced_mqtt_topic_msg_t msg = event->data.pub_recvd;

            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO,
                          "[MQTT] Received %.*s at [%ld] for TOPIC : %.*s\n\n", (int)msg.data_len, msg.data, msg_time, (int)msg.topic_len, msg.topic);

            if (0 < (int)msg.data_len)
            {
                /* the MQTT message is using the CSV format: "PTS,COMMAND,PAYLOAD," */
                /* note that it is always "comma terminated" */
                /* we split the message on the comma to identify the single parts of the message */

                uint32_t idx = 0;

                msg_params_num = 0;

                /* note: chunked MQTT message not supported */

                /* sanity check: message must be comma terminated */
                if (msg.data[(int)msg.data_len-1] == ',')
                {
                    /* count & split params */
                    for (idx = 0; idx < ((int)msg.data_len); idx++)
                    {
                        if (msg.data[idx] == ',')
                        {
                            msg_params_num++;

                            /* replace comma with a string terminator */
                            msg.data[idx]='\0';

                            /* mark the string pointer to the next char position */
                            msg_params[msg_params_num - 1] = (char*)&msg.data[idx+1];
                        }
                    }

                    /* SANITY CHECK minimum params_num is 3 */
                    if (msg_params_num >= 3)
                    {
                        audio_player_console_command((msg_params_num - 1), msg_params);
                    }
                }
            }
        }
        break;

        default:
            break;
    }

    return WICED_SUCCESS;
}


/*
 * Creating and initialize mqtt object
 */

int audio_player_mqtt_init(wiced_mqtt_object_t* mqtt_obj)
{
    wiced_mqtt_object_t mqtt_object;

    if (NULL==mqtt_obj)
    {
        return WICED_ERROR;
    }

    /* Memory allocated for mqtt object*/
    mqtt_object = (wiced_mqtt_object_t) malloc(WICED_MQTT_OBJECT_MEMORY_SIZE_REQUIREMENT);
    if (mqtt_object == NULL)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR,
                      "ERROR: Don't have memory to allocate for mqtt object. Unable to allocate audio packet\n");

        return WICED_ERROR;
    }

    wiced_mqtt_init(mqtt_object);

    /* update reference */
    *mqtt_obj = mqtt_object;

    return WICED_SUCCESS;
}

/*
 *  Removing  mqtt object
 */

int audio_player_mqtt_deinit(wiced_mqtt_object_t* mqtt_obj)
{
    wiced_mqtt_object_t mqtt_object;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (NULL == *mqtt_obj)
    {
        return WICED_ERROR;
    }

    mqtt_object = *mqtt_obj;

    wiced_mqtt_deinit(mqtt_object);

    free(mqtt_object);

    *mqtt_obj = NULL;

    return WICED_SUCCESS;
}


/*
 *  Connect to MQTT Broker
 */
int audio_player_mqtt_connect(wiced_mqtt_object_t* mqtt_obj, wiced_mqtt_callback_t* mqtt_cb, char* mqtt_broker, char* mqtt_client_id)
{
    wiced_ip_address_t ip;
    wiced_mqtt_pkt_connect_t conninfo;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_cb)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_broker)
    {
        return WICED_ERROR;
    }

    memset(&conninfo, 0, sizeof(conninfo));
    str_to_ip(mqtt_broker,&ip);

    conninfo.port_number   = 0;                   /* set to 0 indicates library to use default settings */
    conninfo.mqtt_version  = WICED_MQTT_PROTOCOL_VER4;
    conninfo.clean_session = 1;
    conninfo.client_id     = (uint8_t*)mqtt_client_id;
    conninfo.keep_alive    = WICED_MQTT_KEEPALIVE_IN_MILLISECONDS;
    conninfo.password      = NULL;
    conninfo.username      = NULL;
    conninfo.peer_cn       = NULL;

    if (wiced_mqtt_connect(*mqtt_obj, &ip, WICED_STA_INTERFACE, *mqtt_cb, NULL, &conninfo)!= WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "ERROR: Broker is not connected\n");

        return WICED_ERROR;
    }

    return WICED_SUCCESS;
}


/*
 *  Disconnect from MQTT Broker
 */

int audio_player_mqtt_disconnect(wiced_mqtt_object_t* mqtt_obj)
{
    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (wiced_mqtt_disconnect(*mqtt_obj) != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "ERROR: Broker is not disconnected\n");
        return WICED_ERROR;
    }

   return WICED_SUCCESS;

}

/*
 *  Subscribe to a topic
 */

int audio_player_mqtt_subscribe(wiced_mqtt_object_t* mqtt_obj, char* mqtt_topic, uint8_t mqtt_qos)
{
    wiced_mqtt_msgid_t pktid;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_topic)
    {
        return WICED_ERROR;
    }

    if (mqtt_qos > 2)
    {
        return WICED_ERROR;
    }

    pktid = wiced_mqtt_subscribe(*mqtt_obj, mqtt_topic, mqtt_qos);

    if (pktid == 0)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "ERROR: Topic is not subscribed\n");

        return WICED_ERROR;
    }

    wiced_rtos_delay_milliseconds(WICED_MQTT_DELAY_IN_MILLISECONDS);

    return WICED_SUCCESS;
}

/*
 * Unsubscribe from a topic
 */

int audio_player_mqtt_unsubscribe(wiced_mqtt_object_t* mqtt_obj, char* mqtt_topic)
{
    wiced_mqtt_msgid_t pktid;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_topic)
    {
        return WICED_ERROR;
    }

    pktid = wiced_mqtt_unsubscribe(*mqtt_obj, mqtt_topic);

    if (pktid == 0)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "ERROR! Topic is not unsubscribed\n");
        return WICED_ERROR;
    }

    return WICED_SUCCESS;
}

/*
 * Publish (send) a message
 */
int audio_player_mqtt_publish(wiced_mqtt_object_t* mqtt_obj, uint8_t mqtt_qos, char* mqtt_topic, char* mqtt_payload)
{
    wiced_mqtt_msgid_t pktid;

    if (NULL == mqtt_obj)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_topic)
    {
        return WICED_ERROR;
    }

    if (NULL == mqtt_payload)
    {
        return WICED_ERROR;
    }

    if (mqtt_qos > 2)
    {
        return WICED_ERROR;
    }

    pktid = wiced_mqtt_publish (*mqtt_obj , mqtt_topic, (uint8_t*) mqtt_payload, strlen(mqtt_payload), mqtt_qos);
    if ( pktid == 0)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "ERROR! Message is not published\n");
        return WICED_ERROR;
    }

    wiced_rtos_delay_milliseconds(WICED_MQTT_DELAY_IN_MILLISECONDS);
    return WICED_SUCCESS;
}


/*
 * PUBLIC
 */

int audio_player_mqtt_start(audio_player_mqtt_t** mqtt, audio_player_mqtt_params_t* mqtt_params)
{
    char tmpstr[AUDIO_PLAYER_MQTT_TOPIC_MAX_LEN] = "\0";
    audio_player_mqtt_t *mqtt_ref = NULL;
    char brokerstr[16+1] = "\0";

    if ((NULL == mqtt) || (NULL == mqtt_params) )
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: invalid params\n" );
        return WICED_ERROR;
    }

    if (NULL != (*mqtt))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: obj already created\n" );
        return WICED_ERROR;
    }

    /* Memory allocated for mqtt object*/
    *mqtt = (audio_player_mqtt_t*)calloc(1, sizeof(audio_player_mqtt_t));
    if (*mqtt == NULL)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR,
                      "ERROR: Don't have memory to allocate for audio_player_mqtt object. Unable to allocate audio packet\n");

        return WICED_ERROR;
    }

    mqtt_ref = *mqtt;

    /* init mqtt obj */
    if (WICED_SUCCESS != audio_player_mqtt_init(&mqtt_ref->mqtt_obj))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: cannot init mqtt lib\n");
        mqtt_ref->mqtt_obj = NULL;
        return WICED_ERROR;
    }

    /* register the callback in the mqtt struct */
    mqtt_ref->mqtt_cb = mqtt_connection_event_cb;

    sprintf(brokerstr, "%d.%d.%d.%d", PRINT_IP(mqtt_params->broker_ip));

    wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG1, "MQTT connect to : %s\n", brokerstr);

    /* connect to broker */
    mqtt_ref->is_connected = WICED_FALSE;
    if (WICED_SUCCESS != audio_player_mqtt_connect(&mqtt_ref->mqtt_obj, &mqtt_ref->mqtt_cb, brokerstr, mqtt_params->topic_device))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: cannot connect\n");

        return WICED_ERROR;
    }
    mqtt_ref->is_connected = WICED_TRUE;

    /* subscribe to topic : AREA */
    strncat(tmpstr, mqtt_params->topic_area, (AUDIO_PLAYER_MQTT_TOPIC_MAX_LEN - strlen(mqtt_params->topic_area)));
    wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG1, "MQTT subscribe to: %s\n", tmpstr);

    if (WICED_SUCCESS != audio_player_mqtt_subscribe(&mqtt_ref->mqtt_obj, tmpstr, mqtt_params->qos))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: cannot subscribe to : %s\n",tmpstr);
        return WICED_ERROR;
    }

    /* subscribe to topic : AREA/GROUP */
    strncat(tmpstr, "/", 1);
    strncat(tmpstr, mqtt_params->topic_group, (AUDIO_PLAYER_MQTT_TOPIC_MAX_LEN - strlen(tmpstr)));

    wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG1, "MQTT subscribe to: %s\n", tmpstr);
    if (WICED_SUCCESS != audio_player_mqtt_subscribe(&mqtt_ref->mqtt_obj, tmpstr, mqtt_params->qos))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: cannot subscribe to : %s\n",tmpstr);
        return WICED_ERROR;
    }

    /* subscribe to topic : AREA/GROUP/DEVICE */
    strncat(tmpstr, "/", 1);
    strncat(tmpstr, mqtt_params->topic_device, (AUDIO_PLAYER_MQTT_TOPIC_MAX_LEN - strlen(tmpstr)));

    wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG1, "MQTT subscribe to: %s\n", tmpstr);
    if (WICED_SUCCESS != audio_player_mqtt_subscribe(&mqtt_ref->mqtt_obj, tmpstr, mqtt_params->qos))
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "MQTT start error: cannot subscribe to : %s\n",tmpstr);
        return WICED_ERROR;
    }

    return WICED_SUCCESS;
}


int audio_player_mqtt_stop(audio_player_mqtt_t** mqtt)
{
    audio_player_mqtt_t *mqtt_ref = NULL;

    if ((NULL == mqtt) || (NULL == *mqtt))
    {
        return WICED_ERROR;
    }

    mqtt_ref = *mqtt;

    /* disconnect from broker */
    audio_player_mqtt_disconnect(mqtt_ref->mqtt_obj);

    /* deinit */
    if (WICED_SUCCESS != audio_player_mqtt_deinit(mqtt_ref->mqtt_obj))
    {
        return WICED_ERROR;
    }

    free(*mqtt);
    *mqtt = NULL;

    return WICED_SUCCESS;
}


#else
int audio_player_mqtt_start(audio_player_mqtt_t** mqtt, audio_player_mqtt_params_t* mqtt_params)
{
    return 0;
}

int audio_player_mqtt_stop(audio_player_mqtt_t** mqtt)
{
    return 0;
}

#endif
