var group__wpl__event =
[
    [ "cpl_event_bt_power_state_t", "group__wpl__event.html#gad188658f896faab5c88049e633bbeb7d", [
      [ "EVENT_DESC_BT_POWER_OFF", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7da6c0f904e6312328c2b501fbf6311eb12", null ],
      [ "EVENT_DESC_BT_POWER_IDLE", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7da206758883ae2920fc530f095384ddfd4", null ],
      [ "EVENT_DESC_BT_POWER_TX", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7da2082a5ae4d3853ad22218456441d8159", null ],
      [ "EVENT_DESC_BT_POWER_RX", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7da53cf9bdc85314d0bbb973d8076510935", null ],
      [ "EVENT_DESC_BT_POWER_TX_PDS", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7daf8d1f97eedab9a5c6223c29214d9a79c", null ],
      [ "EVENT_DESC_BT_POWER_RX_PDS", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7da8e54deac98fe61e7b01efa861e671c57", null ],
      [ "EVENT_DESC_BT_POWER_DEEP_SLEEP", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7daa3804180879c042b98bf15c3dcff9bc6", null ],
      [ "EVENT_DESC_BT_MAX", "group__wpl__event.html#ggad188658f896faab5c88049e633bbeb7dabaa48f08ea8fb102216bc7eec4613fd1", null ]
    ] ],
    [ "cpl_event_i2c_state_t", "group__wpl__event.html#gae766e9fb426c1aadf363097beec1b2fa", [
      [ "EVENT_DESC_I2C_IDLE", "group__wpl__event.html#ggae766e9fb426c1aadf363097beec1b2faad4cf2de61634e2f368772440c0b6d521", null ],
      [ "EVENT_DESC_I2C_TX", "group__wpl__event.html#ggae766e9fb426c1aadf363097beec1b2faa0f9c60e0a7ad5b25423264400e5c17ba", null ],
      [ "EVENT_DESC_I2C_RX", "group__wpl__event.html#ggae766e9fb426c1aadf363097beec1b2faa8af81d927353a28dc26be65a87135bc9", null ],
      [ "EVENT_DESC_I2C_MAX", "group__wpl__event.html#ggae766e9fb426c1aadf363097beec1b2faaeceaf8aed263d76b4267a281c66bbe87", null ]
    ] ],
    [ "cpl_event_id_t", "group__wpl__event.html#gaf7dda817dced930b804235a29067fb1a", [
      [ "EVENT_ID_POWERSTATE", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa285aebb209f78f3c6d9066cb8ad1c265", null ],
      [ "EVENT_ID_FLASH", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aab840cac471ab9d7527aa9dcb05fe4162", null ],
      [ "EVENT_ID_UART", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa3c7a2bad41028339de227528d339cc5a", null ],
      [ "EVENT_ID_WIFI_DATA", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa5e706fa337a1980e0c86c29144a5a4cc", null ],
      [ "EVENT_ID_I2S", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa684e91b5352c3365a8026a987a6e82ed", null ],
      [ "EVENT_ID_PROFILING", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa47dffacb80b66712e6431185fba30bcc", null ],
      [ "EVENT_ID_BT_DATA", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa336d18e5ed28c4c744a670d48e423799", null ],
      [ "EVENT_ID_I2C", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aacd0e8b1bb1ca116ca358f9e3c2d5e7e9", null ],
      [ "EVENT_ID_SPI_SFLASH", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aaabce82e06ada40dcfee08c2ffb0b6ec4", null ],
      [ "EVENT_ID_SDIO", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa26177cd83e62571e4c042abdabe23884", null ],
      [ "EVENT_ID_SPI", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa78bc8bfe0326af825888bb0699429a20", null ],
      [ "EVENT_ID_SPI_TRANSPORT", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aab0101786f0f57f2ee69fc18e57d3c33a", null ],
      [ "EVENT_ID_UART_TRANSPORT", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aab3c570024efc6b69922140fc90763255", null ],
      [ "EVENT_ID_MAX", "group__wpl__event.html#ggaf7dda817dced930b804235a29067fb1aa54471df8966564b16aca326f0ab20b59", null ]
    ] ],
    [ "cpl_event_power_state_t", "group__wpl__event.html#ga3842bf2f72d9720728f5c1c67067644d", [
      [ "EVENT_DESC_POWER_ACTIVE1", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644dad8a28382b0cf9e57ba98251ba23fa1c4", null ],
      [ "EVENT_DESC_POWER_ACTIVE2", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644da10ae47dc3d06b03b61986176d686a7ee", null ],
      [ "EVENT_DESC_POWER_SLEEP", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644daba5da4848badd1460c0ec419d8d42056", null ],
      [ "EVENT_DESC_POWER_DEEPSLEEP", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644da03faa12fe36ac5019d90743906d03b72", null ],
      [ "EVENT_DESC_POWER_OFF", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644dad95faddfdde7bde9250006ff32037502", null ],
      [ "EVENT_DESC_POWER_HIBERNATE", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644da7383cd329bf7f21e55d15d8f17540104", null ],
      [ "EVENT_DESC_POWER_PDS", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644da6fedd62559c5d552324afd33df1e660a", null ],
      [ "EVENT_DESC_MAX", "group__wpl__event.html#gga3842bf2f72d9720728f5c1c67067644dac0dc4b54d380f249ed4293ed3c1aee74", null ]
    ] ],
    [ "cpl_event_profiling_state_t", "group__wpl__event.html#ga83a36894fbdd9a61da4b78210484145e", [
      [ "EVENT_DESC_FUNC_IDLE", "group__wpl__event.html#gga83a36894fbdd9a61da4b78210484145eaffc5834d49bb2355dc9045757a631ee9", null ],
      [ "EVENT_DESC_FUNC_TIME", "group__wpl__event.html#gga83a36894fbdd9a61da4b78210484145ea1f198aec560a4752250cb5897041e8f5", null ]
    ] ],
    [ "cpl_event_sdio_state_t", "group__wpl__event.html#ga20fb23d6fbdc7f5b6a8d891fb6e171f7", [
      [ "EVENT_DESC_SDIO_IDLE", "group__wpl__event.html#gga20fb23d6fbdc7f5b6a8d891fb6e171f7a4ec5a7f9f0c3ca5e94b28ae7c15caf8d", null ],
      [ "EVENT_DESC_SDIO_READ", "group__wpl__event.html#gga20fb23d6fbdc7f5b6a8d891fb6e171f7a422cb6c622037bfa0b8d0af0554f9ef7", null ],
      [ "EVENT_DESC_SDIO_WRITE", "group__wpl__event.html#gga20fb23d6fbdc7f5b6a8d891fb6e171f7a7d03c73b205a291de79dd1c1f01159f6", null ],
      [ "EVENT_DESC_SDIO_MAX", "group__wpl__event.html#gga20fb23d6fbdc7f5b6a8d891fb6e171f7ae7c103f1bc7d21f25924a461ce7550e3", null ]
    ] ],
    [ "cpl_event_spi_sflash_state_t", "group__wpl__event.html#gacfbdf923f6a16d72d5c92269a0aeabe0", [
      [ "EVENT_DESC_SPI_SFLASH_IDLE", "group__wpl__event.html#ggacfbdf923f6a16d72d5c92269a0aeabe0a41986fe880fbc7317b3c4ebfa3fc3876", null ],
      [ "EVENT_DESC_SPI_SFLASH_READ", "group__wpl__event.html#ggacfbdf923f6a16d72d5c92269a0aeabe0a992d83c07d9e8f7607bb6ff8b65b5b4c", null ],
      [ "EVENT_DESC_SPI_SFLASH_WRITE", "group__wpl__event.html#ggacfbdf923f6a16d72d5c92269a0aeabe0a09c2e543801d9baa3068863edba1e260", null ],
      [ "EVENT_DESC_SPI_SFLASH_ERASE", "group__wpl__event.html#ggacfbdf923f6a16d72d5c92269a0aeabe0aabcf85ec9bdbd690c2ebe1b6af89a6c3", null ],
      [ "EVENT_DESC_SPI_SFLASH_MAX", "group__wpl__event.html#ggacfbdf923f6a16d72d5c92269a0aeabe0adafe1c19f9943c6006589b2673ca32d6", null ]
    ] ],
    [ "cpl_event_spi_state_t", "group__wpl__event.html#gabf597cc7a68a42ad6909cd0be3f2a61b", [
      [ "EVENT_DESC_SPI_OFF", "group__wpl__event.html#ggabf597cc7a68a42ad6909cd0be3f2a61ba30a22f1ab9dae5f76acc147db46457a6", null ],
      [ "EVENT_DESC_SPI_IDLE", "group__wpl__event.html#ggabf597cc7a68a42ad6909cd0be3f2a61bab93d22a8a0526b919a0677ec54a9e859", null ],
      [ "EVENT_DESC_SPI_READ", "group__wpl__event.html#ggabf597cc7a68a42ad6909cd0be3f2a61ba54e7e66161e6eecdf9def900cdef6f17", null ],
      [ "EVENT_DESC_SPI_WRITE", "group__wpl__event.html#ggabf597cc7a68a42ad6909cd0be3f2a61bad90cdb768d2a26d6b3e609264161d8e4", null ],
      [ "EVENT_DESC_SPI_MAX", "group__wpl__event.html#ggabf597cc7a68a42ad6909cd0be3f2a61baf5299312160cc886edecfe0b3c3a3d4c", null ]
    ] ],
    [ "cpl_event_uart_state_t", "group__wpl__event.html#gab258d4bd72991292c8d0daa660a54681", [
      [ "EVENT_DESC_UART_IDLE", "group__wpl__event.html#ggab258d4bd72991292c8d0daa660a54681a875320fd76e0e530dccf79d35da42d97", null ],
      [ "EVENT_DESC_UART_TX", "group__wpl__event.html#ggab258d4bd72991292c8d0daa660a54681a1ca5148c09e79affcb5581069b4dcc27", null ],
      [ "EVENT_DESC_UART_RX", "group__wpl__event.html#ggab258d4bd72991292c8d0daa660a54681a4d1f51c3a48805be7f31d7c6f24ab301", null ],
      [ "EVENT_DESC_UART_MAX", "group__wpl__event.html#ggab258d4bd72991292c8d0daa660a54681acfbea09b03dd341ba87e4cbe9a69cd05", null ]
    ] ],
    [ "cpl_procid_t", "group__wpl__event.html#gaf1c716d8b62763b0dc37aeae1d81435c", [
      [ "EVENT_PROC_ID_MCU", "group__wpl__event.html#ggaf1c716d8b62763b0dc37aeae1d81435ca1e7c1857210ad7e29d6cbafab318d5e8", null ],
      [ "EVENT_PROC_ID_WIFI", "group__wpl__event.html#ggaf1c716d8b62763b0dc37aeae1d81435cafeaefa7ed4738b9770e4302e5e3a30ec", null ],
      [ "EVENT_PROC_ID_BT", "group__wpl__event.html#ggaf1c716d8b62763b0dc37aeae1d81435ca3314b2a21510c8982a6d00b1f8f21d6b", null ],
      [ "EVENT_PROC_ID_MAX", "group__wpl__event.html#ggaf1c716d8b62763b0dc37aeae1d81435ca643d9ed8bd096f6fde118480571227d7", null ]
    ] ]
];