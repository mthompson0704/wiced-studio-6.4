Demo apps
+++++++++
These applications demonstrate the use of one or more Classic Bluetooth (BR/EDR)
and Bluetooth Low Energy (BLE) profiles, and/or the WICED HCI interface.

Most of the applications here (except where noted) have corresponding sample
implementations of MCU functionality executing on a PC, in the Client Control
application, demonstrating WICED HCI control of the device over UART.  See
apps\host\client_control\README.txt

Some applications also have specific peer sample applications provided to
interact with the embedded application, as noted below.

Details on how to build and run the applications, as well as interact and test
them with the client_control application, can be found in the referenced source
file listed for each application.

Hello Sensor application
    This application shows an example of a BLE vendor specific GATT device and
    service.  See hello_sensor\hello_sensor.c.
    The application can be exercised with the Hello Sensor peer application on
    a PC or phone, or with the Hello Client application on another WICED device,
    Note: This application is not controlled by client_control app.

Hello Sensor peer application
    This application shows an example of a BLE vendor specific GATT client
    running on Windows, Android or iOS. It sends data to Hello Sensor
    application.  See hello_sensor\peer_apps.

Hello Client application
    This application shows an example implementation of a BLE vendor specific
    GATT client profile.  See hello_client\hello_client.c.
    This application can be used with the Hello Sensor application.
    Note: This application is not controlled by client_control app.

Low power sensor application
    This app demonstrates the use of low power modes for a BLE vendor specific
    GATT based sensor device.  See (low_power_sensor/low_power_sensor.c).

Proximity Reporter application
    This application demonstrates the BLE GATT service for Proximity Reporter.  
    See (proximity_reporter\proximity_reporter.c).

BLE keyboard application
    This app provides a turnkey solution for creating a BLE HOGP-based keyboard.
    See (hid\ble_keyboard\ble_keyboard.c).
    Also see note below on HCI control of BLE HID apps.

BLE mouse application
    This app demonstrates an implementation of a BLE HOGP-based mouse. 
    See (hid\ble_mouse\ble_mouse.c).
    Also see note below on HCI control of BLE HID apps.

BLE remote application
    This app demonstrates BLE HOGP-based remote. Features include key,
    microphone (voice over HOGP), Infrared Transmit (IR TX), and TouchPad. See
    (hid\ble_remote\ble_remote.c).
    Also see note below on HCI control of BLE HID apps.
    
NOTE:  HCI control of BLE HID apps (BLE keyboard, BLE mouse, and BLE Remote)
    These application can be optionally controlled via MCU client_control apps by
    editing the app makefile to enable DTESTING_USING_HCI flag. 
    Note: The default baud rate for these apps is 115200. 

BLE Serial Over GATT application
    This application demonstrates a BLE Serial Gatt (BSG) server, a Cypress
    vendor specific service.  See (serial_gatt_service\hci_control.c).

BLE Serial Over GATT peer application
    This application demonstrates how to implement Windows/Android/iOS based 
    BLE Serial Gatt (BSG) client, a Cypress vendor specific service.  It sends 
    data to and receives data from the HCI BLE Serial Over GATT application.

Beacon
   This app demonstrates use of Google Eddystone and Apple iBeacons via the
   beacon library. It also demonstrates uses of multi-advertisement feature.

iAP2 SPP application
    Note: This application and library is available for Apple MFI licensees only.
    This application implements a passthrough serial application. The app can
    use a standard SPP over RFCOMM if peer supports it, or External Accessory 
    iAP2 protocol if connection is established with a iOS device.
    
HomeKit Light Bulb and HomeKit Lock
    Note: These applications and libraries are available for Apple MFI licensees only.
    The HomeKit application, together with the WICED BT HomeKit library, 
    provide examples of Apple HomeKit BLE accessory (light bulb and door lock) 
    that can be accessed and controlled by iOS device.
    See (homekit\lightbulb\btle_homekit2_lightbulb.c
    and homekit\lock\btle_homekit2_lock.c)

