var group___hid_events =
[
    [ "HidEvent", "group___hid_events.html#ga0ba1e6f5e66107361bd95582624dd68d", null ],
    [ "HidEventAny", "group___hid_events.html#gaf464ff8776c421ab12ab7f4387b579e8", null ],
    [ "HidEventButtonStateChange", "group___hid_events.html#ga7e720ba225f313b5c6e3d6f03c44cd3c", null ],
    [ "HidEventKey", "group___hid_events.html#gabb524128c71184b59e41ddf8dd5a9f16", null ],
    [ "HidEventMotionAB", "group___hid_events.html#ga10110b577499906b7c911300c84a34ea", null ],
    [ "HidEventMotionSingleAxis", "group___hid_events.html#ga259701159e2ca84f1e50aca5c7605674", null ],
    [ "HidEventMotionXY", "group___hid_events.html#ga9fd4b9a317a8f1b613c4fccece8d74b8", [
      [ "HID_EVENT_NONE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba2243cca86c24291f214ab45bd03b66b0", null ],
      [ "HID_EVENT_MOTION_AXIS_0", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba3ce7eb9cff667a20719ade1ab25d094a", null ],
      [ "HID_EVENT_MOTION_AXIS_1", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbab219873a2a5cd3b88671a39dc235bfb5", null ],
      [ "HID_EVENT_MOTION_AXIS_2", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba358b1d8379814b05c0c011d54c2f2c1e", null ],
      [ "HID_EVENT_MOTION_AXIS_3", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba01f30d293c9294690b6e0a2b9df8f9a3", null ],
      [ "HID_EVENT_MOTION_AXIS_X_Y", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba460a591f65fb0faa2e9a0eed42c0d5f2", null ],
      [ "HID_EVENT_MOTION_AXIS_A_B", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba0cf07447f77e06975dfe1301b3eb61a1", null ],
      [ "HID_EVENT_NEW_BUTTON_STATE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbaf90c6d8257926fd11dd969eabd312ce2", null ],
      [ "HID_EVENT_KEY_STATE_CHANGE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba64bc100e6b2dd234eeacac1893142bfd", null ],
      [ "HID_EVENT_MOTION_DATA_AVAILABLE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbaf6aadccb13a7278688b6aed47af4ec12", null ],
      [ "HID_EVENT_VOICE_DATA_AVAILABLE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbae203d23ceee31bea04b9bb0a8e61146b", null ],
      [ "HID_EVENT_MIC_START", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbabb61722e463c30dd7801c680f4d969f7", null ],
      [ "HID_EVENT_MIC_STOP", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba0dedaa6d335e930b6b64c09015434cc8", null ],
      [ "HID_EVENT_RC_MIC_START_REQ", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba642211cfff7a363ed053931be58953b4", null ],
      [ "HID_EVENT_RC_MIC_STOP_REQ", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba41149e40c17b9cce06f7df285189dca8", null ],
      [ "HID_EVENT_AUDIO_MODE", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcbab888a33ee16382156da5a5c6728155dc", null ],
      [ "HID_EVENT_AUDIO_CODEC_RD", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba654c728a5dd964195b3cf2a163c4bc78", null ],
      [ "HID_EVENT_AUDIO_CODEC_WT", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba62eba60e91285e4650fc1e2219b7df40", null ],
      [ "HID_EVENT_EVENT_FIFO_OVERFLOW", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba7e7a2e05b86511f014ec9838ad20123b", null ],
      [ "HID_EVENT_ANY", "group___hid_events.html#gga7ee8d0f117a79ca7eb1e0076a9182bcba813b0bbdfb2b0492a5e3bb06b57cb316", null ]
    ] ]
];