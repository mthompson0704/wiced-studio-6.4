/*
 * Copyright 2017, Inventek Systems. All Rights Reserved. This software, associated documentation
 * and materials ("Software"), referenced and provided with this documentation is owned by Inventek
 * Systems and is protected by and subject to worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions. Therefore, you may use this
 * Software only as provided in the license agreement accompanying the software package from which
 * you obtained this Software ("EULA"). If no EULA applies, Inventek Systems hereby grants you a
 * personal, non-exclusive, non-transferable license to copy, modify, and compile the Software source
 * code solely for use in connection with Inventek's integrated circuit products.
 *
 * Any reproduction, modification, translation, compilation, or representation of this Software except
 * as specified above is prohibited without the express written permission of Inventek.
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE.
 * 
 * Inventek reserves the right to make changes to the Software without notice. Inventek does not
 * assume any liability arising out of the application or use of the Software or any product or
 * circuit described in the Software. Inventek does not authorize its products for use in any
 * products where a malfunction or failure of the Inventek product may reasonably be expected to
 * result in significant property damage, injury, or death ("High Risk Product"). By including
 * Inventek's product in a High Risk product, the manufacturer of such system or application
 * assumes all risk of such use and in doing so agrees to indemnify Inventek against all liability.
 * Inventek Systems reserves the right to make changes without further notice to any products or
 * data herein to improve reliability, function, or design. The information contained within is
 * believed to be accurate and reliable. However Inventek does not assume any liability arising
 * out of the application or use of this information, nor the application or use of any product
 * or circuit described herein, neither does it convey any license under its patent rights nor
 * the rights of others.
 */

/*
 * Defines internal configuration of the BCM943903PS board
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Below configuration file defines default BSP settings.
 * Put here platform specific configuration parameters to override these default ones.
 */
#include "platform_config_bsp_default.h"

/*
 * Below configuration file defines default WICED settings.
 * To change settings replace below included file with its contents.
 */
#include "platform_config_wiced_default.h"

#ifdef __cplusplus
} /* extern "C" */
#endif

