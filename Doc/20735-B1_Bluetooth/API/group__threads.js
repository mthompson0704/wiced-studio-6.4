var group__threads =
[
    [ "wiced_rtos_check_for_stack_overflow", "group__threads.html#ga917a4bd4ebe941e7c48c6187c0c44049", null ],
    [ "wiced_rtos_check_stack", "group__threads.html#ga62323073bab8a346f505b92f31c1c031", null ],
    [ "wiced_rtos_create_event_flags", "group__threads.html#ga308a518efb15c5b6935986f1ce5dbbcc", null ],
    [ "wiced_rtos_create_mutex", "group__threads.html#ga5c8cf6b5ce4d8016a54c3ef4063efac7", null ],
    [ "wiced_rtos_create_queue", "group__threads.html#ga341840af6e18113d5eabac91bb1ae3cf", null ],
    [ "wiced_rtos_create_semaphore", "group__threads.html#ga0b31c98acb2d7354af9b2b158ed5b9ef", null ],
    [ "wiced_rtos_create_thread", "group__threads.html#ga97bc8d13bdcd1106d909d97f047fc7fb", null ],
    [ "wiced_rtos_create_timer", "group__threads.html#gabfee3c1258e42abdec74c45ac66dda1c", null ],
    [ "wiced_rtos_create_worker_thread", "group__threads.html#ga3a5819517d83d3c1fae4e36047b2d05c", null ],
    [ "wiced_rtos_delay_microseconds", "group__threads.html#gae41ed6a8aec436bb76711831dccf9224", null ],
    [ "wiced_rtos_delay_milliseconds", "group__threads.html#gac9547485587421485ac0fbce20cddec1", null ],
    [ "wiced_rtos_delete_thread", "group__threads.html#ga23484d9c0c0b723c13eb05ff94ec0884", null ],
    [ "wiced_rtos_init_thread", "group__threads.html#ga60a2f07740eb45a18ff2bae2889353d0", null ],
    [ "wiced_rtos_is_current_thread", "group__threads.html#ga4064584fed8765160ed2f6fa941b32d9", null ],
    [ "wiced_rtos_thread_force_awake", "group__threads.html#gaba4ab4de1a97530b90dfce90869fd2b3", null ],
    [ "wiced_rtos_thread_join", "group__threads.html#gab808616a6761482cf2462029617d9377", null ]
];