#
# Copyright 2016, Cypress Semiconductor
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Cypress Semiconductor.
#


MAKEFILE_TARGETS := help build download debug recover clean Release library

include $(SOURCE_ROOT)wiced_toolchain_common.mk


.PHONY: $(MAKEFILE_TARGETS)

BASE_LOCATIONS  := BASErom BASEram BASEflash
SPAR_LOCATIONS  := SPARrom SPARram SPARflash
TOOLCHAINS      := RealView Wiced CodeSourcery
BUILD_TYPE_LIST := debug release

define USAGE_TEXT

Usage: make <target> [TRANSPORT=SPI] [FREQ=24Mhz|40Mhz] [download] [recover] [DEBUG=1|0] [VERBOSE=1] [UART=yyyy] [JOBS=x] [PLATFORM_NV=EEPROM|SFLASH] [BT_DEVICE_ADDRESS=zzzzzzzzzzzz|random] [OTA_FW_UPGRADE=1] [B49=1] [LEGACY_BOARD=1]

  <target>
    One each of the following mandatory [and optional] components separated by '-'
      * Application (Apps in sub-directories are referenced by subdir.appname)
      * Hardware Platform ($(filter-out common  include README.txt,$(notdir $(wildcard platforms/*))))
      * [BASE location] ($(BASE_LOCATIONS))
      * [SPAR location] ($(SPAR_LOCATIONS))
      * [Toolchain] ($(TOOLCHAINS))

  [TRANSPORT]
    HCI transport interface (default is UART)

  [FREQ]
    Crystal frequency on the reference board (24Mhz/40Mhz)

  [download]
    Download firmware image to target platform

  [build]
    Builds the firmware and OTA images.

  [recover]
    Recover a corrupted target platform
    
  [library]
    Make a library of the app objects instead of fully linking it.

  [DEBUG=1|0]
    Enable or disable debug code in application. When DEBUG=1, watchdog will be disabled,
    sleep will be disabled and the app may optionally wait in a while(1) for the debugger
    to connect

  [VERBOSE=1]
    Shows the commands as they are being executed

  [JOBS=x]
    Sets the maximum number of parallel build threads (default=4)

  [UART=yyyy]
    Use the uart specified here instead of trying to detect the Wiced-BT device.
    This is useful when you are working on multiple BT devices simultaneously.

  [PLATFORM_NV=EEPROM|SFLASH]
    The default non-volatile storage. Default is EEPROM.

  [BT_DEVICE_ADDRESS=zzzzzzzzzzzz|random]
    Use the 48-bit Bluetooth address specified here instead of the default setting from
    platform/*/*.btp file. The special string 'random' (without the quotes) will generate
    a random Bluetooth device address on every download.

  [OTA_FW_UPGRADE]
    Applications which want to upgrade firmware via OTA, have to be downloaded through installer using this option.

  [B49=1]
    Set B49=1 if using the B49 platform board

  [LEGACY_BOARD=1]
    Set LEGACY_BOARD=1 if using boards other than CYW920706WCDEVAL(i.e., if using older boards such as BCM920706V2_EVAL/CYW92070xV3)

  Notes
    * Component names are case sensitive
    * 'Wiced', 'SPI', 'UART' and 'debug' are reserved component names
    * Component names MUST NOT include space or '-' characters
    * Building for release is assumed unless '-debug' is appended to the target
    * Application/target names must begin with an alphabetic character

  Example Usage
    Build for Release
      $> make demo.hello_sensor-CYW920706WCDEVAL build

    Build, Download and Run using the default programming interface
      $> make demo.hello_sensor-CYW920706WCDEVAL download

    Build, Download and Run using specific UART port
      $> make demo.hello_sensor-CYW920706WCDEVAL download UART=COMx

    Build, Download to Serial Flash and Run using default programming interface
      $> make demo.hello_sensor-CYW920706WCDEVAL download PLATFORM_NV=SFLASH

    Clean output directory
      $> make clean

endef


define MAKE_TARGET_HELP

   Please use the targets in the Make Target window to build your application

endef

.PHONY: help

.DEFAULT_GOAL := help

bad_target_specified:
	$(info Invalid target specified)
	$(error $(USAGE_TEXT))


help:
	$(info $(USAGE_TEXT))

.PHONY: Release

Release:
	$(info $(MAKE_TARGET_HELP))


clean:
	$(QUIET)$(ECHO) Cleaning...
	$(QUIET)$(CLEAN_COMMAND)
	$(QUIET)$(RM) -rf .gdbinit
	$(QUIET)$(ECHO) Done

ifneq ($(DIRECT_LOAD),1)
  CONVERT_CGS = convert_cgs_to_hex create_ota_image
else
  CONVERT_CGS = convert_cgs_to_hcd_for_direct_load
endif

ifeq ($(HOST_OS),Win32)
  CHIPLOAD_REBAUD:=-REBAUDRATE 3000000 -LAUNCHADDRESS 0x00000000
else
  CHIPLOAD_REBAUD:= -REBAUDRATE 3000000 -LAUNCHADDRESS 0x00000000
endif


build: $(BUILD_STRING) $(CONVERT_CGS)

library: $(BUILD_STRING)
	$(QUIET)$(ECHO) $(QUOTES_FOR_ECHO)Made library$(QUOTES_FOR_ECHO)

ifneq ($(UART), )
  UART:=$(shell $(ECHO) $(UART) > com_port.txt))
  download: build download_using_chipload
else
  download: build find_com_port download_using_chipload
endif

recover:  build recover_using_chipload

find_com_port:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Detecting device...
	$(QUIET)$(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/get_com_port/get_com_port.pl $(DETECTANDID_FULL_NAME) $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_IDFILE) $(SOURCE_ROOT)com_port.txt $(DETECTANDID_BAUD) > build/$(OUTPUT_NAME)/detect.log 2>&1 \
	&& $(ECHO) Device found \
	|| $(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/print_file/print_file.pl  $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/find_com_port_error.txt
	$(QUIET)$(ECHO_BLANK_LINE)

ifeq ($(HOMEKIT),1)
convert_cgs_to_hex:
ifneq ($(OTA_FW_UPGRADE), 1)
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/ $(BTP_OVERRIDES_SPAR) -A $(PLATFORM_STORAGE_BASE_ADDR) -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) **** Conversion failed ****
ifneq ($(APP_STATIC_DATA),)
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-app-static.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)__.hex
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)__.hex $(SOURCE_ROOT)platforms/ge25519_niels_base_multiples_FF07000.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
else
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex $(SOURCE_ROOT)platforms/ge25519_niels_base_multiples_FF07000.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
endif
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd
else
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
ifeq ($(USE_256K_SECTOR_SIZE),1)
	$(call CONV_SLASHES,$(PERL)) -p -e "s/f4 1f 00 00 aa 55/f4 ff 07 00 aa 55/" $(FAIL_SAFE_OTA_CGS:_256k.cgs=.cgs) > $(FAIL_SAFE_OTA_CGS)
endif
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDES_FAIL_SAFE) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.hex --cgs-files $(FAIL_SAFE_OTA_CGS) > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) -a build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.txt  > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.log 2>&1 && \
	$(call CONV_SLASHES,$(HEAD_OR_TAIL_FULL_NAME)) H -3 build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.txt build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.txt > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDES_SPAR) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(APPEND_TO_INTEL_HEX_FULL_NAME)) -I 0x1b build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.txt build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.hex > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.log 2>&1 && \
	$(ECHO)	Conversion complete || $(ECHO) **** Conversion failed ****
	$(QUIET)$(call CONV_SLASHES,$(SHIFT_INTEL_HEX_FULL_NAME)) 0xFF000000 build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex
ifneq ($(APP_STATIC_DATA),)
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-app-static.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)__.hex
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)__.hex $(SOURCE_ROOT)platforms/ge25519_niels_base_multiples_FF07000.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
else
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_.hex $(SOURCE_ROOT)platforms/ge25519_niels_base_multiples_FF07000.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
endif
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd
endif	
else 
ifeq ($(DIRECT_LOAD), 1)
convert_cgs_to_hcd_for_direct_load:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HCD for direct load...
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/ $(BTP_OVERRIDES_SPAR) -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) -H build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)
else
convert_cgs_to_hex:
ifneq ($(OTA_FW_UPGRADE), 1)
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/ $(BTP_OVERRIDES_SPAR) -A $(PLATFORM_STORAGE_BASE_ADDR) -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -H build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) **** Conversion failed ****
else
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
ifeq ($(USE_256K_SECTOR_SIZE),1)
	$(call CONV_SLASHES,$(PERL)) -p -e "s/f4 1f 00 00 aa 55/f4 ff 07 00 aa 55/" $(FAIL_SAFE_OTA_CGS:_256k.cgs=.cgs) > $(FAIL_SAFE_OTA_CGS)
endif
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDES_FAIL_SAFE) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.hex --cgs-files $(FAIL_SAFE_OTA_CGS) > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) -a build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexss_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.txt  > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.log 2>&1 && \
	$(call CONV_SLASHES,$(HEAD_OR_TAIL_FULL_NAME)) H -3 build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_full.txt build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.txt > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDES_SPAR) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.log 2>&1 && \
	$(call CONV_SLASHES,$(APPEND_TO_INTEL_HEX_FULL_NAME)) -I 0x1b build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ss_temp.txt build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_ihexserial_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.hex > build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.log 2>&1 && \
	$(ECHO)	Conversion complete || $(ECHO) **** Conversion failed ****
	$(QUIET)$(call CONV_SLASHES,$(SHIFT_INTEL_HEX_FULL_NAME)) 0xFF000000 build/$(OUTPUT_NAME)/$(OUTPUT_NAME)_combined_temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd
endif
endif
endif

create_ota_image:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Creating OTA images...
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/ $(BTP_OVERRIDES_OTA) -B $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) **** Conversion failed ****
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hcd
	$(QUIET)$(call CONV_SLASHES,$(PERL)) -e '$$s=-s "build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin";printf "OTA image footprint in NV is %d bytes",$$s;'
	$(QUIET)$(ECHO_BLANK_LINE)

ifneq ($(DIRECT_LOAD), 1)
download_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(eval UART:=$(shell $(CAT) < com_port.txt))
	$(QUIET)$(if $(UART), \
			$(ECHO) Downloading application... && $(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE $(CHIPLOAD_REBAUD) -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) $(MINIDRIVER_DOWNLOAD) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDE_DL) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -LOGTO build/$(OUTPUT_NAME)/logto.log > build/$(OUTPUT_NAME)/download.log 2>&1 \
				&& $(ECHO) Download complete && $(ECHO_BLANK_LINE) && $(ECHO) Application running \
					|| ($(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/print_file/print_file.pl  $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/download_error.txt && \
						$(ECHO) Download failed. This WICED platform of the SDK only supports download to 20706A2 device.))

recover_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Recovering platform ...
	$(QUIET)$(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE $(CHIPLOAD_REBAUD) -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) $(MINIDRIVER_DOWNLOAD) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDE_DL) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex  > build/$(OUTPUT_NAME)/download.log 2>&1 && $(ECHO) Recovery complete && $(ECHO_BLANK_LINE) && $(ECHO) Application running || $(ECHO) **** Recovery failed - retry ****
else
download_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(eval UART:=$(shell $(CAT) < com_port.txt))
	$(QUIET)$(if $(UART), \
			$(ECHO) Downloading application... && $(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE $(CHIPLOAD_REBAUD) -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) $(MINIDRIVER_DOWNLOAD) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDE_DL) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd -LOGTO build/$(OUTPUT_NAME)/logto.log > build/$(OUTPUT_NAME)/download.log 2>&1 \
				&& $(ECHO) Download complete && $(ECHO_BLANK_LINE) && $(ECHO) Application running \
				|| ($(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/print_file/print_file.pl  $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/download_error.txt && \
					$(ECHO) Download failed. This WICED platform of the SDK only supports download to 20706A2 device.))

recover_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Recovering platform ...
	$(QUIET)$(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE $(CHIPLOAD_REBAUD) -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) $(MINIDRIVER_DOWNLOAD) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_BOOTP) $(BTP_OVERRIDE_DL) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd  > build/$(OUTPUT_NAME)/download.log 2>&1 && $(ECHO) Recovery complete && $(ECHO_BLANK_LINE) && $(ECHO) Application running || $(ECHO) **** Recovery failed - retry ****
endif

ifneq ($(DIR_BUILD_STRING),)

# Separate the build string into components
COMPONENTS := $(subst -, ,$(DIR_BUILD_STRING))
COMPONENTS := $(subst .,/,$(COMPONENTS))
BASE_LOC  ?= rom
BTP_OVERRIDES ?=

#Find app and platform
PLATFORM_FULL :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)platforms/$(comp)),$(comp),)))

#if a valid patform is not found indicate the correct usage
ifeq ($(PLATFORM_FULL),)
$(error Usage: <Target string> TRANSPORT=<UART/SPI> FREQ=<xxMhz> ....)
endif

PLATFORM      :=$(notdir $(PLATFORM_FULL))
PLATFORM_MAKE :=$(PLATFORM)$(if $(TRANSPORT),_$(TRANSPORT))
PLATFORM_CGS :=$(PLATFORM)$(if $(FREQ),_$(FREQ))
APP_FULL      :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)apps/$(comp)),$(comp),)))
COMMON_APP_DIR_QUALIFIER :=
ifeq ($(APP_FULL),)
APP_FULL      :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)../common/apps/$(comp)),$(comp),)))
COMMON_APP_DIR_QUALIFIER := ../common/
endif
APP           :=$(notdir $(APP_FULL))

FAIL_SAFE_OTA_CGS := $(SOURCE_ROOT)platforms/20703A2_fail_safe_ota.cgs
ifeq ($(USE_256K_SECTOR_SIZE),1)
  FAIL_SAFE_OTA_CGS := $(FAIL_SAFE_OTA_CGS:.cgs=_256k.cgs)
endif

ifneq ($(filter $(COMPONENTS),$(BASE_LOCATIONS)),)
BASE_LOC      :=$(strip $(subst BASE,,$(filter $(COMPONENTS),$(BASE_LOCATIONS))))
endif
ifneq ($(filter $(COMPONENTS),$(SPAR_LOCATIONS)),)
SPAR_LOC      :=$(strip $(subst SPAR,,$(filter $(COMPONENTS),$(SPAR_LOCATIONS))))
endif
ifneq ($(filter $(COMPONENTS),$(TOOLCHAINS)),)
TOOLCHAIN     :=$(strip $(filter $(COMPONENTS),$(TOOLCHAINS)))
endif
BUILD_TYPE    :=$(strip $(filter $(COMPONENTS),$(BUILD_TYPE_LIST)))


ifeq ($(wildcard $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk),)
$(error Platform makefile not found: $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk)
endif

include $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk

ifeq ($(CHIP)$(CHIP_REV),)
$(error CHIP not defined in Platform makefile $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk)
endif

ifeq ($(BASE_LOC),)
$(error BASE Location not defined  in command-line target or in Platform makefile $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk)
endif

ifeq ($(SPAR_LOC),)
$(error SPAR Location not defined  in command-line target or in Platform makefile $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk)
endif

ifeq ($(TOOLCHAIN),)
$(error Toolchain not defined in command-line target or in Platform makefile $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MAKE).mk)
endif

# For SDK Version
ifneq ("$(wildcard $(SOURCE_ROOT)../version.txt)","")
WICED_SDK_VERSION  = $(subst ., , $(shell $(CAT) < $(call CONV_SLASHES,$(SOURCE_ROOT)../version.txt)))
WICED_SDK_MAJOR_VER    = $(word 2, $(subst _, ,$(word 3, $(WICED_SDK_VERSION))))
WICED_SDK_MINOR_VER    = $(word 4, $(WICED_SDK_VERSION))
WICED_SDK_REV_NUMBER   = $(word 5, $(WICED_SDK_VERSION))
WICED_SDK_BUILD_NUMBER = $(word 6, $(WICED_SDK_VERSION))
else
WICED_SDK_MAJOR_VER    = 000
WICED_SDK_MINOR_VER    = 000
WICED_SDK_REV_NUMBER   = 000
WICED_SDK_BUILD_NUMBER = 0000
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_MAJOR_VER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_MAJOR_VER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_MAJOR_VER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_MAJOR_VER=$(WICED_SDK_MAJOR_VER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_MINOR_VER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_MINOR_VER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_MINOR_VER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_MINOR_VER=$(WICED_SDK_MINOR_VER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_REV_NUMBER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_REV_NUMBER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_REV_NUMBER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_REV_NUMBER=$(WICED_SDK_REV_NUMBER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_BUILD_NUMBER)\" ne \"0000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_BUILD_NUMBER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_BUILD_NUMBER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_BUILD_NUMBER=$(WICED_SDK_BUILD_NUMBER)
endif

# For Power Class
ifneq (,$(findstring Class2,$(PLATFORM)))
C_FLAGS += -DPOWER_CLASS=2
else
# enumeration, means class 1.5
C_FLAGS += -DPOWER_CLASS=1
endif

# For Chip Version
ifneq (,$(findstring CYW920706,$(PLATFORM)))
C_FLAGS += -DPLATFORM=0
else
C_FLAGS += -DPLATFORM=1
endif

C_FLAGS += -DCYW$(CHIP)$(CHIP_REV)=1
C_FLAGS += -DCYW$(CHIP_FLAVOR)$(CHIP_REV)=1
C_FLAGS += -DCHIP=$(CHIP_FLAVOR)

export C_FLAGS
ifeq ($(BUILD_TYPE),)
#default to release build
BUILD_TYPE := release
endif


JOBS ?=4
ifeq (,$(SUB_BUILD))
JOBSNO := $(if $(findstring 1,$(LINT)), , -j$(JOBS) )
endif

OUTPUT_NAME                := $(APP)-$(PLATFORM)-$(BASE_LOC)-$(SPAR_LOC)-$(TOOLCHAIN)-$(BUILD_TYPE)

BIN_OUT_DIR                := ../../build/$(OUTPUT_NAME)

# Include SDK include directories
SDK_INC_FLAGS              :=../../include/ ../../../common/include/ ../../include/Platforms/$(PLATFORM)/ ../../include/Drivers/ ../../include/Drivers/$(CHIP)

# Override the BD_ADDR parameter if provided on the command line.
ifeq ($(BT_DEVICE_ADDRESS),random)
BTP_OVERRIDES := -O DLConfigBD_ADDRBase:$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)r=int(rand(32768));printf "$(CHIP)$(CHIP_REV)00%X",$(PERL_ESC_DOLLAR)r;')
else
ifneq ($(BT_DEVICE_ADDRESS),)
BTP_OVERRIDES := -O DLConfigBD_ADDRBase:$(BT_DEVICE_ADDRESS)
endif
endif

ifeq ($(HOMEKIT),1)
ifneq ($(OTA_FW_UPGRADE), 1)
    BTP_OVERRIDES_SPAR := $(CONFIG_DS_LOC_OVERRIDE)
else
    CONFIG_DS_LOC_OVERRIDE ?= -O ConfigDSLocation:0x4000
    BTP_OVERRIDES_FAIL_SAFE := $(BTP_OVERRIDES) $(CONFIG_DS_LOC_OVERRIDE) -O DLConfigVSLocation:0x2000 -O DLConfigFixedHeader:0 -O DLMaxWriteSize:251
    BTP_OVERRIDES_SPAR := $(BTP_OVERRIDES) $(CONFIG_DS_LOC_OVERRIDE) -O DLConfigVSLocation:0x2000
endif
else
ifeq ($(DIRECT_LOAD), 1)
    BTP_OVERRIDES_SPAR := $(BTP_OVERRIDES) -O DLConfigSSLocation:$(PLATFORM_DIRECT_LOAD_BASE_ADDR) -O "DLConfigTargeting:RAM runtime" -O DLMaxWriteSize:249 
else
ifneq ($(OTA_FW_UPGRADE), 1)
    BTP_OVERRIDES_SPAR = $(BTP_OVERRIDES) $(CONFIG_DS_LOC_OVERRIDE)
else
    CONFIG_DS_LOC_OVERRIDE ?= -O ConfigDSLocation:0x4000
    BTP_OVERRIDES_FAIL_SAFE := $(BTP_OVERRIDES) $(CONFIG_DS_LOC_OVERRIDE) -O DLConfigVSLocation:0x2000 -O DLConfigFixedHeader:0 -O DLMaxWriteSize:251
    BTP_OVERRIDES_SPAR := $(BTP_OVERRIDES) $(CONFIG_DS_LOC_OVERRIDE) -O DLConfigVSLocation:0x2000
endif
endif
endif
ifneq ($(DIRECT_LOAD), 1)
    BTP_OVERRIDES_OTA := $(CONFIG_DS_LOC_OVERRIDE) -O DLConfigFixedHeader:0
endif

export BIN_OUT_DIR
export VERBOSE
export SDK_INC_FLAGS

ifeq ($(TOOLCHAIN),Wiced)
TC :=wiced
GCC_TOOL_DIR=$(call CONV_SLASHES,../../../43xxx_Wi-Fi/tools/ARM_GNU/$(HOST_OS)/bin/)

else
ifeq ($(TOOLCHAIN),RealView)
TC :=rv
else
ifeq ($(TOOLCHAIN),CodeSourcery)
TC :=gcc
else
$(error Toolchain $(TOOLCHAIN) has no mapping to a name for ADK)
endif
endif
endif

ifeq ($(GCC_TOOL_DIR),)
$(error Toolchain not found - implement wildcard macro to find toolchain and define GCC_TOOL_DIR )
endif

export TC
export GCC_TOOL_DIR

BLD :=A_$(CHIP)$(CHIP_REV)
export BLD


TOOLSBIN=$(call CONV_SLASHES,../../$(subst $(SOURCE_ROOT),,$(COMMON_TOOLS_PATH)))
export TOOLSBIN

BASE_IN :=$(BASE_LOC)
SPAR_IN :=$(SPAR_LOC)
export BASE_IN
export SPAR_IN

DIR := ../../$(COMMON_APP_DIR_QUALIFIER)apps/$(APP_FULL)
export DIR

CGS_LIST := $(addprefix ../../platforms/$(PLATFORM)/,$(PLATFORM_CONFIGS))
export CGS_LIST

ifeq ($(PLATFORM_NV),EEPROM)
ifneq ($(DIRECT_LOAD), )
COMPRESSOR := $(call CONV_SLASHES,../../$(LZSS_FULL_PATH))
export COMPRESSOR
COMPRESSION_ENABLED ?= y
export COMPRESSION_ENABLED
endif
endif

ifneq ($(APP_STATIC_DATA),)
APP_STATIC_DATA_HEX = $(BIN_OUT_DIR)/$(OUTPUT_NAME)-app-static.hex
export APP_STATIC_DATA
export APP_STATIC_DATA_HEX
endif

export CONFIG_DEFINITION
export DIRECT_LOAD
export PLATFORM_FULL
export PLATFORM_NV
export DEBUG
export APP_PATCH_DIR
export CHIP_FLAVOR

PLATFORM_CGS_PATH := $(addprefix ../../platforms/$(PLATFORM)/,$(PLATFORM_CONFIGS))
export PLATFORM_CGS_PATH

ifeq ($(VERBOSE),1)
$(info ADK parameters:)
$(info GCC_TOOL_DIR="$(GCC_TOOL_DIR)")
$(info BUILD_STRING="$(BUILD_STRING)")
$(info TC="$(TC)")
$(info DIR="$(DIR)")
$(info BLD="$(BLD)")
$(info TOOLSBIN="$(TOOLSBIN)")
$(info BASE_IN="$(BASE_IN)")
$(info SPAR_IN="$(SPAR_IN)")
$(info BIN_OUT_DIR="$(BIN_OUT_DIR)")
$(info OUTPUT_NAME="$(OUTPUT_NAME)")
$(info CGS_LIST="$(CGS_LIST)")
$(info CONFIG_DEFINITION="$(CONFIG_DEFINITION)")
$(info PLATFORM_CGS_PATH="$(PLATFORM_CGS_PATH)")
$(info APP_FULL="$(APP_FULL)")
$(info APP_PATCH_DIR="$(APP_PATCH_DIR)")
$(info PLATFORM_NV="$(PLATFORM_NV)")
$(info DEBUG="$(DEBUG)")
$(info DIRECT_LOAD="$(DIRECT_LOAD)")
endif


$(BUILD_STRING):
ifeq ($(BUILD_STRING),$(BUILD_TARGET))
	$(QUIET)$(MAKE) -C $(SOURCE_ROOT)Wiced-BT/spar $(SILENT) $(JOBSNO) cgs
	$(QUIET)$(call MKDIR,$(SOURCE_ROOT)build/eclipse_debug/)
	$(QUIET)$(CP) $(SOURCE_ROOT)build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.elf $(SOURCE_ROOT)build/eclipse_debug/last_built.elf
else
	$(QUIET)$(MAKE) -C $(SOURCE_ROOT)Wiced-BT/spar $(SILENT) $(JOBSNO) library
endif

endif
