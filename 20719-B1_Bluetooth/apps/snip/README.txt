Snip apps
+++++++++
These snippet applications demonstrate a single functionality of the BT software
or the WICED board. See the applications in the apps\snip\ folder.

Over the air Firmware Upgrade
  This is a stub application that demonstrates how to link with the
  OTA FW Upgrade library.  See ota_firmware_upgrade\ota_firmware_upgrade.c
  - Over the air Firmware Upgrade peer application
    This is an application that demonstrates how to peform OTA firmware upgrade
    from a peer Windows or Android app communicating with the ota_firmware_upgrade
    snippet running on a WICED board.  See ota_firmware_upgrade\peer_apps
  
Mesh sample apps
    Sample apps based on BT SIG Mesh protocol. Sample included for 
    various models including power on/off, level, battery, light controls, 
    power level, transition, location, property, time, scene, scheduler,  
    provision, sensor and time. Client and Server models apps are included 
    for each of the models. Windows and Android based peer apps are included
    for provisioning and control.

    Mesh applications are contained in subfolders under the mesh folder, and
    demonstrate implementions of the various BLE Mesh models, as indicated by folder
    name.
    - See the application .c file for each of these apps for more details on setup.
    - See the mesh\peerapps folder for the Windows application used to provision and
      control the mesh devices.
    - See the mesh\ClientControl folder for the demonstation host MCU Client Control
      app with custom mesh support that can communicate with the embedded HCI app
      over the WICED HCI interface.  Note this is a mesh-specific separate instance
      of the Client Control host MCU app, not to be confused with the generic Client
      Control app under apps\host.
