
#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"

//
// resolves IP address of DNS
//

wiced_result_t dns_lookup()
{
	int i;

    WPRINT_APP_INFO( ( "Resolving IP address of server %s\r\n", dct.PumpSpyHostName) );
    result = -1;
    i = 0;
    while ((result != WICED_SUCCESS) && (i++ < 3))
    {
    	result = wiced_hostname_lookup(dct.PumpSpyHostName, &ip_address, 10000, WICED_STA_INTERFACE);
    }
    WPRINT_APP_INFO( ( "result %d Server is at %u.%u.%u.%u\r\n",   result, (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 24),
                                                       (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 16),
                                                       (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 8),
                                                       (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 0) ) );
    /*
    if (((uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 24) == 0) &&
    		((uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 16) == 0) &&
			((uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 8) == 0) &&
			((uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 0) == 0))
    {
        wiced_framework_reboot();
    }
    */
    return result;
}

