-----------------------------------------------------------------
WICED Studio - 20719-B1 Bluetooth Application Directory - README
-----------------------------------------------------------------

This directory contains applications that demonstrate how to
use the WICED Studio API and how to build advanced applications.
Several applications are also provided to test WICED hardware.

Pre-Requisites for running WICED Studio Applications
-----------------------------------------------------
 ***PLEASE*** read through the WICED Studio Kit Guide located
 in the 'doc' directory before attempting to use any of 
 the applications in this directory!

Application Demonstration Requirements
--------------------------------------
* A development computer with the following software:
    * WICED Studio installed
    * A terminal application (such as PuTTY). The WICED Studio Kit Guide 
      located in the 'doc' directory describes how to configure a terminal
      application.

Sample Applications
--------------------

Applications types:
 
  Snip apps   : Application snippets to demonstrate WICED BT APIs for specific
                functionality of the BT software or the WICED board. See the
                applications in the apps\snip\ folder

  Demo apps   : Applications that demonstrate usage of Bluetooth profiles and
                the WICED HCI interface to the host app (client_control). See
                applications in the apps\demo\ folder
                
  Host app    : Application used to demonstrate WICED BT APIs usage for host 
                MCU app (client_control). This application communicates with
                embedded HCI apps over the WICED HCI interface. See the
                application in the apps\host\ folder

  Peer apps   : Application that run on Windows, iOS or Androind and act as peer 
                BT apps to demonstrate specific profiles or features.  Found
                under sub-folders for the embedded applications they
                are used with.

See the README.txt file in the appropriate apps sub-folder for more information
on each application supplied with WICED Studio.

