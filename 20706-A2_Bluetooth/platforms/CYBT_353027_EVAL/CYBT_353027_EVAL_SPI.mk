CHIP                       := 20703
CHIP_REV                   := A2
CHIP_FLAVOR                := 20706
BASE_LOC                   ?= rom
SPAR_LOC                   ?= ram
TOOLCHAIN                  ?= Wiced
PLATFORM_NV                ?= SFLASH
PLATFORM_STORAGE_BASE_ADDR := 0xFF000000
PLATFORM_BOOTP             := $(PLATFORM_CGS)_$(PLATFORM_NV).btp
PLATFORM_MINIDRIVER        := uart.hex
PLATFORM_CONFIGS           := $(PLATFORM_CGS).cgs
PLATFORM_IDFILE            := CYW20706A2_IDFILE.txt
PLATFORM_BAUDRATE          := AUTO
PLATFORM_DIRECT_LOAD_BASE_ADDR := 0x2404F0
APP_PATCH_DIR              := brcm/wiced_spi

MINIDRIVER_DOWNLOAD        :=
ifdef USE_256K_SECTOR_SIZE
    CONFIG_DS_LOC_OVERRIDE := -O DLConfigVSLocation:0x40000 -O DLConfigVSLength:0x40000 -O ConfigDSLocation:0xC0000
    BTP_OVERRIDE_DL        := -O DLSectorEraseMode:"Written sectors only"
endif

MINIDRIVER_DOWNLOAD    := -MINIDRIVER $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM_MINIDRIVER)
