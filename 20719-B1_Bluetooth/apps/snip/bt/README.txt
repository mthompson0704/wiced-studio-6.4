-----------------------------------------------
Classic Bluetooth snippets (under apps\snip\bt)
-----------------------------------------------
Serial Port Profile application
   SPP application uses SPP profile library to establish, terminate, send and
   receive SPP data over BR/EDR.  See (spp\spp.c).
 
iAP2 application
    Note: This application and library is available for Apple MFI licensees only.
    This snippet uses the WICED iAP2 library to send data to an iOS device using
    Apple's External Accessory iAP2 protocol. See iap2\iap2.c
