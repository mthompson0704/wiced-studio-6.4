var searchData=
[
  ['gatt_20database',['GATT Database',['../group__gattdb__api__functions.html',1,'']]],
  ['gki',['GKI',['../group___g_k_i.html',1,'']]],
  ['gpio',['GPIO',['../group___g_p_i_o_driver.html',1,'']]],
  ['gatt_20multi_2dprofile_20_28gatt_5fmp_29_20library',['GATT Multi-Profile (GATT_MP) library',['../group__wiced__bt__gatt__mp__api__functions.html',1,'']]],
  ['generic_20attribute_20_28gatt_29',['Generic Attribute (GATT)',['../group__wicedbt__gatt.html',1,'']]],
  ['gatt_20utilities',['GATT Utilities',['../group__wicedbt__gatt__utils.html',1,'']]]
];
