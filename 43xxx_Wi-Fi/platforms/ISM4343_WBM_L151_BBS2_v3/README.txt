--------------------------------------------
ISM4343-WBM-L151 EVB - README
--------------------------------------------

Provider    : Inventek Systems
Website     : http://www.inventeksys.com/products-page/wifi-modules/serial-wifi/ism4343-wbm-l151/
Description : Inventek Systems ISM4343-WBM-L151 mounted on a Inventek ISM4343-WBM-L151 evaluation board

Module
  Mfr     : Inventek Systems
  P/N     : ISM4343-WBM-L151 (N12)
  MCU     : STM32F412VEH6
  WLAN    : BCM4343W Wi-Fi CoB
  Antenna : SMA connector

EVB Features
  USB-JTAG debug interface
  USB-serial UART interface
  Power supply : USB and/or external +5v
  Reset button

