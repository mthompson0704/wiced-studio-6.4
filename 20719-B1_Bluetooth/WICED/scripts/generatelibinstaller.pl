#!/usr/bin/perl
###############################################################################
#
# THIS INFORMATION IS PROPRIETARY TO
#     Cypress Semiconductor
#  All rights reserved
#
#
#  Given the overlaid object files, this file creates the required linker
# scatter files or the trampoline functions for each given function in the overlay.
###############################################################################

#use strict;
#use warnings;
#use Getopt::Long;
#use File::Basename;

my $help = undef;
my $tool = undef;
my $toolchain = undef;
my $verbose = 0;

sub basename($) ;

my @libs = ();


my $argid = 0;
while ($argid <= $#ARGV ) 
{
  	my $arg = $ARGV[$argid];
  	$arg =~ s/^\s*(.*?)\s*$/$1/sgi;  # trim whitespace
	
	if($arg =~ /^-(.*)=(.*)/)
	{
		if   ($1 eq 'tool')  {$tool = $2;      splice(@ARGV, $argid, 1); next;}
		elsif($1 eq 'tc')    {$toolchain = $2; splice(@ARGV, $argid, 1); next;}
		elsif($1 eq 'v')     {$verbose = $2;   splice(@ARGV, $argid, 1); next;}
        else{$help = 1; last;}
	}
	
	$argid++;
}

printf STDERR "VERBOSE is ON\n" if ($verbose);

# Sort the objects to keep some things simple.
@libs = sort(@ARGV);

if($help)
{
    printf(STDERR "USAGE: generatelibinstaller.pl -tool={path_to_objdump|path_to_fromelf} -toolchain={rv|gcc} list\n");
    exit -1;
}

print "#include <stdint.h>\n";
print "typedef struct tag_PATCH_TABLE_ENTRY_t {\n";
print "\tuint32_t breakout;\n";
print "\tuint32_t replacement;\n";
print "} PATCH_TABLE_ENTRY_t;\n";
print "void patch_autoInstall(uint32_t old_address, uint32_t new_address);\n";
print "void patch_autoReplace(uint32_t breakout_address, uint32_t replacement);\n";
print "void patch_autoReplaceData(uint32_t breakout_address, uint32_t replacement);\n";

# Print install function
print "void install_libs(void);\n\nvoid install_libs(void)\n{\n";

if ($toolchain eq 'rv')
  {
  	printf STDERR "Tool chain is RV\n" if ($verbose);
  	
    foreach (@libs)
      {
        my $f = $_;
        my $init = basename($f) . "_init";
        
        printf STDERR "Reading symbol table from $f:\n" if ($verbose);
        printf STDERR "$tool -s $f\n" if ($verbose);
        
        open(SYMBOLS, "$tool -s $f |") || die "Could not read symbol table from $f with $tool.\n";
        while (<SYMBOLS>)
          {
            my $line = $_;
            if ($line =~ /\s+\d+\s+auto_install_(.*)(.*)/)
              {
              	printf STDERR "Found auto_install_$1\n" if ($verbose);
                print "\t{\n\t\textern void auto_install_$1(void);\n\t\tauto_install_$1();\n\t}\n";
              }
            if ( $line =~ /\s+\d+\s+$init(.*)/)
              {
              	printf STDERR "Found $init\n" if ($verbose);
                print "\t{\n\t\textern void $init(void);\n\t\t$init();\n\t}\n";
              }
            if ( $line =~ /\s+\d+\s+(__patch_.*)/)
              {
                handle_patch_entry($3);
              }
          }
        close(SYMBOLS);
      }
  }
else
  {
  	printf STDERR "Tool chain is GCC\n" if ($verbose);
    foreach (@libs)
      {
        my $f = $_;
        my $init = basename($f) . "_init";
        printf STDERR "Reading symbol table from $f:\n" if ($verbose);
        printf STDERR "$tool -t $f\n" if ($verbose);
        
        open(SYMBOLS, "$tool -t $f |") || die "Could not read symbol table from $f with $tool.\n";
        while (<SYMBOLS>)
          {
            my $line  = $_;
            if ($line =~ /(.*)\.init_text\s+([\dA-Fa-f]{8})(.*)auto_install_(.*)$/ )
              {
              	printf STDERR "Found install_$4\n" if ($verbose);
                print "\t{\n\t\textern void auto_install_$4(void);\n\t\tauto_install_$4();\n\t}\n";
              }
            elsif ( $line =~ /(.*)\.init_text\s+([\dA-Fa-f]{8})(.*)$init(.*)/ )
              {
              	printf STDERR "Found $init\n" if ($verbose);
                print "\t{\n\t\textern void $init(void);\n\t\t$init();\n\t}\n";
              }
            elsif ( $line =~ /(.*)\.init_patch_table\s+([\dA-Fa-f]{8})\s+\.\w+\s+(__patch_.*)/ )
              {
                handle_patch_entry($3);
              }
          }
        close(SYMBOLS);
      }
  }

print "}\n";

printf STDERR "Lib installer done.\n" if ($verbose);

sub basename($) 
{
	my $file = shift;
	$file =~ s!^(?:.*/)?(.+?)(?:\.[^.]*)?$!$1!;
	return $file;
}

sub handle_patch_entry
{
	my ($patch_symbol) = @_;
	my $patch_install_fn;
	
	printf STDERR "Found $patch_symbol\n" if ($verbose);
	print "\t{\n";
	print "\t\textern PATCH_TABLE_ENTRY_t $patch_symbol;\n";
	if($patch_symbol =~ /^__patch__/) {
		$patch_install_fn = "patch_autoInstall";
	}
	elsif($patch_symbol =~ /^__patch_addin__/) {
		$patch_install_fn = "patch_autoInstall";
	}
	elsif($patch_symbol =~ /^__patch_rpl_/) {
		$patch_install_fn = "patch_autoReplace";
	}
	elsif($patch_symbol =~ /^__patch_data__/) {
		$patch_install_fn = "patch_autoReplaceData";
	}
	print "\t\t$patch_install_fn\($patch_symbol\.breakout & 0x00ffffff, $patch_symbol\.replacement\);\n"; 
	print "\t}\n";
}
