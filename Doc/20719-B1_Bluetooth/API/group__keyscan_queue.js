var group__keyscan_queue =
[
    [ "KeyscanQueueState", "struct_keyscan_queue_state.html", [
      [ "bufStart", "struct_keyscan_queue_state.html#a6962f48708a3763598bcda95a97ddee4", null ],
      [ "curNumElements", "struct_keyscan_queue_state.html#a98eb8301eed3a43f1d6f8ea814442379", null ],
      [ "elementSize", "struct_keyscan_queue_state.html#aae929fff50bb45d722ffef5e98b88e0a", null ],
      [ "maxNumElements", "struct_keyscan_queue_state.html#a446aae120574b49d75c79e621d7d1e7b", null ],
      [ "readIndex", "struct_keyscan_queue_state.html#ae5d0c2231dd75a46f35a20c4018e9c13", null ],
      [ "savedNumElements", "struct_keyscan_queue_state.html#af619faf0444aa39d5b38d869e269f789", null ],
      [ "savedWriteIndexForRollBack", "struct_keyscan_queue_state.html#aaee00d7ee9d7c82f28982081a567a8fc", null ],
      [ "writeIndex", "struct_keyscan_queue_state.html#a75b399a70dc5b784d95741db3c30b783", null ]
    ] ],
    [ "KeyEvent", "group__keyscan_queue.html#ga86be38ec890da51c57fb2bd8c85fc114", [
      [ "ROLLOVER", "group__keyscan_queue.html#gga92c4bd66c8074c3d126a069275a0a80cab4a53dc50ba38ef5c3f266d1499ecf32", null ],
      [ "END_OF_SCAN_CYCLE", "group__keyscan_queue.html#gga92c4bd66c8074c3d126a069275a0a80cad8ba23ff4d3c25d36207e0da40355a22", null ],
      [ "MIA_KEY_EVENT_FIFO_SIZE", "group__keyscan_queue.html#gga2ee507fea44084259042ff7bb45ecabfa259261502f7197594f2a387a4d2fc07e", null ],
      [ "KEYSCAN_FW_FIFO_SIZE", "group__keyscan_queue.html#gga2ee507fea44084259042ff7bb45ecabfa28ef28c4eb7c16aca74f3b3e139a1879", null ]
    ] ],
    [ "ksq_isEmpty", "group__keyscan_queue.html#ga80211eaef24176654c2c8987ecb4cdf9", null ],
    [ "wiced_hal_keyscan_events_pending", "group__keyscan_queue.html#ga9bc21fd697240669295621a8e794e3c9", null ],
    [ "wiced_hal_keyscan_flush_HW_events", "group__keyscan_queue.html#ga2a5aecb0cc03d6a0b697788d48caf427", null ],
    [ "wiced_hal_keyscan_get_next_event", "group__keyscan_queue.html#ga6033a5527d20ba7310fef232caa48e9b", null ],
    [ "EVENT_NONE", "group__keyscan.html#gga2f1398dba5e4a5616b83437528bdb28ea939a6cc6d749a232a23ff324dca6d48c", null ]
];