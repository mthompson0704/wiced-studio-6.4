/*
 * Copyright 2016-2021, PumpSpy
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of PumpSpy.
 */

#include <stdlib.h>
#include "wiced.h"
#include "http_wiced.h"
#include "string.h"
#include "math.h"
//#include "wwd_debug.h"
#include "wiced_framework.h"
//#include "wiced_ota_server.h"
//#include "default_wifi_config_dct.h"
//#include "network_config_dct.h"
#include "platform.h"
//#include "aes.h"
#include "waf_platform.h"
//#include "wiced_management.h"

#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "get_deviceid.h"
#include "control_leds.h"
#include "config_button_handler.h"
#include "restful_methods.h"
#include "new_firmware.h"
#include "dns_lookup.h"
#include "initialization.h"
#include "timers.h"
#include "access_points.h"
//#include "set_bor.h"
#include "isI2c/isI2C.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

//#define STOP_APP_HERE
//#define NO_LEDS
//#define ENABLE_SYSTEM_MONITOR
//#define TEST_AES_128
//#define REMOTEDEBUG

// 
// firmware version
// 
#define FIRMWARE_VERSION 5.53
#define FIRMWARE_VERSION_STRING "5.53 - BBS 2 - 4343 "
float firmware_version = 5.53;
//
// LED control parameters
//
#define LED_DRIVER  0xC0         //address of TLC59116 LED DRIVER
#define MODE1       0x00         //Mode 1 register address
#define MODE2       0x01         //Mode 2 register address
#define PWM0        0x02         //PWM0 Brightness control LED0
#define PWM2        0x04         //PWM2 Brightness control LED2
#define PWM3        0x05         //PWM3 Brightness control LED3
#define PWM4        0x06         //PWM4 Brightness control LED4
#define PWM5        0x07         //PWM5 Brightness control LED5
#define PWM6        0x08         //PWM6 Brightness control LED6
#define PWM7        0x09         //PWM7 Brightness control LED7
#define PWM8        0x0A         //PWM8 Brightness control LED8
#define PWM9        0x0B         //PWM9 Brightness control LED9
#define PWM10       0x0C         //PWM10 Brightness control LED10
#define PWM11       0x0D         //PWM11 Brightness control LED11
#define PWM12       0x0E         //PWM12 Brightness control LED12
#define PWM13       0x0F         //PWM13 Brightness control LED13
#define PWM15       0x11         //PWM15 Buzzer output
#define GRPPWM      0x12         //Group duty cycle control register
#define LEDOUT0     0x14         //LED output state 0
#define LEDOUT1     0x15         //LED output state 1
#define LEDOUT2     0x16         //LED output state 2
#define LEDOUT3     0x17         //LEd output state 3
#define TWI_TX      0x84         //write data to TWI
#define MODE1REGSET 0x00         //Set MODE1 reg values
#define MODE2REGSET 0x80         //Set MODE2 reg values
#define OFF         0x00         //SET LED Brightness to zero
#define ON          0xFF         //Set full ON
#define BRIGHTSET   0x28 //0x5F         //SET Brightness of LEDs
#define YELLOW1     0x3F         //SET Color YELLOW green
#define YELLOW2     0x2F         //SET Color YELLOW red
#define BUZZER      0xFF         //Set buzzer to full volume.
#define GRPPWMSET   0xAF         //SET Group Duty Cycle
#define LEDSETON    0XAA         //SET LED output to full ON
#define LEDSETOFF   0x00         //SET LED output to full OFF
#define NUM_I2C_MESSAGE_RETRIES     3
#define DC_PUMP_CYCLE_TIME          6
#define TIMER_TIME (1000)       //Approximately 1 Sec. Adjust to calibrate
#define OV_DELAY 3              //3 second Delay for overvoltage event

#define WICED_I2C_START_FLAG                    (1U << 0)
#define WICED_I2C_REPEATED_START_FLAG           (1U << 1)
#define WICED_I2C_STOP_FLAG                     (1U << 2)

uint8_t current_tlc59116_registers[50] = {0};
uint8_t new_tlc59116_registers[50] = {0};
uint64_t set_led_timer = 0;
/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/** Runs device configuration (if required)
 *
 * @param[in] config  : an array of user configurable variables in configuration_entry_t format.
 *                      The array must be terminated with a "null" entry {0,0,0,0}
 *
 * @return    WICED_SUCCESS
 */
extern wiced_result_t pumpspy_configure_device( const configuration_entry_t* config );

extern void platform_init_charger();

wiced_bool_t set_i2c_tlc59116(wiced_bool_t force_set);


/******************************************************
 *               Static Variable Definitions
 ******************************************************/
//
// serial port control
//
#define RX_BUFFER_SIZE 250
#define TEST_STR "."

wiced_uart_config_t uart_config =
{
    .baud_rate    = 115200,
    .data_width   = DATA_WIDTH_8BIT,
    .parity       = NO_PARITY,
    .stop_bits    = STOP_BITS_1,
    .flow_control = FLOW_CONTROL_DISABLED,
};

wiced_ring_buffer_t rx_buffer;
uint8_t rx_data[RX_BUFFER_SIZE];
char json_input[RX_BUFFER_SIZE];


//
// ac sense
//
#define AC_POWER_OUT_LEVEL 1500 //2450
uint16_t ac_sense_level = 0;
wiced_bool_t ac_power_on = WICED_TRUE;

//
// ac pump motor
//
#define AC_MOTOR 1
#define AC_PUMP_CURRENT_RUN_LIMIT 2000
#define AC_PUMP_UNKNOWN          0x0000
#define AC_PUMP_IDLE             0x0001
#define AC_PUMP_RUNNING          0x0002
#define AC_PUMP_RUN_TOO_LONG     0x0004
#define AC_PUMP_OVER_CURRENT     0x0008
#define AC_PUMP_FAILURE          0x0010
uint16_t ac_pump_state = AC_PUMP_IDLE;
uint16_t ac_pump_current = 0;
uint32_t motor_run_timeout = 300000; //	"1" : 3000, --> "motor_run_timeout" (V002)
uint16_t primary_pump_current_limit = 15000; //	"10" : 15000 --> primary_pump_current_limit
//
// dc pump & green float
//
#define DC_MOTOR 0
#define DC_MOTOR_SELF_TEST 2
#define DC_PUMP_CURRENT_RUN_LIMIT 500
#define DC_PUMP_UNKNOWN          0x0000
#define DC_PUMP_IDLE             0x0001
#define DC_PUMP_RUNNING          0x0002
#define DC_PUMP_RUN_TOO_LONG     0x0004
#define DC_PUMP_OVER_CURRENT     0x0008
#define DC_PUMP_COOL_DOWN        0x0010
#define DC_PUMP_SELF_TEST		 0x0020
#define DC_PUMP_FAILURE          0x0040
#define DC_PUMP_BATTERY_TOO_LOW  0x0080
uint16_t dc_pump_state = DC_PUMP_IDLE;
uint32_t dc_motor_run_timeout = 300000;
uint16_t dc_pump_current = 0;
uint16_t dc_pump_current_raw = 0;
uint16_t battery_measure_delay = 150; //	"5" : 150, --> "battery_measure_delay" (V002)
uint16_t motor_current_limit = 20000; //	"9" : 20000 --> motor_current_limit in milliamps, ie 15000 = 15 amps/15000 milliamps.
wiced_bool_t g_float_current = WICED_TRUE;
wiced_bool_t g_float_last = WICED_TRUE;
wiced_bool_t g_float_falling = WICED_FALSE;
wiced_bool_t g_float_rising = WICED_FALSE;
uint64_t dc_pump_start_time = 0;
uint64_t dc_pump_end_time = 1;
uint64_t dc_pump_running_time;
//
// high water - red float
//
wiced_bool_t r_float_current = WICED_TRUE;
wiced_bool_t r_float_last = WICED_TRUE;
wiced_bool_t r_float_falling = WICED_FALSE;
wiced_bool_t r_float_rising = WICED_FALSE;
//wiced_bool_t high_water_detected = WICED_FALSE; // declared in globals.c
//
// battery sensing
//
#define CHARGER_CUTTOFF_LEVEL    4000
#define VBATT_CHARGING_OFF_LEVEL 13400
#define BATTERY_VOLTAGE_UNKNOWN  0x0000
#define BATTERY_VOLTAGE_OK       0x0001
#define BATTERY_VOLTAGE_LOW      0x0002
#define BATTERY_VOLTAGE_CRITICAL 0x0004
#define BATTERY_VOLTAGE_PUMP_OFF 0x0008
#define BATTERY_NOT_DETECTED     0x0010
#define BATTERY_VOLTAGE_RISING   0x0020
uint16_t last_battery_voltage_unloaded = 0;
uint16_t battery_state = BATTERY_VOLTAGE_OK;
uint16_t battery_voltage_unloaded = 13550;
uint16_t battery_voltage_loaded = 0;
uint16_t battery_level_good = 13550;
uint16_t battery_level_ok = 12500; //	"2" : 12500, --> "battery_level_ok" 
uint16_t battery_level_low = 11000; //	"3" : 11000, --> "battery_level_low" 
uint16_t battery_level_critical = 10000; //	"4" : 10000, --> "battery_level_critical" 
uint16_t pump_off_voltage = 7000; //	“6” : 7000, --> “pump off voltage” (V002)
uint16_t pump_resume_voltage = 10000; //	“7” : 10000 --> “pump resume voltage” (V002)
wiced_bool_t vBatt_rising = WICED_FALSE;
//
// alarm mute button
//
#define ALARM_MUTE_TIME_LIMIT 43200000
wiced_bool_t alarm_muted = WICED_FALSE;
wiced_bool_t system_fault_in_process =  WICED_FALSE;
uint64_t alarm_muted_timer = 0;
wiced_bool_t alarm_mute_current = WICED_FALSE;
wiced_bool_t alarm_mute_last = WICED_TRUE;
wiced_bool_t alarm_mute_falling = WICED_FALSE;
wiced_bool_t alarm_mute_rising = WICED_FALSE;

//
// self-test timer
//
uint64_t self_test_timer;
uint16_t hours_between_self_tests = 48; //	"8" : 48 --> hours between self tests
//
// battery charger
//
uint64_t vBatt_dwell_timer = 0;
wiced_bool_t charger_enabled = WICED_FALSE;
uint16_t charger_on_time = 55000;
uint16_t charger_off_time = 5000;
//
// wi-fi network
//
wiced_bool_t wifi_enabled = WICED_FALSE;
//
// json defines
//
#define JSON_post_bbs_json \
    "{\"deviceid\": %ld%ld, " \
    "\"utcunixtime\": %ld000," \
    "\"json\": \"%s\" }"

#define JSON_post_pump_run \
	"{\\\"motor\\\": %d, \\\"time\\\": %d ,\\\"mamp\\\": %d, \\\"battery_voltage\\\": %d, \\\"loaded\\\": %d}" 

#define JSON_post_ac_power \
	"{\\\"ac_power\\\": %d}" 

#define JSON_post_high_water \
	"{\\\"high_water\\\": %d}" 

#define JSON_post_motor_hc \
	"{\\\"motor_hc\\\": %d}" 

#define JSON_post_dc_highamps \
	"{\\\"dc_highamps\\\": %d}" 

#define JSON_post_motor_fail \
	"{\\\"motor_fail\\\": %d}" 

//ToDo
#define JSON_post_dc_norecord \
	"{\\\"dc_norecord\\\": %d}" 

//ToDo
#define JSON_post_dc_highamps \
	"{\\\"dc_highamps\\\": %d}" 

//?? - handled already at server
#define JSON_post_dc_runtime \
	"{\\\"dc_runtime\\\": %d}" 

//ToDo
#define JSON_post_nobatt \
	"{\\\"no_batt\\\": %d}" 

//??
#define JSON_post_revpol \
	"{\\\"revpol\\\": %d}" 

#define GET_bbs_parameters \
    "GET /bbs_parameters/%ld%ld HTTP/1.1\r\n" \
    "Host: %s:%d \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

/******************************************************
 *               Function Definitions
 ******************************************************/

wiced_bool_t check_main_pump_baseline_initialization(uint16_t current)
{
	//
	// check if the pump has run 16 times
	// 8 ignores + 8 we count
	// ignore amps < AC_PUMP_CURRENT_RUN_LIMIT
	//
	if (current < AC_PUMP_CURRENT_RUN_LIMIT)
		return WICED_FALSE;
	//
	// set the index
	//
	dct.main_pump_init_index++;
	if (dct.main_pump_init_index > 15)
	{
		//
		// don't do anything
		//
		dct.main_pump_init_index = 15;
	}
	//
	// if runs 8 - 15
	//
	else if ((dct.main_pump_init_index > 7) && (dct.main_pump_init_index <= 15 ))
	{
		dct.main_pump_baseline_array[dct.main_pump_init_index-8] = current;
		if (dct.main_pump_init_index >= 15)
		{
			uint32_t average_current = 0;
			for (int i=0; i<8; i++)
				average_current += dct.main_pump_baseline_array[i];
			average_current /= 8; 
			dct.default_primary_pump_current_limit = 1.5*average_current;
			write_app_dct();
		}
	}
	//
	// runs 0 - 7
	// otherwise write the new baseline index to flash
	//
	else
	{
		write_app_dct();
	}
	WPRINT_APP_INFO(("check_main_pump_baseline\r\n"));
	WPRINT_APP_INFO(("check_main_pump_baseline dct.main_pump_init_index %d\r\n", dct.main_pump_init_index));
	for (int j=0; j<8; j++)
	{
		WPRINT_APP_INFO(("check_main_pump_baseline dct.main_pump_init_array[%d] %d\r\n", j, dct.main_pump_baseline_array[j]));
	}
	WPRINT_APP_INFO(("check_main_pump_baseline dct.defaul_primary_pump_current_limit %d\r\n", dct.default_primary_pump_current_limit));
	return WICED_TRUE;
}

wiced_bool_t check_backup_pump_baseline_initialization(uint16_t current)
{
	//
	// check if the pump has run 16 times
	// 8 ignores + 8 we count
	// has to be > DC_PUMP_CURRENT_RUN_LIMIT
	//
	if (current < DC_PUMP_CURRENT_RUN_LIMIT)
		return WICED_FALSE;
	//
	// set the index
	//
	dct.backup_pump_init_index++;
	if (dct.backup_pump_init_index > 15)
	{
		//
		// don't do anything
		//
		dct.backup_pump_init_index = 15;
	}
	//
	// if runs 8 - 15
	//
	else if ((dct.backup_pump_init_index > 7) && (dct.backup_pump_init_index <= 15 ))
	{
		dct.backup_pump_baseline_array[dct.backup_pump_init_index-8] = current;
		if (dct.backup_pump_init_index >= 15)
		{
			uint32_t average_current = 0;
			for (int i=0; i<8; i++)
				average_current += dct.backup_pump_baseline_array[i];
			average_current /= 8; 
			dct.default_motor_current_limit = 1.5*average_current;
			write_app_dct();
		}
	}
	//
	// runs 0 - 7
	// otherwise write the new baseline index to flash
	//
	else
	{
		write_app_dct();
	}
	WPRINT_APP_INFO(("check_backup_pump_baseline\r\n"));
	WPRINT_APP_INFO(("check_backup_pump_baseline dct.backup_pump_init_index %d\r\n", dct.backup_pump_init_index));
	for (int j=0; j<8; j++)
	{
		WPRINT_APP_INFO(("check_backup_pump_baseline dct.backup_pump_init_array[%d] %d\r\n", j, dct.backup_pump_baseline_array[j]));
	}
	WPRINT_APP_INFO(("check_backup_pump_baseline dct.default_motor_current_limit %d\r\n", dct.default_motor_current_limit));

	return WICED_TRUE;
}

wiced_result_t get_parameters()
{
	//
	// *TODO 2
	// set up the base current parameters based on the 1st 10 runs 
	//
    char *p;
    char *pos;
	int i;
/*	
	PARAMETERS 
	{
	"1" : 3000, --> "motor_run_timeout" (V002)
	"2" : 12500, --> "battery_level_ok" 
	"3" : 11000, --> "battery_level_low" 
	"4" : 10000, --> "battery_level_critical" 
	"5" : 15, --> "battery_measure_delay" (V002)
	“6” : 7000, --> “pump off voltage” (V002)
	“7” : 10000 --> “pump resume voltage” (V002)
	"8" : 48 --> hours between self tests
	"9" : 200000 --> motor_current_limit in milliamps, ie 15000 = 15 amps/15000 milliamps.
	"10" : 15000 --> primary_pump_current_limit
	}

	{"p1":3000,"p2":11750,"p3":11750,"p4":11250,"p5":15,"p6":7000,"p7":10000,"p8":48,"p9":20000,"p10":15000}

*/
    clear_post_buffers();
    WPRINT_APP_INFO(("GET /bbs_parameters \r\n"));
    sprintf(post, GET_bbs_parameters, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
		{
        	return WICED_ERROR;
		}
		//
		//    scan buffer for "p1" motor_run_timeout 
		//
		pos = strstr((char*)buffer, "\"p1\":");
//		WPRINT_APP_INFO( ( "p1 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_run_timeout = i*100;
		}
		else
		{
			motor_run_timeout = 300000;
		}
		//
		//    scan buffer for "p2" battery_level_ok 
		//
		pos = strstr((char*)buffer, "\"p2\":");
//		WPRINT_APP_INFO( ( "p2 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_ok = i;
		}
		else
		{
			battery_level_ok = 11750;
		}
		//
		//    scan buffer for "p3" battery_level_low 
		//
		pos = strstr((char*)buffer, "\"p3\":");
//		WPRINT_APP_INFO( ( "p3 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_low = i;
		}
		else
		{
			battery_level_low = 11750;
		}
		//
		//    scan buffer for "p4" battery_level_critical
		//
		pos = strstr((char*)buffer, "\"p4\":");
//		WPRINT_APP_INFO( ( "p4 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_critical = i;
		}
		else
		{
			battery_level_critical = 11250;
		}
		//
		//    scan buffer for "p5" battery_measure_delay
		//
		pos = strstr((char*)buffer, "\"p5\":");
//		WPRINT_APP_INFO( ( "p5 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_measure_delay = i*1000;
		}
		else
		{
			battery_measure_delay = 15000;
		}
		//
		//    scan buffer for "p6" pump_off_voltage
		//
		pos = strstr((char*)buffer, "\"p6\":");
//		WPRINT_APP_INFO( ( "p6 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			pump_off_voltage = i;
		}
		else
		{
			pump_off_voltage = 7000;
		}
		//
		//    scan buffer for "p7" pump_resume_voltage
		//
		pos = strstr((char*)buffer, "\"p7\":");
//		WPRINT_APP_INFO( ( "p7 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			pump_resume_voltage = i;
		}
		else
		{
			pump_resume_voltage = 10000;
		}
		//
		//    scan buffer for "p8" hours_between_self_tests
		//
		pos = strstr((char*)buffer, "\"p8\":");
//		WPRINT_APP_INFO( ( "p8 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			hours_between_self_tests = i;
		}
		else
		{
			hours_between_self_tests = 48;
		}
		//
		//    scan buffer for "p9" motor_current_limit - dc/backup pump
		//
		pos = strstr((char*)buffer, "\"p9\":");
//		WPRINT_APP_INFO( ( "p9 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			//
			// if baseline is calculated and i == 20000 (default /bbs_parameters = 20000
			//    (if we want to over ride, p9 must != 20000 so 20001 would work - backward compatible )
			// then use baseline
			// if i != 20000 then use baseline
			// else use default 20000 
			//
			if (i != 20000)
			{
				motor_current_limit = i;
			}
			else if (dct.backup_pump_init_index >= 15)
			{
				motor_current_limit = dct.default_motor_current_limit;
			}
			else
			{
				motor_current_limit = 20000;
			}
		}
		else
		{
			motor_current_limit = 20000;
		}
		//
		//    scan buffer for "p10" primary_pump_current_limit - ac
		//
		pos = strstr((char*)buffer, "\"p10\":");
//		WPRINT_APP_INFO( ( "p10 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 6;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			//
			// if baseline is calculated and i == 15000 (default /bbs_parameters = 15000
			//    (if we want to over ride, p10 must != 15000 so 15001 would work - backward compatible )
			// then use baseline
			// if i != 15000 then use baseline
			// else use default 15000 
			//
			if (i != 15000)
			{
				primary_pump_current_limit = i;
			}
			else if (dct.main_pump_init_index >= 15)
			{
				primary_pump_current_limit = dct.default_primary_pump_current_limit;
			}
			else
			{
				primary_pump_current_limit = 15000;
			}
		}
		else
		{
			primary_pump_current_limit = 15000;
		}
    }
	WPRINT_APP_INFO( ( "p1:  motor_run_timeout          |%d| \r\n", (int)motor_run_timeout ) );
	WPRINT_APP_INFO( ( "p2:  battery_level_ok           |%d| \r\n", (int)battery_level_ok ) );
	WPRINT_APP_INFO( ( "p3:  battery_level_low          |%d| \r\n", (int)battery_level_low ) );
	WPRINT_APP_INFO( ( "p4:  battery_level_critical     |%d| \r\n", (int)battery_level_critical ) );
	WPRINT_APP_INFO( ( "p5:  battery_measure_delay      |%d| \r\n", (int)battery_measure_delay ) );
	WPRINT_APP_INFO( ( "p6:  pump_off_voltage           |%d| \r\n", (int)pump_off_voltage ) );
	WPRINT_APP_INFO( ( "p7:  pump_resume_voltage        |%d| \r\n", (int)pump_resume_voltage ) );
	WPRINT_APP_INFO( ( "p8:  hours_between_self_tests   |%d| \r\n", (int)hours_between_self_tests ) );
	WPRINT_APP_INFO( ( "p9:  motor_current_limit        |%d| \r\n", (int)motor_current_limit ) );
	WPRINT_APP_INFO( ( "p10: primary_pump_current_limit |%d| \r\n", (int)primary_pump_current_limit ) );
    //
    // return
    //
    return result;
}

uint16_t get_ac_pump_current()
{
	// rev2 bd curve
	// 0aac=85, (dry)4aac=820
	// AmpsAC=4.6*adc - 389
	//
	// rev3 board curve
	// y = 7.8586x
	//
	uint16_t adc_sample = 0;
	uint32_t ac_pump_current_return = 0;
	//
	// read ac_sense a2d
	// average 30 data points
	//
	result = wiced_adc_take_sample( AC_CURRENT_SENSE, &adc_sample );
//	ac_pump_current_return = (uint16_t)((4.6*adc_sample)-389);
	ac_pump_current_return = (uint16_t)(7.9*adc_sample);
	//
	// return the value
	//
	return (ac_pump_current_return);

}

uint16_t get_ac_sense_level()
{
	//
	uint16_t adc_sample = 0;
	//
	// read a2d
	//
	result = wiced_adc_take_sample( AC_SENSE, &adc_sample );
	//
	// return the value
	//
	return (adc_sample);

}


uint16_t get_dc_pump_current()
{
	// 
	// add curent sense conversion 
	// 0adc = 0, (dry)4adc=40
	// AmpsDC=62.5*adc
	//
	// change curve for rev 3
	// AmpsDC = 6.5*adc - 19
	//
	uint16_t dc_pump_current_raw = 0;
	uint32_t dc_pump_current_return = 0;
	//
	// read ac_sense a2d
	// average 30 data points
	//
	result = wiced_adc_take_sample( CURRENT_SENSE, &dc_pump_current_raw );
//	dc_pump_current_return = (uint16_t)(62.5*dc_pump_current_raw);
	dc_pump_current_return = (uint16_t)((6.5*dc_pump_current_raw) - 19);
	//
	// return the value
	//
	return (dc_pump_current_return);
}

uint16_t measure_v_batt()
{
	//
	// insert the real conversion
	// 0vdc = 805 13vdc=3415
	// vac = 5*adc - 4009
	//
	// rev3 board
	// mVoltsAC = 4.6*adc - 2535
	//
	// after build of 10 and test
	// 3.7*adc - 107
	//
	uint16_t raw_v_batt;
	uint16_t return_v_batt;
	//
	// measurev_sense
	//
	result = wiced_adc_take_sample( V_SENSE, &raw_v_batt );
	//
	// look up real value
	//
//	return_v_batt = (uint16_t)(5*raw_v_batt) - 4009;
//	return_v_batt = (uint16_t)((4.6*raw_v_batt) - 2535);
	return_v_batt = (uint16_t)((3.7*raw_v_batt) - 107);
	//
	// return real value
	//
	return(return_v_batt);

}

void post_bbs_json(uint64_t utcunixtime)
{
	clear_post_buffers();
	WPRINT_APP_INFO( ( "POST /bbs_json \r\n"));
	sprintf(json, JSON_post_bbs_json, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), (long unsigned int)(utcunixtime/1000), json_input);
    WPRINT_APP_INFO(("POST /bbs_json |%s| len=%d\r\n", json, strlen(json)));
	sprintf(post, POST_STRING, "/bbs_json", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
//	    WPRINT_APP_INFO(("sprintf done\r\n"));
//	    WPRINT_APP_INFO(( post )); //This result looks correct
	post_the_post();
}

void check_ac_pump_high_current()
{
	//
	// check for high current
	//
	WPRINT_APP_INFO(("comparing peak_current pump_peak_current %d primary_pump_current_limit %d\r\n", pump_peak_current, primary_pump_current_limit));
	//
	// if baseline calculated and a new event
	//
	if ((pump_peak_current > primary_pump_current_limit) && (dct.main_pump_init_index >= 15) && ((ac_pump_state & AC_PUMP_OVER_CURRENT) == 0))
	{
		WPRINT_APP_INFO(("pump current too high - posting data\r\n"));

		sprintf(json_input, JSON_post_motor_hc, 1);
		post_bbs_json(pump_end_time);

		ac_pump_state |= AC_PUMP_OVER_CURRENT;

	}
	else
	{
		ac_pump_state &= ~AC_PUMP_OVER_CURRENT;
	}
}

void monitor_ac_pump()
{
	//
	// get the ac current
	//
	ac_pump_current = get_ac_pump_current();
	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("\r\nac_pump_current %d ac_pump_state %04X\r\n", ac_pump_current, ac_pump_state));
	//
	// if ac pump idle or stopping  
	//
	if (ac_pump_current < AC_PUMP_CURRENT_RUN_LIMIT)
	{
//			WPRINT_APP_INFO(("pump idle\r\n"));

		if ((ac_pump_state & AC_PUMP_RUNNING) > 0)
//		if (pump_is_running == WICED_TRUE)
		{
			//
			// check if pump ran long enough
			//
			if ((pump_end_time - pump_start_time) >= 750)
			{
				WPRINT_APP_INFO(("pump ran %d time with %d current\r\n", (uint16_t)pump_end_time, pump_peak_current));
				//
				// check curent too high
				// 
				check_ac_pump_high_current();
				//
				// POST /bbs_json
				//
				// {\"motor\":1,\"time\":75,\"mamp\":7500,\"battery_voltage\":13550,\"loaded\":13087}" }

				sprintf(json_input, JSON_post_pump_run, AC_MOTOR, (unsigned int)(pump_end_time-pump_start_time)/100, pump_peak_current, (unsigned int) battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(pump_start_time);
				//
				// set the baseline 
				//
				check_main_pump_baseline_initialization(pump_peak_current);
			}
			ac_pump_state &= ~AC_PUMP_RUNNING; 
			ac_pump_state &= ~AC_PUMP_RUN_TOO_LONG;
			ac_pump_state |= AC_PUMP_IDLE;
		}
	}
	else
	{

		if ((ac_pump_state & AC_PUMP_RUNNING) == 0)
//		if (pump_is_running == WICED_FALSE)
		{
			result = wiced_time_get_utc_time_ms  (&pump_start_time);
//			    WPRINT_APP_INFO( ( "get start time result %d\r\n", result ) );
			pump_end_time = pump_start_time;
			pump_peak_current = 0;
//			pump_is_running = WICED_TRUE;
			ac_pump_state &= ~AC_PUMP_IDLE;
			ac_pump_state &= ~AC_PUMP_FAILURE;
			ac_pump_state |= AC_PUMP_RUNNING;
			WPRINT_APP_INFO(("pump started %d time with %d current %d\r\n", (uint16_t)pump_start_time, ac_pump_current, ac_pump_state));
		}
		else
		{
			//
			// measure run time and current
			//
			result = wiced_time_get_utc_time_ms  (&pump_end_time);
//			    WPRINT_APP_INFO( ( "get end time result %d\r\n", result ) );
			ac_pump_current = get_ac_pump_current();
			//
			// set peak current after 2 seconds
			//

			if ((pump_end_time - pump_start_time) >= 2000)
			{
				if (pump_peak_current < ac_pump_current)
					pump_peak_current = ac_pump_current;
			}
			else
			{
				pump_peak_current = 0;
			}
			if ((server_time % 1000) == 0)
				WPRINT_APP_INFO(("AC Pump Running, %lu, ac_pump_current %d pump_peak_current %d ac_pump_state %d\r\n", (unsigned long)pump_end_time, ac_pump_current, pump_peak_current, ac_pump_state ));
			//
			// check for running too long
			//
			if ((pump_end_time - pump_start_time) > motor_run_timeout)
			{
				WPRINT_APP_INFO(("pump running too long - posting data\r\n"));
				//
				// check curent too high
				// 
				check_ac_pump_high_current();
				//
				// POST /pump_outlet_cycles
				//
				sprintf(json_input, JSON_post_pump_run, AC_MOTOR, (unsigned int)(pump_end_time - pump_start_time)/100, pump_peak_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(pump_start_time);
//				post_pump_cycles(pump_end_time, (pump_end_time - pump_start_time), pump_peak_current);
				pump_start_time = pump_end_time;
				ac_pump_state = ac_pump_state | AC_PUMP_RUN_TOO_LONG;
				//
				// set the baseline 
				//
				check_main_pump_baseline_initialization(pump_peak_current);

			}
		}
	}
}

void turn_on_dc_pump(void)
{
	//
	// check if ac power on
	//
	if ((ac_power_on == WICED_TRUE) && ((dc_pump_state & DC_PUMP_SELF_TEST) == 0))
	{
		sprintf(json_input, JSON_post_motor_fail, 1);
		post_bbs_json(dc_pump_start_time);
		ac_pump_state |= AC_PUMP_FAILURE;
	}
	//
	// set flags
	//
	if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
		WPRINT_APP_INFO(("turning on dc pump self test\r\n"));
	else
		WPRINT_APP_INFO(("turning on dc pump green float\r\n"));
	dc_pump_state |= DC_PUMP_RUNNING;
	dc_pump_state &= ~DC_PUMP_IDLE;
	dc_pump_state &= ~DC_PUMP_BATTERY_TOO_LOW;
	g_float_falling = WICED_FALSE;
	result = wiced_time_get_utc_time_ms  (&dc_pump_start_time);
	dc_pump_running_time = dc_pump_start_time;
	//
	// turn on the pump 
	//
	wiced_gpio_output_high(RELAY);
}

void check_dc_pump_current()
{
	uint64_t current_error_time;
	wiced_time_get_utc_time_ms  (&current_error_time);

	//
	// pump over current
	// if the baseline is calculated and it's not over current already
	//
	if ((dc_pump_current > motor_current_limit) && (dct.backup_pump_init_index >= 15) && ((dc_pump_state & DC_PUMP_OVER_CURRENT) == 0))
	{
		WPRINT_APP_INFO(("dcpump current over limit  - posting data\r\n"));

		sprintf(json_input, JSON_post_dc_highamps, 1);
		WPRINT_APP_INFO(("check_dc_pump_current dc_highamps |%s|\r\n", json_input));
		post_bbs_json(current_error_time);
		dc_pump_state |= DC_PUMP_OVER_CURRENT;
	}
	else if ((dc_pump_current <= motor_current_limit) && (dct.backup_pump_init_index >= 15) && ((dc_pump_state & DC_PUMP_OVER_CURRENT) > 0))
	{
		dc_pump_state &= ~DC_PUMP_OVER_CURRENT;
	}
	//
	// pump not connected 
	//
	if ((dc_pump_current < DC_PUMP_CURRENT_RUN_LIMIT) && ((dc_pump_state & DC_PUMP_FAILURE) == 0))
	{
		WPRINT_APP_INFO(("dcpump current failure - posting data %d state %d \r\n", dc_pump_current, dc_pump_state));

		sprintf(json_input, JSON_post_dc_norecord, 1);
		WPRINT_APP_INFO(("check_dc_pump_current dc_norecord |%s|\r\n", json_input));
		post_bbs_json(current_error_time);
		dc_pump_state |= DC_PUMP_FAILURE;
	}
	else if ((dc_pump_current >= DC_PUMP_CURRENT_RUN_LIMIT) && ((dc_pump_state & DC_PUMP_FAILURE) > 0))
	{
		WPRINT_APP_INFO(("dcpump current ok - posting data %d state %d \r\n", dc_pump_current, dc_pump_state));
		dc_pump_state &= ~DC_PUMP_FAILURE;
	}

}

int monitor_dc_pump()
{
	//
	//
	uint8_t motor = 0;
//	wiced_bool_t b;
	//
	// green float
	//
    g_float_current = wiced_gpio_input_get( G_FLOAT ); //? WICED_FALSE : WICED_TRUE;
	if ((g_float_current == WICED_FALSE) && (g_float_last == WICED_TRUE))
		g_float_falling = WICED_TRUE;
	else if ((g_float_current == WICED_TRUE) && (g_float_last == WICED_FALSE))
		g_float_rising = WICED_TRUE;
	g_float_last = g_float_current;
	//
	// if falling and !rising 
	// turn on motor
	//
	if ((g_float_falling == WICED_TRUE) && (g_float_rising == WICED_FALSE))
	{
		//
		// some temporary filtering
		//
		turn_on_dc_pump();
	}
	//
	// if !rising and falling
	//
	else if ((g_float_falling == WICED_FALSE) && (g_float_rising == WICED_TRUE))
	{
		WPRINT_APP_INFO(("g_float_rising \r\n"));
		g_float_rising = WICED_FALSE;
	}
	//
	// if the green or red switch is engaged (pump under water)
	// if the flag is set
	// if loaded vbat > resume voltage  
	// start the pump again 	
	//
	else if ( 
			 ( ((g_float_current == WICED_FALSE) && (g_float_last == WICED_FALSE)) || ((r_float_current == WICED_FALSE) && (r_float_last == WICED_FALSE)) )
			  && ((dc_pump_state & DC_PUMP_BATTERY_TOO_LOW) > 0)
			)
	//
	// look at loaded voltage
	//
	{
		battery_voltage_loaded = measure_v_batt();
		if (battery_voltage_loaded >= pump_resume_voltage)
		{
			turn_on_dc_pump();
		}
	} 
	//
	// otherwise check running 
	//
	else 
	{
		//
		// check if the dc motor is running
		//
		if ((dc_pump_state & DC_PUMP_RUNNING) > 0)
		{
			//
			// get the float state - check if it's still engaged
			//
			if ((wiced_gpio_input_get( G_FLOAT ) == WICED_FALSE) || (wiced_gpio_input_get( R_FLOAT ) == WICED_FALSE))
			{
				result = wiced_time_get_utc_time_ms  (&dc_pump_running_time);
			}
			//
			// check how long pump has run
			//
			result = wiced_time_get_utc_time_ms  (&dc_pump_end_time);
			//
			// current
			//
			dc_pump_current = get_dc_pump_current();
			//
			// check high current
			//
			if ((dc_pump_end_time - dc_pump_start_time) > 1000)
				check_dc_pump_current();
			//
			// loaded v_bat - moved to monitor_vbatt logic
			//
			battery_voltage_loaded = measure_v_batt();
			//
			// check for loaded voltage droop
			//
			if (battery_voltage_loaded < pump_off_voltage)
			{
				WPRINT_APP_DEBUG(("turning off dc pump because batt low\r\n"));
				//
				// turn of the pump 
				//
				wiced_gpio_output_low(RELAY);
				//
				// POST /bbs_jason & check flag 
				//
				if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
					motor = DC_MOTOR_SELF_TEST;
				else
					motor = DC_MOTOR;
				if (dc_pump_current > DC_PUMP_CURRENT_RUN_LIMIT)
				{
					sprintf(json_input, JSON_post_pump_run, motor, (unsigned int)(dc_pump_end_time - dc_pump_start_time)/100, (unsigned int)dc_pump_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
					post_bbs_json(dc_pump_start_time);
				}
				dc_pump_state &= ~DC_PUMP_RUNNING;
				dc_pump_state &= ~DC_PUMP_SELF_TEST;
				dc_pump_state &= ~DC_PUMP_RUN_TOO_LONG;
				dc_pump_state |= DC_PUMP_IDLE;
				dc_pump_state |= DC_PUMP_BATTERY_TOO_LOW;
				//
				// check and average the baseline
				//
				check_backup_pump_baseline_initialization(dc_pump_current);


			}

			//
			// pump too long
			//
			if ((dc_pump_running_time - dc_pump_start_time) > motor_run_timeout)
			{
				WPRINT_APP_DEBUG(("dc pump running too long \r\n"));
				//
				// POST /bbs_jason & check flag 
				//
				if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
					motor = DC_MOTOR_SELF_TEST;
				else
					motor = DC_MOTOR;
				if (dc_pump_current > DC_PUMP_CURRENT_RUN_LIMIT)
				{
					sprintf(json_input, JSON_post_pump_run, motor, (unsigned int)(motor_run_timeout)/100, (unsigned int)dc_pump_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
					post_bbs_json(dc_pump_start_time);
				}
				dc_pump_start_time = dc_pump_running_time;
				dc_pump_state |= DC_PUMP_RUN_TOO_LONG;
				//
				// check and average the baseline
				//
				check_backup_pump_baseline_initialization(dc_pump_current);

			}
			//
			// pump cycle complete
			//
			if ((dc_pump_end_time - dc_pump_running_time) > 10000)
			{
				WPRINT_APP_DEBUG(("turning off dc pump \r\n"));
				//
				// turn of the pump 
				//
				wiced_gpio_output_low(RELAY);
				//
				// POST /bbs_jason & check flag 
				//
				if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
					motor = DC_MOTOR_SELF_TEST;
				else
					motor = DC_MOTOR;
				if (dc_pump_current > DC_PUMP_CURRENT_RUN_LIMIT)
				{
					sprintf(json_input, JSON_post_pump_run, motor, (unsigned int)(dc_pump_end_time - dc_pump_start_time)/100, (unsigned int)dc_pump_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
					post_bbs_json(dc_pump_start_time);
				}
				dc_pump_state &= ~DC_PUMP_RUNNING;
				dc_pump_state &= ~DC_PUMP_SELF_TEST;
				dc_pump_state &= ~DC_PUMP_RUN_TOO_LONG;
				dc_pump_state |= DC_PUMP_IDLE;
				//
				// check and average the baseline
				//
				check_backup_pump_baseline_initialization(dc_pump_current);
			}
			if ((server_time % 1000) == 0)
				WPRINT_APP_INFO(("DC Pump Running, %lu, %d, %d\r\n", (unsigned long)dc_pump_end_time, dc_pump_current, dc_pump_state ));
		}
	}
	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("dc_pump_state %04X \r\n", dc_pump_state));
	return(0);
}

void monitor_alarm_mute(void)
{
    uint64_t local_timer;
    uint64_t mute_time;
	uint8_t noise_counter = 0;
	wiced_bool_t alarm_mute_pressed = WICED_FALSE;
	//
	// green float
	//
    alarm_mute_current = wiced_gpio_input_get( ALARM_MUTE ); //? WICED_FALSE : WICED_TRUE;
	if ((alarm_mute_current == WICED_FALSE) && (alarm_mute_last == WICED_TRUE))
		alarm_mute_falling = WICED_TRUE;
	else if ((alarm_mute_current == WICED_TRUE) && (alarm_mute_last == WICED_FALSE))
		alarm_mute_rising = WICED_TRUE;
	alarm_mute_last = alarm_mute_current;
	//
	// if falling and !rising 
	// button pressed 
	//
	if ((alarm_mute_falling == WICED_TRUE) && (alarm_mute_rising == WICED_FALSE))
	{
		alarm_mute_pressed = WICED_TRUE;
		alarm_mute_falling = WICED_FALSE;
	}
	//
	// if !rising and falling
	//
	else if ((alarm_mute_falling == WICED_FALSE) && (alarm_mute_rising == WICED_TRUE))
	{
		alarm_mute_pressed = WICED_FALSE;
		alarm_mute_rising = WICED_FALSE;
	}
	//
	// if it's pressed
	//
	if (alarm_mute_pressed == WICED_TRUE)
	{
		//
		// set alarm_muted flag
		//
		WPRINT_APP_INFO(("alarm_mute is pressed\r\n"));
		if (system_fault_in_process == WICED_TRUE)
		{
			if (alarm_muted == WICED_FALSE)
			{
				WPRINT_APP_INFO(("alarm_mute pressed and  system_fault setting alarm_muted \r\n"));
				alarm_muted = WICED_TRUE;
				wiced_time_get_utc_time_ms(&alarm_muted_timer);
				set_i2c_tlc59116(WICED_TRUE);
			}
			else
			{
				WPRINT_APP_INFO(("alarm_mute pressed and  system_fault UNsetting alarm_muted \r\n"));
				alarm_muted = WICED_FALSE;
				wiced_time_get_utc_time_ms(&alarm_muted_timer);
				set_i2c_tlc59116(WICED_TRUE);
			}
		}
		//
		// look for button pressed long 
		//
	//    config_mode = WICED_TRUE;
		alarm_mute_pressed = wiced_gpio_input_get( ALARM_MUTE ) ? WICED_FALSE : WICED_TRUE;
		if (alarm_mute_pressed == WICED_TRUE)
		{
			WPRINT_APP_INFO(("Alarm Mute is still Pressed\r\n"));
			//
			// measure button press time
			//
			wiced_time_get_utc_time_ms(&local_timer);
			while ((alarm_mute_pressed == WICED_TRUE) && ((dc_pump_state & DC_PUMP_SELF_TEST) == 0))
			{
	//	        WPRINT_APP_INFO(("Alarm Mute Pressed\r\n"));
				//
				// check how long
				//
				wiced_time_get_utc_time_ms(&mute_time);
				if ((mute_time - local_timer) >= 10000)
				{
					WPRINT_APP_INFO(("Running Test Mode\r\n"));
					if ((dc_pump_state & DC_PUMP_SELF_TEST) == 0)
					{
						dc_pump_state |= DC_PUMP_SELF_TEST;
						turn_on_dc_pump();
					}
					self_test_timer = local_timer;
					local_timer = mute_time;
					alarm_mute_pressed = WICED_FALSE;
				}
				//
				// debounce and see if it's still pressed
				//
				wiced_rtos_delay_milliseconds(100);
				alarm_mute_pressed = wiced_gpio_input_get( ALARM_MUTE ) ? WICED_FALSE : WICED_TRUE;
				if (alarm_mute_pressed == WICED_FALSE)
				{
					if (noise_counter++ > 3)
					{
						alarm_mute_pressed = WICED_FALSE;
					}
					else
					{
						alarm_mute_pressed = WICED_TRUE;
					}
									
				}
				else
				{
					noise_counter = 0;
				}
				
			}
			WPRINT_APP_INFO(("Alarm Mute Pressed is over \r\n"));
		}
	}
}


int monitor_high_water()
{
	//
	//
	uint64_t high_water_event_time = 0;
//	wiced_bool_t b;

    r_float_current = wiced_gpio_input_get( R_FLOAT ); // ? WICED_FALSE : WICED_TRUE;
//	if ((server_time % 1000) == 0)
//		WPRINT_APP_INFO(("r_float_current %d r_float_last %d r_float_falling %d r_float_rising %d\r\n", r_float_current, r_float_last, r_float_falling, r_float_rising ));

	if ((r_float_current == WICED_FALSE) && (r_float_last == WICED_TRUE))
		r_float_falling = WICED_TRUE;
	else if ((r_float_current == WICED_TRUE) && (r_float_last == WICED_FALSE))
		r_float_rising = WICED_TRUE;
	r_float_last = r_float_current;

	if ((r_float_falling == WICED_TRUE) && (r_float_rising == WICED_FALSE))
	{
		//
		// high water engaged
		//
		r_float_falling = WICED_FALSE;
		high_water_detected = WICED_TRUE;
		sprintf(json_input, JSON_post_high_water, 1);
		result = wiced_time_get_utc_time_ms  (&high_water_event_time);
		post_bbs_json(high_water_event_time);
		//
		// dc pump
		//
		if ((dc_pump_state & DC_PUMP_RUNNING ) == 0)
		{
			turn_on_dc_pump();
		}
	}
	else if ((r_float_falling == WICED_FALSE) && (r_float_rising == WICED_TRUE))
	{
		//
		// high water off
		//
		r_float_rising = WICED_FALSE;
		high_water_detected = WICED_FALSE;
		sprintf(json_input, JSON_post_high_water, 0);
		result = wiced_time_get_utc_time_ms  (&high_water_event_time);
		post_bbs_json(high_water_event_time);
	}
	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("high_water_detected %d \r\n", high_water_detected));
	return(0);
}


void turn_off_charger(void)
{
	charger_enabled = WICED_FALSE;
	wiced_gpio_output_low(CHARGER);
	vBatt_dwell_timer = server_time;

}

void turn_on_charger(void)
{
	charger_enabled = WICED_TRUE;
	wiced_gpio_output_high(CHARGER);
	vBatt_dwell_timer = server_time;
}

void monitor_ac_sense()
{
	uint64_t power_event_time = 0;

	ac_sense_level = get_ac_sense_level();
	if ((ac_sense_level < AC_POWER_OUT_LEVEL) && (ac_power_on == WICED_TRUE))
	{
		WPRINT_APP_INFO(("AC_POWER_OUT_LEVEL LOW ac_sense_level %d ac_power_on %d \r\n", ac_sense_level, ac_power_on));
		//
		// power out
		//
		ac_power_on = WICED_FALSE;
		sprintf(json_input, JSON_post_ac_power, 0);
		result = wiced_time_get_utc_time_ms  (&power_event_time);
		post_bbs_json(power_event_time);
		//
		// turn off the charger
		//
		turn_off_charger();
	}
	else if ((ac_sense_level > AC_POWER_OUT_LEVEL) && (ac_power_on == WICED_FALSE))
	{
		//
		// power on
		//
		ac_power_on = WICED_TRUE;
		sprintf(json_input, JSON_post_ac_power, 1);
		result = wiced_time_get_utc_time_ms(&power_event_time);
		post_bbs_json(power_event_time);
	}
	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("ac_sense_level %d ac_power_on %d \r\n", ac_sense_level, ac_power_on));

}

void monitor_v_batt()
{
	//
	// look up real value
	// do not measure vBatt for 10 seconds after pump run
	// do not measure vBatt during pump run - but do measure loaded vBatt
	// do not measure vBatt when charger is on
	// do not measure vBatt for 5 seconds after charger turned off
	// turn off charger with AC power failure
	// turn off charger when DC motor runs ?? (primary pump failure??)
	//
	wiced_bool_t ok_to_measure_vbatt = WICED_TRUE;
	if (vBatt_dwell_timer == 0)
		vBatt_dwell_timer = server_time;
	//
	// if the charger is on, turn it off for 5 seconds
	//
	if (charger_enabled == WICED_TRUE)
	{
		if ((server_time - vBatt_dwell_timer) > charger_on_time)
		{
			WPRINT_APP_INFO(("turning off CHARGER\r\n"));
			//
			// you can always turn off charger
			//
			turn_off_charger();
		}
	}
	else
	{
		if ((server_time - vBatt_dwell_timer) > charger_off_time)
		{
//			WPRINT_APP_INFO(("turning off charger\r\n"));
//			WPRINT_APP_INFO(("measuring vbatt\r\n"));
			//
			// if the DC pump is running, can't read unloaded voltage
			//
			if ((dc_pump_state & DC_PUMP_RUNNING) > 0)
			{
				ok_to_measure_vbatt = WICED_FALSE;
				//
				// measure loaded voltage
				// 
				battery_voltage_loaded = measure_v_batt();
//				WPRINT_APP_INFO(("measuring vbatt - dc pump running\r\n"));
			}
			if ((server_time - dc_pump_end_time) < battery_measure_delay)
			{
				ok_to_measure_vbatt = WICED_FALSE;
//				WPRINT_APP_INFO(("measuring vbatt - dc pump run delay \r\n"));
			}
			if (ok_to_measure_vbatt == WICED_TRUE)
			{
				last_battery_voltage_unloaded = battery_voltage_unloaded;
				battery_voltage_unloaded = measure_v_batt();
				//
				// make sure the power is on
				//
				if ((ac_power_on == WICED_TRUE) && (battery_voltage_unloaded > CHARGER_CUTTOFF_LEVEL))
					turn_on_charger();
			}
		}
	}
	//
	// **DEBUG vbatt
	/*
	battery_voltage_loaded = 12000;
	battery_voltage_unloaded = 11550;
	last_battery_voltage_unloaded = 11500;
	vBatt_rising = WICED_TRUE;
	*/

	//
	// set the flags, charger, pump_on 
	//
	//
	// check unloaded v_batt state
	//

	if (battery_voltage_unloaded >= VBATT_CHARGING_OFF_LEVEL) // > 13.4
	{
		//
		// charger "pulsing", regardless of voltage direction
		//
		charger_off_time = 5000;
		charger_on_time = 5000;
		battery_state |= BATTERY_VOLTAGE_OK;
		battery_state &= ~BATTERY_VOLTAGE_LOW;
		battery_state &= ~BATTERY_VOLTAGE_CRITICAL;
		vBatt_rising = WICED_TRUE;
		if ((server_time % 1000) == 0)
			WPRINT_APP_INFO(("BATTERY_VOLTAGE_GOOD(>13.5) battery_voltage_unloaded %d battery_voltage_loaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_voltage_loaded, battery_state));
	}
	else if ((battery_voltage_unloaded <= battery_level_good) && (battery_voltage_unloaded > battery_level_ok)) // > 12.5
	{
		//
		// chrger on 
		//
		charger_off_time = 5000;
		charger_on_time = 55000;
		battery_state |= BATTERY_VOLTAGE_OK;
		battery_state &= ~BATTERY_VOLTAGE_LOW;
		battery_state &= ~BATTERY_VOLTAGE_CRITICAL;
		vBatt_rising = WICED_TRUE;
		if ((server_time % 1000) == 0)
			WPRINT_APP_INFO(("BATTERY_VOLTAGE_GOOD(>12.5) battery_voltage_unloaded %d battery_voltage_loaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_voltage_loaded, battery_state));
	}
	else if ((battery_voltage_unloaded <= battery_level_ok) && (battery_voltage_unloaded > battery_level_low)) // > 11.0
	{
		//
		// chrger on 
		//
		charger_off_time = 5000;
		charger_on_time = 55000;
		battery_state |= BATTERY_VOLTAGE_OK;
		battery_state &= ~BATTERY_VOLTAGE_LOW;
		battery_state &= ~BATTERY_VOLTAGE_CRITICAL;
		vBatt_rising = WICED_TRUE;
		if ((server_time % 1000) == 0)
			WPRINT_APP_INFO(("BATTERY_VOLTAGE_OK battery_voltage_unloaded %d battery_voltage_loaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_voltage_loaded, battery_state));
	}
	else if ((battery_voltage_unloaded <= battery_level_low) && (battery_voltage_unloaded > battery_level_critical)) // > 10
	{
		charger_off_time = 5000;
		charger_on_time = 55000;
		battery_state &= ~BATTERY_VOLTAGE_OK;
		battery_state |= BATTERY_VOLTAGE_LOW;
		battery_state &= ~BATTERY_VOLTAGE_CRITICAL;

		vBatt_rising = WICED_TRUE;
		if ((server_time % 1000) == 0)
			WPRINT_APP_INFO(("BATTERY_VOLTAGE_LOW battery_voltage_unloaded %d battery_voltage_loaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_voltage_loaded, battery_state));
	}
	else if ((battery_voltage_unloaded <= battery_level_critical)) // < 10
	{
		charger_off_time = 5000;
		charger_on_time = 55000;
		battery_state &= ~BATTERY_VOLTAGE_OK;
		battery_state &= ~BATTERY_VOLTAGE_LOW;
		battery_state |= BATTERY_VOLTAGE_CRITICAL;
		vBatt_rising = WICED_TRUE;
		if ((server_time % 1000) == 0)
			WPRINT_APP_INFO(("BATTERY_VOLTAGE_CRITICAL battery_voltage_unloaded %d battery_voltage_loaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_voltage_loaded, battery_state));
	}

	//
	// see if it's connected at all
	//
	if (battery_voltage_unloaded <= CHARGER_CUTTOFF_LEVEL)
	{
		if ((battery_state & BATTERY_NOT_DETECTED) == 0)
		{
			battery_state |= BATTERY_NOT_DETECTED;
			sprintf(json_input, JSON_post_nobatt, 1);
			post_bbs_json(server_time);
		}
	}
	else
	{
		if ((battery_state & BATTERY_NOT_DETECTED) > 0)
		{
			battery_state &= ~BATTERY_NOT_DETECTED;
			sprintf(json_input, JSON_post_nobatt, 0);
			post_bbs_json(server_time);
		}
	}
	//
	// set pump run flags
	// 	
	if (battery_voltage_unloaded < pump_off_voltage)
	{
		battery_state |= BATTERY_VOLTAGE_PUMP_OFF;
	}
	else if (battery_voltage_unloaded >= pump_resume_voltage)
	{
		battery_state &= ~BATTERY_VOLTAGE_PUMP_OFF;
	}
	else
	{
		if (vBatt_rising == WICED_TRUE)
			battery_state |= BATTERY_VOLTAGE_PUMP_OFF;
		else
			battery_state &= ~BATTERY_VOLTAGE_PUMP_OFF;
	}

	if ((server_time % 1000) == 0)
	{
		WPRINT_APP_INFO(("battery_voltage_unloaded %d last_battery_voltage_unloaded %d vbatt_rising %d battery_voltage_loaded %d \r\n", battery_voltage_unloaded, last_battery_voltage_unloaded, vBatt_rising, battery_voltage_loaded));
		WPRINT_APP_INFO(("charger_on_time %d charger_off_time %d charger_enabled %d battery_state %04X\r\n", charger_on_time, charger_off_time, charger_enabled, battery_state ));
	}
}

void check_self_test()
{
	// 
	// run every self_test_timer number of hours
	//
	// check the timer
	//
	if ((server_time - self_test_timer) > (hours_between_self_tests*60*60*1000))
	{
		WPRINT_APP_INFO(("running self test @ server_time %ld%ld self_test_timer %ld%ld \r\n", (unsigned long int)(server_time/10000000), (unsigned long int)(server_time%10000000),(unsigned long int)(self_test_timer/10000000), (unsigned long int)(self_test_timer%10000000) ));
		//
		// turn on motor for 10 seconds
		// set self-test flag
		//
		dc_pump_state |= DC_PUMP_SELF_TEST;
		turn_on_dc_pump();
		self_test_timer = server_time;
	}

}



void reset_i2c_tlc59116(void)
{
	uint8_t buf[2] = {0};

	WPRINT_APP_INFO(("calling reset_i2c_tlc59116\r\n"));

	WPRINT_APP_INFO(("calling ism_i2c_init(true)\r\n"));
    result = ism_i2c_init(WICED_TRUE);
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init_reset error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init_reset success\r\n"));

	int i=0;
//	buf[i++] = 0xD6; the address
	buf[i++] = 0xA5;
	buf[i++] = 0x5A;
    result = ism_i2c_write(WICED_TRUE, buf, i );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c software reset write error  %d\r\n", result));
	else
		WPRINT_APP_INFO(("i2c software reset write success %d\r\n", result));

	WPRINT_APP_INFO(("calling ism_i2c_deinit(true)\r\n"));
    result = ism_i2c_deinit(WICED_TRUE);
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_deinit_reset error %d\r\n", result));
	else
		WPRINT_APP_INFO(("done ism_i2c_deinit_reset\r\n"));

	WPRINT_APP_INFO(("done reset_i2c_tlc59116\r\n"));
}

void read_i2c_tlc59116(void)
{
	uint32_t i = 1;
//	WPRINT_APP_INFO(("\r\ncalling read_i2c_tlc59116\r\n"));

	memset(current_tlc59116_registers, 0, 50);
	current_tlc59116_registers[0] = 0x80; //0x8B;
    result = ism_i2c_write(WICED_FALSE, current_tlc59116_registers, i );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("read ism_i2c_write error result %d\r\n", result));
//	else
//		WPRINT_APP_INFO(("read ism_i2c_write success result %d\r\n", result));

	i = 0x1E; // 31;
    result = ism_i2c_read( current_tlc59116_registers, i, 1000 );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("read ism_i2c_read error result %d\r\n", result));
//	else
//		WPRINT_APP_INFO(("read ism_i2c_read success result %d\r\n", result));
/*	for (i=0; i<0x1F;i++)
	{
		WPRINT_APP_INFO(("%02x ", current_tlc59116_registers[i]));
		if (i==15)
			WPRINT_APP_INFO(("\r\n"));
	}
	WPRINT_APP_INFO(("\r\n"));
*/
//	WPRINT_APP_INFO(("done read_i2c_tlc59116\r\n"));

}

wiced_bool_t check_i2c_tlc59116_mode1(void)
{
	uint32_t i = 1;
	wiced_bool_t bad_mode1_value = WICED_FALSE;
	uint8_t buf[5] = {0};

//	WPRINT_APP_INFO(("\r\ncalling check_i2c_tlc59116_mode1 \r\n"));

	buf[0] = 0x80; //0x8B;
    result = ism_i2c_write(WICED_FALSE, current_tlc59116_registers, i );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("check_i2c_tlc59116_mode1 write error result %d\r\n", result));
//	else
//		WPRINT_APP_INFO(("read ism_i2c_write success result %d\r\n", result));

	i = 2;
    result = ism_i2c_read(buf, i, 1000 );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("check_i2c_tlc59116_mode1 read error result %d\r\n", result));
//	else
//		WPRINT_APP_INFO(("read ism_i2c_read success result %d\r\n", result));

//	WPRINT_APP_INFO(("mode1 %02x %02x\r\n", buf[0], buf[1]));
	if ((buf[0] & 0x10) > 0 )
		bad_mode1_value = WICED_TRUE;

//	WPRINT_APP_INFO(("done check_i2c_tlc59116\r\n"));
	return (bad_mode1_value);
}

void init_i2c_tlc59116()
{

	uint32_t i = 0;

	WPRINT_APP_INFO(("\r\ncalling init_i2c_tlc59116\r\n"));

	WPRINT_APP_INFO(("calling ism_i2c_deinit\r\n"));
    result = ism_i2c_deinit(WICED_FALSE);
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_deinit error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_deinit success\r\n"));

	reset_i2c_tlc59116();

	WPRINT_APP_INFO(("calling ism_i2c_init\r\n"));
    result = ism_i2c_init(WICED_FALSE);
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));

	//
	// init tlc59116
	// 
	i = 0;
	current_tlc59116_registers[i++] = 0x80;       // control register = auto-inc all regs - addr 0
    current_tlc59116_registers[i++] = 0x80;       // MODE1 - turn on auto-inc
    current_tlc59116_registers[i++] = 0x80;       // MODE2 - clear error statys 
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM0 - n/c
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM1 - n/c
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM2 - 
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM3 -
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM4
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM5
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM6
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM7
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM8
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM9
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM10
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM11
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM12
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM13
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM14
	current_tlc59116_registers[i++] = LEDSETOFF;  // PWM15
	current_tlc59116_registers[i++] = GRPPWMSET;  // GRPPWM
	current_tlc59116_registers[i++] = 0x00;       // GRPFREQ
	current_tlc59116_registers[i++] = LEDSETON;   // LEDOUT0
	current_tlc59116_registers[i++] = LEDSETON;   // LEDOUT1
	current_tlc59116_registers[i++] = LEDSETON;   // LEDOUT2
	current_tlc59116_registers[i++] = LEDSETON;   // LEDOUT3
	//
	// write to i2c
	//
    result = ism_i2c_write(WICED_FALSE, current_tlc59116_registers, i );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("init ism_i2c_write error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("init ism_i2c_write success result %d\r\n", result));

	WPRINT_APP_INFO(("done calling init_i2c_tlc59116\r\n"));

}

void print_tlc59116_buffers(void)
{
	int i = 0;
	WPRINT_APP_INFO(("current_tlc59116_registers\r\n"));
	WPRINT_APP_INFO(("new_tlc59116_registers\r\n"));
	WPRINT_APP_INFO(("M1 M2 NC NC WF WF VB VB PW PW MP MP BP BP HW HW NC BZ \r\n"));
	WPRINT_APP_INFO(("            RR GG RR GG RR GG RR GG RR GG RR GG       \r\n"));
	for (i=0; i<0x1F;i++)
	{
		WPRINT_APP_INFO(("%02x ", current_tlc59116_registers[i]));
	}
	WPRINT_APP_INFO(("\r\n"));
	for (i=0; i<0x1F;i++)
	{
		WPRINT_APP_INFO(("%02x ", new_tlc59116_registers[i+1]));
	}
	WPRINT_APP_INFO(("\r\n"));
//	char c = getchar();
//	while (1) {}

}

wiced_bool_t set_i2c_tlc59116(wiced_bool_t force_set)
{
	//
	//
	// n/c         - 0x02
	// n/c         - 0x03
	// led1 - WiFi - 0x04 - PWM2  - Red
	// led1 - WiFi - 0x05 - PWM3  - Green 
	// led2 - batt - 0x06 - PWM4  - Red
	// led2 - batt - 0x07 - PWM5  - Green
	// led3 - pwr  - 0x08 - PWM6  - Red
	// led3 - pwr  - 0x09 - PWM7  - Green
	// led4 - mpmp - 0x0A - PWM8  - Red
	// led4 - mpmp - 0x0B - PWM9  - Green
	// led5 - bpmp - 0x0C - PWM10 - Red
	// led5 - bpmp - 0x0D - PWM11 - Green
	// led6 - hwtr - 0x0E - PWM12 - Red
	// led6 - hwtr - 0x0F - PWM13 - Green
	// n/c         - 0x10 - PWM14        
	// buzzer      - 0x11 - PWM15
 	// 

//	WPRINT_APP_INFO(("calling set_i2c_tlc59116\r\n"));

	//
	// check mode1 to see if the osc is turned off (==0x91)
	//
//	uint8_t mode1 = current_tlc59116_registers[0];
//	read_i2c_tlc59116();
/*	if (mode1 != current_tlc59116_registers[0])
	{
		WPRINT_APP_INFO(("set_i2c_tlc59116 detected wink out setting force_set to true\r\n"));
		force_set = WICED_TRUE;
	}
*/

	memset(new_tlc59116_registers, 0, sizeof(new_tlc59116_registers));
	system_fault_in_process = WICED_FALSE;

/*    result = ism_i2c_init();
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));
*/
	int i = 0;
	new_tlc59116_registers[i++] = 0x80;       // control register = auto-inc all regs - addr 0
    new_tlc59116_registers[i++] = 0x80;       // MODE1 - turn on auto-inc
    new_tlc59116_registers[i++] = 0x80;       // MODE2 - clear error statys 
	//
	// n/c
	//
	new_tlc59116_registers[i++] = LEDSETOFF;  // PWM0
	new_tlc59116_registers[i++] = LEDSETOFF;  // PWM1
	//
	// wi-fi led - PWM2 PWM3
	//
	switch (system_state)
	{
		case STATE_INITIALIZING:
			new_tlc59116_registers[i++] = BRIGHTSET;   // red
			new_tlc59116_registers[i++] = LEDSETOFF;  //
			break; 
		case STATE_RUNNING:
			if (wifi_error_condition == WICED_FALSE)
			{
				new_tlc59116_registers[i++] = LEDSETOFF;  // 
				new_tlc59116_registers[i++] = BRIGHTSET;   // green
			}
			else
			{
				new_tlc59116_registers[i++] = BRIGHTSET;   // red
				new_tlc59116_registers[i++] = LEDSETOFF;  //
			}
			break;
		case STATE_PROVISIONING:
			new_tlc59116_registers[i++] = BRIGHTSET;  // yellow
			new_tlc59116_registers[i++] = BRIGHTSET;  //
			break; 
		case STATE_TEST_MODE:
			new_tlc59116_registers[i++] = BRIGHTSET;  // 
			new_tlc59116_registers[i++] = LEDSETOFF;  // red
			break;
		default:
			new_tlc59116_registers[i++] = BRIGHTSET;  // yellow
			new_tlc59116_registers[i++] = BRIGHTSET;  //
			break; 
	}
	//
	// battery - PWM4 PWM5
	//
	if ((battery_state & BATTERY_VOLTAGE_OK) > 0)
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	else if (battery_state & BATTERY_VOLTAGE_LOW)
	{
		new_tlc59116_registers[i++] = BRIGHTSET; // yellow
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	else
	{
		new_tlc59116_registers[i++] = BRIGHTSET; // red
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		system_fault_in_process = WICED_TRUE;
	}
	
	//
	// power - PWM6 PWM7 
	//
	if (ac_power_on == WICED_TRUE)
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	else
	{
		new_tlc59116_registers[i++] = BRIGHTSET; // red
		new_tlc59116_registers[i++] = LEDSETOFF; // 
	}
	//
	// main pump - PWM8 PWM9
	//
	if (ac_pump_state == AC_PUMP_UNKNOWN)
	{
		new_tlc59116_registers[i++] = LEDSETOFF;  // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	else if (((ac_pump_state & AC_PUMP_RUN_TOO_LONG) > 0) || ((ac_pump_state & AC_PUMP_OVER_CURRENT) > 0) || ((ac_pump_state & AC_PUMP_FAILURE) > 0))
	{
		new_tlc59116_registers[i++] = BRIGHTSET;  // red
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		system_fault_in_process = WICED_TRUE;
	}
	else 
	{
		new_tlc59116_registers[i++] = LEDSETOFF;  // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	//
	// backup pump PWM10- PWM11 
	//
	if (dc_pump_state == DC_PUMP_UNKNOWN)
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // green
		new_tlc59116_registers[i++] = BRIGHTSET; //
	}
	else if (((dc_pump_state & DC_PUMP_RUN_TOO_LONG) > 0) || ((dc_pump_state & DC_PUMP_OVER_CURRENT) > 0) || ((dc_pump_state & DC_PUMP_FAILURE) > 0))
	{
		new_tlc59116_registers[i++] = BRIGHTSET; // red
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		system_fault_in_process = WICED_TRUE;
	}
	else
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	//
	// high water PWM12 PWM13
	//
	if (high_water_detected == WICED_TRUE)
	{
		new_tlc59116_registers[i++] = BRIGHTSET; // red
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		system_fault_in_process = WICED_TRUE;
	}
	else
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // green
		new_tlc59116_registers[i++] = BRIGHTSET; // 
	}
	//
	// n/c PWM 14
	//
	new_tlc59116_registers[i++] = LEDSETOFF;
	//
	// buzzer PWM15
	//
	if (system_fault_in_process == WICED_TRUE)
	{
		if (alarm_muted == WICED_TRUE)
		{
//			WPRINT_APP_INFO(("alarm_muted pressed in led set\r\n"));
			if  ((server_time - alarm_muted_timer) < ALARM_MUTE_TIME_LIMIT)
			{
//				WPRINT_APP_INFO(("alarm_muted timer not expired\r\n"));
				new_tlc59116_registers[i++] = LEDSETOFF; // pwm15
//				force_set = WICED_TRUE;
			}
			else
				new_tlc59116_registers[i++] = ON; // pwm15
		}
		else
		{
			new_tlc59116_registers[i++] = ON; // pwm15
		}
	}
	else
	{
		alarm_muted = WICED_FALSE;
		new_tlc59116_registers[i++] = LEDSETOFF; // pwm15
	}
	//
	// the rest
	//
	new_tlc59116_registers[i++] = GRPPWMSET;  // GRPPWM
	new_tlc59116_registers[i++] = 0x00;       // GRPFREQ
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT0
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT1
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT2
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT3
	//
	// look to see if there is a state change
	//
	if ((force_set == WICED_TRUE) || (memcmp(current_tlc59116_registers, new_tlc59116_registers+1, i-1) != 0))
	{
		//
		// write to i2c
		//
		result = ism_i2c_write(WICED_FALSE, new_tlc59116_registers, i );
		if (result != WICED_SUCCESS)
		{
			WPRINT_APP_INFO(("set_i2c_tlc59116 write error result %d\r\n", result));
			return (WICED_FALSE);
		}
		else
		{
			WPRINT_APP_INFO(("set_i2c_tlc59116 write success %d\r\n", result));
		}
		//
		// double check what's written
		//
		read_i2c_tlc59116();
		print_tlc59116_buffers();

		if (memcmp(current_tlc59116_registers, new_tlc59116_registers+1, i-1) != 0)
		{
			WPRINT_APP_INFO(("i2c write error comparing read \r\n"));
			return(WICED_FALSE);
		}
		//
		// copy new to current
		//
//		memcpy(new_tlc59116_registers, current_tlc59116_registers, sizeof(current_tlc59116_registers));

		WPRINT_APP_INFO(("completed set_i2c_tlc59116\r\n"));
		return WICED_TRUE;

	}
	else
	{
		return(WICED_TRUE);
	}
	

}

wiced_bool_t test_mode_set_i2c_tlc59116(wiced_bool_t set_red, wiced_bool_t buzzer_on)
{
	//
	//
	// n/c         - 0x02
	// n/c         - 0x03
	// led1 - WiFi - 0x04 - PWM2  - Red
	// led1 - WiFi - 0x05 - PWM3  - Green 
	// led2 - batt - 0x06 - PWM4  - Red
	// led2 - batt - 0x07 - PWM5  - Green
	// led3 - pwr  - 0x08 - PWM6  - Red
	// led3 - pwr  - 0x09 - PWM7  - Green
	// led4 - mpmp - 0x0A - PWM8  - Red
	// led4 - mpmp - 0x0B - PWM9  - Green
	// led5 - bpmp - 0x0C - PWM10 - Red
	// led5 - bpmp - 0x0D - PWM11 - Green
	// led6 - hwtr - 0x0E - PWM12 - Red
	// led6 - hwtr - 0x0F - PWM13 - Green
	// n/c         - 0x10 - PWM14        
	// buzzer      - 0x11 - PWM15
 	// 


	memset(new_tlc59116_registers, 0, sizeof(new_tlc59116_registers));
	system_fault_in_process = WICED_FALSE;

	int i = 0;
	new_tlc59116_registers[i++] = 0x80;       // control register = auto-inc all regs - addr 0
    new_tlc59116_registers[i++] = 0x80;       // MODE1 - turn on auto-inc
    new_tlc59116_registers[i++] = 0x80;       // MODE2 - clear error statys 
	//
	// n/c
	//
	new_tlc59116_registers[i++] = LEDSETOFF;  // PWM0
	new_tlc59116_registers[i++] = LEDSETOFF;  // PWM1
	//
	// wi-fi led - PWM2 PWM3
	//
	if (set_red == WICED_TRUE)
	{
		new_tlc59116_registers[i++] = BRIGHTSET;   // red wifi
		new_tlc59116_registers[i++] = LEDSETOFF;  //
		new_tlc59116_registers[i++] = BRIGHTSET; // red batt
		new_tlc59116_registers[i++] = LEDSETOFF; // 
	
		new_tlc59116_registers[i++] = BRIGHTSET; // red pwr
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET;  // red main
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET; // red backup
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET; // red hw
		new_tlc59116_registers[i++] = LEDSETOFF; // 
	}
	else
	{
		new_tlc59116_registers[i++] = LEDSETOFF;  //
		new_tlc59116_registers[i++] = BRIGHTSET;   // green wifi
		new_tlc59116_registers[i++] = LEDSETOFF;  //
		new_tlc59116_registers[i++] = BRIGHTSET; // green batt
		new_tlc59116_registers[i++] = LEDSETOFF; // 
	
		new_tlc59116_registers[i++] = BRIGHTSET; // green pwr
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET;  // green main
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET; // green backup
		new_tlc59116_registers[i++] = LEDSETOFF; // 
		new_tlc59116_registers[i++] = BRIGHTSET; // green hw
	}
	//
	// n/c PWM 14
	//
	new_tlc59116_registers[i++] = LEDSETOFF;
	//
	// buzzer PWM15
	//
	if (buzzer_on == WICED_TRUE)
	{
		new_tlc59116_registers[i++] = ON; // pwm15
	}
	else
	{
		new_tlc59116_registers[i++] = LEDSETOFF; // pwm15
	}
	//
	// the rest
	//
	new_tlc59116_registers[i++] = GRPPWMSET;  // GRPPWM
	new_tlc59116_registers[i++] = 0x00;       // GRPFREQ
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT0
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT1
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT2
	new_tlc59116_registers[i++] = LEDSETON;   // LEDOUT3

	result = ism_i2c_write(WICED_FALSE, new_tlc59116_registers, i );
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("set_i2c_tlc59116 write error result %d\r\n", result));
		return (WICED_FALSE);
	}
	else
	{
		WPRINT_APP_INFO(("set_i2c_tlc59116 write success %d\r\n", result));
	}
	return WICED_TRUE;
}

void test_mode()
{
	//
	// *TODO 2 - improvement 
	// menu, user input, for e.g. turn on/off dc motor, turn on/off charger
	//
	// Test without manual
	// - States
	// - 1 - idle - measure vbatt , charger off, pump off
	// - 2 - pump on - measure dc load loaded vbatt, charger off, 
	// - 3 - pump off, charger off, dwell 10 
	// - 4 - LEDs ? Buzzer
	//    wiced_bool_t test_mode_value_changed = WICED_TRUE;
    wiced_bool_t wifi_passed = WICED_TRUE;
	//    wiced_bool_t last_config_button_passed = WICED_FALSE;
	wiced_bool_t config_button_enabled = WICED_FALSE;
	wiced_bool_t r_float_enabled = WICED_FALSE;
	wiced_bool_t g_float_enabled = WICED_FALSE;
	wiced_bool_t alarm_mute_enabled = WICED_FALSE;
	uint16_t ac_current_sense = 0;
	uint16_t v_sense = 0;
	uint16_t current_sense = 0;
	uint16_t ac_sense = 0;
	wiced_bool_t relay_on_off = WICED_FALSE;
	wiced_bool_t led1_on_off = WICED_FALSE;
	int loop_count = 0;
	int mode = 0;
	//
	// disable interrupts
	//
	result = wiced_gpio_input_irq_disable( ALARM_MUTE);
	result = wiced_gpio_input_irq_disable( CONFIG_BUTTON);

	//
	// init i2c
	//
	init_i2c_tlc59116();
	//
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// reset Config Flag - boot to init mode next time
	//
	if (dct.NewConfigurationFlag != 0x0A0B0C0D)
//	if (dct.NewConfigurationFlag != 0x0F0F0F0F)
	{
		//
		// initialization / run mode for BBS2
		//
		dct.NewConfigurationFlag = 0x0A0B0C0D;
//		dct.NewConfigurationFlag = 0x0F0F0F0F;
		write_app_dct();
		//
		// reset the DCT
		//
		configure_app_dct();
		configure_network_dct();
		configure_wifi_dct();
	}
	//
	// loop here until power off
	//
	while (1)
	{
		//
		// display test parameters
		// Battery voltage, AC current, DC current, Config button check, Flash wifi led (green, red).
		//
	//	if (test_mode_value_changed == WICED_TRUE)
	//	{
			WPRINT_APP_INFO(("\033[2J\033[1;1H"));
			WPRINT_APP_INFO(( "\r\n\r\n" ));
			WPRINT_APP_INFO(( "DeviceID           = %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
			WPRINT_APP_INFO(( "Firmware Ver.      = %s\r\n", FIRMWARE_VERSION_STRING));
			WPRINT_APP_INFO(( "Wi-Fi Scan         = %d\r\n", wifi_passed ));
			WPRINT_APP_INFO(( "Wi-Fi Memory       = %d\r\n", check_wifi_dct() ));
			WPRINT_APP_INFO(( "Config Button      = %d\r\n", config_button_enabled));
			WPRINT_APP_INFO(( "Red Float          = %d\r\n", r_float_enabled ));
			WPRINT_APP_INFO(( "Green Float        = %d\r\n", g_float_enabled ));
			WPRINT_APP_INFO(( "Alarm Mute         = %d\r\n", alarm_mute_enabled ));
			WPRINT_APP_INFO(( "AC_Current_Sense   = %d\r\n", ac_current_sense ));
			WPRINT_APP_INFO(( "V_Sense            = %d\r\n", v_sense ));
			WPRINT_APP_INFO(( "Current_Sense      = %d\r\n", current_sense ));
			WPRINT_APP_INFO(( "AC_Sense           = %d\r\n", ac_sense ));
			WPRINT_APP_INFO(( "Charger            = %d\r\n", charger_enabled ));
			WPRINT_APP_INFO(( "Relay              = %d\r\n", relay_on_off ));
			WPRINT_APP_INFO(( "LED1               = %d\r\n", led1_on_off ));			
			WPRINT_APP_INFO(( "loop_count         = %d\r\n", loop_count ));
			WPRINT_APP_INFO(( "mode               = %d\r\n", mode ));
		//
		// config button
		//
		/*
		if ((config_button_passed == WICED_TRUE) && (last_config_button_passed == WICED_FALSE))
		{
//			test_mode_value_changed = WICED_TRUE;
			last_config_button_passed = config_button_passed;
		}
		*/
	    config_button_enabled = wiced_gpio_input_get( CONFIG_BUTTON ) ? WICED_FALSE : WICED_TRUE;
		//
		// red float
		//
	    r_float_enabled = wiced_gpio_input_get( R_FLOAT ) ? WICED_FALSE : WICED_TRUE;
		//
		// green float
		//
	    g_float_enabled = wiced_gpio_input_get( G_FLOAT ) ? WICED_FALSE : WICED_TRUE;
		//
		// alarm mute
		//
	    alarm_mute_enabled = wiced_gpio_input_get( ALARM_MUTE ) ? WICED_FALSE : WICED_TRUE;
		//
		// ac_current_sense
		//
		ac_current_sense = get_ac_pump_current();
		//
		// v_sense
		//
		v_sense = measure_v_batt();
		//
		// current_sense
		//
		current_sense = get_dc_pump_current();
		//
		// ac_sense
		//
		ac_sense = get_ac_sense_level();
		//
		//  relay turn on every 10 seconds 
		//
		if ((loop_count % 20) == 0)
		{
			mode++;
			if (mode > 4)
				mode = 0;
			switch (mode)
			{
				case 0: // i/o test led's green - buzzer - off
					wiced_gpio_output_low(RELAY);
					relay_on_off = WICED_FALSE;
					wiced_gpio_output_low(LED1);
					led1_on_off = WICED_FALSE;
					test_mode_set_i2c_tlc59116(WICED_FALSE, WICED_FALSE);
					turn_off_charger();
					break;
				case 1: // i/o test led's red - buzzer off
					wiced_gpio_output_low(RELAY);
					relay_on_off = WICED_FALSE;
					wiced_gpio_output_high(LED1);
					led1_on_off = WICED_FALSE;
					test_mode_set_i2c_tlc59116(WICED_TRUE, WICED_FALSE);
					turn_off_charger();
					break;
				case 2: // i/o test leds green - buzzer on
					wiced_gpio_output_low(RELAY);
					relay_on_off = WICED_FALSE;
					wiced_gpio_output_low(LED1);
					led1_on_off = WICED_FALSE;
					test_mode_set_i2c_tlc59116(WICED_FALSE, WICED_TRUE);
					turn_off_charger();
					break;
				case 3: // i/o test led's green - buzzer off - pump on
					wiced_gpio_output_high(RELAY);
					relay_on_off = WICED_TRUE;
					wiced_gpio_output_high(LED1);
					led1_on_off = WICED_FALSE;
					test_mode_set_i2c_tlc59116(WICED_TRUE, WICED_FALSE);
					turn_off_charger();
					break;
				case 4: // i/o test led's green - buzzer off - pump off charger on
					wiced_gpio_output_low(RELAY);
					relay_on_off = WICED_FALSE;
					wiced_gpio_output_low(LED1);
					led1_on_off = WICED_FALSE;
					test_mode_set_i2c_tlc59116(WICED_FALSE, WICED_FALSE);
					turn_on_charger();
					break;
			}
		}
		//
		// delay
		//
		wiced_rtos_delay_milliseconds( 500 );
		loop_count++;
	}
}


/******************************************************
 *               Main - application starts here
 ******************************************************/
//
// Power-on
//
void application_start( )
{
	//
    // stop app
    //
	#ifdef STOP_APP_HERE
	main_thread_counter_enabled = WICED_FALSE;
	while ( 1 )
	{
		wiced_rtos_delay_milliseconds( 1000 );
	}
	#endif
    //
    // Init the wiced stack
    //
    wiced_init( );
	platform_init_charger();
	//
    // setup leds & buzzer
    //
    //
    // display app version
    //
    WPRINT_APP_INFO( ("*************************\r\n") );
    WPRINT_APP_INFO( ( "Starting Version %s \r\n", FIRMWARE_VERSION_STRING) );
    WPRINT_APP_INFO( ("*************************\r\n") );
    //
    // initialize the AD-C
    //
    result = wiced_adc_init( AC_CURRENT_SENSE, 480 );
    WPRINT_APP_INFO( ( "AC_CURRENT_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( V_SENSE, 480 );
    WPRINT_APP_INFO( ( "V_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( CURRENT_SENSE, 480 );
    WPRINT_APP_INFO( ( "CURRENT_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( AC_SENSE, 480 );
    WPRINT_APP_INFO( ( "AC_SENSE init result %d\r\n", result ) );
    // Run the Start-up
    //
    run_common_startup_sequence();
	//
	// turn off the charger
	//
	turn_off_charger();
	battery_voltage_unloaded = measure_v_batt();
	last_battery_voltage_unloaded = battery_voltage_unloaded;
		
    //
	// initialize the red green switch IRQ's
	//
//	result = wiced_gpio_input_irq_enable( R_FLOAT, IRQ_TRIGGER_FALLING_EDGE, r_float_falling_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq falling enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( R_FLOAT, IRQ_TRIGGER_RISING_EDGE, r_float_rising_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq risign enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( G_FLOAT, IRQ_TRIGGER_FALLING_EDGE, g_float_falling_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq falling enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( G_FLOAT, IRQ_TRIGGER_RISING_EDGE, g_float_rising_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq risign enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( ALARM_MUTE, IRQ_TRIGGER_FALLING_EDGE, alarm_mute_handler, NULL);
//	WPRINT_APP_INFO( ( "alarm_mute irq falling enable %d\r\n", result ) );
	//
	// set up the leds
	//
	init_i2c_tlc59116();
	set_i2c_tlc59116(WICED_TRUE);
	//
	// set up the self-test timer
	//
	self_test_timer = server_time;
	set_led_timer = server_time;
    //
    // configure the brown out reset	//
	//
	// TODO	
	//set_bor();
	//
	// main loop
	//
    while ( 1 )
    {
    	//
    	// increment the main_thread_counter every 12 seconds
    	// system_monitor decrements every 12 seconds
    	// if main loop stops, system_monitor will reboot when
    	// main_thread_counter <= 0
    	//
    	current_time = host_rtos_get_time();
//		WPRINT_APP_INFO(("current_time %ld\r\n", current_time));
    	if ((current_time - last_time) > 12000)
    	{
    		last_time = current_time;
			main_thread_counter = 5;
//			WPRINT_APP_INFO(("main_thread_counter %d\r\n", main_thread_counter));
    	}
		//
		// set the LEDs
		//

		if (set_i2c_tlc59116(WICED_FALSE) == WICED_FALSE)
		{
			init_i2c_tlc59116();
			set_i2c_tlc59116(WICED_TRUE);
		}
        //
        // main state machine
        //
    	switch (system_state)
    	{
			//
			// start up the network
			//
			case STATE_INITIALIZING:
				//
				// run the initialization
				//
				run_common_initialization_sequence(FIRMWARE_VERSION);
				//
				// turn off chaarger
				//
				turn_off_charger();
				//
				// set up the self-test timer
				//
				self_test_timer = server_time;

				break;
			//
			// STA up running normally
			//
			case STATE_RUNNING:
				//
				// get the current system time
				//
				wiced_time_get_utc_time_ms  (&server_time);
				//
				// turn on system monitor
				//
				main_thread_counter_enabled = WICED_TRUE;
				//
				// check network status
				//
				if (wiced_network_is_up(WICED_STA_INTERFACE) != WICED_TRUE)
				{
					wifi_error_condition = WICED_TRUE;
				}
				else
				{
					wifi_error_condition = WICED_FALSE;
				}

				//
				// set led every minute
				//
				if ((server_time - set_led_timer) > 500)
				{
					set_led_timer = server_time;
					if (check_i2c_tlc59116_mode1() == WICED_TRUE)
					{
						WPRINT_APP_INFO(("Mode1 error\r\n"));
						if (set_i2c_tlc59116(WICED_TRUE) == WICED_FALSE)
						{
							init_i2c_tlc59116();
							set_i2c_tlc59116(WICED_TRUE);
						}
					}
//						else
//							WPRINT_APP_INFO(("Mode1 ok\r\n"));

				}

				//
				// handle the ping timer
				//
				process_ping_timer();
				//
				// handle parameters time
				//
				process_get_parameters_timer();
				//
				// handle the reset timer
				//
				process_daily_reset_timer();
				//
				// handle the OTA firmware
				//
				process_ota_firmware();
				//
				// handle the clock & dns reset
				//
				process_clock_timer();
				//
				// look at the ac_pump
				//
				monitor_ac_pump();
				//
				// look at the dc_pump
				//
				monitor_dc_pump();
				//
				// look at ac sense
				//
				monitor_ac_sense();
				//
				// high water
				// 
				monitor_high_water();
				//
				// battery voltage
				//
				monitor_v_batt();
				//
				// mute button
				//
				monitor_alarm_mute();
				//
				// self test timer check
				//
				check_self_test();
	    		break;
    		//
    		// provisioning AP state
    		//
        	case STATE_PROVISIONING:
				//
				// turn off chaarger
				//
				turn_off_charger();
        		//
        		// run provisioning AP
        		//
        		run_provisioning_ap();
        		break;
    		//
    		// OTA state
    		//
        	case STATE_OTA:
				WPRINT_APP_INFO(("OTA  \r\n"));
        		//
        		// run the ota AP
	      		run_ota_ap();
        		break;
			case STATE_TEST_MODE:
				//
				// run test_mode
				//
				test_mode();
				break;
    	}
	}
//        wiced_rtos_delay_milliseconds(500);
    //
    // de-init the wiced stack and end
    //
    wiced_deinit();
}


