@echo off

rem Check Window version
ver | find "Version 10" > version_out.txt

rem ERRORLEVEL is 0 when string "Version 10" is found
rem WRRORLEVEL is 1 when string "Version 10" is not found

set WIN10_ERRORLEVEL=%ERRORLEVEL%

if %WIN10_ERRORLEVEL% NEQ 0 (
rem	echo "Not Window 10"
rem call dpscat.exe to generate certification for driver package
	del cyw9wcd1eval1_a.cat
	dpscat.exe > dpscat.log
)
