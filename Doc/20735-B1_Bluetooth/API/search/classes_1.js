var searchData=
[
  ['adc_5fctl0_5freg',['ADC_CTL0_REG',['../union_a_d_c___c_t_l0___r_e_g.html',1,'']]],
  ['adc_5fctl1_5freg',['ADC_CTL1_REG',['../union_a_d_c___c_t_l1___r_e_g.html',1,'']]],
  ['adc_5fctl2_5freg',['ADC_CTL2_REG',['../union_a_d_c___c_t_l2___r_e_g.html',1,'']]],
  ['adcstate',['AdcState',['../struct_adc_state.html',1,'']]],
  ['ancs_5fevent_5ft',['ancs_event_t',['../structancs__event__t.html',1,'']]],
  ['api_5fctl0_5freg',['API_CTL0_REG',['../union_a_p_i___c_t_l0___r_e_g.html',1,'']]],
  ['api_5fctl1_5freg',['API_CTL1_REG',['../union_a_p_i___c_t_l1___r_e_g.html',1,'']]],
  ['api_5fctl2_5freg',['API_CTL2_REG',['../union_a_p_i___c_t_l2___r_e_g.html',1,'']]],
  ['api_5fctl3_5freg',['API_CTL3_REG',['../union_a_p_i___c_t_l3___r_e_g.html',1,'']]],
  ['api_5fctl4_5freg',['API_CTL4_REG',['../union_a_p_i___c_t_l4___r_e_g.html',1,'']]]
];
