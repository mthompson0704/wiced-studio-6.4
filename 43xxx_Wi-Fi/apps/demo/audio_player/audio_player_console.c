/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * Audio Player Console routines
 *
 */

#include <malloc.h>

#include "wiced.h"
#include "wiced_log.h"

#include "command_console.h"
#include "wifi/command_console_wifi.h"
#include "dct/command_console_dct.h"

#include "audio_player.h"
#include "audio_player_tracex.h"

/******************************************************
 *                      Macros
 ******************************************************/

#define AUDIO_PLAYER_CONSOLE_COMMANDS \
    { (char*) "exit",           audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Exit application" }, \
    { (char*) "config",         audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Display / change config values" }, \
    { (char*) "log",            audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Set the current log level" }, \
    { (char*) "play",           audio_player_console_command,    1, NULL, NULL, (char *)"",                     (char *)"Play a track" }, \
    { (char*) "stop",           audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Stop playing a track" }, \
    { (char*) "pause",          audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Pause playing a track" }, \
    { (char*) "resume",         audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Resume playing a track" }, \
    { (char*) "volume",         audio_player_console_command,    1, NULL, NULL, (char *)"<volume>",             (char *)"Change the playback volume" }, \
    { (char*) "load_playlist",  audio_player_console_command,    1, NULL, NULL, (char *)"<ipaddr>",             (char *)"Load an audio playlist from a server" }, \
    { (char*) "playlist",       audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Display the current audio playlist" }, \
    { (char*) "clear",          audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Clear the current audio playlist" }, \
    { (char*) "stats",          audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Display playback stats" }, \
    { (char*) "seek",           audio_player_console_command,    1, NULL, NULL, (char *)"<ms>",                 (char *)"Seek to the requested time (ms)" }, \
    { (char*) "memory",         audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Display the current system memory info" }, \
    { (char*) "effect",         audio_player_console_command,    1, NULL, NULL, (char *)"<index>",              (char *)"Set audio effect" }, \
    { (char*) "play_offset",    audio_player_console_command,    2, NULL, NULL, (char *)"<track> <offset_ms>",  (char *)"Play playlist track at a specified offset" }, \
    { (char*) "playc",          audio_player_console_command,    1, NULL, NULL, (char *)"<track>",              (char *)"Play a contiguous track" }, \
    { (char*) "suspend",        audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Suspend playback" }, \
    { (char*) "suspend_resume", audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"Resume suspended playback" }, \
    { (char*) "play_raw",       audio_player_console_command,    2, NULL, NULL, (char *)"<sample rate> <ch>",   (char *)"Play a raw file" }, \
    { (char*) "play_voice",     audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"play voice file from sdcard" }, \
    { (char*) "rstats",         audio_player_console_command,    0, NULL, NULL, (char *)"",                     (char *)"audio render stats" }, \

/******************************************************
 *                    Constants
 ******************************************************/

#define AUDIO_PLAYER_CONSOLE_COMMAND_MAX_LENGTH      (120)
#define AUDIO_PLAYER_CONSOLE_COMMAND_HISTORY_LENGTH  (10)

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    AUDIO_PLAYER_CONSOLE_CMD_EXIT = 0,
    AUDIO_PLAYER_CONSOLE_CMD_CONFIG,
    AUDIO_PLAYER_CONSOLE_CMD_LOG,
    AUDIO_PLAYER_CONSOLE_CMD_PLAY,
    AUDIO_PLAYER_CONSOLE_CMD_STOP,
    AUDIO_PLAYER_CONSOLE_CMD_PAUSE,
    AUDIO_PLAYER_CONSOLE_CMD_RESUME,
    AUDIO_PLAYER_CONSOLE_CMD_VOLUME,
    AUDIO_PLAYER_CONSOLE_CMD_LOAD_PLAYLIST,
    AUDIO_PLAYER_CONSOLE_CMD_PLAYLIST,
    AUDIO_PLAYER_CONSOLE_CMD_CLEAR,
    AUDIO_PLAYER_CONSOLE_CMD_STATS,
    AUDIO_PLAYER_CONSOLE_CMD_SEEK,
    AUDIO_PLAYER_CONSOLE_CMD_MEMORY,
    AUDIO_PLAYER_CONSOLE_CMD_EFFECT,
    AUDIO_PLAYER_CONSOLE_CMD_PLAY_OFFSET,
    AUDIO_PLAYER_CONSOLE_CMD_PLAY_CONTIGUOUS,
    AUDIO_PLAYER_CONSOLE_CMD_SUSPEND,
    AUDIO_PLAYER_CONSOLE_CMD_SUSPEND_RESUME,
    AUDIO_PLAYER_CONSOLE_CMD_PLAY_RAW,
    AUDIO_PLAYER_CONSOLE_CMD_PLAY_VOICE,
    AUDIO_PLAYER_CONSOLE_CMD_RENDER_STATS,

    AUDIO_PLAYER_CONSOLE_CMD_MAX,
} AUDIO_PLAYER_CONSOLE_CMDS_T;

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    char *cmd;
    uint32_t event;
} cmd_lookup_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

static char audio_player_command_buffer[AUDIO_PLAYER_CONSOLE_COMMAND_MAX_LENGTH];
static char audio_player_command_history_buffer[AUDIO_PLAYER_CONSOLE_COMMAND_MAX_LENGTH * AUDIO_PLAYER_CONSOLE_COMMAND_HISTORY_LENGTH];

const command_t audio_command_table[] =
{
#ifdef AUDIO_PLAYER_ENABLE_WIFI_CMDS
    WIFI_COMMANDS
#endif
    DCT_CONSOLE_COMMANDS
    AUDIO_PLAYER_CONSOLE_COMMANDS
    AUDIO_PLAYER_TRACEX_COMMANDS
    CMD_TABLE_END
};

static cmd_lookup_t command_lookup[AUDIO_PLAYER_CONSOLE_CMD_MAX] =
{
    { "exit",           PLAYER_EVENT_SHUTDOWN           },
    { "config",         0                               },
    { "log",            0                               },
    { "play",           0                               },
    { "stop",           AUDIO_CLIENT_IOCTL_STOP         },
    { "pause",          AUDIO_CLIENT_IOCTL_PAUSE        },
    { "resume",         AUDIO_CLIENT_IOCTL_RESUME       },
    { "volume",         AUDIO_CLIENT_IOCTL_SET_VOLUME   },
    { "load_playlist",  0                               },
    { "playlist",       PLAYER_EVENT_OUTPUT_PLAYLIST    },
    { "clear",          0                               },
    { "stats",          0                               },
    { "seek",           0                               },
    { "memory",         0                               },
    { "effect",         0                               },
    { "play_offset",    0                               },
    { "playc",          0                               },
    { "suspend",        0                               },
    { "suspend_resume", 0                               },
    { "play_raw",       0                               },
    { "play_voice",     0                               },
    { "rstats",         0                               },
};

/******************************************************
 *               Function Definitions
 ******************************************************/

static void audio_player_console_dct_callback(console_dct_struct_type_t struct_changed, void* app_data)
{
    audio_player_t* player = (audio_player_t*)app_data;

    /* sanity check */
    if (player == NULL)
    {
        return;
    }

    switch (struct_changed)
    {
        case CONSOLE_DCT_STRUCT_TYPE_WIFI:
            /* Get WiFi configuration */
            wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_RELOAD_DCT_WIFI);
            break;

        case CONSOLE_DCT_STRUCT_TYPE_NETWORK:
            /* Get network configuration */
            wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_RELOAD_DCT_NETWORK);
            break;

        default:
            break;
    }
}


int audio_player_console_command(int argc, char *argv[])
{
    audio_client_track_info_t info;
    wiced_audio_config_t config;
    audio_render_stats_t rstats;
    wiced_result_t result;
    uint32_t event = 0;
    uint32_t current_ms;
    uint32_t total_ms;
    char* ptr;
    int log_level;
    int volume;
    int index;
    int i;

    wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Received command: %s\n", argv[0]);

    if (g_player == NULL || g_player->tag != AUDIO_PLAYER_TAG_VALID)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "audio_player_console_command() Bad player structure\n");
        return ERR_CMD_OK;
    }

    /*
     * Lookup the command in our table.
     */

    for (i = 0; i < AUDIO_PLAYER_CONSOLE_CMD_MAX; ++i)
    {
        if (strcmp(command_lookup[i].cmd, argv[0]) == 0)
            break;
    }

    if (i >= AUDIO_PLAYER_CONSOLE_CMD_MAX)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unrecognized command: %s\n", argv[0]);
        audio_player_play_voice(g_player, VOICE_AUDIO_PLAYER_WRONG_COMMAND, 0);
        return ERR_CMD_OK;
    }

    switch (i)
    {
        case AUDIO_PLAYER_CONSOLE_CMD_EXIT:
        case AUDIO_PLAYER_CONSOLE_CMD_PLAYLIST:
            event = command_lookup[i].event;
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_CONFIG:
            audio_player_set_config(&g_player->dct_tables, argc, argv);
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_LOG:
            log_level = atoi(argv[1]);
            wiced_log_printf("Setting new log level to %d (0 - off, %d - max debug)\n", log_level, WICED_LOG_DEBUG4);
            wiced_log_set_all_levels(log_level);
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_PLAY:
        case AUDIO_PLAYER_CONSOLE_CMD_PLAY_CONTIGUOUS:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            g_player->starting_offset_ms = 0;
            g_player->play_index_start   = -1;
            g_player->play_index_end     = -1;
            g_player->play_index_current = -1;
            g_player->play_repeat        = WICED_FALSE;
            g_player->play_contiguous    = (i == AUDIO_PLAYER_CONSOLE_CMD_PLAY_CONTIGUOUS ? WICED_TRUE : WICED_FALSE);
            if (g_player->playlist_entries != NULL && g_player->num_playlist_entries > 0)
            {
                /*
                 * There's a playlist loaded. Are we playing everything?
                 */

                if (strncasecmp(argv[1], "all", 3) == 0)
                {
                    g_player->play_index_start = 0;
                    g_player->play_index_end   = g_player->num_playlist_entries - 1;
                }
                else
                {
                    index = atoi(argv[1]);
                    if (index >= 0 && index < g_player->num_playlist_entries)
                    {
                        g_player->play_index_start = index;
                    }
                    else
                    {
                        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid playlist index %d\n", index);
                    }
                    ptr = strchr(argv[1], '-');
                    if (ptr != NULL)
                    {
                        /*
                         * We found a '-' in the argument. We assume that we're being given a range.
                         */

                        if (ptr[1] == '\0')
                        {
                            g_player->play_index_end = g_player->num_playlist_entries - 1;
                        }
                        else
                        {
                            index = atoi(&ptr[1]);
                            if (index >= 0 && index < g_player->num_playlist_entries)
                            {
                                g_player->play_index_end = index;
                            }
                            else
                            {
                                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid playlist index %d\n", index);
                            }
                        }
                    }
                }

                if (argc > 2 && strncasecmp(argv[2], "repeat", 6) == 0)
                {
                    g_player->play_repeat = WICED_TRUE;
                }

                if (g_player->play_index_start >= 0)
                {
                    g_player->play_index_current = g_player->play_index_start;
                    event = PLAYER_EVENT_PLAY;
                }
            }
            else
            {
                /*
                 * No playlist loaded. Just pass the URI directly to the audio client.
                 */

                if (g_player->audio_client)
                {
                    audio_player_display_title( &g_player->display, argv[1], " no song info " );

                    audio_client_ioctl(g_player->audio_client,
                                       (i == AUDIO_PLAYER_CONSOLE_CMD_PLAY_CONTIGUOUS ? AUDIO_CLIENT_IOCTL_PLAY_CONTIGUOUS : AUDIO_CLIENT_IOCTL_PLAY), argv[1]);
                }
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_PLAY_OFFSET:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            if (g_player->playlist_entries == NULL && g_player->num_playlist_entries == 0)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "No playlist loaded\n");
                break;
            }

            g_player->starting_offset_ms = 0;
            g_player->play_index_start   = -1;
            g_player->play_index_end     = -1;
            g_player->play_index_current = -1;
            g_player->play_repeat        = WICED_FALSE;
            g_player->play_contiguous    = WICED_FALSE;
            index = atoi(argv[1]);
            if (index >= 0 && index < g_player->num_playlist_entries)
            {
                g_player->play_index_start = index;
            }
            else
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid playlist index %d\n", index);
                break;
            }

            g_player->starting_offset_ms = atoi(argv[2]);
            g_player->play_index_current = g_player->play_index_start;
            event = PLAYER_EVENT_PLAY;
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_STOP:
            /* clear title */
            audio_player_display_title(&g_player->display, " player is ready", NULL);
            /* Fall through */

        case AUDIO_PLAYER_CONSOLE_CMD_PAUSE:
        case AUDIO_PLAYER_CONSOLE_CMD_RESUME:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            if (g_player->audio_client)
            {
                audio_client_ioctl(g_player->audio_client, command_lookup[i].event, NULL);
                if (i == AUDIO_PLAYER_CONSOLE_CMD_STOP && g_player->play_raw_active)
                {
                    audio_player_delete_raw_socket(g_player);

                    /*
                     * Pretend we're a disconnect event so we ensure that we shut down cleanly.
                     */

                    event = PLAYER_EVENT_DISCONNECT;
                }
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_VOLUME:
            volume = atoi(argv[1]);
            volume = MIN(volume, AUDIO_PLAYER_VOLUME_MAX);
            volume = MAX(volume, AUDIO_PLAYER_VOLUME_MIN);
            if (g_player->audio_client)
            {
                g_player->current_volume = volume;
                audio_client_ioctl(g_player->audio_client, command_lookup[i].event, (void*)volume);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_EFFECT:
            if (AUDIO_EFFECT_MODE_MAX == 0)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Audio effects not supported\n");
            }
            else
            {
                int16_t index = atoi(argv[1]);
                if (index >= AUDIO_EFFECT_MODE_NONE && index < AUDIO_EFFECT_MODE_MAX)
                {
                    g_player->effect_mode = index;
                    event = PLAYER_EVENT_EFFECT;
                }
                else
                {
                    wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid audio effect mode index\n");
                }
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_LOAD_PLAYLIST:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            strlcpy(g_player->server_uri, argv[1], sizeof(g_player->server_uri));
            event = PLAYER_EVENT_LOAD_PLAYLIST;
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_CLEAR:
            g_player->play_index_start   = -1;
            g_player->play_index_end     = -1;
            g_player->play_index_current = -1;
            g_player->play_repeat        = WICED_FALSE;
            if (g_player->playlist_entries)
            {
                free(g_player->playlist_entries);
                g_player->playlist_entries = NULL;
                g_player->num_playlist_entries = 0;
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_STATS:
            if (g_player->audio_client)
            {
                audio_client_ioctl(g_player->audio_client, AUDIO_CLIENT_IOCTL_TRACK_INFO, &info);
                current_ms = (uint32_t)(1000.0 / ((double)info.sample_rate / (double)info.current_sample));

                if ( info.total_samples != 0 )
                {
                    total_ms = ( uint32_t )( 1000.0 / ( (double) info.sample_rate / (double) info.total_samples ) );
                }
                else if ( info.total_samples == 0 && info.bitrate != 0 )
                {
                    total_ms = ( g_player->audio_client->http_params.http_total_content_length ) / ( info.bitrate / 8 ) * 1000.0;
                }
                else
                {
                    total_ms = 0;
                }

                wiced_log_printf("Playback: %lu out of %lu samples   %3lu.%03lu of %3lu.%03lu\n",
                                 (uint32_t)info.current_sample, (uint32_t)info.total_samples, current_ms / 1000, current_ms % 1000, total_ms / 1000, total_ms % 1000);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_SEEK:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            current_ms = atoi(argv[1]);
            if (g_player->audio_client)
            {
                audio_client_ioctl(g_player->audio_client, AUDIO_CLIENT_IOCTL_SEEK, (void*)current_ms);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_MEMORY:
            {
                extern unsigned char _heap[];
                extern unsigned char _eheap[];
                extern unsigned char *sbrk_heap_top;
                volatile struct mallinfo mi = mallinfo();

                wiced_log_printf("sbrk heap size:    %7lu\n", (uint32_t)_eheap - (uint32_t)_heap);
                wiced_log_printf("sbrk current free: %7lu \n", (uint32_t)_eheap - (uint32_t)sbrk_heap_top);

                wiced_log_printf("malloc allocated:  %7d\n", mi.uordblks);
                wiced_log_printf("malloc free:       %7d\n", mi.fordblks);

                wiced_log_printf("\ntotal free memory: %7lu\n", mi.fordblks + (uint32_t)_eheap - (uint32_t)sbrk_heap_top);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_SUSPEND:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            if (g_player->audio_client != NULL)
            {
                if (g_player->suspend)
                {
                    free(g_player->suspend);
                    g_player->suspend = NULL;
                }
                result = audio_client_ioctl(g_player->audio_client, AUDIO_CLIENT_IOCTL_SUSPEND, (void*)&g_player->suspend);
                wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Audio client suspend returns %d\n", result);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_SUSPEND_RESUME:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            if (g_player->suspend)
            {
                result = audio_client_ioctl(g_player->audio_client, AUDIO_CLIENT_IOCTL_SUSPEND_RESUME, (void*)g_player->suspend);
                g_player->suspend = NULL;
                wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Audio client suspend resume returns %d\n", result);
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_PLAY_RAW:
            /* skip on play_voice running */
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            if (g_player->play_raw_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play raw is currently active\n");
                break;
            }
            memset(&config, 0, sizeof(wiced_audio_config_t));
            config.sample_rate     = atoi(argv[1]);
            config.channels        = atoi(argv[2]);
            config.bits_per_sample = 16;
            config.frame_size      = 4;
            config.volume          = g_player->current_volume;

            result = audio_client_ioctl(g_player->audio_client, AUDIO_CLIENT_IOCTL_SET_RAW_CONFIG, (void*)&config);
            if (result == WICED_SUCCESS)
            {
                g_player->play_raw_active = WICED_TRUE;
                event = PLAYER_EVENT_PLAY_RAW;
            }
            else
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to configure audio client for raw playback\n");
            }
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_PLAY_VOICE:
            if (WICED_TRUE == g_player->voice.play_voice_active)
            {
                wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Play voice (System) is currently active\n");
                break;
            }

            audio_player_play_voice(g_player, VOICE_AUDIO_PLAYER_PLL_PPM_TUNING, VOICE_AUDIO_PLAYER_PLL_PPM_TUNING_DURATION_MS);
            break;

        case AUDIO_PLAYER_CONSOLE_CMD_RENDER_STATS:
            if (g_player->dct_tables.dct_app->app_playback && g_player->audio)
            {
                audio_render_command(g_player->audio, AUDIO_RENDER_CMD_GET_STATS, (void*)&rstats);
                wiced_log_printf("Render stats: frames played %lu, dropped %lu, inserted %lu\n",
                                 (uint32_t)rstats.audio_frames_played, (uint32_t)rstats.audio_frames_dropped, (uint32_t)rstats.audio_frames_inserted);
                wiced_log_printf("              max early: %09ld, max late: %09ld, audio underruns: %lu\n",
                                 (int32_t)rstats.audio_max_early, (int32_t)rstats.audio_max_late, (uint32_t)rstats.audio_underruns);
            }
            else
            {
                wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Audio player not directly controlling audio render\n");
            }
            break;
    }

    if (event)
    {
        /*
         * Send off the event to the main audio loop.
         */

        wiced_rtos_set_event_flags(&g_player->player_events, event);
    }

    return ERR_CMD_OK;
}


wiced_result_t audio_player_console_start(audio_player_t* player)
{
    wiced_result_t result = WICED_SUCCESS;

    /*
     * Create the command console.
     */

    wiced_log_msg(WLF_DEF, WICED_LOG_INFO, "Start the command console\n");
    result = command_console_init(STDIO_UART, sizeof(audio_player_command_buffer), audio_player_command_buffer,
                                  AUDIO_PLAYER_CONSOLE_COMMAND_HISTORY_LENGTH, audio_player_command_history_buffer, " ");
    if (result != WICED_SUCCESS)
    {
        return result;
    }
    console_add_cmd_table(audio_command_table);
    console_dct_register_callback(audio_player_console_dct_callback, player);

    return result;
}


wiced_result_t audio_player_console_stop(audio_player_t* player)
{
    /*
     * Shutdown the console.
     */

    console_dct_register_callback(NULL, NULL);
    command_console_deinit();

    return WICED_SUCCESS;
}
