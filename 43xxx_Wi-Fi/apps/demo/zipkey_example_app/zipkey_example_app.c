/*
 * Copyright (C) 2018, Cirrent Inc
 *
 * All use of this software must be covered by a license agreement signed by Cirrent Inc.
 *
 * DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
 * ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE.
 */

/*
 * The Zipkey example application code which demonstrates usage of Cirrent Agent
 */

#include <stdio.h>

#include "wiced_management.h"
#include "wiced_tls.h"
#include "wiced_platform.h"
#include "wiced_framework.h"
#include "wiced_utilities.h"
#include "wiced_time.h"
#include "wiced_rtos.h"
#include "wwd_debug.h"

#include "button_manager.h"
#include "led_service.h"

#include "cirrent.h"
#include "cirrent_api.h"
#include "zipkey_app_dct.h"

#ifndef MEMBER_SIZE
#define MEMBER_SIZE(type, member) sizeof(((type *)0)->member)
#endif

#define API_CB_STACK_SIZE 2048
#define BUTTON_WORKER_STACK_SIZE 4096
#define BUTTON_WORKER_QUEUE_SIZE 3

#if WICED_PLATFORM_BUTTON_COUNT > 0
typedef enum
{
    CA_CONFIG_BUTTON,
} application_button_t;

static void _app_button_event_handler(const button_manager_button_t* button,
        button_manager_event_t event, button_manager_button_state_t state);

static wiced_worker_thread_t _button_worker_thread;
static button_manager_t _button_manager;
static const wiced_button_manager_configuration_t _button_manager_config =
{
    .short_hold_duration = 250 * MILLISECONDS,
    .medium_hold_duration = 3 * SECONDS,
    .long_hold_duration = 10 * SECONDS,
    .very_long_hold_duration = 20 * SECONDS, // Not used but should be initialized
    .event_handler = _app_button_event_handler,
};

static const wiced_button_configuration_t _button_configurations[] =
{
    [ CA_CONFIG_BUTTON ] = {PLATFORM_BUTTON_1, BUTTON_CLICK_EVENT | BUTTON_MEDIUM_DURATION_EVENT | BUTTON_LONG_DURATION_EVENT, 0},
};

static button_manager_button_t _buttons[] =
{
    [CA_CONFIG_BUTTON] = {&_button_configurations[CA_CONFIG_BUTTON]},
};

static void _app_button_event_handler(const button_manager_button_t* button,
        button_manager_event_t event_id, button_manager_button_state_t state)
{
    switch (event_id)
    {
        case BUTTON_MEDIUM_DURATION_EVENT:
            if (state == BUTTON_STATE_RELEASED)
            {
                cac_make_discoverable();
            }
            break;

        case BUTTON_LONG_DURATION_EVENT:
            if (state == BUTTON_STATE_HELD)
            {
                cac_reset_device();
            }
            break;

        default:
            break;
    }
}
#endif

#ifndef GPIO_LED_NOT_SUPPORTED
static CAC_RESULT_T _status_hdl(const CAC_NETWORK_INFO_T *network_status)
{
    static bool onboarding_succeed = false;

    switch (network_status->connection_status)
    {
        case CAC_CONNECTED:
            if (network_status->network_type == CAC_NETWORK_TYPE_PRIVATE)
            {
                onboarding_succeed = true;
                wiced_led_service_start_pattern(WICED_LED_PATTERN_LED_1_ONE_FLASH);
            }
            break;

        case CAC_DISCONNECTED:
            if (onboarding_succeed)
            {
                onboarding_succeed = false;
                wiced_led_service_start_pattern(WICED_LED_PATTERN_LED_1_TWO_FLASH);
            }
            break;

        case CAC_ONBOARDING_DISABLED:
            if (!onboarding_succeed)
            {
                wiced_led_service_start_pattern(WICED_LED_PATTERN_LED_1_FAST_FLASH);
            }
            break;

        case CAC_READY_FOR_ONBOARDING:
            wiced_led_service_start_pattern(WICED_LED_PATTERN_LED_1_THREE_FLASH);
            break;

        default:
            break;
    }
    return CAC_SUCCESS;
}
#endif

void application_start(void)
{
    CA_INIT_CONFIG_T ca_config =
    {
        .user_binding_enabled = false,
        .onboarding_button_exists = true,
        .no_log_print = false,
        .softap_config =
        {
            .softap_enabled = true,
            .ssid = "ca-softap-$$$$DUB$$$$",
            .server_ip = "192.168.211.1",
            .server_port = 80,
        },
       .ca_dct_size = MEMBER_SIZE(APP_DCT_T, ca_config),
       .ca_dct_offset = OFFSETOF(APP_DCT_T, ca_config),
    };

    wiced_result_t wiced_res;

    WPRINT_APP_INFO(("starting example app\n"));

    if (!wiced_core_init())
    {
        if (!wiced_tls_init_root_ca_certificates(CA_PEM_CERT, strlen(CA_PEM_CERT)))
        {

            if (!ca_start(&ca_config))
            {
                if (!cac_init(API_CB_STACK_SIZE))
                {
#if WICED_PLATFORM_BUTTON_COUNT > 0
                    wiced_res = wiced_rtos_create_worker_thread(&_button_worker_thread,
                            WICED_DEFAULT_WORKER_PRIORITY,BUTTON_WORKER_STACK_SIZE,
                            BUTTON_WORKER_QUEUE_SIZE);
                    if (!wiced_res)
                    {
                        wiced_res = button_manager_init(&_button_manager, &_button_manager_config,
                                &_button_worker_thread, _buttons, ARRAY_SIZE(_buttons));
                        if (wiced_res)
                        {
                            WPRINT_APP_INFO(("button mgr init err=%d\n", wiced_res));
                        }
                    }
                    else
                    {
                        WPRINT_APP_INFO(("Button WT create err=%d\n", wiced_res));
                    }
#endif

#ifndef GPIO_LED_NOT_SUPPORTED
                    wiced_res = wiced_led_service_init();
                    if (!wiced_res)
                    {
                        wiced_led_set_state(WICED_LED_INDEX_1, WICED_LED_OFF);
                        cac_register_network_status_handler(_status_hdl);
                        cac_enable_notifications();
                    }
#endif
                }
                else
                {
                    WPRINT_APP_INFO(("cac_init err\n"));
                }
            }
            else
            {
                WPRINT_APP_INFO(("ca start err\n"));
            }
        }
        else
        {
            WPRINT_APP_INFO(("root cert init err\n"));
        }
    }
}
