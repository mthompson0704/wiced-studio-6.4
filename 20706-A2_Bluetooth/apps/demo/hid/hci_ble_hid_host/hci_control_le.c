/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/**
 *
 * This file implement BLE application controlled over UART.
 *
 */
#include "wiced.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_uuid.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_trace.h"
#include "wiced_bt_cfg.h"
#include "wiced_app_cfg.h"
#include "wiced_timer.h"
#include "hci_control.h"
#include "wiced_memory.h"
#include "wiced_bt_gatt_mp.h"
#include "hci_control_le.h"
#include "wiced_app_cfg.h"
#include "string.h"
#include "hci_control.h"
/******************************************************
 *                     Constants
 ******************************************************/
#define HCI_CONTROL_LE_SCAN_RESULT_MAX      20
#define HCI_CONTROL_LE_CONNECTIONS_MAX      5

/******************************************************
 *                     Structures
 ******************************************************/
typedef uint8_t wiced_hci_status_t;
typedef struct
{
    wiced_bool_t in_use;
    wiced_bt_device_address_t address;
    wiced_bt_ble_address_type_t address_type;
} hci_control_le_scanned_device_t;

typedef enum
{
    LE_CONTROL_STATE_IDLE = 0,
    LE_CONTROL_STATE_DISCOVER_PRIMARY_SERVICES,
    LE_CONTROL_STATE_DISCOVER_CHARACTERISTICS,
    LE_CONTROL_STATE_DISCOVER_DESCRIPTORS,
    LE_CONTROL_STATE_READ_VALUE,
    LE_CONTROL_STATE_WRITE_VALUE,
    LE_CONTROL_STATE_WRITE_NO_RESPONSE_VALUE,
    LE_CONTROL_STATE_NOTIFY_VALUE,
    LE_CONTROL_STATE_INDICATE_VALUE,
    LE_CONTROL_STATE_WRITE_DESCRIPTOR_VALUE,
    LE_CONTROL_STATE_DISCONNECTING
} hci_control_le_state_t;

typedef struct
{
    hci_control_le_state_t state;           // LE Connection state
    wiced_bool_t      indication_sent;      // TRUE if indication sent and not acked
    wiced_bt_device_address_t bd_addr;
    uint16_t          conn_id;              // Connection ID used for exchange with the stack
    uint16_t          peer_mtu;             // MTU received in the MTU request (or 23 if peer did not send MTU request)
    uint8_t           role;                 // HCI_ROLE_MASTER or HCI_ROLE_SLAVE
} hci_control_le_conn_state_t;

typedef struct
{
    hci_control_le_scanned_device_t scanned_devices[HCI_CONTROL_LE_SCAN_RESULT_MAX];
    hci_control_le_conn_state_t conn[HCI_CONTROL_LE_CONNECTIONS_MAX + 1];
    wiced_bt_gatt_mp_pid_t profile_id;
} hci_control_le_cb_t;


/******************************************************
 *                     Global variables
 ******************************************************/
static hci_control_le_cb_t hci_control_le_cb;

/******************************************************
 *                     Local functions
 ******************************************************/
static wiced_hci_status_t hci_control_le_handle_scan_cmd( wiced_bool_t enable, wiced_bool_t filter_duplicates );
static wiced_hci_status_t hci_control_le_gatt_handle_service_discovery( uint16_t conn_id, uint16_t s_handle, uint16_t e_handle );

static void hci_control_le_scan_result_cback( wiced_bt_ble_scan_results_t *p_scan_result,
        uint8_t *p_adv_data );
static void hci_control_le_send_advertisement_report( wiced_bt_ble_scan_results_t *p_scan_result,
        uint8_t *p_adv_data );
static void hci_control_le_send_scan_state_event( uint8_t status );
static void hci_control_le_scan_result_save( wiced_bt_ble_scan_results_t *p_scan_result);

static wiced_bt_gatt_status_t hci_control_le_gatt_mp_cback(wiced_bt_gatt_evt_t event,
        wiced_bt_gatt_event_data_t *p_event_data);

static void hci_control_le_connection_up(wiced_bt_gatt_connection_status_t *p_connection_status);
static void hci_control_le_connection_down(wiced_bt_gatt_connection_status_t *p_connection_status);
static void hci_control_le_send_connect_event(uint8_t addr_type, BD_ADDR addr, uint16_t con_handle,
        uint8_t role);
static void hci_control_le_send_disconnect_evt(uint8_t reason, uint16_t con_handle);
static void hci_control_le_gatt_send_discovered_service(uint16_t conn_id,
        wiced_bt_gatt_group_value_t *p_discovery_group);
static void hci_control_le_gatt_send_discovered_characteristic(uint16_t conn_id,
        wiced_bt_gatt_char_declaration_t *p_discovery_char);
static void hci_control_le_gatt_send_discovered_descriptor(uint16_t conn_id,
        wiced_bt_gatt_char_descr_info_t *p_discovery_desc);
static void hci_control_le_gatt_send_discover_complete(uint16_t conn_id);
static wiced_result_t hci_control_le_gatt_disc_result_handler(
        wiced_bt_gatt_discovery_result_t *p_result);

/*
 * hci_control_le_init
 * Handle various LE GAP and GATT commands received from MCU.
 */
void hci_control_le_init(void)
{
    wiced_bt_gatt_status_t status;

    memset(&hci_control_le_cb, 0, sizeof(hci_control_le_cb));

    /* Register HCI LE GATT to GATT Multi-Profile library */
    /* This application does not need it. It's here for GATT MP Library education only */
    status = wiced_bt_gatt_mp_register (hci_control_le_gatt_mp_cback, &hci_control_le_cb.profile_id);
    if (status != WICED_BT_GATT_SUCCESS)
        WICED_BT_TRACE("Err: hci_control_le_init wiced_bt_gatt_mp_register failed status:%d\n",
                status);

    /* This is the Default Server application. Register every handles for Server */
    wiced_bt_gatt_mp_register_handles(hci_control_le_cb.profile_id, 0x0001, 0xFFFF, 0);

}
/*
 * hci_control_le_handle_command
 * Handle various LE GAP commands received from MCU.
 */
void hci_control_le_handle_command(uint16_t cmd_opcode, uint8_t* p, uint32_t data_len)
{
    wiced_hci_status_t status;
    wiced_bool_t enable, filter_duplicates;

    switch (cmd_opcode)
    {
    case HCI_CONTROL_LE_COMMAND_SCAN:
        STREAM_TO_UINT8(enable, p);
        STREAM_TO_UINT8(filter_duplicates, p);
        status = hci_control_le_handle_scan_cmd(enable, filter_duplicates);
        break;

    default:
        status = HCI_CONTROL_STATUS_UNKNOWN_COMMAND;
        break;
    }

    hci_control_send_command_status_evt(HCI_CONTROL_LE_EVENT_COMMAND_STATUS, status);
}

/*
 * hci_control_le_gatt_handle_command
 * Handle various GATT commands received from MCU.
 */
void hci_control_le_gatt_handle_command(uint16_t cmd_opcode, uint8_t* p, uint32_t data_len)
{
    wiced_hci_status_t status;
    uint16_t conn_id, s_handle, e_handle;

    switch (cmd_opcode)
    {
    case HCI_CONTROL_GATT_COMMAND_DISCOVER_SERVICES:
        STREAM_TO_UINT16(conn_id, p);
        STREAM_TO_UINT16(s_handle, p);
        STREAM_TO_UINT16(e_handle, p);
        status = hci_control_le_gatt_handle_service_discovery(conn_id, s_handle, e_handle);
        break;

    default:
        status = HCI_CONTROL_STATUS_UNKNOWN_COMMAND;
        break;
    }

    hci_control_send_command_status_evt(HCI_CONTROL_GATT_EVENT_COMMAND_STATUS, status);
}

/*
 * handle scan command from UART
 */
static wiced_hci_status_t hci_control_le_handle_scan_cmd(wiced_bool_t enable,
        wiced_bool_t filter_duplicates)
{
    wiced_result_t status;

    if (enable)
    {
        memset(hci_control_le_cb.scanned_devices, 0, sizeof(hci_control_le_cb.scanned_devices));
        status = wiced_bt_ble_scan(BTM_BLE_SCAN_TYPE_HIGH_DUTY, filter_duplicates,
                hci_control_le_scan_result_cback);
    }
    else
    {
        status = wiced_bt_ble_scan(BTM_BLE_SCAN_TYPE_NONE, filter_duplicates,
                hci_control_le_scan_result_cback);
    }
    WICED_BT_TRACE("hci_control_le_handle_scan_cmd:%d status:%x\n", enable, status);

    if ((status == WICED_BT_SUCCESS) ||
        (status == WICED_BT_PENDING))
    {
        return HCI_CONTROL_STATUS_SUCCESS;
    }

    return HCI_CONTROL_STATUS_FAILED;
}

/*
 * handle start services discovery command from UART
 */
static wiced_hci_status_t hci_control_le_gatt_handle_service_discovery(uint16_t conn_id,
        uint16_t s_handle, uint16_t e_handle)
{
    wiced_bt_gatt_discovery_param_t params;
    wiced_bt_gatt_status_t rc;

    if (conn_id > HCI_CONTROL_LE_CONNECTIONS_MAX)
    {
        WICED_BT_TRACE( "illegal conn_id:%04x\n", conn_id );
        return HCI_CONTROL_STATUS_BAD_HANDLE;
    }
    if (hci_control_le_cb.conn[conn_id].state != LE_CONTROL_STATE_IDLE)
    {
        WICED_BT_TRACE( "illegal state:%d\n", hci_control_le_cb.conn[conn_id].state );
        return HCI_CONTROL_STATUS_WRONG_STATE;
    }
    if ((s_handle > e_handle) || (s_handle == 0) || (e_handle == 0))
    {
        WICED_BT_TRACE( "illegal handles:%04x-%04x\n", s_handle, e_handle );
        return HCI_CONTROL_STATUS_INVALID_ARGS;
    }

    // perform service search
    memset(&params, 0, sizeof(params));
    params.s_handle = s_handle;
    params.e_handle = e_handle;

    WICED_BT_TRACE("discover services conn_id:%d %04x-%04x\n", conn_id, s_handle, e_handle);
    rc = wiced_bt_gatt_mp_send_discover(hci_control_le_cb.profile_id, conn_id,
            GATT_DISCOVER_SERVICES_ALL, &params);
    if (rc != WICED_BT_GATT_SUCCESS)
    {
        WICED_BT_TRACE( "discover failed:0x%x\n", rc );
        return HCI_CONTROL_STATUS_FAILED;
    }
    hci_control_le_cb.conn[conn_id].state = LE_CONTROL_STATE_DISCOVER_PRIMARY_SERVICES;
    return HCI_CONTROL_STATUS_SUCCESS;
}

/*
 * hci_control_le_scan_result_cback
 * Process advertisement packet received
 */
static void hci_control_le_scan_result_cback(wiced_bt_ble_scan_results_t *p_scan_result,
        uint8_t *p_adv_data)
{
    if (p_scan_result)
    {
        WICED_BT_TRACE("BLE Scan Result Device:%B AddrType:%d\n", p_scan_result->remote_bd_addr,
                p_scan_result->ble_addr_type);
        hci_control_le_scan_result_save(p_scan_result);
        hci_control_le_send_advertisement_report(p_scan_result, p_adv_data);
    }
    else
    {
        WICED_BT_TRACE("Scan completed\n");
    }
}

/*
 * Stack runs the scan state machine switching between high duty, low
 * duty, no scan, based on the wiced_cfg.  All changes are notified
 * through this callback.
 */
void hci_control_le_scan_state_changed(wiced_bt_ble_scan_type_t state)
{
    uint8_t hci_control_le_event;

    WICED_BT_TRACE("Scan State Changed:%d\n", state);

    switch ( state )
    {
    case BTM_BLE_SCAN_TYPE_NONE:
        hci_control_le_event = HCI_CONTROL_SCAN_EVENT_NO_SCAN;
        break;
    case BTM_BLE_SCAN_TYPE_HIGH_DUTY:
        hci_control_le_event = HCI_CONTROL_SCAN_EVENT_HIGH_SCAN;
        break;
    case BTM_BLE_SCAN_TYPE_LOW_DUTY:
        hci_control_le_event = HCI_CONTROL_SCAN_EVENT_LOW_SCAN;
        break;
    }
    hci_control_le_send_scan_state_event(hci_control_le_event);
}

/*
 *  transfer scan event to UART
 */
static void hci_control_le_send_scan_state_event(uint8_t status)
{
    wiced_transport_send_data (HCI_CONTROL_LE_EVENT_SCAN_STATUS, &status, 1);
}

/*
 * hci_control_le_send_advertisement_report
 *  transfer advertisement report event to UART
 */
static void hci_control_le_send_advertisement_report(wiced_bt_ble_scan_results_t *p_scan_result,
        uint8_t *p_adv_data)
{
    int i;
    uint8_t tx_buf[70];
    uint8_t *p = tx_buf;
    uint8_t len;

    *p++ = p_scan_result->ble_evt_type;
    *p++ = p_scan_result->ble_addr_type;
    for (i = 0; i < 6; i++)
        *p++ = p_scan_result->remote_bd_addr[5 - i];
    *p++ = p_scan_result->rssi;

    // currently callback does not pass the data of the adv data, need to go through the data
    // zero len in the LTV means that there is no more data
    while ((len = *p_adv_data) != 0)
    {
        for (i = 0; i < len + 1; i++)
            *p++ = *p_adv_data++;
    }
    wiced_transport_send_data( HCI_CONTROL_LE_EVENT_ADVERTISEMENT_REPORT, tx_buf,
            (int) (p - tx_buf));
}

/*
 * hci_control_le_get_address_type
 * Used to get a BLE Address type (from either scan results or connected devices)
 */
wiced_bt_ble_address_type_t hci_control_le_get_address_type(wiced_bt_device_address_t bdaddr)
{
    int i;
    hci_control_le_scanned_device_t *p_dev;

    /* Check if this device is in the Scan Result database */
    p_dev = &hci_control_le_cb.scanned_devices[0];
    for (i = 0 ; i < HCI_CONTROL_LE_SCAN_RESULT_MAX ; i++, p_dev++)
    {
        /* Device found */
        if ((p_dev->in_use) &&
            (memcmp(p_dev->address, bdaddr, BD_ADDR_LEN) == 0))
        {
            return p_dev->address_type;
        }
    }

    /* If we reach this point, this means that the device is not in the Scan Result database */
    WICED_BT_TRACE( "Err: hci_control_le_get_address_type Failed" );
    return 0xFF;
}

/*
 * hci_control_le_scan_result_cback
 * Process advertisement packet received
 */
static void hci_control_le_scan_result_save(wiced_bt_ble_scan_results_t *p_scan_result)
{
    int i;
    hci_control_le_scanned_device_t *p_dev;

    /* Check if this device is already in the Scan Result database */
    p_dev = &hci_control_le_cb.scanned_devices[0];
    for (i = 0; i < HCI_CONTROL_LE_SCAN_RESULT_MAX; i++, p_dev++)
    {
        /* Device found */
        if ((p_dev->in_use) &&
            (memcmp(p_dev->address, p_scan_result->remote_bd_addr, BD_ADDR_LEN) == 0))
        {
            p_dev->address_type = p_scan_result->ble_addr_type;
            return;
        }
    }

    /* If we reach this point, this means that the device is not in the Scan Result database */
    /* Find a free entry and save it */
    p_dev = &hci_control_le_cb.scanned_devices[0];
    for ( i = 0 ; i < HCI_CONTROL_LE_SCAN_RESULT_MAX ; i++, p_dev++)
    {
        if (p_dev->in_use == WICED_FALSE)
        {
            p_dev->in_use = WICED_TRUE;
            memcpy(p_dev->address, p_scan_result->remote_bd_addr, BD_ADDR_LEN);
            p_dev->address_type = p_scan_result->ble_addr_type;
            WICED_BT_TRACE( "hci_control_le_scan_result_save index:%d", i);
            return;
        }
    }

    /* If we reach this point, this means that the Scan Result database is full */
    WICED_BT_TRACE( "Err: hci_control_le_scan_result_save Mem Full" );
}

/*
 * hci_control_le_encryption_changed
 */
//#define GATT_MT_DISCOVERY_COLLISION_TEST
void hci_control_le_encryption_changed(wiced_bt_dev_encryption_status_t *p_encryption_status)
{
#ifdef GATT_MT_DISCOVERY_COLLISION_TEST
    wiced_bt_gatt_status_t status;
    int conn_id;
    wiced_bt_gatt_discovery_param_t discovery_param;

    if (p_encryption_status->result != WICED_BT_SUCCESS)
        return;

    for(conn_id = 1 ; conn_id < (HCI_CONTROL_LE_CONNECTIONS_MAX + 1) ; conn_id++)
    {
        if (memcmp(hci_control_le_cb.conn[conn_id].bd_addr, p_encryption_status->bd_addr,
                BD_ADDR_LEN) == 0)
            break;
    }
    if (conn_id >= (HCI_CONTROL_LE_CONNECTIONS_MAX + 1))
        return;

    /* Perform Device Information primary service search (for collition test) */
    discovery_param.s_handle = 1;
    discovery_param.e_handle = 0xFFFF;
    status = wiced_bt_gatt_mp_send_discover(hci_control_le_cb.profile_id, conn_id,
            GATT_DISCOVER_SERVICES_ALL, &discovery_param);
    if (status != WICED_BT_GATT_SUCCESS)
    {
        WICED_BT_TRACE("hci_control_le_encryption_changed wiced_bt_gatt_mp_send_discover failed status:%d\n", status);
    }
#endif
}

/*
 * hci_control_le_gatt_cback
 */
wiced_bt_gatt_status_t hci_control_le_gatt_mp_cback(wiced_bt_gatt_evt_t event,
        wiced_bt_gatt_event_data_t *p_data)
{
    wiced_bt_gatt_status_t result = WICED_BT_GATT_SUCCESS;

    WICED_BT_TRACE("hci_control_le_gatt_mp_cback event:%d\n", event);

    switch (event)
    {
    case GATT_CONNECTION_STATUS_EVT:
        /* BLE/GATT Connection/Disconnection events */
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback Up/Down:%d BdAddr:%B conn_id:%d\n",
                p_data->connection_status.connected, p_data->connection_status.bd_addr,
                p_data->connection_status.conn_id);
        if (p_data->connection_status.connected)
        {
            hci_control_le_connection_up(&p_data->connection_status);
        }
        else
        {
            hci_control_le_connection_down(&p_data->connection_status);
        }
        break;

    case GATT_OPERATION_CPLT_EVT:
        /* GATT Read/Write complete events */
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback OpComplete conn_id:%d\n",
                p_data->operation_complete.conn_id);
        break;

    case GATT_DISCOVERY_RESULT_EVT:
        /* GATT Discovery events */
        result = hci_control_le_gatt_disc_result_handler(&p_data->discovery_result);
        break;

    case GATT_DISCOVERY_CPLT_EVT:
        /* GATT Discovery Complete events */
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback DiscComplete conn_id:%d\n",
                p_data->discovery_complete.conn_id);
        hci_control_le_cb.conn[p_data->discovery_complete.conn_id].state = LE_CONTROL_STATE_IDLE;

        hci_control_le_gatt_send_discover_complete(p_data->discovery_complete.conn_id);
        break;

    case GATT_ATTRIBUTE_REQUEST_EVT:
        /* GATT Request events (received from peer, Client, device) */
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback AttRequest conn_id:%d\n",
                p_data->attribute_request.conn_id);
        break;

    default:
        break;
    }

    return result;
}

/*
 * hci_control_le_connection_up
 */
static void hci_control_le_connection_up(wiced_bt_gatt_connection_status_t *p_connection_status)
{
    wiced_result_t sec_status;
    wiced_bt_gatt_status_t status;
    uint32_t conn_id = p_connection_status->conn_id;
    int nvram_id;
    wiced_bt_ble_sec_action_type_t encryption_type = BTM_BLE_SEC_ENCRYPT;
    uint8_t role;

    memcpy(hci_control_le_cb.conn[conn_id].bd_addr, p_connection_status->bd_addr, BD_ADDR_LEN);
    hci_control_le_cb.conn[conn_id].state      = LE_CONTROL_STATE_IDLE;
    hci_control_le_cb.conn[conn_id].conn_id    = p_connection_status->conn_id;
    hci_control_le_cb.conn[conn_id].peer_mtu   = GATT_DEF_BLE_MTU_SIZE;
    wiced_bt_dev_get_role( p_connection_status->bd_addr, &role, p_connection_status->transport);
    hci_control_le_cb.conn[conn_id].role = role;

    /* This is the Default Client application. Register every handles for Client */
    wiced_bt_gatt_mp_register_handles(hci_control_le_cb.profile_id, 0x0001, 0xFFFF, conn_id);

    /* Check if this device is already Paired */
    nvram_id = hci_control_nvram_find_id(p_connection_status->bd_addr);
    if (nvram_id == HCI_CONTROL_INVALID_NVRAM_ID)
    {
        /* The device is not yet Paired => Start Bonding (Pairing) */
        WICED_BT_TRACE("Bond/Pair dev:%B\n", p_connection_status->bd_addr);
        sec_status = wiced_bt_dev_sec_bond (p_connection_status->bd_addr,
                p_connection_status->addr_type, BT_TRANSPORT_LE, 0, NULL);
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback wiced_bt_dev_sec_bond returns:%d\n",
                sec_status);
    }
    else
    {
        /* The Device is already bonded (Paired). Start Encryption */
        WICED_BT_TRACE("Encrypt dev:%B\n", p_connection_status->bd_addr);
        sec_status = wiced_bt_dev_set_encryption (p_connection_status->bd_addr,
                BT_TRANSPORT_LE, &encryption_type);
        WICED_BT_TRACE("hci_control_le_gatt_mp_cback wiced_bt_dev_set_encryption returns:%d\n",
                sec_status);
    }

    /* Send BLE GATT Connected to MCU */
    hci_control_le_send_connect_event(p_connection_status->addr_type,
            p_connection_status->bd_addr, p_connection_status->conn_id, role);
}

/*
 * hci_control_le_connection_down
 */
static void hci_control_le_connection_down(wiced_bt_gatt_connection_status_t *p_connection_status)
{
    uint32_t conn_id = p_connection_status->conn_id;

    hci_control_le_cb.conn[conn_id].state   = LE_CONTROL_STATE_IDLE;
    hci_control_le_cb.conn[conn_id].conn_id = 0;

    /* Send BLE GATT Disconnected to MCU */
    hci_control_le_send_disconnect_evt(p_connection_status->reason, conn_id);
}

/*
 * Discovery result received from the GATT server
 */
wiced_result_t hci_control_le_gatt_disc_result_handler(wiced_bt_gatt_discovery_result_t *p_result)
{
    uint16_t conn_id = p_result->conn_id;

    WICED_BT_TRACE("Discovery result conn_id:%d state:%d\n",
            conn_id, hci_control_le_cb.conn[conn_id].state);

    switch (hci_control_le_cb.conn[conn_id].state)
    {
    case LE_CONTROL_STATE_DISCOVER_PRIMARY_SERVICES:
        if ((p_result->discovery_type == GATT_DISCOVER_SERVICES_ALL) ||
            (p_result->discovery_type == GATT_DISCOVER_SERVICES_BY_UUID))
        {
            WICED_BT_TRACE("Service s:%04x e:%04x len:%d uuid:%04x\n",
                    p_result->discovery_data.group_value.s_handle,
                    p_result->discovery_data.group_value.e_handle,
                    p_result->discovery_data.group_value.service_type.len,
                    p_result->discovery_data.group_value.service_type.uu.uuid16);
            hci_control_le_gatt_send_discovered_service(conn_id,
                    &p_result->discovery_data.group_value);
        }
        else
        {
            WICED_BT_TRACE("Err: wrong state:%d svc_disc_type:%d\n",
                    hci_control_le_cb.conn[conn_id].state,
                    p_result->discovery_type);
        }
        break;

    case LE_CONTROL_STATE_DISCOVER_CHARACTERISTICS:
        if (p_result->discovery_type == GATT_DISCOVER_CHARACTERISTICS)
        {
            WICED_BT_TRACE("Found Char - len:%d uuid:%04x, hdl:%04x\n",
                    p_result->discovery_data.characteristic_declaration.char_uuid.uu.uuid16,
                    p_result->discovery_data.characteristic_declaration.char_uuid.len,
                    p_result->discovery_data.characteristic_declaration.handle );
            hci_control_le_gatt_send_discovered_characteristic(conn_id,
                    &p_result->discovery_data.characteristic_declaration);
        }
        else
        {
            WICED_BT_TRACE("Err: wrong state:%d char_disc_type:%d\n",
                    hci_control_le_cb.conn[conn_id].state,
                    p_result->discovery_type);
        }
        break;

    case LE_CONTROL_STATE_DISCOVER_DESCRIPTORS:
        if (p_result->discovery_type == GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS)
        {
            WICED_BT_TRACE( "Found Descr - len:%d uuid:%04x handle:%04x\n",
                    p_result->discovery_data.char_descr_info.type.len,
                    p_result->discovery_data.char_descr_info.type.uu.uuid16,
                    p_result->discovery_data.char_descr_info.handle);

            hci_control_le_gatt_send_discovered_descriptor(conn_id,
                    &p_result->discovery_data.char_descr_info);
        }
        else
        {
            WICED_BT_TRACE("Err: wrong state:%d desc_disc_type:%d\n",
                    hci_control_le_cb.conn[conn_id].state,
                    p_result->discovery_type);
        }
        break;

    default:
        WICED_BT_TRACE( "ignored\n" );
        break;
    }
    return ( WICED_SUCCESS );
}

/*
 *  transfer connection event to uart
 */
void hci_control_le_send_connect_event( uint8_t addr_type, BD_ADDR addr, uint16_t con_handle, uint8_t role )
{
    int i;
    uint8_t   tx_buf [30];
    uint8_t   *p = tx_buf;

    *p++ = addr_type;
    for ( i = 0; i < 6; i++ )
        *p++ = addr[5 - i];
    *p++ = con_handle & 0xff;
    *p++ = ( con_handle >> 8 ) & 0xff;
    *p++ = role;

    wiced_transport_send_data ( HCI_CONTROL_LE_EVENT_CONNECTED, tx_buf, ( int )( p - tx_buf ) );
}


/*
 *  transfer disconnection event to UART
 */
void hci_control_le_send_disconnect_evt( uint8_t reason, uint16_t con_handle )
{
    uint8_t   tx_buf [3];
    uint8_t   *p = tx_buf;

    *p++ = con_handle & 0xff;
    *p++ = ( con_handle >> 8 ) & 0xff;
    *p++ = reason;

    wiced_transport_send_data ( HCI_CONTROL_LE_EVENT_DISCONNECTED, tx_buf, ( int )( p - tx_buf ) );
}

/*
 *  transfer GATT Discovered Service event to UART
 */
void hci_control_le_gatt_send_discovered_service(uint16_t conn_id,
        wiced_bt_gatt_group_value_t *p_discovery_group)
{
    uint8_t   tx_buf[2 + 16 + 2 + 2];
    uint8_t   *p = tx_buf;

    UINT16_TO_STREAM(p, conn_id);
    if (p_discovery_group->service_type.len == LEN_UUID_16)
        UINT16_TO_STREAM(p, p_discovery_group->service_type.uu.uuid16);
    if (p_discovery_group->service_type.len == LEN_UUID_32)
        UINT32_TO_STREAM(p, p_discovery_group->service_type.uu.uuid32);
    if (p_discovery_group->service_type.len == LEN_UUID_128)
        REVERSE_ARRAY_TO_STREAM(p, p_discovery_group->service_type.uu.uuid128, LEN_UUID_128);
    UINT16_TO_STREAM(p, p_discovery_group->s_handle);
    UINT16_TO_STREAM(p, p_discovery_group->e_handle);

    wiced_transport_send_data( HCI_CONTROL_GATT_EVENT_SERVICE_DISCOVERED, tx_buf,
            (int) (p - tx_buf));
}

/*
 *  transfer GATT Discovered Characteristic event to UART
 */
void hci_control_le_gatt_send_discovered_characteristic(uint16_t conn_id,
        wiced_bt_gatt_char_declaration_t *p_discovery_char)
{
    uint8_t   tx_buf[2 + 2 + 16 + 1 + 2];
    uint8_t   *p = tx_buf;

    UINT16_TO_STREAM(p, conn_id);
    UINT16_TO_STREAM(p, p_discovery_char->handle);
    if (p_discovery_char->char_uuid.len == LEN_UUID_16)
        UINT16_TO_STREAM(p, p_discovery_char->char_uuid.uu.uuid16);
    if (p_discovery_char->char_uuid.len == LEN_UUID_32)
        UINT32_TO_STREAM(p, p_discovery_char->char_uuid.uu.uuid32);
    if (p_discovery_char->char_uuid.len == LEN_UUID_128)
        REVERSE_ARRAY_TO_STREAM(p, p_discovery_char->char_uuid.uu.uuid128, LEN_UUID_128);
    UINT8_TO_STREAM(p, p_discovery_char->characteristic_properties);
    UINT16_TO_STREAM(p, p_discovery_char->val_handle);

    wiced_transport_send_data(HCI_CONTROL_GATT_EVENT_CHARACTERISTIC_DISCOVERED, tx_buf,
            (int) (p - tx_buf));
}

void hci_control_le_gatt_send_discovered_descriptor(uint16_t conn_id,
        wiced_bt_gatt_char_descr_info_t *p_discovery_desc)
{
    uint8_t   tx_buf [30];
    uint8_t   *p = tx_buf;

    UINT16_TO_STREAM(p, conn_id);
    if (p_discovery_desc->type.len == LEN_UUID_16)
        UINT16_TO_STREAM(p, p_discovery_desc->type.uu.uuid16);
    if (p_discovery_desc->type.len == LEN_UUID_32)
        UINT32_TO_STREAM(p, p_discovery_desc->type.uu.uuid32);
    if (p_discovery_desc->type.len == LEN_UUID_128)
        REVERSE_ARRAY_TO_STREAM(p, p_discovery_desc->type.uu.uuid128, LEN_UUID_128);
    UINT16_TO_STREAM(p, p_discovery_desc->handle);

    wiced_transport_send_data(HCI_CONTROL_GATT_EVENT_DESCRIPTOR_DISCOVERED, tx_buf,
            (int) (p - tx_buf));
}

/*
 *  transfer discover complete event to UART
 */
void hci_control_le_gatt_send_discover_complete(uint16_t conn_id)
{
    uint8_t   tx_buf [2];
    uint8_t   *p = tx_buf;

    UINT16_TO_STREAM(p, conn_id);

    wiced_transport_send_data(HCI_CONTROL_GATT_EVENT_DISCOVERY_COMPLETE, tx_buf,
            (int) (p - tx_buf));
}

