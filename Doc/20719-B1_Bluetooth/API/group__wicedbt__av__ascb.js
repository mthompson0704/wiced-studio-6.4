var group__wicedbt__av__ascb =
[
    [ "wiced_bt_acsb_lt_addr_reserved_t", "structwiced__bt__acsb__lt__addr__reserved__t.html", [
      [ "hci_status", "structwiced__bt__acsb__lt__addr__reserved__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ],
      [ "lt_addr", "structwiced__bt__acsb__lt__addr__reserved__t.html#acd419aa6e0a405f31b246110fc6415f8", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_write_param_t", "structwiced__bt__acsb__sync__train__write__param__t.html", [
      [ "hci_status", "structwiced__bt__acsb__sync__train__write__param__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ],
      [ "interval", "structwiced__bt__acsb__sync__train__write__param__t.html#a6c52a3d605c056768b59164ce8df8334", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_enable_t", "structwiced__bt__acsb__sync__train__enable__t.html", [
      [ "hci_status", "structwiced__bt__acsb__sync__train__enable__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_complete_t", "structwiced__bt__acsb__sync__train__complete__t.html", [
      [ "hci_status", "structwiced__bt__acsb__sync__train__complete__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_receive_t", "structwiced__bt__acsb__sync__train__receive__t.html", [
      [ "hci_status", "structwiced__bt__acsb__sync__train__receive__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_channel_t", "structwiced__bt__acsb__sync__train__channel__t.html", [
      [ "interval", "structwiced__bt__acsb__sync__train__channel__t.html#a6c52a3d605c056768b59164ce8df8334", null ],
      [ "lt_addr", "structwiced__bt__acsb__sync__train__channel__t.html#acd419aa6e0a405f31b246110fc6415f8", null ],
      [ "next_instant", "structwiced__bt__acsb__sync__train__channel__t.html#ae5e6310b09f81a99e7001ff732b0f6a6", null ]
    ] ],
    [ "wiced_bt_acsb_sync_train_received_t", "structwiced__bt__acsb__sync__train__received__t.html", [
      [ "bdaddr", "structwiced__bt__acsb__sync__train__received__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "channel_map", "structwiced__bt__acsb__sync__train__received__t.html#ac3b187a92ffca24663a7c683330862af", null ],
      [ "channel_nb", "structwiced__bt__acsb__sync__train__received__t.html#a9080cc0b84ce76e61f3d54b22f49ee30", null ],
      [ "channels", "structwiced__bt__acsb__sync__train__received__t.html#aac23cf3b17bfb7010645e1e22beb7b46", null ],
      [ "clock_offset", "structwiced__bt__acsb__sync__train__received__t.html#a7e83035271a1b1ad73e9366bde8bf016", null ],
      [ "hci_status", "structwiced__bt__acsb__sync__train__received__t.html#aa904d111ff996e4722c677ab290fbc77", null ],
      [ "rssi", "structwiced__bt__acsb__sync__train__received__t.html#a3b962e67ba74725bd60ca3c29f785abe", null ],
      [ "service_data", "structwiced__bt__acsb__sync__train__received__t.html#a8bd1cf81911bc4f127f5749410e20f5b", null ]
    ] ],
    [ "wiced_bt_acsb_set_transmit_t", "structwiced__bt__acsb__set__transmit__t.html", [
      [ "conn_handle", "structwiced__bt__acsb__set__transmit__t.html#a0d5ffe38d68e48d81e61fc6a4999ae68", null ],
      [ "hci_status", "structwiced__bt__acsb__set__transmit__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ],
      [ "interval", "structwiced__bt__acsb__set__transmit__t.html#a6c52a3d605c056768b59164ce8df8334", null ],
      [ "lt_addr", "structwiced__bt__acsb__set__transmit__t.html#acd419aa6e0a405f31b246110fc6415f8", null ]
    ] ],
    [ "wiced_bt_acsb_set_receive_t", "structwiced__bt__acsb__set__receive__t.html", [
      [ "bdaddr", "structwiced__bt__acsb__set__receive__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "conn_handle", "structwiced__bt__acsb__set__receive__t.html#a0d5ffe38d68e48d81e61fc6a4999ae68", null ],
      [ "hci_status", "structwiced__bt__acsb__set__receive__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ],
      [ "lt_addr", "structwiced__bt__acsb__set__receive__t.html#acd419aa6e0a405f31b246110fc6415f8", null ]
    ] ],
    [ "wiced_bt_acsb_receive_timeout_t", "structwiced__bt__acsb__receive__timeout__t.html", [
      [ "conn_handle", "structwiced__bt__acsb__receive__timeout__t.html#a0d5ffe38d68e48d81e61fc6a4999ae68", null ],
      [ "lt_addr", "structwiced__bt__acsb__receive__timeout__t.html#acd419aa6e0a405f31b246110fc6415f8", null ]
    ] ],
    [ "wiced_bt_acsb_read_buffer_size_t", "structwiced__bt__acsb__read__buffer__size__t.html", [
      [ "buffer_nb", "structwiced__bt__acsb__read__buffer__size__t.html#ab2036da68bb1b84b036f918217a319bd", null ],
      [ "buffer_size", "structwiced__bt__acsb__read__buffer__size__t.html#adae708b20ac4fe01990712c8041e560a", null ],
      [ "hci_status", "structwiced__bt__acsb__read__buffer__size__t.html#a0caaac0ab51ed36c80d4548680ecd0ab", null ],
      [ "lt_addr", "structwiced__bt__acsb__read__buffer__size__t.html#acd419aa6e0a405f31b246110fc6415f8", null ]
    ] ],
    [ "wiced_bt_acsb_data_t", "unionwiced__bt__acsb__data__t.html", [
      [ "lt_addr_reserved", "unionwiced__bt__acsb__data__t.html#a59509d5ab5dd2bf9824d040faa092d29", null ],
      [ "read_buffer_size", "unionwiced__bt__acsb__data__t.html#a42a50ad594a47e320f94fddd117a631a", null ],
      [ "receive_timeout", "unionwiced__bt__acsb__data__t.html#adcc43476744bdd96033a22dcf4885241", null ],
      [ "set_receive", "unionwiced__bt__acsb__data__t.html#a8367ebf106ad4ef3f384a4d9e3a0abb7", null ],
      [ "set_transmit", "unionwiced__bt__acsb__data__t.html#a89621b07db2f7d440439a1edfad17ef3", null ],
      [ "sync_train_complete", "unionwiced__bt__acsb__data__t.html#a3e20fd3c34e9001529fb837d49cc99ac", null ],
      [ "sync_train_enable", "unionwiced__bt__acsb__data__t.html#aa35ff568f62f3dba0f0f94aa1bbe5833", null ],
      [ "sync_train_param", "unionwiced__bt__acsb__data__t.html#a0286aa32d60fa883a49c387f45ddc9e3", null ],
      [ "sync_train_receive", "unionwiced__bt__acsb__data__t.html#a61a444e22cbfebf06db65179b685d721", null ],
      [ "sync_train_received", "unionwiced__bt__acsb__data__t.html#a5757bb1287588e54195a475f130137ec", null ]
    ] ],
    [ "wiced_bt_acsb_cback_t", "group__wicedbt__av__ascb.html#ga5d8c6ba06d757e08140b57196a698eed", null ],
    [ "wiced_bt_acsb_event_t", "group__wicedbt__av__ascb.html#gabc2088ea09c16f6e2b02eac0523584e3", [
      [ "WICED_BT_ACSB_LT_ADDR_RESERVED_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3a93cbb93a2b5f34dbe9d325c8413a4811", null ],
      [ "WICED_BT_ACSB_SYNC_TRAIN_WRITE_PARAM_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3a65ebbc9a2c8f1fb60131baf3b2b1356e", null ],
      [ "WICED_BT_ACSB_SYNC_TRAIN_ENABLE_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3a92ddf614a5643b25ae47e2e5cbef6b63", null ],
      [ "WICED_BT_ACSB_SYNC_TRAIN_RECEIVE_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3ad317c6da5c8cbbde07da14b38e660c75", null ],
      [ "WICED_BT_ACSB_SYNC_TRAIN_COMPLETE_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3adfe05d53e4520dc0f19a61f532ba2c8c", null ],
      [ "WICED_BT_ACSB_SYNC_TRAIN_RECEIVED_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3af01884f131f64d947470f68f60a0dffe", null ],
      [ "WICED_BT_ACSB_SET_TRANSMIT_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3a6cd04de29ef3c10defbe8e2d6ad4dc03", null ],
      [ "WICED_BT_ACSB_SET_RECEIVE_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3a93f6380fc1f4f5734d8135272abf0c00", null ],
      [ "WICED_BT_ACSB_RECEIVE_TIMEOUT_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3af6c516d2fea9676a437c1c2ea11af327", null ],
      [ "WICED_BT_ACSB_READ_BUFFER_SIZE_EVT", "group__wicedbt__av__ascb.html#ggabc2088ea09c16f6e2b02eac0523584e3af2de25e9cf13933423f19efbdc6dfbd9", null ]
    ] ],
    [ "wiced_bt_acsb_status_t", "group__wicedbt__av__ascb.html#ga6280fe6b224714cd4b4ff38cb9ca5705", [
      [ "WICED_BT_ACSB_STATUS_OK", "group__wicedbt__av__ascb.html#gga6280fe6b224714cd4b4ff38cb9ca5705a3898506390ad09f83c415e4822993878", null ],
      [ "WICED_BT_ACSB_STATUS_ERROR", "group__wicedbt__av__ascb.html#gga6280fe6b224714cd4b4ff38cb9ca5705a21d6ba087c95d17fccb59eddd75fe95f", null ],
      [ "WICED_BT_ACSB_STATUS_NYI", "group__wicedbt__av__ascb.html#gga6280fe6b224714cd4b4ff38cb9ca5705a17e182d3c4e19da0f57d54de74fb2efa", null ]
    ] ],
    [ "wiced_bt_acsb_enable", "group__wicedbt__av__ascb.html#gad0e41154359051da57ca0e34ac798209", null ],
    [ "wiced_bt_acsb_init", "group__wicedbt__av__ascb.html#gab2e68c0c96f7ba72731bb1c1052b7374", null ],
    [ "wiced_bt_acsb_lt_addr_reserve", "group__wicedbt__av__ascb.html#ga9b4548eb3e80f92b35da4132000b8658", null ],
    [ "wiced_bt_acsb_read_buffer_size", "group__wicedbt__av__ascb.html#gaabd0c754417662c3459048751526d1d2", null ],
    [ "wiced_bt_acsb_set_receive", "group__wicedbt__av__ascb.html#gaac4486e0ff8b2c1ac9425457437b56ee", null ],
    [ "wiced_bt_acsb_set_transmit", "group__wicedbt__av__ascb.html#ga2cb405b8ffa255ee557bbc53194d96a7", null ],
    [ "wiced_bt_acsb_sync_train_enable", "group__wicedbt__av__ascb.html#gaf841678466563c468186c7f7506fb58e", null ],
    [ "wiced_bt_acsb_sync_train_receive", "group__wicedbt__av__ascb.html#ga5458723aeb4b8bfd513108df7253f224", null ],
    [ "wiced_bt_acsb_sync_train_write_param", "group__wicedbt__av__ascb.html#ga0aa1f222ce696805221b6b1b89767fdd", null ]
];