var group__wicedbt =
[
    [ "Memory Management", "group__wiced__mem.html", "group__wiced__mem" ],
    [ "AMS Library API", "group__wiced__bt__ams__api__functions.html", "group__wiced__bt__ams__api__functions" ],
    [ "ANCS Library API", "group__wiced__bt__ancs__api__functions.html", "group__wiced__bt__ancs__api__functions" ],
    [ "Audio / Video", "group__wicedbt__av.html", "group__wicedbt__av" ],
    [ "Device Management", "group__wicedbt___device_management.html", "group__wicedbt___device_management" ],
    [ "Generic Attribute (GATT)", "group__wicedbt__gatt.html", "group__wicedbt__gatt" ],
    [ "Human Interface Device (HID)", "group__wiced__bt__hid.html", "group__wiced__bt__hid" ],
    [ "Logical Link Control and Adaptation Protocol (L2CAP)", "group__l2cap.html", "group__l2cap" ],
    [ "OBEX", "group__wicedbt__obex.html", "group__wicedbt__obex" ],
    [ "RFCOMM", "group__rfcomm__api__functions.html", "group__rfcomm__api__functions" ],
    [ "Synchronous Connection Oriented (SCO) Channel", "group__sco__api__functions.html", "group__sco__api__functions" ],
    [ "Service Discovery Protocol (SDP)", "group__sdp.html", "group__sdp" ],
    [ "Framework", "group__wicedbt___framework.html", "group__wicedbt___framework" ],
    [ "ANC Library API", "group__wiced__bt__anc__api__functions.html", "group__wiced__bt__anc__api__functions" ],
    [ "ANS Library API", "group__wiced__bt__ans__api__functions.html", "group__wiced__bt__ans__api__functions" ],
    [ "BAC Library API", "group__wiced__bt__bac__api__functions.html", "group__wiced__bt__bac__api__functions" ],
    [ "HRC Library API", "group__wiced__bt__hrc__api__functions.html", "group__wiced__bt__hrc__api__functions" ],
    [ "HRS Library API", "group__wiced__bt__hrs__api__functions.html", "group__wiced__bt__hrs__api__functions" ],
    [ "BLE Mesh", "group__wiced__bt__mesh.html", "group__wiced__bt__mesh" ]
];