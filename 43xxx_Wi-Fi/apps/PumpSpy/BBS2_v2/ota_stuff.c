

typedef struct wiced_app_s
{
        uint8_t           app_id;                 /**< Application Identifier                   */
        uint32_t          offset;                 /**< Offset from the start of the application */
        uint32_t          last_erased_sector;     /**< Last Erased Sector                       */
        image_location_t  app_header_location;    /**< Location of Application header           */
} wiced_app_t;


typedef struct
{
    image_location_id_t id;
    union
    {
        fixed_location_t internal_fixed;
        fixed_location_t external_fixed;
        char             filesystem_filename[32];
    } detail;
} image_location_t;

typedef struct
{
        uint32_t location;
        uint32_t size;
} fixed_location_t;