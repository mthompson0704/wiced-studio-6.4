# Define the base chip family name
BASE_NAME = 20735

# Define patch entries
NUM_PATCH_ENTRIES = 192
PATCH_ENTRY_SIZE  = 4

# Define 20735 common limits
# Begin address of ROM
IROM_BEGIN_ADDR		= 0x00000000
# Available ROM = 2M
IROM_LENGTH			= 0x200000
# Begin address of RAM
SRAM_BEGIN_ADDR		= 0x00200000
# Available RAM = 320K
SRAM_LENGTH			= 0x50000
# Length of patch table = 1024 bytes
PATCH_TABLE_LENGTH	= ($(NUM_PATCH_ENTRIES) * $(PATCH_ENTRY_SIZE))
# Begin address of AON.
AON_BEGIN_ADDR      = 0x280000
# Length of AON
AON_LENGTH          = 0x4000
# This is a cortex m4
CHIP_CORE           = cortex-m4
# Core specific options for this chip
CORE_COMMON_OPTIONS = -mcpu=$(CHIP_CORE) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -mthumb -mlittle-endian
