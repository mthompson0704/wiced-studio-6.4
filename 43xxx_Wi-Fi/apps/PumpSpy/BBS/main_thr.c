/*
 * Copyright 2016, PumpSpy
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of PumpSpy.
 */

#include <stdlib.h>
#include "wiced.h"
#include "http.h"
#include "string.h"
#include "math.h"
//#include "wwd_debug.h"
#include "wiced_framework.h"
#include "wiced_ota_server.h"
//#include "default_wifi_config_dct.h"
//#include "network_config_dct.h"
#include "platform.h"
//#include "aes.h"
#include "waf_platform.h"
//#include "wiced_management.h"

#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "get_deviceid.h"
#include "control_leds.h"
#include "config_button_handler.h"
#include "restful_methods.h"
#include "new_firmware.h"
#include "dns_lookup.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

//#define STOP_APP_HERE
//#define NO_LEDS
//#define ENABLE_SYSTEM_MONITOR
//#define TEST_AES_128
//#define REMOTEDEBUG

#define FIRMWARE_VERSION 3.00
#define FIRMWARE_VERSION_STRING "3.00 - BBS - revision 1   "




/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/** Runs device configuration (if required)
 *
 * @param[in] config  : an array of user configurable variables in configuration_entry_t format.
 *                      The array must be terminated with a "null" entry {0,0,0,0}
 *
 * @return    WICED_SUCCESS
 */
extern wiced_result_t pumpspy_configure_device( const configuration_entry_t* config );

/******************************************************
 *               Static Variable Definitions
 ******************************************************/
static const configuration_entry_t app_config[] =
{
    {"NewConfigurationFlag", DCT_OFFSET(config_mode_app_dct_t, NewConfigurationFlag), 4, CONFIG_UINT32_DATA },
    {"DeviceID", DCT_OFFSET(config_mode_app_dct_t, DeviceID), 8, CONFIG_UINT32_DATA },
    {"PumpSpyHostName", DCT_OFFSET(config_mode_app_dct_t, PumpSpyHostName), 100, CONFIG_STRING_DATA },
    {"PumpSpyHostName", DCT_OFFSET(config_mode_app_dct_t, PumpSpyHostPort), 2, CONFIG_UINT16_DATA },
    {"startup_delay", DCT_OFFSET(config_mode_app_dct_t, startup_delay), 2, CONFIG_UINT16_DATA },
    {0,0,0,0}
};


static const wiced_ip_setting_t device_init_ip_settings =
{
	    INITIALISER_IPV4_ADDRESS( .ip_address, MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
	    INITIALISER_IPV4_ADDRESS( .netmask,    MAKE_IPV4_ADDRESS( 255,255,255,  0 ) ),
	    INITIALISER_IPV4_ADDRESS( .gateway,    MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
};
//
// serial port control
//

wiced_uart_config_t uart_config =
{
    .baud_rate    = 115200,
    .data_width   = DATA_WIDTH_8BIT,
    .parity       = NO_PARITY,
    .stop_bits    = STOP_BITS_1,
    .flow_control = FLOW_CONTROL_DISABLED,
};

#define RX_BUFFER_SIZE 100
#define SERIAL_BUFFER_SIZE 250
#define JSON_STRING_SIZE 250
//#define TEST_STR "."
wiced_ring_buffer_t rx_buffer;
uint8_t rx_data[RX_BUFFER_SIZE];
uint8_t serial_buffer[SERIAL_BUFFER_SIZE];
uint8_t json_string[JSON_STRING_SIZE];
#define SERIAL_THREAD_PRIORITY (WICED_DEFAULT_LIBRARY_PRIORITY)
#define SERIAL_STACK_SIZE      (100)
static wiced_thread_t serial_thread;
static uint32_t thread_count = 0;
uint8_t json_string_index = 0;
uint8_t serial_end_index = 0;
uint8_t serial_start_index = 0;

/******************************************************
 *               Function Definitions
 ******************************************************/
static void get_json_from_serial_port( )
{
    char c; //='a';
    wiced_bool_t json_string_started = WICED_FALSE;
    wiced_result_t serial_result=0;
    uint32_t expected_data_size = 1;
	while (1)
	{
//		wiced_rtos_delay_milliseconds(1000);
        serial_result = wiced_uart_receive_bytes( STDIO_UART, &c, &expected_data_size, 100 );
//    	WPRINT_APP_INFO( ( "uart_recieve result %d\r\n", serial_end_index ) );
        if (serial_result == WICED_SUCCESS)
        {
        	if (c == '{')
        	{
        		json_string_started = WICED_TRUE;
        	}
        	if (json_string_started)
        	{
				if (c == '"')
				{
					serial_buffer[serial_end_index++] = '\\';
				}
				if (serial_end_index >= SERIAL_BUFFER_SIZE)
					serial_end_index = 0;
				serial_buffer[serial_end_index++] = c;
		//        wiced_uart_transmit_bytes( STDIO_UART, &c, 1 );
				expected_data_size = 1;
				if (serial_end_index >= SERIAL_BUFFER_SIZE)
					serial_end_index = 0;
				if (c == '}')
				{
					json_string_started = WICED_FALSE;
				}
        	}
        }
//		else
//		{
//			WPRINT_APP_INFO( ( "timeout probably result %d serial_end_index %d c %d\r\n", serial_result, serial_end_index, (int)c ) );
//		}
	}
    WICED_END_OF_CURRENT_THREAD( );
}

#define JSON_post_bbs_json \
    "{\"deviceid\": %ld%ld, " \
    "\"utcunixtime\": %ld000," \
    "\"json\": \"%s\" }"

#define GET_bbs_parameters \
    "GET /bbs_parameters/%ld%ld HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"


void process_microchip_data(uint64_t utcunixtime)
{
	wiced_bool_t endbrace_found = WICED_FALSE;
	wiced_bool_t transfer_done = WICED_FALSE;
//	WPRINT_APP_INFO( ( "." ) );

	if (serial_start_index != serial_end_index)
	{
		WPRINT_APP_INFO( ( "serial data found - serial_start_index=%d serial_end_index=%d json_string_index=%d\r\n", serial_start_index, serial_end_index, json_string_index ) );
		//
		// move from serial_buffer to json_buffer
		//
		//
		while ((endbrace_found == WICED_FALSE) && (transfer_done == WICED_FALSE))
		{
			json_string[json_string_index] = serial_buffer[serial_start_index];
			if ((int)json_string[json_string_index] == 125)
				endbrace_found = WICED_TRUE;
			json_string_index++;
			serial_start_index++;
			if (serial_start_index >= SERIAL_BUFFER_SIZE)
				serial_start_index = 0;
			if (json_string_index >= JSON_STRING_SIZE)
				json_string_index = 0;
			if (serial_start_index == serial_end_index)
				transfer_done = WICED_TRUE;
		}
		WPRINT_APP_INFO( ( "serial data transferred - serial_start_index=%d serial_end_index=%d json_string_index=%d\r\n", serial_start_index, serial_end_index, json_string_index ) );
	}
	if (endbrace_found == WICED_TRUE)
	{
		WPRINT_APP_INFO( ( "end brace found - serial_start_index=%d serial_end_index=%d json_string_index=%d\r\n", serial_start_index, serial_end_index, json_string_index ) );
		//
		// post it
		//
	    clear_post_buffers();
	    WPRINT_APP_INFO( ( "POST /bbs_json %s\r\n", json_string));
	    sprintf(json, JSON_post_bbs_json, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), (long unsigned int)(utcunixtime/1000), json_string);
	    WPRINT_APP_INFO(("POST /bbs_json |%s| len=%d\r\n", json, strlen(json)));
	    sprintf(post, POST_STRING, "/bbs_json", bearer_token, strlen(json), json);
	    WPRINT_APP_INFO(("sprintf done\r\n"));
	    WPRINT_APP_INFO(( post )); //This result looks correct
	    post_the_post();
	    //
		// reset the string buffer
	    //
		memset(json_string, 0, JSON_STRING_SIZE);
		json_string_index = 0;
		WPRINT_APP_INFO( ( "Post done - serial_start_index=%d serial_end_index=%d json_string_index=%d\r\n", serial_start_index, serial_end_index, json_string_index ) );

	}
}

wiced_result_t get_parameters()
{
//    char *p;
//    char *p2;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /bbs_parameters \r\n"));
    sprintf(post, GET_bbs_parameters, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
        	return WICED_ERROR;
    }
    //
    // it prints the json string - so it's sent to microchip
    //
    return result;
}

/******************************************************
 *               Main - application starts here
 ******************************************************/
//
// Power-on
//
void application_start( )
{
//    wiced_utc_time_t server_time;
    uint64_t server_time;
//    uint64_t config_time;
    uint64_t ping_timer = 0;
    uint64_t set_clock_timer = 0;
//    uint64_t high_water_timer = 0;
    uint64_t new_firmware_timer = 0;
    uint64_t new_parameters_timer = 0;
    uint64_t reset_24hr_timer = 0;
//    wiced_bool_t high_water_sent_to_server = WICED_FALSE;
//    wiced_bool_t low_water_sent_to_server = WICED_TRUE;
//    wiced_bool_t ac_loss_sent_to_server = WICED_FALSE;
//    wiced_bool_t ac_loss_recovery_sent_to_server = WICED_FALSE;
//    wiced_bool_t high_water_signal = WICED_FALSE;
//    wiced_bool_t last_high_water_signal = WICED_FALSE;
//    wiced_bool_t pump_is_running = WICED_FALSE;
//    int i;
//    uint16_t adc_baseline = 0;
    wiced_bool_t high_water_passed = WICED_FALSE;
    wiced_bool_t ac_loss_passed = WICED_FALSE;
    wiced_bool_t ac_current_passed = WICED_FALSE;
    wiced_bool_t test_mode_value_changed = WICED_TRUE;
    wiced_bool_t wifi_passed = WICED_TRUE;
    wiced_bool_t last_config_button_passed = WICED_FALSE;
	uint32_t current_time = 0;
	uint32_t last_time = 0;
	uint32_t new_firmware_block_number = 0;
	wiced_bool_t new_firmware_download_in_progress = WICED_FALSE;
	int32_t ap_signal_strength = 0;
//	int number_of_ping_retries = 0;
	wiced_bool_t serial_setup = WICED_FALSE;

    //
    // Init the wiced stack
    //
    wiced_init( );
    //
    // display app version
    //
    WPRINT_APP_INFO( ( "Starting Version %s \r\n", FIRMWARE_VERSION_STRING) );

    //
    //**** initialize the AD-C
    //
//*****    result = wiced_adc_init( PUMP_CURRENT, 480 );
//*****    WPRINT_APP_INFO( ( "PUMP CURRENT init result %d\r\n", result ) );
    //
    // initialize the IRQ's
    // eval board buttons are normally high
    //
//    result = wiced_gpio_input_irq_enable( HIGH_WATER, IRQ_TRIGGER_FALLING_EDGE, high_water_handler, NULL );
//    WPRINT_APP_INFO( ( "high water irq FE %d\r\n", result ) );
    result = wiced_gpio_input_irq_enable( CONFIG_BUTTON, IRQ_TRIGGER_FALLING_EDGE, config_button_handler, NULL);
    WPRINT_APP_INFO( ( "config irq falling enable %d\r\n", result ) );
    //
    // Init the watch dog
    //
    result = platform_watchdog_init();
    WPRINT_APP_INFO( ( "watchdog init result %d\r\n", result ) );
    result = wiced_watchdog_kick();
    WPRINT_APP_INFO( ( "watchdog kick result %d\r\n", result ) );

    /*
    // display the external flash app segments
    //
    #define DCT_FR_APP_INDEX            ( 0 )
    #define DCT_DCT_IMAGE_INDEX         ( 1 )
    #define DCT_OTA_APP_INDEX           ( 2 )
    #define DCT_FILESYSTEM_IMAGE_INDEX  ( 3 )
    #define DCT_WIFI_FIRMWARE_INDEX     ( 4 )
    #define DCT_APP0_INDEX              ( 5 )
    #define DCT_APP1_INDEX              ( 6 )
    #define DCT_APP2_INDEX              ( 7 )
    */

    wiced_app_t app2;
	uint32_t    current_size;
	int app_index;
	for (app_index = DCT_FR_APP_INDEX; app_index <= DCT_APP2_INDEX; app_index++)
	{
		if (wiced_framework_app_open( app_index, &app2 ) != WICED_SUCCESS)
		{
			printf("app [%d] open error\r\n", app_index);
		}
		if (wiced_framework_app_get_size( &app2, &current_size ) != WICED_SUCCESS)
		{
			printf("app [%d] get sized error\r\n", app_index);
		}
		printf("app [%d] size %ld\r\n", app_index, current_size);
		wiced_framework_app_close( &app2 );
	}


    //
    // LEDs - red
    //
	control_leds(LED_RED);
    //
    // get the deviceid from the mac address
    //
    deviceid = get_deviceid();
    //
    // read the dct's
    //
    read_app_dct();
    read_network_dct();
    read_wifi_dct();
	print_app_dct();
	print_wifi_dct();
	print_network_dct();
    //
    // Check the new configuration flag
    //
    WPRINT_APP_INFO(("New config flag %08lx\r\n", dct.NewConfigurationFlag));
    if (dct.NewConfigurationFlag == 0x0F0F0F0F)
    {
        WPRINT_APP_INFO(("New Configuration \r\n"));
        system_state = STATE_PROVISIONING;
    }
    else if (dct.NewConfigurationFlag == 0x0D0C0B0A)
	{
		WPRINT_APP_INFO(("OTA Configuration \r\n"));
		system_state = STATE_OTA;
	}
    else if (dct.NewConfigurationFlag == 0x0B0B0B0B)
	{
		WPRINT_APP_INFO(("TEST_MODE Configuration \r\n"));
		system_state = STATE_TEST_MODE;
	}
    else if (dct.NewConfigurationFlag == 0x0A0B0C0D)
    	{
    		WPRINT_APP_INFO(("STATE_INITIALIZING Configuration \r\n"));
    		system_state = STATE_INITIALIZING;
    	}
    else
    {
		dct.NewConfigurationFlag = 0x0B0B0B0B;
		write_app_dct();
		//
		// reset the DCT
		//1
        configure_app_dct();
        configure_network_dct();
        configure_wifi_dct();
		WPRINT_APP_INFO(("TEST_MODE Configuration \r\n"));
		system_state = STATE_TEST_MODE;

    }
#ifdef REMOTEDEBUG
    //
    // *** test mode
//	dct.NewConfigurationFlag = 0x0D0C0B0A; // OTA
	dct.NewConfigurationFlag = 0x0A0B0C0D; // Initializing
	write_app_dct();
    configure_app_dct();
	configure_network_dct();
    configure_wifi_dct();
	WPRINT_APP_INFO(("Initialization Configuration \r\n"));
//	system_state = STATE_OTA;
	system_state = STATE_INITIALIZING;
	//
	// *** test mode
#endif
    //
    // set up timers
    //
	current_time = host_rtos_get_time();
	last_time = host_rtos_get_time();
    //
    // this is the beginning of application
    //
#ifdef STOP_APP_HERE
    main_thread_counter_enabled = WICED_FALSE;
    while ( 1 )
    {
        wiced_rtos_delay_milliseconds( 1000 );
    }
#endif
    while ( 1 )
    {

    	//
    	// increment the main_thread_counter every 12 seconds
    	// system_monitor decrements every 12 seconds
    	// if main loop stops, system_monitor will reboot when
    	// main_thread_counter <= 0
    	//
    	current_time = host_rtos_get_time();
//		WPRINT_APP_INFO(("current_time %ld\r\n", current_time));
    	if ((current_time - last_time) > 12000)
    	{
    		last_time = current_time;
			main_thread_counter = 5;
//			WPRINT_APP_INFO(("main_thread_counter %d\r\n", main_thread_counter));
    	}
        //
        // main state machine
        //
    	switch (system_state)
    	{
    	//
    	// start up the network
    	//
    	case STATE_INITIALIZING:
    		//
    		// delay based on dct.startup_delay
    		//
            WPRINT_APP_INFO(("Delay Starup %d milliseconds\r\n", dct.startup_delay*1000));
            wiced_rtos_delay_milliseconds( dct.startup_delay*1000 );
            //
            // turn on system monitor
            //
            main_thread_counter_enabled = WICED_TRUE;
    		//
    		// Bring up the STA interface
    		//
            WPRINT_APP_INFO(("brining up the STA interface\r\n"));
            control_leds(LED_RED);
            result = -1;
			//
			// bring up newtwork
			//
			result = wiced_network_up(WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL);
			WPRINT_APP_INFO(("STA interface result = %d\r\n", result));
            if (result != WICED_SUCCESS)
            {
            	//
            	// configure the delay
            	//
            	dct.startup_delay++;
            	if (dct.startup_delay > 10)
            		dct.startup_delay = 0;
            	write_app_dct();
				system_state = STATE_INITIALIZING;
				//
				// reboot
				//
				wiced_framework_reboot();
				break;
            }
            else // (result == WICED_SUCCESS)
            {
                //
                // get the deviceid from the mac address
                //
                deviceid = get_deviceid();
				//
				// read and print the app & network dct (don't think need to read wifi) configuration record
				//
				read_app_dct();
				print_app_dct();
				//
				// read and print the app dct configuration record
				//
				read_network_dct();
				print_network_dct();
				//
				// read and print the wifi dct configuration record
				//
				read_wifi_dct();
				print_wifi_dct();
				//
				// look up the DNS
				//
				result = dns_lookup();
				//
				// get the bearer token
				//
				get_bearer_token();
				//
				// get and set the time
				//
				get_system_time();

				//
				// set the timers
				//
			    wiced_time_get_utc_time_ms  (&server_time);
			    set_clock_timer = server_time;
			    ping_timer = server_time;
			    new_firmware_timer = server_time + 30000;
			    new_parameters_timer = server_time + 60000;
			    reset_24hr_timer = server_time;
				//
				// set the 5 second timer
				//
				wiced_time_get_utc_time_ms  (&server_time);
				ping_timer = server_time;
				result = post_ping(PINGS_WakeUp, ping_timer, (float)FIRMWARE_VERSION);
				WPRINT_APP_INFO(("Initialization Ping result = %d\r\n", result));
				if (result != WICED_SUCCESS)
				{
	            	//
	            	// configure the delay
	            	//
	            	dct.startup_delay++;
	            	if (dct.startup_delay > 10)
	            		dct.startup_delay = 0;
	            	write_app_dct();
					system_state = STATE_INITIALIZING;
					//
					// reboot
					//
					wiced_framework_reboot();
					break;
				}
				//
				// POST /pump_outlet_alerts
				//
				post_outlet_alerts(AC_LOSS_ALERT, server_time, 0);
				//
			    // set system state
			    //
	    		system_state = STATE_RUNNING;
				//
				// configuration is complete
				// LEDs red off, green on
				//
				control_leds(LED_GREEN);
	    		break;
            }
    		break;
    	//
    	// STA up running normally
    	//
    	case STATE_RUNNING:
            //
            // turn on system monitor
            //
            main_thread_counter_enabled = WICED_TRUE;
            //
            // setup the serial thread
            //
            if (!serial_setup)
            {
				//
				// init the ring buffer & UART & json_string;
				//
				ring_buffer_init(&rx_buffer, rx_data, RX_BUFFER_SIZE );
				wiced_uart_init( STDIO_UART, &uart_config, &rx_buffer );
				memset(json_string, 0, JSON_STRING_SIZE);
				json_string_index = 0;
				WPRINT_APP_INFO( ( "Ring buffer set up  \r\n") );
				//
				// start the serial thread
				//
				WPRINT_APP_INFO( ( "Starting Serial Thread %ld\r\n", thread_count ) );
				result = wiced_rtos_create_thread(&serial_thread, SERIAL_THREAD_PRIORITY, "serial_thread", get_json_from_serial_port, (uint32_t)SERIAL_STACK_SIZE, (void *)thread_count);
				if (result != WICED_SUCCESS)
					WPRINT_APP_INFO( ( "thread start ERROR result=%d\r\n", result ) );
				else
					WPRINT_APP_INFO( ( "thread start success ERROR result=%d\r\n", result ) );
				//
				// let microchip run
				//
		//		wiced_gpio_output_high( MCP_MASTER_RESET );
				//
				// set flag
				//
				serial_setup = WICED_TRUE;
            }
            //
            // check network status
            //
            if (wiced_network_is_up(WICED_STA_INTERFACE) != WICED_TRUE)
            {
				control_leds(LED_RED);
            	system_state = STATE_INITIALIZING;
            }
            else
            {
//                WPRINT_APP_INFO( ( "stored ap list[0].rssi = %d\r\n", dct_wifi_config.stored_ap_list[0].details.signal_strength ) );
				//
				// get the current system time
				//
				wiced_time_get_utc_time_ms  (&server_time);
				//
				// look for serial datat
				//
				process_microchip_data(server_time);
				//
				// Ping timer check - 2 minutes
				//
				if ((server_time - ping_timer) > PING_TIMER_LIMIT)
				{
					//
					// POST /pings/baseline
					//

					ping_timer = server_time;
//					adc_baseline = set_baseline_current();
//					result = post_ping(PINGS_BaseCurrent, ping_timer, (float)adc_baseline);
					ap_signal_strength = 0;
					wwd_wifi_get_rssi(&ap_signal_strength);
					WPRINT_APP_INFO(("rssi %d\r\n", (int)ap_signal_strength));
					result = post_ping(PINGS_RSSI, ping_timer, (float)ap_signal_strength);
					wiced_time_get_utc_time_ms  (&server_time);
					WPRINT_APP_INFO(("/pings POST time %d\r\n", (int)(server_time - ping_timer)));
					if (result != WICED_SUCCESS)
					{
		            	//
		            	// configure the delay
		            	//
		            	dct.startup_delay++;
		            	if (dct.startup_delay > 10)
		            		dct.startup_delay = 0;
		            	write_app_dct();
						system_state = STATE_INITIALIZING;
						//
						// reboot
						//
						wiced_framework_reboot();
						break;
					}
				}
				//
				// check for get parameters timer - 2 minutes
				//
				if ((server_time - new_parameters_timer) > NEW_PARAMETERS_TIMER_LIMIT)
				{
					//
					// get parameters
					//
					new_parameters_timer = server_time;
					result = get_parameters();
				}
				//
				// check for 24 hour reset timer
				//
				if ((server_time - reset_24hr_timer) > RESET_24HR_TIME)
				{
					result = post_ping(PINGS_DailyReset, ping_timer, (float)0.0);
		            wiced_framework_reboot();
				}
				//
				// New Firmware Time  - 2 minutes
				//
				if ((server_time - new_firmware_timer) > NEW_FIRMWARE_TIMER_LIMIT)
				{
					//
					// check if there is a download in progress
					//
					if (new_firmware_download_in_progress == WICED_FALSE)
					{
						//
						// GET /new_firmware/{deviceid}
						//
						new_firmware_timer = server_time;
						result = get_new_firmware();
						if (result == WICED_SUCCESS)
						{
							if ((new_firmware_available == WICED_TRUE) && (new_firmware_approved == WICED_TRUE) && (new_firmware_downloaded == WICED_FALSE))
							{
								WPRINT_APP_INFO(( "new firmware is available and approved and not downloaded approved  %d\r\n", new_firmware_approved));
								new_firmware_download_in_progress = WICED_TRUE;
								new_firmware_block_number = 0;
								result = put_new_firmware(1, server_time, DOWNLOAD_STARTED);
							}
						}
						else
						{
							WPRINT_APP_INFO(( "no new firmware shown on server\r\n"));
						}
					}
				}
            	if (new_firmware_download_in_progress == WICED_TRUE)
				{
					result = get_new_firmware_download_block(new_firmware_block_number);
					if (result == WICED_SUCCESS)
					{
						result = process_new_firmware_block(new_firmware_block, new_firmware_size_in_bytes, new_firmware_size_in_blocks, new_firmware_block_size, new_firmware_block_number);
						if (result == WICED_SUCCESS)
						{
							if (new_firmware_block_number == new_firmware_size_in_blocks)
							{
								wiced_time_get_utc_time_ms  (&server_time);
								result = put_new_firmware(1, server_time, COMPLETE_AND_REBOOTING);
								if (result == WICED_SUCCESS)
									WPRINT_APP_INFO(( "/new_firmware posted r\n"));
								else
									WPRINT_APP_INFO(( "/new_firmware put failed r\n"));
								//
								// restarting
								//
								WPRINT_APP_INFO(("Restarting.. \r\n"));
								wiced_framework_reboot();

							}
							else
							{
								new_firmware_block_number++;
							}
						}
						else
						{
							result = put_new_firmware(1, server_time, FLASH_MEMORY_ERROR);
							close_firmware_app();
							new_firmware_download_in_progress = WICED_FALSE;
							new_firmware_block_number = 0;
						}
					}
					else
					{
						result = put_new_firmware(1, server_time, DOWNLOAD_FAILED);
						close_firmware_app();
						new_firmware_download_in_progress = WICED_FALSE;
						new_firmware_block_number = 0;
					}
					//
					// check for the change in donwload status
					//
					if ((new_firmware_block_number%100) == 0)
					{
						result = get_new_firmware();
						if (result == WICED_SUCCESS)
						{
							if ((new_firmware_available == WICED_FALSE) || (new_firmware_approved == WICED_FALSE))
							{
								result = put_new_firmware(1, server_time, DOWNLOAD_HALTED);
								WPRINT_APP_INFO(( "new firmware download cancelled %d\r\n", new_firmware_approved));
								close_firmware_app();
								new_firmware_download_in_progress = WICED_FALSE;
								new_firmware_block_number = 0;
							}
						}
					}
				}

				//
				// get_time timer check - 4 hours
				//
				if ((server_time - set_clock_timer) > SET_CLOCK_TIMER_LIMIT)
				{
					//
					// get and set the time
					//
					set_clock_timer = server_time;
					get_system_time();
					//
					// get the bearer token
					//
					get_bearer_token();
				}
            }
    		break;
		//
		// provisioning AP state
		//
    	case STATE_PROVISIONING:
            WPRINT_APP_INFO(("Provisioning Mode \r\n"));
            //
            // turn off system monitor
            //
            main_thread_counter_enabled = WICED_FALSE;
			//
            // LEDs - red / geeen
            //
            control_leds(LED_RED_GREEN);
            configure_app_dct();
            configure_network_dct();
            configure_wifi_dct();

            //
            // run the config AP
            //
            WPRINT_APP_INFO(("turning on config AP \r\n"));
//            result = wiced_configure_device( app_config );
            result = pumpspy_configure_device( app_config );
            if ( result != WICED_SUCCESS )
            {
                WPRINT_APP_INFO(( "Unable start config AP server. Error = [%d]\r\n", result ));
                if (result == WICED_OUT_OF_HEAP_SPACE)
                    WPRINT_APP_INFO(( "Out of heap space.\r\n"));
                if (result == WICED_BADARG)
                    WPRINT_APP_INFO(( "bad arg.\r\n"));
               return;
            }
            read_app_dct();
            WPRINT_APP_INFO(("wiced_configure_device = %d\r\n", result));
            //
            // get deviceid
            //
            deviceid = get_deviceid();
            //
            // set the flag, and device id and write to DCT
            //
            dct.NewConfigurationFlag = 0x0A0B0C0D; //0xFFFFFFFF;
            dct.DeviceID = deviceid;
            write_app_dct();
            //
            // reboot
            //
            wiced_framework_reboot();
    		break;
		//
		// OTA state
		//
    	case STATE_OTA:
            //
            // turn off system monitor
            //
            main_thread_counter_enabled = WICED_FALSE;
    		//
    		// reset config flag
    		//
            dct.NewConfigurationFlag = 0x0A0B0C0D;
            write_app_dct();
            //
            // turn off STA
            //
			WPRINT_APP_INFO(( "bring down  WCED_STA_INTERFACE\n" ));
			result = wiced_network_down(WICED_STA_INTERFACE);
			if ( result != WICED_SUCCESS )
			{
				WPRINT_APP_INFO(( "Unable bring down WICED_STA_INTERFACE. Error = [%d]\r\n", result ));
			}
			WPRINT_APP_INFO(( "Start WCED_AP_INTERFACE\n" ));
            wiced_network_up( WICED_AP_INTERFACE, WICED_USE_INTERNAL_DHCP_SERVER, &device_init_ip_settings );
			if ( result != WICED_SUCCESS )
			{
				WPRINT_APP_INFO(( "Unable bring down WICED_AP_INTERFACE. Error = [%d]\r\n", result ));
			}
            wiced_ota_server_start( WICED_AP_INTERFACE );
            while ( 1 )
            {
                wiced_rtos_delay_milliseconds( 100 );
            }
            //
            // reboot
            //
			WPRINT_APP_INFO(( "rebooting \n" ));
            wiced_framework_reboot();
    		break;
    	case STATE_TEST_MODE:
            //
            // turn off system monitor
            //
            main_thread_counter_enabled = WICED_FALSE;
            //
			// reset Config Flag - boot to provisioning mode next time
			//
    		if (dct.NewConfigurationFlag != 0x0F0F0F0F)
    		{
    			dct.NewConfigurationFlag = 0x0F0F0F0F;
    			write_app_dct();
        		//
        		// reset the DCT
        		//
                configure_app_dct();
                configure_network_dct();
                configure_wifi_dct();
    		}
            //
            // display test parameters
            //
    		if (test_mode_value_changed == WICED_TRUE)
    		{
				WPRINT_APP_INFO(("\033[2J\033[1;1H"));
				WPRINT_APP_INFO(( "DeviceID =      %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
				WPRINT_APP_INFO(( "Firmware Ver. = %s\r\n", FIRMWARE_VERSION_STRING));
				WPRINT_APP_INFO(( "Wi-Fi Scan =    %d\r\n", wifi_passed ));
				WPRINT_APP_INFO(( "Wi-Fi Memory =  %d\r\n", check_wifi_dct() ));
				WPRINT_APP_INFO(( "High Water =    %d\r\n", high_water_passed ));
				WPRINT_APP_INFO(( "Config Button = %d\r\n", config_button_passed ));
				WPRINT_APP_INFO(( "Power Fail =    %d\r\n", ac_loss_passed ));
				WPRINT_APP_INFO(( "Current Sense = %d\r\n", ac_current_passed ));
				WPRINT_APP_INFO(( "Current =       %d, %f0.0\r\n", pump_run_current, (double)pump_run_current/215 ));
				WPRINT_APP_INFO(( "\r\n" ));
				test_mode_value_changed = WICED_FALSE;
    		}
			// .75A - 130-230
			// 5.8A - 1200
			// 10.5 - 2000
			// HW Test
			//
    		//
    		// Wi-Fi Config & DCT Memory Test
    		//
            //
            // pump
            //
			pump_run_current = 0;
//			monitor_pump();
			if ((pump_run_current < 430) && (pump_run_current > 215))
			{
				if (ac_current_passed == WICED_FALSE)
				{
					test_mode_value_changed = WICED_TRUE;
				}
				ac_current_passed = WICED_TRUE;
			}
			else
			{
				if (ac_current_passed == WICED_TRUE)
				{
					test_mode_value_changed = WICED_TRUE;
				}
				ac_current_passed = WICED_FALSE;
			}
			//
			// config button
			//
			if ((config_button_passed == WICED_TRUE) && (last_config_button_passed == WICED_FALSE))
			{
				test_mode_value_changed = WICED_TRUE;
				last_config_button_passed = config_button_passed;
			}
			//
			// set the LEDs
			//
//			if ((ac_current_passed == WICED_TRUE) && (config_button_passed == WICED_TRUE) &&
//					(high_water_passed == WICED_TRUE) && (wifi_passed == WICED_TRUE) )
	            control_leds(LED_GREEN);
//			else
	            control_leds(LED_RED);
			//
			// delay
			//
            wiced_rtos_delay_milliseconds( 100 );

    		break;
    	}

//        wiced_rtos_delay_milliseconds(500);
    }
    //
    // de-init the wiced stack and end
    //
    wiced_deinit();
}

