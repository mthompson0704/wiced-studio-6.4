/*
 * Copyright 2017, Inventek Systems. All Rights Reserved. This software, associated documentation
 * and materials ("Software"), referenced and provided with this documentation is owned by Inventek
 * Systems and is protected by and subject to worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions. Therefore, you may use this
 * Software only as provided in the license agreement accompanying the software package from which
 * you obtained this Software ("EULA"). If no EULA applies, Inventek Systems hereby grants you a
 * personal, non-exclusive, non-transferable license to copy, modify, and compile the Software source
 * code solely for use in connection with Inventek's integrated circuit products.
 *
 * Any reproduction, modification, translation, compilation, or representation of this Software except
 * as specified above is prohibited without the express written permission of Inventek.
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE.
 *
 * Inventek reserves the right to make changes to the Software without notice. Inventek does not
 * assume any liability arising out of the application or use of the Software or any product or
 * circuit described in the Software. Inventek does not authorize its products for use in any
 * products where a malfunction or failure of the Inventek product may reasonably be expected to
 * result in significant property damage, injury, or death ("High Risk Product"). By including
 * Inventek's product in a High Risk product, the manufacturer of such system or application
 * assumes all risk of such use and in doing so agrees to indemnify Inventek against all liability.
 * Inventek Systems reserves the right to make changes without further notice to any products or
 * data herein to improve reliability, function, or design. The information contained within is
 * believed to be accurate and reliable. However Inventek does not assume any liability arising
 * out of the application or use of this information, nor the application or use of any product
 * or circuit described herein, neither does it convey any license under its patent rights nor
 * the rights of others.
 */

/** @file
 *  NVRAM variables which define BCM43362 Parameters for the USI module used on the BCM943362WCD4 board
 *
 *  As received from USI
 *
 */

#ifndef INCLUDED_NVRAM_IMAGE_H_
#define INCLUDED_NVRAM_IMAGE_H_

#include <string.h>
#include <stdint.h>
#include "../generated_mac_address.txt"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Character array of NVRAM image
 */

static const char wifi_nvram_image[] =
        "cbuckout=1500"                                                      "\x00"
        "sromrev=3"                                                          "\x00"
        "boardtype=0x05a0"                                                   "\x00"
        "boardrev=0x1203"                                                    "\x00"
        "manfid=0x2d0"                                                       "\x00"
        "prodid=0x492"                                                       "\x00"
        "vendid=0x14e4"                                                      "\x00"
        "devid=0x4343"                                                       "\x00"
        "boardflags=0x200"                                                   "\x00"
        "nocrc=1"                                                            "\x00"
        "xtalfreq=26000"                                                     "\x00"
        "boardnum=777"                                                       "\x00"
        NVRAM_GENERATED_MAC_ADDRESS                                          "\x00"
        "aa2g=3"                                                             "\x00"
        "ag0=0"                                                              "\x00"
        "ccode=xx"                                                           "\x00"
        "regrev=17"                                                          "\x00"
        "pa0b0= 0x13F9"                                                      "\x00"
        "pa0b1= 0xFD93"                                                      "\x00"
        "pa0b2= 0xFF4D"                                                      "\x00"
        "rssismf2g=0xa"                                                      "\x00"
        "rssismc2g=0x3"                                                      "\x00"
        "rssisav2g=0x7"                                                      "\x00"
        "maxp2ga0=0x40"                                                      "\x00"
        "cck2gpo=0x0"                                                        "\x00"
        "ofdm2gpo=0x22222222"                                                "\x00"
        "mcs2gpo0=0x3333"                                                    "\x00"
        "mcs2gpo1=0x6333"                                                    "\x00"
        "wl0id=0x431b"                                                       "\x00"
        "cckdigfilttype=22"                                                  "\x00"
        "cckPwrOffset=5"                                                     "\x00"
        "ofdmanalogfiltbw2g=3"                                               "\x00"
        "rfreg033=0x19"                                                      "\x00"
        "rfreg033_cck=0x1f"                                                  "\x00"
        "noise_cal_enable_2g=0"                                              "\x00"
        "pacalidx2g=10"                                                      "\x00"
        "swctrlmap_2g=0x0c050c05,0x0a030a03,0x0a030a03,0x0,0x1ff"            "\x00"
        "triso2g=1"                                                          "\x00"
        "RAW1=4a 0b ff ff 20 04 d0 02 62 a9"                                 "\x00"
        "otpimagesize=76"                                                    "\x00"
        "\x00\x00";


#ifdef __cplusplus
} /* extern "C" */
#endif

#else /* ifndef INCLUDED_NVRAM_IMAGE_H_ */

#error Wi-Fi NVRAM image included twice

#endif /* ifndef INCLUDED_NVRAM_IMAGE_H_ */
