var group___hid_events =
[
    [ "HidEvent", "group___hid_events.html#gaa9fb99822becafbbe18cf2e2cb47292b", null ],
    [ "HidEventAny", "group___hid_events.html#ga74b515b3a733d0178abd929f467d939e", null ],
    [ "HidEventButtonStateChange", "group___hid_events.html#gad859148c1cc371ff14d1d83f2cf87ef4", null ],
    [ "HidEventKey", "group___hid_events.html#gab0b37401ce95b2ed572876c8ffa3163b", null ],
    [ "HidEventMotionAB", "group___hid_events.html#gaa726934254b148908a1d4ac42e98ad9f", null ],
    [ "HidEventMotionSingleAxis", "group___hid_events.html#ga4c04c09b607e666e8cf77c8571dd19d7", null ],
    [ "HidEventMotionXY", "group___hid_events.html#ga545fe157ab4a81f53db9161e5818b0a7", [
      [ "HID_EVENT_NONE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a2243cca86c24291f214ab45bd03b66b0", null ],
      [ "HID_EVENT_MOTION_AXIS_0", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a3ce7eb9cff667a20719ade1ab25d094a", null ],
      [ "HID_EVENT_MOTION_AXIS_1", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1ab219873a2a5cd3b88671a39dc235bfb5", null ],
      [ "HID_EVENT_MOTION_AXIS_2", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a358b1d8379814b05c0c011d54c2f2c1e", null ],
      [ "HID_EVENT_MOTION_AXIS_3", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a01f30d293c9294690b6e0a2b9df8f9a3", null ],
      [ "HID_EVENT_MOTION_AXIS_X_Y", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a460a591f65fb0faa2e9a0eed42c0d5f2", null ],
      [ "HID_EVENT_MOTION_AXIS_A_B", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a0cf07447f77e06975dfe1301b3eb61a1", null ],
      [ "HID_EVENT_NEW_BUTTON_STATE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1af90c6d8257926fd11dd969eabd312ce2", null ],
      [ "HID_EVENT_KEY_STATE_CHANGE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a64bc100e6b2dd234eeacac1893142bfd", null ],
      [ "HID_EVENT_MOTION_DATA_AVAILABLE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1af6aadccb13a7278688b6aed47af4ec12", null ],
      [ "HID_EVENT_VOICE_DATA_AVAILABLE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1ae203d23ceee31bea04b9bb0a8e61146b", null ],
      [ "HID_EVENT_MIC_START", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1abb61722e463c30dd7801c680f4d969f7", null ],
      [ "HID_EVENT_MIC_STOP", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a0dedaa6d335e930b6b64c09015434cc8", null ],
      [ "HID_EVENT_RC_MIC_START_REQ", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a642211cfff7a363ed053931be58953b4", null ],
      [ "HID_EVENT_RC_MIC_STOP_REQ", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a41149e40c17b9cce06f7df285189dca8", null ],
      [ "HID_EVENT_AUDIO_MODE", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1ab888a33ee16382156da5a5c6728155dc", null ],
      [ "HID_EVENT_AUDIO_CODEC_RD", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a654c728a5dd964195b3cf2a163c4bc78", null ],
      [ "HID_EVENT_AUDIO_CODEC_WT", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a62eba60e91285e4650fc1e2219b7df40", null ],
      [ "HID_EVENT_EVENT_FIFO_OVERFLOW", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a7e7a2e05b86511f014ec9838ad20123b", null ],
      [ "HID_EVENT_ANY", "group___hid_events.html#gga29d44341ce767c5dfc737d622fc97ba1a813b0bbdfb2b0492a5e3bb06b57cb316", null ]
    ] ]
];