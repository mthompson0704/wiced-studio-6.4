#
# Copyright 2017, Inventek Systems. All Rights Reserved. This software, associated documentation
# and materials ("Software"), referenced and provided with this documentation is owned by Inventek
# Systems and is protected by and subject to worldwide patent protection (United States and foreign),
# United States copyright laws and international treaty provisions. Therefore, you may use this
# Software only as provided in the license agreement accompanying the software package from which
# you obtained this Software ("EULA"). If no EULA applies, Inventek Systems hereby grants you a
# personal, non-exclusive, non-transferable license to copy, modify, and compile the Software source
# code solely for use in connection with Inventek's integrated circuit products.
#
# Any reproduction, modification, translation, compilation, or representation of this Software except
# as specified above is prohibited without the express written permission of Inventek.
# Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE.
# 
# Inventek reserves the right to make changes to the Software without notice. Inventek does not
# assume any liability arising out of the application or use of the Software or any product or
# circuit described in the Software. Inventek does not authorize its products for use in any
# products where a malfunction or failure of the Inventek product may reasonably be expected to
# result in significant property damage, injury, or death ("High Risk Product"). By including
# Inventek's product in a High Risk product, the manufacturer of such system or application
# assumes all risk of such use and in doing so agrees to indemnify Inventek against all liability.
# Inventek Systems reserves the right to make changes without further notice to any products or
# data herein to improve reliability, function, or design. The information contained within is
# believed to be accurate and reliable. However Inventek does not assume any liability arising
# out of the application or use of this information, nor the application or use of any product
# or circuit described herein, neither does it convey any license under its patent rights nor
# the rights of others.
#

NAME := ISM43903-R48-L54

WLAN_CHIP            := 43909
WLAN_CHIP_REVISION   := B0
WLAN_CHIP_FAMILY     := 4390x
APPS_CHIP_REVISION   := B1
HOST_MCU_FAMILY      := BCM4390x
HOST_MCU_VARIANT     := BCM43903
HOST_MCU_PART_NUMBER := BCM43903KUBG

PLATFORM_SUPPORTS_BUTTONS := 1

#Module Revision (RevB, RevCc, RevCm, RevCcm)
DEFAULT_BOARD_REVISION   		:= RevCc
BOARDS_WITH_MICRON       		:= RevB		#Micron Only, modules before date code 1805
BOARDS_WITH_CYPRESS      		:= RevCc	#Cypress or Micron
BOARDS_WITH_MACRONIX     		:= RevCm	#Macronix or Micron
BOARDS_WITH_CYPRESS_MACRONIX	:= RevCcm	#Cypress or Macronix
SUPPORTED_BOARD_REVISION := $(BOARDS_WITH_MICRON) $(BOARDS_WITH_CYPRESS) $(BOARDS_WITH_MACRONIX) $(BOARDS_WITH_CYPRESS_MACRONIX)
BOARD_REVISION_DIR       := board_revision

$(info BOARD_REVISION = $(BOARD_REVISION) )
ifeq ($(BOARD_REVISION), )
#RevCc Cypress or Micron QSPI FLASH
GLOBAL_DEFINES += SFLASH_SUPPORT_CYPRESS_PARTS \
				  SFLASH_SUPPORT_MICRON_PARTS
else

ifeq ($(BOARD_REVISION), RevB )
#RevB Micron QSPI FLASH
GLOBAL_DEFINES += SFLASH_SUPPORT_MICRON_PARTS
else

ifeq ($(BOARD_REVISION), RevCc)
#RevCc Cypress or Micron QSPI FLASH
GLOBAL_DEFINES += SFLASH_SUPPORT_CYPRESS_PARTS \
				  SFLASH_SUPPORT_MICRON_PARTS
else #RevCm
ifeq ($(BOARD_REVISION), RevCm)
#RevCm Macronix or Micron QSPI FLASH
GLOBAL_DEFINES += SFLASH_SUPPORT_MACRONIX_PARTS \
				  SFLASH_SUPPORT_MICRON_PARTS
else #RevCcm
#RevCcm Cypress or Macronix QSPI FLASH
GLOBAL_DEFINES += SFLASH_SUPPORT_CYPRESS_PARTS \
				  SFLASH_SUPPORT_MACRONIX_PARTS
endif #RevCm
endif #RevCc
endif #RevB
endif #None

$(NAME)_SOURCES := platform.c

GLOBAL_INCLUDES := .

ifneq ($(VALID_PLATFORMS),ISM43903*)
$(info Adding ISM43903* to valid platforms)
VALID_PLATFORMS    += ISM43903*
else
$(info VALID_PLATFORMS = $(VALID_PLATFORMS) )
endif

#***** Inventek Systems *****
#This is Inventek Systems internal use only
ifneq ($(wildcard ./platforms/$(PLATFORM)/iwin/iwin.mk),)
include ./platforms/$(PLATFORM)/iwin/iwin.mk
else
$(info APPLICATION: WICED APP )
endif #./platforms/$(PLATFORM)/iwin/iwin.mk
#****************************
