
extern wiced_result_t get_new_firmware();
extern wiced_result_t put_new_firmware(uint16_t new_firmware_downloaded, uint64_t utcunixtime, uint16_t new_firmware_download_status_id);
extern wiced_result_t get_new_firmware_download_block(long int block_number);
extern int close_firmware_app();
extern int process_new_firmware_block( uint8_t* new_firmware_block, uint32_t new_firmware_size_in_bytes, uint32_t new_firmware_size_in_blocks, uint new_firmware_block_size, uint32_t new_firmware_block_number);

extern void process_ota_firmware();
