Host app
++++++++
A Host application is the end user application running on the host MCU and 
contains the customer business logic. The Host application controls the embedded
application via WICED HCI commands. See the \apps\host folder.

common
    Platform independent C code that includes - 
    -  wiced_hci : API and source code for WICED HCI commands and event processing.
    -  app_host  : application logic for device management.
      
client_control
    Client Control is a multi-platform sample UI application for controlling various 
    embedded applications. The application uses "wiced_hci" and "app_host" files and 
    is developed using QT IDE. See apps\host\client_control\README.txt.
