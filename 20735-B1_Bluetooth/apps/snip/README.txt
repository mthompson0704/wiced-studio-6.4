Snip apps
+++++++++
These snippet applications demonstrate a single functionality of the BT software
or the WICED board. See the applications in the apps\snip\ folder.

Over the air Firmware Upgrade
  This is a stub application that demonstrates how to link with the
  OTA FW Upgrade library.  See ota_firmware_upgrade\ota_firmware_upgrade.c
  - Over the air Firmware Upgrade peer application
    This is an application that demonstrates how to peform OTA firmware upgrade
    from a peer Windows or Android app communicating with the ota_firmware_upgrade
    snippet running on a WICED board.  See ota_firmware_upgrade\peer_apps
  
-----------------------------------------------
Classic Bluetooth snippets (under apps\snip\bt)
-----------------------------------------------
Serial Port Profile application
   SPP application uses SPP profile library to establish, terminate, send and
   receive SPP data over BR/EDR.  See (spp\spp.c).
 
iAP2 application
    Note: This application and library is available for Apple MFI licensees only.
    This snippet uses the WICED iAP2 library to send data to an iOS device using
    Apple's External Accessory iAP2 protocol. See iap2\iap2.c
    
---------------------------------------------------
Bluetooth Low Energy snippets (under apps\snip\ble)
---------------------------------------------------
AMS application
    This application snippet shows how to use the Apple Media Service (AMS). See
    (ams\ams.c).

ANCS application
    This application snippet shows how to use the Apple Notification Center
    Service (ANCS). See (ancs\ancs.c).

ANC/ANS apps
    Sample apps for BT SIG Alert Notification profile 
    ANC: Client - see anc/anc.c and ANS: Service - see ans/anc.c

BAC/BAS apps
    Sample apps for BT SIG Battery Service profile 
    BAC: Client - see bac/bac.c and BAS: Service - see bas/bas.c 

HRC/HRS apps
    Sample apps for BT SIG Heart Rate profile 
    HRC: Client - see hrc/hrc.c, HRS: Server - see hrs/hrs.c

GATT DB app
    Sample application to dynamically configure GATT datatbase over WICED HCI. 
    See gatt_db/hci_control.c

Mesh sample apps
    Sample apps based on BT SIG Mesh protocol. Sample included for 
    various models including power on/off, level, battery, light controls, 
    power level, transition, location, property, time, scene, scheduler,  
    provision, sensor and time. Client and Server models apps are included 
    for each of the models. Windows and Android based peer apps are included
    for provisioning and control.

    Mesh applications are contained in subfolders under the mesh folder, and
    demonstrate implementions of the various BLE Mesh models, as indicated by folder
    name.
    - See the application .c file for each of these apps for more details on setup.
    - See the mesh\peerapps folder for the Windows application used to provision and
      control the mesh devices.
    - See the mesh\ClientControl folder for the demonstation host MCU Client Control
      app with custom mesh support that can communicate with the embedded HCI app
      over the WICED HCI interface.  Note this is a mesh-specific separate instance
      of the Client Control host MCU app, not to be confused with the generic Client
      Control app under apps\host.

--------------------------------------------------------------
HAL (Hardware Adaptation Layer) snippets (under apps\snip\hal)
--------------------------------------------------------------

ADC application
    This application demonstrates how to configure and use ADC in WICED Eval.
    boards to measure DC voltage on various DC input channels. See
    (adc\hal_adc_app.c).

Sample application for GPIO usage
    This application demonstrates how to use WICED GPIO APIs to configure GPIO's
    as Input(with interrupt enabled) or Output pins.  See 
    (gpio\hal_gpio_app.c).
    
