/*
 * AES.c
 *
 *  Created on: Dec 21, 2017
 *      Author: mthom
 */

#ifdef TEST_AES_128
//
// AES-128 Decryption Function - used by provisioning
// part of daemon
// wi-fi passphrase is 64 bytes
//
//
// dump bytes to console
//
void dump_bytes(const uint8_t* bptr, uint32_t len)
{
    int i = 0;

    for (i = 0; i < len; )
    {
        if ((i & 0x0f) == 0)
        {
            WPRINT_APP_INFO( ( "\r\n" ) );
        }
        else if ((i & 0x07) == 0)
        {
            WPRINT_APP_INFO( (" ") );
        }
        WPRINT_APP_INFO( ( "%02x ", bptr[i++] ) );
    }
    WPRINT_APP_INFO( ( "\r\n" ) );
}

#define AES_CBC_IV_LENGTH        16              /* Length of the AES-CBC initialization vector in octets */
#define AES_CBC_DATA_LENGTH      64              /* Length of the AES-CBC data for encrypt in octets */
#define AES_CBC_KEY_LENGTH       128             /* Length of the AES-CBC key in bits */

void aes_128_decrypt_test(void)
{
    wiced_time_t t1, t2;
//    uint8_t cipher_text[64];
//    uint8_t plain_text[64];
//    uint8_t iv[16];
//    char *plain_text_ptr;
    aes_context_t context_aes;
    uint8_t encrypted_text[AES_CBC_DATA_LENGTH];
    uint8_t decrypted_text[AES_CBC_DATA_LENGTH];
	uint8_t wi_fi_passphrase[AES_CBC_DATA_LENGTH] = "EmmaAlexisIan07040704";
/*
	uint8_t wi_fi_passphrase[AES_CBC_DATA_LENGTH] =
    { \
            0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, \
            0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, \
            0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, \
            0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, \
            0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, \
            0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, \
            0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, \
            0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, \
        };
*/

	/*
	uint8_t iv[AES_CBC_IV_LENGTH] = {0x8c, 0xe8, 0x2e, 0xef, 0xbe, 0xa0, 0xda, 0x3c, \
			  	  	  	  	  0x44, 0x69, 0x9e, 0xd7, 0xdb, 0x51, 0xb7, 0xd9, \
			 	 	 	 	 };
	*/
	uint8_t iv[AES_CBC_IV_LENGTH] = {0x2c, 0x8d, 0x1c, 0xff, 0xa2, 0x90, 0x45, 0x9c, \
			  	  	  	  	  0x22, 0x9f, 0x1a, 0x0b, 0xd2, 0x52, 0x1a, 0x9c, \
			 	 	 	 	 };
	uint8_t iv_d[AES_CBC_IV_LENGTH] = {0x2c, 0x8d, 0x1c, 0xff, 0xa2, 0x90, 0x45, 0x9c, \
			  	  	  	  	  0x22, 0x9f, 0x1a, 0x0b, 0xd2, 0x52, 0x1a, 0x9c, \
			 	 	 	 	 };
	/*
	uint8_t hex_key[AES_CBC_IV_LENGTH] = {0x56, 0xe4, 0x7a, 0x38, 0xc5, 0x59, 0x89, 0x74, \
			       	   	   	   	   	   0xbc, 0x46, 0x90, 0x3d, 0xba, 0x29, 0x03, 0x49, \
				  	  	  	  	  	  };
	*/
	uint8_t hex_key[AES_CBC_IV_LENGTH] = {0x22, 0xc4, 0x5a, 0x98, 0xa5, 0x89, 0xc9, 0x14, \
			       	   	   	   	   	   0x4c, 0x86, 0xa0, 0xfd, 0xda, 0x39, 0x83, 0x99, \
				  	  	  	  	  	  };

//	WPRINT_APP_INFO( ( "ecrypting passphrase %s\n\r", wi_fi_passphrase ) );
	memset(encrypted_text,0, AES_CBC_DATA_LENGTH);
	memset(decrypted_text,0, AES_CBC_DATA_LENGTH);
    /* Test encryption */
//    WPRINT_APP_INFO( ( "\n%s\n", aes_cbc_test_cases[i].label ) );
	WPRINT_APP_INFO( ( "iv bytes \n\r" ) );
	dump_bytes( iv, AES_CBC_IV_LENGTH);
	WPRINT_APP_INFO( ( "hex_key bytes \n\r" ) );
	dump_bytes( hex_key, AES_CBC_IV_LENGTH);
	WPRINT_APP_INFO( ( "ecrypting passphrase bytes \n\r" ) );
	dump_bytes( wi_fi_passphrase, AES_CBC_DATA_LENGTH);
//	memcpy(iv, aes_cbc_test_cases[i].iv, AES_CBC_IV_LENGTH);
	memset(&context_aes, 0, sizeof(context_aes));
//	aes_setkey_enc(&context_aes, aes_cbc_test_cases[i].hex_key, AES_CBC_KEY_LENGTH);
	aes_setkey_enc(&context_aes, hex_key, AES_CBC_KEY_LENGTH);

	wiced_time_get_time( &t1 );
//  aes_crypt_cbc(&context_aes, AES_ENCRYPT, aes_cbc_test_cases[i].data_len, iv, (unsigned char*)plain_text_ptr, cipher_text);
//	aes_crypt_cbc(&context_aes, AES_ENCRYPT, aes_cbc_test_cases[i].data_len, iv, (unsigned char*)plain_text_ptr, cipher_text);
	aes_crypt_cbc(&context_aes, AES_ENCRYPT, AES_CBC_DATA_LENGTH, iv, wi_fi_passphrase, encrypted_text);
//	aes_crypt_cbc(&context_aes, AES_ENCRYPT, strlen(wi_fi_passphrase), iv, wi_fi_passphrase, encrypted_text);
	wiced_time_get_time( &t2 );
	t2 = t2 - t1;
    WPRINT_APP_INFO( ( "Time for AES-CBC encrypt = %u ms\r\n", (unsigned int) t2 ) );
	WPRINT_APP_INFO( ( "Encrypted Bytes: \r\n" ) );
//	dump_bytes( cipher_text, aes_cbc_test_cases[i].data_len);
	dump_bytes( encrypted_text, AES_CBC_DATA_LENGTH);
	WPRINT_APP_INFO( ( "serialize encrypted bytes \n\r" ) );
	for (int i=0;i<64;i++)
	{
		WPRINT_APP_INFO(("%02X", encrypted_text[i]));
	}
	WPRINT_APP_INFO(("\r\n"));

	/* Test decryption */
	WPRINT_APP_INFO( ( "iv_d bytes \n\r" ) );
	dump_bytes( iv_d, AES_CBC_IV_LENGTH);
	WPRINT_APP_INFO( ( "hex_key bytes \n\r" ) );
	dump_bytes( hex_key, AES_CBC_IV_LENGTH);
    wiced_time_get_time( &t1 );
//	memcpy(iv, aes_cbc_test_cases[i].iv, AES_CBC_IV_LENGTH);
	memset(&context_aes, 0, sizeof(context_aes));
//	aes_setkey_dec(&context_aes, aes_cbc_test_cases[i].hex_key, AES_CBC_KEY_LENGTH);
	aes_setkey_dec(&context_aes, hex_key, AES_CBC_KEY_LENGTH);
//	aes_crypt_cbc(&context_aes, AES_DECRYPT, aes_cbc_test_cases[i].data_len, iv, cipher_text, plain_text );
	aes_crypt_cbc(&context_aes, AES_DECRYPT, AES_CBC_DATA_LENGTH, iv_d, encrypted_text, decrypted_text);
	wiced_time_get_time( &t2 );
	t2 = t2 - t1;
    WPRINT_APP_INFO( ( "Time for AES-CBC decrypt = %u ms\r\n", (unsigned int) t2 ) );
	WPRINT_APP_INFO( ( "Decrypted Bytes: \r\n" ) );
	dump_bytes( decrypted_text, AES_CBC_DATA_LENGTH);
	WPRINT_APP_INFO( ( "Resulting passphrase %s\n\r", decrypted_text ) );

}
#endif



