var modules =
[
    [ "WICED trace utils", "group__wiced__utils.html", "group__wiced__utils" ],
    [ "Bluetooth", "group__wicedbt.html", "group__wicedbt" ],
    [ "CyPE", "group__wpl.html", "group__wpl" ],
    [ "RTOS", "group__rtos.html", "group__rtos" ],
    [ "Managing compiler diagnostics.", "group___compiler_diagnostics.html", "group___compiler_diagnostics" ],
    [ "Support for packed enumerations.", "group___packed_enum.html", "group___packed_enum" ],
    [ "WICED OTA Firmware Upgrade", "group__group__ota__wiced__firmware__upgrade.html", "group__group__ota__wiced__firmware__upgrade" ],
    [ "Hardware Drivers", "group___hardware_drivers.html", "group___hardware_drivers" ],
    [ "HID Events", "group___hid_events.html", "group___hid_events" ],
    [ "Interfaces", "group__interfaces.html", "group__interfaces" ],
    [ "WICED Result Codes", "group___result.html", "group___result" ],
    [ "SPP Protocol Library API", "group__wiced__bt__spp__api__functions.html", "group__wiced__bt__spp__api__functions" ]
];