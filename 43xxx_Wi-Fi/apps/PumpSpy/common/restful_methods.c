
#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "math.h"
#include "http_wiced.h"
#include "dct.h"
#include "initialization.h"

//
// clears the buffers used in POST ing to API
//

void clear_post_buffers()
{
    memset(post, 0, sizeof(post));
    memset(json, 0, sizeof(json));
    memset(buffer, 0, sizeof(buffer));
}

//
// POSTs the data to the API
//

wiced_result_t post_the_post(void)
{

    //
    // POST the data to the socket
    //
    result = wiced_http_port_get( &ip_address, dct.PumpSpyHostPort, post, buffer, BUFFER_LENGTH);
    //
    // look at the result
    //
    if ( result == WICED_SUCCESS )
    {
        WPRINT_APP_INFO( ( "non_threaded POST Success \r\n" ) );
//        WPRINT_APP_INFO( ( "POST Success Server returned \r\n %s \r\n", buffer ) );
    }
    else
    {
        WPRINT_APP_INFO( ( "non_threaded POST failure result: %d\r\n", result) );
        WPRINT_APP_INFO( ( "POST result: %u \r\n %s \r\n", result, buffer ) );
    }
    return result;
}

//
// Posts pump cycle data to the API
// Creates the http string in post buffer and calls post_the_post
//

wiced_result_t post_pump_cycles(uint64_t utcunixtime, uint64_t cycle_time, uint16_t current)
{
    clear_post_buffers();
    WPRINT_APP_INFO( ( "POST /pump_outlet_cycles - utcunixtime, cycle_time, current is %lx%lx | %lx%lx | %d \r\n", (long unsigned int)(utcunixtime>>32), (long unsigned int)utcunixtime, (long unsigned int)(cycle_time>>32), (long unsigned int)cycle_time, current) );
    sprintf(json, JSON_insertPumpOutletData, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), (long unsigned int)0, (long unsigned int)(utcunixtime/1000), (int)(cycle_time), current);
//    WPRINT_APP_INFO(("POST /pump_outlet_cycles json |%s| len=%d\r\n", json, strlen(json)));
//    sprintf(post, POST_STRING, "/pump_outlet_cycles", strlen(json), json);
    sprintf(post, POST_STRING, "/pump_outlet_cycles", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
//    WPRINT_APP_INFO(("sprintf done\r\n"));
//    WPRINT_APP_INFO(( post )); //This result looks correct
    return post_the_post();
}

//
// Posts ping data to the API
// Creates the http string in post buffer and calls post_the_post
//

wiced_result_t post_ping(uint16_t idpings_data_type, uint64_t utcunixtime, float value)
{
	int try_count = 0;
	//
	// POST the ping 2x
	//
	do
	{
		clear_post_buffers();
		WPRINT_APP_INFO( ( "POST /pings -  data_type %d value is %f\r\n", idpings_data_type, value ) );
		sprintf(json, JSON_post_pings, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), (long unsigned int)(utcunixtime/1000), idpings_data_type, value);
	//    WPRINT_APP_INFO(("json |%s| len=%d\r\n", json, strlen(json)));
	//    sprintf(post, POST_STRING, "/pings", strlen(json), json);
		sprintf(post, POST_STRING, "/pings", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
	//    WPRINT_APP_INFO(("sprintf done\r\n"));
	//    WPRINT_APP_INFO(( post )); //This result looks correct
		result = post_the_post();
	//    WPRINT_APP_INFO( ( "POST /pings reuslt = %d Server returned \r\n%s\r\n", result, buffer ) );
		if (result == WICED_SUCCESS)
		{
			if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
			{
				result = WICED_ERROR;
				random_delay();
			}
		}
	} while ((try_count++ < 2) && (result != WICED_SUCCESS));
	//
	// return result
	//
	return result;
}

wiced_bool_t post_outlet_alerts(uint16_t idalerts_type, uint64_t utcunixtime, uint16_t value)
{
    clear_post_buffers();
    WPRINT_APP_INFO( ( "POST /pump_outlet_alerts -  data_type %d value is %d\r\n", idalerts_type, value ) );
    //                WPRINT_APP_INFO( ( "server_time is %lx%lx\r\n", (long unsigned int)(server_time>>32), (long unsigned int)server_time ) );
    sprintf(json, JSON_insertPumpOutletAlerts, idalerts_type, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000),  (long unsigned int)0, (long unsigned int)(utcunixtime/1000), value);
    //                WPRINT_APP_INFO(("json |%s| len=%d\r\n", json, strlen(json)));
//    sprintf(post, POST_STRING, "/pump_outlet_alerts", strlen(json), json);
    sprintf(post, POST_STRING, "/pump_outlet_alerts", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
//    WPRINT_APP_INFO(("sprintf done\r\n"));
//    WPRINT_APP_INFO(( post )); //This result looks correct
    result = post_the_post();
//    WPRINT_APP_INFO(( buffer )); //This result looks correct
//    return(result);
    //
    // look at the result
    //
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "POST Success Server returned \r\n %s \r\n", buffer ) );
        return WICED_TRUE;
//        ac_loss_sent_to_server = WICED_TRUE;
    }
    else
    {
        WPRINT_APP_INFO( ( "POST result: %u \r\n %s \r\n", result, buffer ) );
        return WICED_FALSE;
    }
}

//
// gets the sustem time from API via /tm
//

wiced_result_t get_system_time()
{
    char *p;
    char *p2;
    uint64_t system_time;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /tm \r\n"));
    sprintf(post, GET_tm, dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post )); //This result looks correct
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        p = strchr((const char *)buffer, '{');
//        WPRINT_APP_INFO( ( "p points to |%s| \r\n", p ) );
        p = strchr(p, ':');
    //        WPRINT_APP_INFO( ( "p points to |%s| \r\n", p ) );
        p2 = strchr(p, '}');
    //        WPRINT_APP_INFO( ( "p2 points to |%s| \r\n", p2 ) );
        system_time = 0;
        p2--;
        int i = 0;
        while (p2 > p)
        {
            system_time += (uint32_t)(p2[0] - 0x30)*pow(10,i++);
    //            WPRINT_APP_INFO( ( "server_time is %lx%lx\r\n", (long unsigned int)(server_time>>32), (long unsigned int)server_time ) );
            p2--;
        }
        WPRINT_APP_INFO( ( "system_time is %lx%lx\r\n", (long unsigned int)(system_time>>32), (long unsigned int)system_time ) );
        result = wiced_time_set_utc_time_ms(&system_time);
        WPRINT_APP_INFO( ( "system set time result %d\r\n", result) );
    }
    else
    {
        WPRINT_APP_INFO( ( "/tm GET failed: %u\r\n", result ) );
    }
    return result;
}

//
// request bearer token from API
//

wiced_result_t get_bearer_token()
{
    char *p;
    char *p2;

    clear_post_buffers();
    WPRINT_APP_INFO(("POST /oauth/token \r\n"));
//    sprintf(post, POST_authorization_token, strlen(POST_authorization_token));
    sprintf(post, POST_authorization_token, dct.PumpSpyHostName, dct.PumpSpyHostPort);
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );

        p = strchr((const char *)buffer, '{');
//        WPRINT_APP_INFO( ( "p points to |%s| \r\n", p ) );
        p = strchr(p, ':');
//        WPRINT_APP_INFO( ( "p points to |%s| \r\n", p ) );
        p = strchr(p, '"');
        p++;
//        WPRINT_APP_INFO( ( "p points to |%s| \r\n", p ) );
        p2 = strchr(p, '"');
//        WPRINT_APP_INFO( ( "p2 points to |%s| \r\n", p2 ) );
//        p2--;
        memcpy(bearer_token, p, p2-p);
        WPRINT_APP_INFO( ( "bearer_token is %s\r\n", bearer_token));
    }
    else
    {
        WPRINT_APP_INFO( ( "POST /oauth/token failed: %u\r\n", result ) );
    }
    return result;
}

