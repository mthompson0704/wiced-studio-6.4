/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * BLE Proximity Reporter Sample Application
 *
 * BLE Proximity Reporter sample application on initialization configures the
 * stack to start sending advertisement packets to allow connections from a
 * Proximity Monitor device, such as phone. Proximity reporter implements the
 * Link Loss Service, Immediate Alert Service and TX Power Service. Once the
 * connection is established between Proximity Monitor and Reporter, Proximity
 * Monitor can configure the alert levels( High, Mid or No ) and trigger the
 * alerts on the Proximity Reporter.
 *
 * Features demonstrated
 *  - Initializes the GATT subsystem
 *  - Registration with LE stack for various events
 *  - Begins advertising
 *  - Waits for GATT clients to connect
 *  - Processing GATT write and read requests from the client
 *  - Process the immediate alert write request from the client and blink the
 *    led depending on the configured immediate alert level
 *  - Detect the connection loss and blink the led depending on the configured
 *    link loss alert level.
 *  - LED blinks 10 times every 500 milliseconds to indicate high alert level
 *    and 5 times every 1000 milliseconds to indicate mid alert level
 *
 * To demonstrate the app, work through the following steps.
 * 1. Plug the WICED eval board into your computer
 * 2. Build and download the application( to the WICED board )
 * 3. Pair with a Proximity Monitor client
 * 4. Configures the alert levels on the Proximity reporter from the Proximity
 *    Monitor
 * 5. Write the Immediate Alert Level from the Monitor and see the LED blinks
 *    depending on the configured immediate alert level
 * 6. Disconnect the link and see the LED blinks depending on the configured
 *    link loss alert level
 */
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_cfg.h"
#include "wiced_hal_gpio.h"
#include "wiced_bt_uuid.h"
#include "wiced_result.h"
#include "wiced_bt_trace.h"
#include "wiced_bt_cfg.h"
#include "wiced_bt_gatt_db.h"
#include "proximity_reporter.h"
#include "wiced_transport.h"
#include "wiced_hal_puart.h"
#include "wiced_bt_stack.h"
#include "wiced_timer.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
static void                     ble_proximity_reporter_init      ( void );
static wiced_bt_dev_status_t    ble_proximity_management_callback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data );
static void                     ble_proximity_tx_power_callback  ( wiced_bt_tx_power_result_t *p_tx_power );
static wiced_bt_gatt_status_t   ble_proximity_gatt_write_request ( wiced_bt_gatt_write_t *p_write_request );
static wiced_bt_gatt_status_t   ble_proximity_gatt_read_request  ( wiced_bt_gatt_read_t *p_read_request );
static wiced_bt_gatt_status_t   ble_proximity_gatt_cback         ( wiced_bt_gatt_evt_t event, wiced_bt_gatt_event_data_t *p_event_data );
static void                     proximity_reporter_transport_init( void );
static void                     proximity_reporter_transport_status_call_back( wiced_transport_type_t type );
static void                     proximity_reporter_hci_trace_cback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data );
static void                     proximity_reporter_led_blink(uint16_t on_ms, uint16_t off_ms, uint8_t num_of_blinks );
static void                     proximity_reporter_led_timeout( uint32_t count );
/******************************************************
 *               Variable Definitions
 ******************************************************/

/* Proximity reporter attribute values */
static uint8_t  proximity_immediate_alert_level;
static uint8_t  proximity_link_loss_alert_level;
static int8_t   proximity_tx_power_level;

/* GATT attrIbute values */
static uint32_t proximity_gatt_attribute_service_changed = 0;
static uint16_t proximity_gatt_generic_access_appearance = 0;

/* LED timer and counters */
wiced_timer_t proximity_led_timer;
uint8_t       proximity_led_blink_count  = 0;
uint16_t      proximity_led_on_ms  = 0;
uint16_t      proximity_led_off_ms  = 0;
extern wiced_platform_led_config_t platform_led[];
extern size_t led_count;
wiced_platform_led_number_t proximity_led_pin = WICED_PLATFORM_LED_2;

/* transport configuration */
const wiced_transport_cfg_t  transport_cfg =
{
    .type = WICED_TRANSPORT_UART,
    .cfg =
    {
        .uart_cfg =
        {
            .mode = WICED_TRANSPORT_UART_HCI_MODE,
            .baud_rate =  HCI_UART_DEFAULT_BAUD
        },
    },
    .rx_buff_pool_cfg =
    {
        .buffer_size  = 0,
        .buffer_count = 0
    },
    .p_status_handler = NULL,
    .p_data_handler = NULL,
    .p_tx_complete_cback = NULL
};

/******************************************************
 *               Function Definitions
 ******************************************************/

void application_start( void )
{
    wiced_transport_init( &transport_cfg );

#ifdef WICED_BT_TRACE_ENABLE
    // Set the debug uart as WICED_ROUTE_DEBUG_NONE to get rid of prints
    // wiced_set_debug_uart(WICED_ROUTE_DEBUG_NONE);

    // Set to PUART to see traces on peripheral uart(puart)
    wiced_set_debug_uart( WICED_ROUTE_DEBUG_TO_PUART );
#if ( defined(CYW20706A2) || defined(CYW20735B0) || defined(CYW20719B0) || defined(CYW43012C0) )
    wiced_hal_puart_select_uart_pads( WICED_PUART_RXD, WICED_PUART_TXD, 0, 0);
#endif
    // Set to HCI to see traces on HCI uart - default if no call to wiced_set_debug_uart()
    // wiced_set_debug_uart( WICED_ROUTE_DEBUG_TO_HCI_UART );

    // Use WICED_ROUTE_DEBUG_TO_WICED_UART to send formatted debug strings over the WICED
    // HCI debug interface to be parsed by ClientControl/BtSpy.
    // wiced_set_debug_uart(WICED_ROUTE_DEBUG_TO_WICED_UART);
#endif //WICED_BT_TRACE_ENABLE
    WICED_BT_TRACE ("BLE Proximity Reporter Application start\n");
    /* Initialize Bluetooth controller and host stack */
    wiced_bt_stack_init( ble_proximity_management_callback, &wiced_bt_cfg_settings, wiced_bt_cfg_buf_pools );
}

/* Link loss and Immediate Alert handling */
static void ble_proximity_reporter_activate_alert( uint8_t alert_level )
{
    if ( alert_level == ALERT_LEVEL_NONE)
    {
        /* NO ALERT */
        WICED_BT_TRACE("ble_proximity_reporter_activate_alert: NO_ALERT\n");
        return;
    }
    else if  ( ( alert_level == ALERT_LEVEL_MID ) || ( alert_level == ALERT_LEVEL_HIGH ) ) /* MID or HIGH ALERT */
    {
        uint8_t blink_duration = ( alert_level == ALERT_LEVEL_MID ) ? LED_BLINK_DURATION_MID_ALERT : LED_BLINK_DURATION_HIGH_ALERT;
        uint8_t num_blinks = ( alert_level == ALERT_LEVEL_MID ) ? LED_BLINK_COUNT_MID_ALERT : LED_BLINK_COUNT_HIGH_ALERT;

        proximity_reporter_led_blink( blink_duration, blink_duration, num_blinks );
        WICED_BT_TRACE( "ble_proximity_reporter_activate_alert: %s\n", alert_level_string[alert_level] );
    }
    return;
}

/* TX Power report handler */
static void ble_proximity_tx_power_callback( wiced_bt_tx_power_result_t *p_tx_power )
{
    if ( ( p_tx_power->status == WICED_BT_SUCCESS ) && ( p_tx_power->hci_status == HCI_SUCCESS ) )
    {
        WICED_BT_TRACE("Local TX power callback success %i\n", p_tx_power->tx_power);
        proximity_tx_power_level = p_tx_power->tx_power;
    }
    else
    {
        WICED_BT_TRACE("Unable to read Local TX power. (btm_status=0x%x, hci_status=0x%x)\n", p_tx_power->status, p_tx_power->hci_status);
        proximity_tx_power_level = 0;
    }
}

/* Bluetooth management event handler */
static wiced_bt_dev_status_t ble_proximity_management_callback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data )
{
    wiced_bt_dev_status_t       status = WICED_BT_SUCCESS;
    wiced_bt_device_address_t   bda;
    WICED_BT_TRACE ("ble_proximity_management_callback 0x%x\n", event);
    switch ( event )
    {
        case BTM_ENABLED_EVT:
            /* Bluetooth controller and host stack enabled */
            WICED_BT_TRACE("Bluetooth enabled (%s)\n", ((p_event_data->enabled.status == WICED_BT_SUCCESS) ? "success":"failure"));

            if ( p_event_data->enabled.status == WICED_BT_SUCCESS )
            {
                wiced_bt_dev_read_local_addr( bda );
                WICED_BT_TRACE("Local Bluetooth Address: [%02X:%02X:%02X:%02X:%02X:%02X]\n", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);

                /* Enable proximity reporter */
                ble_proximity_reporter_init( );
            }
            break;

        case BTM_SECURITY_REQUEST_EVT:
            WICED_BT_TRACE("Security reqeust\n");
            wiced_bt_ble_security_grant( p_event_data->security_request.bd_addr, WICED_BT_SUCCESS );
            break;

        case BTM_PAIRING_IO_CAPABILITIES_BLE_REQUEST_EVT:
            WICED_BT_TRACE("Pairing IO Capabilities reqeust\n");
            p_event_data->pairing_io_capabilities_ble_request.local_io_cap = BTM_IO_CAPABILITIES_NONE; /* No IO capabilities on this platform */
            p_event_data->pairing_io_capabilities_ble_request.auth_req = BTM_LE_AUTH_REQ_BOND; /* Bonding required */
            p_event_data->pairing_io_capabilities_ble_request.init_keys = BTM_LE_KEY_PENC | BTM_LE_KEY_PID;
            p_event_data->pairing_io_capabilities_ble_request.resp_keys = BTM_LE_KEY_PENC | BTM_LE_KEY_PID;
            break;

        case BTM_PAIRING_COMPLETE_EVT:
            WICED_BT_TRACE("Pairing complete %i.\n", p_event_data->pairing_complete.pairing_complete_info.ble.status);
            break;

        case BTM_BLE_ADVERT_STATE_CHANGED_EVT:
            /* adv state change callback */
            WICED_BT_TRACE("---->>> New ADV state: %d\n", p_event_data->ble_advert_state_changed);
            break;

        default:
            WICED_BT_TRACE("Unhandled Bluetooth Management Event: 0x%x\n", event);
            break;
    }

    return ( status );
}

/* Handler for attrubute write requests */
static wiced_bt_gatt_status_t ble_proximity_gatt_write_request( wiced_bt_gatt_write_t *p_write_request )
{
    uint8_t attribute_value         = *(uint8_t *) p_write_request->p_val;
    wiced_bt_gatt_status_t status   = WICED_BT_GATT_SUCCESS;

    switch ( p_write_request->handle )
    {
        case HDLC_LINK_LOSS_ALERT_LEVEL_VALUE:
            if ( attribute_value >= ALERT_LEVEL_HIGH )
            {
                 proximity_link_loss_alert_level = ALERT_LEVEL_HIGH;
            }
            else
            {
                proximity_link_loss_alert_level = attribute_value;
            }
            WICED_BT_TRACE( "Link loss alert level changed to: %s\n", alert_level_string[proximity_link_loss_alert_level] );
            break;

        case HDLC_IMMEDIATE_ALERT_LEVEL_VALUE:
            if ( attribute_value >= ALERT_LEVEL_HIGH)
            {
                 proximity_immediate_alert_level = ALERT_LEVEL_HIGH;
            }
            else
            {
                proximity_immediate_alert_level = attribute_value;
            }
            WICED_BT_TRACE( "Immediate alert level changed to: %s\n", alert_level_string[proximity_immediate_alert_level] );
            ble_proximity_reporter_activate_alert( proximity_immediate_alert_level);
            break;

        default:
            WICED_BT_TRACE("Write request to invalid handle: 0x%x\n", p_write_request->handle);
            status = WICED_BT_GATT_WRITE_NOT_PERMIT;
            break;
    }

    return ( status );
}

/* Handler for attrubute read requests */
static wiced_bt_gatt_status_t ble_proximity_gatt_read_request( wiced_bt_gatt_read_t *p_read_request )
{
    wiced_bt_gatt_status_t status       = WICED_BT_GATT_SUCCESS;
    uint16_t attribute_value_length     = 0;
    void*    p_attribute_value_source;

    switch ( p_read_request->handle )
    {
        case HDLC_GENERIC_ATTRIBUTE_SERVICE_CHANGED_VALUE:
            p_attribute_value_source = &proximity_gatt_attribute_service_changed;
            attribute_value_length   = sizeof( proximity_gatt_attribute_service_changed );
            break;

        case HDLC_GENERIC_ACCESS_DEVICE_NAME_VALUE:
            p_attribute_value_source = (void *) wiced_bt_cfg_settings.device_name;
            attribute_value_length   = strlen( (char *) wiced_bt_cfg_settings.device_name );
            break;

        case HDLC_GENERIC_ACCESS_APPEARANCE_VALUE:
            p_attribute_value_source = &proximity_gatt_generic_access_appearance;
            attribute_value_length   = sizeof( proximity_gatt_generic_access_appearance );
            break;

        case HDLC_TX_POWER_LEVEL_VALUE:
            p_attribute_value_source = &proximity_tx_power_level;
            attribute_value_length   = sizeof( proximity_tx_power_level );
            break;

        case HDLC_LINK_LOSS_ALERT_LEVEL_VALUE:
            p_attribute_value_source = &proximity_link_loss_alert_level;
            attribute_value_length   = sizeof( proximity_link_loss_alert_level );
            break;

        default:
            status = WICED_BT_GATT_READ_NOT_PERMIT;
            WICED_BT_TRACE("Read request to invalid handle: 0x%x\n", p_read_request->handle);
            break;
    }

    /* Validate destination buffer size */
    if ( attribute_value_length > *p_read_request->p_val_len )
    {
        *p_read_request->p_val_len = attribute_value_length;
    }

    /* Copy the attribute value */
    if ( attribute_value_length )
    {
        memcpy( p_read_request->p_val, p_attribute_value_source, attribute_value_length );
    }

    /* Indicate number of bytes copied */
    *p_read_request->p_val_len = attribute_value_length;

    return ( status );
}

/* GATT event handler */
static wiced_bt_gatt_status_t ble_proximity_gatt_cback( wiced_bt_gatt_evt_t event, wiced_bt_gatt_event_data_t *p_event_data )
{
    wiced_bt_gatt_status_t status = WICED_BT_GATT_SUCCESS;
    uint8_t *bda;

    WICED_BT_TRACE(" ble_proximity_gatt_cback event %d \n",event);
    switch ( event )
    {
        case GATT_CONNECTION_STATUS_EVT:
            /* GATT connection status change */
            bda = p_event_data->connection_status.bd_addr;
            WICED_BT_TRACE("GATT connection to [%02X:%02X:%02X:%02X:%02X:%02X] %s.\n", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5], (p_event_data->connection_status.connected ? "established" : "released"));

            if ( p_event_data->connection_status.connected )
            {
                /* Connection established. Get current TX power  (required for setting TX power attribute in GATT database) */
                wiced_bt_dev_read_tx_power( p_event_data->connection_status.bd_addr, p_event_data->connection_status.transport, (wiced_bt_dev_cmpl_cback_t *) ble_proximity_tx_power_callback );

                /* Disable connectability. */
                wiced_bt_start_advertisements( BTM_BLE_ADVERT_OFF, 0, NULL );
            }
            else
            {
                /* Activate Alert */
                ble_proximity_reporter_activate_alert( proximity_link_loss_alert_level );

                /* Connection released. Re-enable BLE connectability. */
                wiced_bt_start_advertisements( BTM_BLE_ADVERT_UNDIRECTED_HIGH, 0, NULL );
                WICED_BT_TRACE("Waiting for proximity monitor to connect...\n");
            }
            break;

        case GATT_ATTRIBUTE_REQUEST_EVT:
            /* GATT attribute read/write request */
            if ( p_event_data->attribute_request.request_type == GATTS_REQ_TYPE_WRITE )
            {
                status = ble_proximity_gatt_write_request( &p_event_data->attribute_request.data.write_req );
            }
            else if ( p_event_data->attribute_request.request_type == GATTS_REQ_TYPE_READ )
            {
                status = ble_proximity_gatt_read_request( &p_event_data->attribute_request.data.read_req );
            }
            break;
        case GATTS_REQ_TYPE_MTU:
            WICED_BT_TRACE("req_mtu: %d\n", p_event_data->attribute_request.data.mtu);
            status = WICED_BT_GATT_SUCCESS;
            break;

        default:
            break;
    }

    return ( status );
}

/* Initialize Proximity Reporter */
static void ble_proximity_reporter_init( void )
{
    wiced_result_t              result;
    wiced_bt_ble_advert_elem_t  adv_elem[1];
    uint8_t num_elem    = 0;
    uint8_t flag        = BTM_BLE_GENERAL_DISCOVERABLE_FLAG | BTM_BLE_BREDR_NOT_SUPPORTED;

    /* Set advertising data: device name and discoverable flag */
    adv_elem[num_elem].advert_type  = BTM_BLE_ADVERT_TYPE_FLAG;
    adv_elem[num_elem].len          = sizeof(uint8_t);
    adv_elem[num_elem].p_data       = &flag;
    num_elem++;
    WICED_BT_TRACE ("ble_proximity_reporter_init\n");


    /* Register for gatt event notifications */
    wiced_bt_gatt_register( &ble_proximity_gatt_cback );

    /* Initialize GATT database */
    wiced_bt_gatt_db_init( (uint8_t *) gatt_db, gatt_db_size );

    wiced_bt_dev_register_hci_trace( proximity_reporter_hci_trace_cback );

    /* Allow peer to pair */
    wiced_bt_set_pairable_mode(WICED_TRUE, 0);

    wiced_bt_ble_set_raw_advertisement_data(num_elem, adv_elem);

    result = wiced_bt_start_advertisements( BTM_BLE_ADVERT_UNDIRECTED_HIGH, 0, NULL );
    WICED_BT_TRACE( "wiced_bt_start_advertisements %d\n", result );

    wiced_init_timer(&proximity_led_timer, proximity_reporter_led_timeout, 0, WICED_MILLI_SECONDS_TIMER);

}

/*
 *  Pass protocol traces up through the UART
 */
static void proximity_reporter_hci_trace_cback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data )
{
    wiced_transport_send_hci_trace(NULL, type, length, p_data);
}

/*
 * Initialize HCI transport
 */
void proximity_reporter_transport_init( void )
{
    wiced_transport_cfg_t trans_config;
    memset(&trans_config, 0, sizeof(wiced_transport_cfg_t));

    trans_config.type                           = WICED_TRANSPORT_UART;
    trans_config.cfg.uart_cfg.mode              = WICED_TRANSPORT_UART_HCI_MODE;
    trans_config.cfg.uart_cfg.baud_rate         = HCI_UART_DEFAULT_BAUD;
    trans_config.rx_buff_pool_cfg.buffer_size   = 0;
    trans_config.rx_buff_pool_cfg.buffer_count  = 0;
    trans_config.p_status_handler               = proximity_reporter_transport_status_call_back;
    trans_config.p_data_handler                 = NULL;
    trans_config.p_tx_complete_cback            = NULL;
    wiced_transport_init(&trans_config);
}
void proximity_reporter_transport_status_call_back( wiced_transport_type_t type )
{
    if( type == WICED_TRANSPORT_UART )
    {
        wiced_bt_dev_register_hci_trace( proximity_reporter_hci_trace_cback );
    }
}

/*
 * The function invoked on timeout of led timer.
 */
void proximity_reporter_led_timeout( uint32_t count )
{
    static wiced_bool_t led_on = WICED_TRUE;
    if ( led_on )
    {
        wiced_hal_gpio_set_pin_output((uint32_t)*platform_led[proximity_led_pin].gpio, GPIO_PIN_OUTPUT_HIGH);
        if (--proximity_led_blink_count)
        {
            led_on = WICED_FALSE;
            wiced_start_timer( &proximity_led_timer, proximity_led_off_ms );
        }
    }
    else
    {
        led_on = WICED_TRUE;
        wiced_hal_gpio_set_pin_output((uint32_t)*platform_led[proximity_led_pin].gpio, GPIO_PIN_OUTPUT_LOW);
        wiced_start_timer( &proximity_led_timer, proximity_led_on_ms );
    }
}

/**
 * Blinks the LED
 */
void proximity_reporter_led_blink(uint16_t on_ms, uint16_t off_ms, uint8_t num_of_blinks )
{
    if (num_of_blinks && proximity_led_pin < led_count)
    {
        proximity_led_blink_count = num_of_blinks;
        proximity_led_off_ms = off_ms;
        proximity_led_on_ms = on_ms;
        wiced_hal_gpio_set_pin_output((uint32_t)*platform_led[proximity_led_pin].gpio, GPIO_PIN_OUTPUT_LOW);
        wiced_stop_timer(&proximity_led_timer);
        wiced_start_timer(&proximity_led_timer,on_ms);
    }
}

