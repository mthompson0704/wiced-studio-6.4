---------------------------------------------------
Bluetooth Low Energy snippets (under apps\snip\ble)
---------------------------------------------------
AMS application
    This snippet shows how to use the Apple Media Service (AMS).  See
    ams\ams.c.

ANCS application
    This snippet shows how to use the Apple Notification Center Service (ANCS).
    See ancs\ancs.c.

Environment Sensing Temperature application
    This GATT snippet implements a portion of the environment sensing service
    and profile and interfaces with Murata's NCU15WF104F60RC NTC Thermistor using ADC. 
    See env_sensing_temp/thermistor_app.c.

ANC/ANS apps
    Sample apps for BT SIG Alert Notification profile 
    ANC: Client - see anc/anc.c and ANS: Service - see ans/anc.c

BAC/BAS apps
    Sample apps for BT SIG Battery Service profile 
    BAC: Client - see bac/bac.c and BAS: Service - see bas/bas.c 

HRC/HRS apps
    Sample apps for BT SIG Heart Rate profile 
    HRC: Client - see hrc/hrc.c, HRS: Server - see hrs/hrs.c

GATT DB app
    Sample application to dynamically configure GATT datatbase over WICED HCI. 
    See gatt_db/hci_control.c
