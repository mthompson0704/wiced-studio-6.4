/*
 * Copyright (C) 2018, Cirrent Inc
 *
 * All use of this software must be covered by a license agreement signed by Cirrent Inc.
 *
 * DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
 * ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE.
 */

/*
 * The additional Cirrent Agent (CA) API
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

#include "cirrent_types.h"

#define CAC_API_VERSION  (2)

/******************************************************
 *                   Enumerations
 ******************************************************/

/* CA Result  - result returned from API calls */

typedef enum cac_result_t
{
    CAC_SUCCESS = 0,            /* API call was successful     */
    CAC_FAIL = 1,               /* API call failed - TBD add more reasons */
    CAC_NOT_STARTED = 2,        /* API call failed as CA is not currently running - must call cac_start first */
    CAC_NOT_SUPPORTED = 3,      /* NOT supported call */
    CAC_NOT_CONNECTED = 4,      /* API call failed as CA is not currently connected to the internet */
    CAC_NOT_FOUND = 5,          /* API call failed if no required data available */
} CAC_RESULT_T;


/* CA Connection Status - current internet connection status */

typedef enum cac_connection_status_t
{
    CAC_CONNECTED = 0,                     /* Device is connected to a network        */
    CAC_DISCONNECTED = 1,                  /* Device is not connected to any network  */
    CAC_READY_FOR_ONBOARDING = 2,          /* Device is ready for onboarding          */
    CAC_ONBOARDING_DISABLED =  3,          /* Device onboarding was disabled          */
    CAC_FAILED_TO_JOIN_PRIVATE_NETWORK = 4 /* Device failed to join a private network */
} CAC_CONNECTION_STATUS_T;


/* CA Network Type - specifies the class of network the device is on
 *
 * ZipKey - network is part of the Cirrent ZipKey Network
 * User - network was configured by the user (either through CA API or SoftAP process
 *
 */

typedef enum cac_network_type_t
{
    CAC_NETWORK_TYPE_NONE       = 0,            /* Not connected to any network         */
    CAC_NETWORK_TYPE_ZIPKEY     = 1,            /* Network is part of ZipKey network    */
    CAC_NETWORK_TYPE_PRIVATE    = 2             /* Network is a user-defined network    */
} CAC_NETWORK_TYPE_T;


/* CA Network Source - specifies how network was configured */

typedef enum cac_network_source_t
{
    CAC_SOURCE_NETWORK_CONFIG = 0,   /* Network was read from ZipKey network config  */
    CAC_SOURCE_CIRRENT_CLOUD  = 1,   /* Network was passed through Cirrent cloud     */
    CAC_SOURCE_CA_API         = 2,   /* Network was passed through this CA API       */
    CAC_SOURCE_SOFTAP         = 3,   /* Network was passed through SoftAP            */
    CAC_SOURCE_WPS            = 4,   /* Network was passed through WPS               */
    CAC_SOURCE_BLE            = 5,   /* Network was passed through BLE               */
    CAC_SOURCE_OTHER          = 6    /* Network was passed through other             */
} CAC_NETWORK_SOURCE_T;


/* CA Network Security - specifies what type of security is enabled on this network
 *
 * NONE - no security - network is open
 * WPA - network is protected by a pre-shared key
 * WPA2_ENTERPRISE - network is protected by 802.1x enterprise-grade security (stronger than pre-shared key)
 * WISPR - network authentication is over https, but resulting network is open (no security)
 *
 */

typedef enum cac_network_security_t
{
    CAC_NONE = 1,               /* Open network - no security                               */
    CAC_WPA_PSK = 2,            /* Network is secured by a pre-shared key                   */
    CAC_WPA2_ENTERPRISE = 3,    /* WPA2 Enterprise security                                 */
    CAC_WISPR = 4,              /* Captive portal authenticated via WISPr, network is open  */
} CAC_NETWORK_SECURITY_T;

/* CA Network Status - network status */

typedef enum cac_network_status_t
{
    CAC_STATUS_UNUSED        = 0,    /* Connection attempt has not been made          */
    CAC_STATUS_JOINING       = 1,    /* Connection attempt is in progess              */
    CAC_STATUS_JOINED        = 2,    /* Joined successfully                           */
    CAC_STATUS_FAILED        = 3,    /* Failed to join                                */
    CAC_STATUS_NOT_FOUND     = 4,    /* The network was not found during a scan       */
    CAC_STATUS_DISCONNECTED  = 5,    /* Disconnected from the network                 */
    CAC_STATUS_CONFIRMED     = 6     /* Confirmed by successfull connection attempt   */
} CAC_NETWORK_STATUS_T;

/* CA Network Disconnection Reason - specifies reason of disconnect */

typedef enum cac_disconnection_reason
{
    CAC_REASON_UNSET                     = 0,    /* Not set                                     */
    CAC_REASON_DISCONNECT_REQUEST        = 1,    /* Disconnected by Cirrent Agent request       */
    CAC_REASON_LOST_CONNECTION           = 2,    /* The network connection was lost             */
    CAC_REASON_NETWORK_NOT_FOUND         = 3,    /* The network not found in scan list          */
    CAC_REASON_ASSOCIATION_ERROR         = 4,    /* Failed to associate with the network        */
    CAC_REASON_INCORRECT_PASSWORD        = 5,    /* Provided incorrect pre-shared key           */
    CAC_REASON_DHCP_TIMEOUT              = 6,    /* Failed to obtain IP address                 */
    CAC_REASON_INTERNET_NOT_REACHABLE    = 7,    /* The network has no Internet connectivity    */
    CAC_REASON_RSA_DECRYPTION_ERROR      = 8     /* Failed to decrypt pre-shared key            */
} CAC_DISCONNECTION_REASON_T;

/* CA WPS mode - specifies what Wi-Fi Protected Setup (WPS) mode we want to use
 *
 * PIN - use WPS in PIN mode
 * PBC - use WPB in Push-Button-Control mode
 *
 */

typedef enum cac_wps_mode_t
{
    CAC_PIN    = 1,             /* WPS uses PIN                     */
    CAC_PBC    = 2              /* WPS is triggered by push button  */
} CAC_WPS_MODE_T;


/* CA WPS mode - specifies what Wi-Fi Protected Setup (WPS) mode we want to use
 *
 * PIN - use WPS in PIN mode
 * PBC - use WPB in Push-Button-Control mode
 *
 */

typedef enum cac_action_type_t
{
    CAC_IDENTIFY_YOURSELF   = 1,             /* Device should identify itself - e.g. flash light */
    CAC_PRODUCT_SPECIFIC    = 2              /* Device should perform product-specific action    */
} CAC_ACTION_TYPE_T;



/******************************************************
 *                   Type Definitions
 ******************************************************/

#define CAC_DOMAINS_SIZE 256
#define CAC_PSK_LEN (64+1)
#define CAC_SSID_LEN (32+1)
#define CAC_HEX_SSID_LEN (32*2+1)
#define CAC_BSSID_LEN (17+1)
#define CAC_ROAMING_ID_LEN (16+1)
#define CAC_CREDENTIAL_ID_LEN (20+1)
#define CAC_DEVICE_ID_LEN (20+1)
#define CAC_DUB_KEY_LEN (64+1)
#define CAC_SCD_KEY_LEN (451+1)
#define CAC_RC_LEN (15*3 + 2*3 + 1)

#define CAC_VERSION_BUF_SIZE 32

/******************************************************
 *                    Structures
 ******************************************************/

/*
 * Network status
 */

typedef struct cac_network_info_t
{
    CAC_CONNECTION_STATUS_T connection_status;   /* Connection status (connected or disconnected)          */
    CAC_NETWORK_TYPE_T network_type;             /* Network type (ZipKey or Private)                       */
    CAC_NETWORK_SECURITY_T network_security;     /* Type of security on this network                       */
    char domains[CAC_DOMAINS_SIZE];              /* What domains are reachable by the device on            */
                                                  /* this network - none, any or comma-delimited list       */
    uint8_t priority;                             /* Priority that was assigned to this network             */
    uint16_t bandwidth;                           /* Bandwidth limit in kbps, or 0 if unlimited             */
    char ssid[CAC_SSID_LEN];                     /* SSID as printable ASCII string                         */
    char hex_ssid[CAC_HEX_SSID_LEN];             /* SSID as hexdump string                                 */
    char bssid[CAC_BSSID_LEN];                   /* BSSID of the router the device is connected to         */
    char roaming_id[CAC_ROAMING_ID_LEN];         /* Hotspot 2.0 roaming consortium id                      */
    char credential_id[CAC_CREDENTIAL_ID_LEN];   /* The credential id that was used to define this network */
} CAC_NETWORK_INFO_T;


/*
 * Device information
 */

typedef struct cac_device_info_t
{
    char device_id[CAC_DEVICE_ID_LEN];           /* Cirrent device id                                          */
    char dub_key[CAC_DUB_KEY_LEN];               /* Device-user-binding key - used to uniquely identify device */
    char scd_key[CAC_SCD_KEY_LEN];               /* Public key used to encrypt private network credentials     */
} CAC_DEVICE_INFO_T;


/*
 * Private Network
 */

typedef struct cac_private_network_t
{
    CAC_NETWORK_SOURCE_T network_source;           /* Where credentials for this network were configured   */
    CAC_NETWORK_SECURITY_T network_security;       /* Type of security on this network                     */
    CAC_NETWORK_STATUS_T status;                   /* Network connection status                            */
    CAC_DISCONNECTION_REASON_T reason;             /* Reason of network connection failure                 */
    uint8_t priority;                              /* Priority that was assigned to this network           */
    char ssid[CAC_SSID_LEN];                       /* SSID as printable ASCII string                       */
    char hex_ssid[CAC_HEX_SSID_LEN];               /* SSID as hexdump string                               */
    char roaming_id[CAC_ROAMING_ID_LEN];           /* Hotspot 2.0 roaming consortium id                    */
    char credential_id[CAC_CREDENTIAL_ID_LEN];     /* Credential id that was used to define this network   */
} CAC_PRIVATE_NETWORK_T;


/*
 * Wi-fi scan
 */

typedef struct cac_wifi_scan_t
{
    char ssid[CAC_SSID_LEN];                       /* SSID as printable ASCII string                       */
    char hex_ssid[CAC_HEX_SSID_LEN];               /* SSID as hexdump string                               */
    char roaming_consortium[CAC_RC_LEN];           /* If a Hotspot 2.0 network, the roaming consortium     */
    char bssid[CAC_BSSID_LEN];                     /* BSSID                                                */
    CAC_NETWORK_SECURITY_T security;               /* Type of security on this network                     */
    int8_t signal_level;                           /* Signal level                                         */
    uint16_t frequency;                            /* frequency in MHz (e.g. 2412 = channel 1)             */
} CAC_WIFI_SCAN_T;


/*
 * Certificate validation parameters
 */

typedef struct cac_cert_validation_t
{
    const char *domain;                                 /* Domain to match, for cert to be deemed valid         */
    const char *domain_suffix;                          /* Domain suffix must match, for cert to be valid       */
    const char *subject;                                /* Subject must match, for cert to be valid             */
    const char *alt_subject;                            /* Alt subject must match, for cert to be valid         */

} CAC_CERT_VALIDATION_T;


/*
 * Add a Network
 */

typedef struct cac_add_network_t
{
    const char *credential_id;                          /* Unique identifier, to be able to match status to this set of creds   */
    const char *ssid;                                   /* SSID as printable ASCII string, if used for network identification   */
    const char *hex_ssid;                               /* SSID as hexdump                                                      */
    const char *roaming_id;                             /* Hotspot 2.0 roaming consortium id                                    */
    CAC_NETWORK_SECURITY_T network_security;           /* Type of security on this network                                     */
    bool hidden;                                        /* Whether the SSID is hidden or broadcast                              */
    const char *pre_shared_key;                         /* The key to use on WPA-PSK networks                                   */
    const char *encrypted_pre_shared_key;               /* The key to use on WPA-PSK networks, encrypted with SCD key           */
    const char *username;                               /* The username to use on WPA2 or WISPR network                         */
    const char *password;                               /* the password to use on WPA2 or WISPR network                         */
    const char *encrypted_password;                     /* the password to use on WPA2 or WISPR network, encrypted with SCD key */
    uint8_t priority;                                   /* Priority that was assigned to this network                           */
    const CAC_CERT_VALIDATION_T *cert_params;          /* Optional - if server cert should be validated                        */
    bool replace_flag;                                  /* If set, then all private networks will be deleted before adding      */

} CAC_ADD_NETWORK_T;


/*
 * CA Action - action to be performed
 */

typedef struct cac_action_t
{
    CAC_ACTION_TYPE_T action_type;         /* Type of action to perform (identify-yourself or product-specific)     */
    char *action_data;                      /* Details on the action to perform                                      */
} CAC_ACTION_T;


/*
 * Location coordinates  (GPS)
 */

typedef struct cac_location_t
{
    float latitude;                                    /* Latitude at where the device is located              */
    float longitude;                                   /* Longitude at where the device is located             */
    float accuracy;                                    /* Accuracy of the location estimate in meters          */

} CAC_LOCATION_T;


/******************************************************
 *               Type Definitions
 ******************************************************/

/*
 * Function callback to be called whenever an action needs to be performed. The action will be passed
 * as an input parameter into the call.
 *
 * This callback will come from a worker thread. So the handler is not thread safe and must not call any API
 * functions
 *
 */
typedef CAC_RESULT_T (*CAC_ACTION_HANDLER_T)( CAC_ACTION_T action );

/*
 * Function callback to be called whenever network status changes. The current network status will be passed
 * as an input parameter into the call.
 *
 * This callback will come from a worker thread. So the handler is not thread safe and must not call any API
 * functions
 *
 */
typedef CAC_RESULT_T (*CAC_NETWORK_STATUS_HANDLER_T)( const CAC_NETWORK_INFO_T *network_status );


/******************************************************
 *               Function Declarations
 ******************************************************/

/*
 * set api_version to use for the client app and path to the ca configuration file.
 * If path equals NULL, then use default path
 * [in] stack_size       : The size of stack for thread which will run callbacks
 */
extern CAC_RESULT_T cac_init(unsigned long stack_size);

/*
 * cleanup after done with ca api, i.e. client app shuts down
 */
extern CAC_RESULT_T cac_cleanup();

/*
 *  Retrieves the current network status and the network capabilities.
 *  Your device code should use this information to determine which services can be started on the device.
 *  For example, with a 20kbps bandwidth limitation you will not want to start video streaming.
 *
 *  [out] status      : network_status structure containing information about the current network
 *                     (if connected)
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_get_network_status(CAC_NETWORK_INFO_T* status);

/*
 *  Retrieves the most recent wi-fi scan list from the CA.
 *
 *  [out] malloced_scan      : dynamically allocated array of wifi scans
 *
 *  Note: Caller must free the array after use.
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_get_wifi_scan(CAC_WIFI_SCAN_T** malloced_scan, size_t *length);

extern CAC_RESULT_T cac_free_wifi_scan(CAC_WIFI_SCAN_T* malloced_scan);

/*
 *  Retrieves identifying information about the device (its device id, DUB key and SCD keys).
 *
 *  [out] info      : device information structure
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_get_device_info(CAC_DEVICE_INFO_T* info);

/*
 *  Retrieves a list of user-configured networks for this device.
 *
 * [out] networks      : dynamically allocated array of user networks.
 *
 * Note: Caller must free the array after use.
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_get_private_networks(CAC_PRIVATE_NETWORK_T** malloced_network, size_t *length);

extern CAC_RESULT_T cac_free_private_networks(CAC_PRIVATE_NETWORK_T* malloced_network);

/*
 * Add Private Network
 *
 * [in] network     : network and authentication details for network being added
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_add_private_network(const CAC_ADD_NETWORK_T *network,
                                    char credential_id[CAC_CREDENTIAL_ID_LEN]);


/*
 * Set Location
 *
 * [in] location    : the GPS coordinates for where the device is located
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_set_location(const CAC_LOCATION_T *location);


/*
 * Delete Private Network
 *
 * [in] ssid       : SSID for private network (if not a Hotspot 2.0 network)
 * [in] hex_ssid   : SSID hexdump for private network (if not a Hotspot 2.0 network)
 * [in] roaming_id : Roaming consortium id (if a Hotspot 2.0 network)
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_delete_private_network(const char *ssid,
                                                 const char *hex_ssid,
                                                 const char *roaming_id);

/*
 * Set User Action
 *
 * [in] action_name : The name of the action
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_set_user_action(const char *action_name);


/*
 * Upload Data
 *
 * [in] data     : JSON-encoded data to upload
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_upload_data(const char *data);


/*
 * Reset Device - resets Cirrent cloud status for this device
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_reset_device();


/*
 * Wake up the CA - resumes CA normal operation (looking for GCN networks, sending network
 * updates, uploading log files, polling for local wi-fi credentials etc.)
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_wake();


/*
 * Suspend the CA - the process continues running, but all timers are stopped, so the CA no
 * longer actively connects to any networks, or uploads data, or polls the Cirrent cloud for local
 * wi-fi credentials.
 *
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_sleep();


/*
 * Start WPS
 *
 * [in] wps_mode     : Whether to execute WPS in PIN or Push Button Configuration mode
 * [in] wps_pin      : If in PIN mode, PIN to use
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_start_wps(CAC_WPS_MODE_T wps_mode, const char *wps_pin);


/*
 * Make CA discoverable
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_make_discoverable();


/*
 * Revert CA to last known good configuration
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_revert();


/*
 * Register for network status change callbacks
 *
 * [in] status_handler  : A function pointer for the handler that will be notified when there is
 *                        a change in the network status.
 *
 *
 * return cac_result_t
 */
extern CAC_RESULT_T cac_register_network_status_handler(CAC_NETWORK_STATUS_HANDLER_T status_handler);


/*
 * Register for action callbacks
 *
 * [in] action_handler  : A function pointer for the handler that will be notified when there is
 *                        an action to be performed.
 *
 *
 * return cac_result_t
 */
extern CAC_RESULT_T cac_register_action_handler( CAC_ACTION_HANDLER_T action_handler);


/*
 * Enable notifications from cirrent_agent. Status or action handler will be called depending
 * on type of received notification.
 *
 * return cac_result_t
 */
extern CAC_RESULT_T cac_enable_notifications(void);


/*
 * Disable notifications from cirrent_agent. Status and action handlers will not be called
 * in case of disabled notifications.
 *
 * return cac_result_t
 */
extern CAC_RESULT_T cac_disable_notifications(void);


/*
 *  Retrieves the version of CA
 *
 * [out] version : null-terminated string containing the version of CA.
 *
 * returns cac_result_t
 */
extern CAC_RESULT_T cac_get_ca_version(char version[CAC_VERSION_BUF_SIZE]);

#ifdef __cplusplus
} /*extern "C" */
#endif
