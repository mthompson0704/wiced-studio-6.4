/*
 *  Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */
/** @file
 *
 * WICED BLE FindMe Locator Application
 *
 */

#include "wiced_bt_trace.h"
#include "wiced_bt_ble_findmel.h"
#include "hci_control_ble_findmel.h"
#include "hci_control_api.h"
#include "hci_control.h"

/*
 * Local functions
 */
static void hci_control_ble_findmel_cback(wiced_bt_ble_findmel_event_t event,
        wiced_bt_ble_findmel_event_data_t *p_event_data);

/*
 * hci_control_ble_findmel_init
 */
void hci_control_ble_findmel_init( void )
{
    wiced_bt_ble_findmel_init(hci_control_ble_findmel_cback);
}

/*
 * hci_control_ble_findmel_handle_command
 * Handles FindMe Locator Commands from MCU
 */
void hci_control_ble_findmel_handle_command(uint16_t cmd_opcode, uint8_t* p_data, uint32_t data_len)
{
    wiced_bt_device_address_t bdaddr;
    wiced_bt_ble_address_type_t addr_type;
    uint16_t handle;
    wiced_bt_ble_findmel_status_t status;
    wiced_bt_ble_findmel_gatt_cache_t gatt_cache;
    int gatt_cache_size;
    wiced_bt_ble_findmel_level_t alert_level;

    switch(cmd_opcode)
    {
    /* MCU Request Connection to an FindMe Target */
    case HCI_CONTROL_FINDME_LOCATOR_COMMAND_CONNECT:
        if (data_len == sizeof(bdaddr))
        {
            /* Extract the BdAddr from the Wiced HCI command */
            STREAM_TO_BDADDR(bdaddr, p_data);

            /* To connect a BLE Device, we need to know the Address Type (Random or Public)
             * Let's try to retrieve it from either the last Scan or from the Security database.
             */
            addr_type = hci_control_get_address_type(bdaddr);
            if (addr_type > BLE_ADDR_RANDOM_ID)
            {
                status = WICED_BT_BLE_FINDMEL_STATUS_ERROR;
            }
            else
            {
                /* Open the BLE FINDMEL Connection */
                status = wiced_bt_ble_findmel_connect(bdaddr, addr_type);
            }
        }
        else
            status = WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM;
        break;

    /* MCU Request disconnection of an FindMe Target */
    case HCI_CONTROL_FINDME_LOCATOR_COMMAND_DISCONNECT:
        if (data_len == sizeof(handle))
        {
            /* Extract the BLE FindMe Connection Handle from the Wiced HCI command */
            STREAM_TO_UINT16(handle, p_data);
            /* Disconnect the BLE HID connection */
            status = wiced_bt_ble_findmel_disconnect(handle);
        }
        else
            status = WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM;
        break;

    /* MCU Request Adds an FindMe Target Device */
    case HCI_CONTROL_FINDME_LOCATOR_COMMAND_ADD:
        if (data_len == sizeof(bdaddr))
        {
            /* Extract the BdAddr from the Wiced HCI command */
            STREAM_TO_BDADDR(bdaddr, p_data);
            /* To Add a BLE Device, we need to know the Address Type (Random or Public)
             * Let's try to retrieve it from either the last Scan or from the Security database.
             */
            addr_type = hci_control_get_address_type(bdaddr);
            if (addr_type > BLE_ADDR_RANDOM_ID)
            {
                status = WICED_BT_BLE_FINDMEL_STATUS_ERROR;
            }
            else
            {
                /* Retrieve the BLE FindMe Target GATT Cache for this device */
                gatt_cache_size = hci_control_nvram_read_ble_findmel_gatt_cache(bdaddr, &gatt_cache);
                if (gatt_cache_size != sizeof(gatt_cache))
                {
                    WICED_BT_TRACE("Err: hci_control_ble_findmel_handle_command ADD without GATT Cache\n");
                    /* No GATT Cache. Add it anyway. The BLE FindMe Locator library will have to browse &
                     * configure the GATT attributes from the device (not very efficient) */
                    status = wiced_bt_ble_findmel_add(bdaddr, NULL);
                }
                else
                {
                    /* Add the BLE FindMe Target device */
                    status = wiced_bt_ble_findmel_add(bdaddr, &gatt_cache);
                }
            }
        }
        else
            status = WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM;
        break;

    /* MCU Request Removes a FindMe Target device (to do not allow it to reconnect) */
    case HCI_CONTROL_FINDME_LOCATOR_COMMAND_REMOVE:
        if (data_len == sizeof(bdaddr))
        {
            /* Extract the BdAddr from the Wiced HCI command */
            STREAM_TO_BDADDR(bdaddr, p_data);
            /* Remove this BLE FindMe Target Device. It will not be allowed to reconnect. */
            status = wiced_bt_ble_findmel_remove(bdaddr);
        }
        else
            status = WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM;
        break;

    /* MCU Requests to Write the Alert Level of a Device */
    case HCI_CONTROL_FINDME_LOCATOR_COMMAND_WRITE:
        if (data_len == (sizeof(handle) + sizeof(uint8_t)))
        {
            /* Extract the BLE HID Connection Handle from the Wiced HCI command */
            STREAM_TO_UINT16(handle, p_data);
            /* Extract the Alert Level from the Wiced HCI command */
            STREAM_TO_UINT8(alert_level, p_data);
            /* Read the HID Descriptor */
            status = wiced_bt_ble_findmel_write_alert(handle, alert_level);
        }
        else
            status = WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM;
        break;

    default:
        WICED_BT_TRACE("Unknown FINDMEL opcode:0x%04X\n", cmd_opcode);
        status = WICED_BT_BLE_FINDMEL_STATUS_ERROR;
        break;
    }

    /* Send back the Command status to the Host */
    wiced_transport_send_data(HCI_CONTROL_FINDME_LOCATOR_EVENT_STATUS, &status, sizeof(status));
}

/*
 * hci_control_ble_findmel_cback
 * Handles FindMe Locator events
 */
static void hci_control_ble_findmel_cback(wiced_bt_ble_findmel_event_t event,
        wiced_bt_ble_findmel_event_data_t *p_event_data)
{
    uint8_t event_data[sizeof(wiced_bt_ble_findmel_event_data_t)];
    uint8_t *p = event_data;

    WICED_BT_TRACE("hci_control_ble_findmel_cback event:%d\n", event);

    switch(event)
    {
    case WICED_BT_BLE_FINDMEL_OPEN_EVT:
        WICED_BT_TRACE("hci_control_ble_findmel_cback Connected :%B status:%d handle:0x%x\n",
                p_event_data->connected.bdaddr, p_event_data->connected.status,
                p_event_data->connected.handle);
        UINT8_TO_STREAM(p, p_event_data->connected.status);
        BDADDR_TO_STREAM(p, p_event_data->connected.bdaddr);
        UINT16_TO_STREAM(p, p_event_data->connected.handle);
        wiced_transport_send_data( HCI_CONTROL_FINDME_LOCATOR_EVENT_CONNECTED, event_data,
                (uint16_t)(p - event_data));
        break;

    case WICED_BT_BLE_FINDMEL_CLOSE_EVT:
        WICED_BT_TRACE("hci_control_ble_findmel_cback Disconnected handle:%d reason:%d\n",
                p_event_data->disconnected.handle, p_event_data->disconnected.reason);
        UINT16_TO_STREAM(p, p_event_data->disconnected.handle);
        UINT8_TO_STREAM(p, p_event_data->disconnected.reason);
        wiced_transport_send_data( HCI_CONTROL_FINDME_LOCATOR_EVENT_DISCONNECTED, event_data,
                (uint16_t)(p - event_data));
        break;

    case WICED_BT_BLE_FINDMEL_GATT_CACHE_EVT:
        WICED_BT_TRACE("hci_control_ble_findmel_cback GATT Cache %B val_hdl:0x%x\n",
                p_event_data->gatt_cache.bdaddr,
                p_event_data->gatt_cache.characteristic.val_handle);
        hci_control_nvram_write_ble_findmel_gatt_cache(p_event_data->gatt_cache.bdaddr,
                &p_event_data->gatt_cache, WICED_TRUE);
        break;

    default:
        WICED_BT_TRACE("Err: hci_control_ble_findmel_cback unknown event:%d\n", event);
        break;
    }
}

