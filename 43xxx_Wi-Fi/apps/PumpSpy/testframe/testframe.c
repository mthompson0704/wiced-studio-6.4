/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */


#include <stdio.h>
#include "wiced.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
#define LEDSETOFF 0x00
#define LEDSETON  0xAA
#define GRPPWMSET   0xAF         //SET Group Duty Cycle
#define BRIGHTSET    0x5F         //SET Brightness of LEDs

#define I2C_DELAY 10
extern void platform_init_i2c_sda( int is_input );


void write_i2c_byte2(uint8_t data_byte)
{
	uint8_t bit_count = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	WPRINT_APP_INFO( ( "starting write_i2c_byte %02x\n\r", data_byte ) );
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		if ((data_byte & 0x80) > 0)
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit high \n\r"  ) );
			wiced_gpio_output_high( I2C_SDA );
		}
		else
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit low \n\r"  ) );
			wiced_gpio_output_low( I2C_SDA );
		}
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		data_byte = data_byte << 1;
	}
	// check the ack
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x nack\n\r", data_byte ) );
	}
	else
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x ack\n\r", data_byte ) );
	}
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
}

uint8_t  read_i2c_byte(wiced_bool_t last_byte)
{
	uint8_t bit_count = 0;
	uint8_t data_byte = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	data_byte = 0;
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		data_byte = data_byte << 1;
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit high\n\r" ) );
			data_byte = data_byte ^ 0x01;
		}
		else
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit low\n\r" ) );
		}
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
	}
	//
	// set SDA to output
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	if (last_byte)
	{
		//
		// nack the slave
		//
		WPRINT_APP_INFO( ( "nack tlc59116\n\r" ) );
		wiced_gpio_output_high( I2C_SDA );
	}
	else
	{
		//
		// ack the slave
		//
		WPRINT_APP_INFO( ( "ack tlc59116\n\r" ) );
		wiced_gpio_output_low( I2C_SDA );
	}
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	WPRINT_APP_INFO( ( "read_i2c_byte returning %02x\n\r", data_byte ) );
	return data_byte;
}


void start_i2c_bus(void)
{
	//
	//start
	//
	WPRINT_APP_INFO( ( "start_i2c_bus START\n\r" ) );
	wiced_gpio_output_low( I2C_SDA );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);

}
void stop_i2c_bus(void)
{
	//
	//stop
	//
	WPRINT_APP_INFO( ( "stop_i2c_bus STOP\n\r" ) );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SDA );

}

void read_i2c_registers_tlc59116(void )
{
    uint8_t buf[50] = {0};

	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// address
	//
	stop_i2c_bus();
	start_i2c_bus();
	//
	//write the i2c address 1 read
	//
	write_i2c_byte2(0xC1);
	//
	// read 32 bytes
	//
	int i = 0;
	for (i=0;i<31;i++)
	{ 
		buf[i] = read_i2c_byte(WICED_FALSE);
	}
	buf[i] = read_i2c_byte(WICED_TRUE);
	for (i=0; i<32;i++)
	{
		WPRINT_APP_INFO(("%02x ", buf[i]));
		if (i==15)
			WPRINT_APP_INFO(("\r\n"));
	}
	WPRINT_APP_INFO(("\r\n"));
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "read_i2c_registers_tlc59116 complete\n\r" ) );
}

void reset_i2c_tlc59116(void )
{
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xD6); // reset byte 1
	write_i2c_byte2(0xA5); // reset byte 1
	write_i2c_byte2(0x5A); // reset byte 1
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "reset_i2c_tlc59116 complete\n\r" ) );
}

void init_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// init tlc59116
	// 
	write_i2c_byte2(0x80);      // 00 MODE1 - defaults - turn on auto-inc
    write_i2c_byte2(0x80);      // 01 MODE2 - defaults no bits set - MODE2REGSET;
	write_i2c_byte2(LEDSETOFF);  // 02 PWM0
	write_i2c_byte2(LEDSETOFF);  // 03 PWM1
	write_i2c_byte2(LEDSETOFF);  // 04 PWM2
	write_i2c_byte2(LEDSETOFF);  // 05 PWM3
	write_i2c_byte2(LEDSETOFF);  // 06 PWM4
	write_i2c_byte2(LEDSETOFF);  // 07 PWM5
	write_i2c_byte2(LEDSETOFF);  // 08 PWM6
	write_i2c_byte2(LEDSETOFF);  // 09 PWM7
	write_i2c_byte2(LEDSETOFF);  // 0A PWM8
	write_i2c_byte2(LEDSETOFF);  // 0B PWM9
	write_i2c_byte2(LEDSETOFF);  // 0C PWM10
	write_i2c_byte2(LEDSETOFF);  // 0D PWM11
	write_i2c_byte2(LEDSETOFF);  // 0E PWM12
	write_i2c_byte2(LEDSETOFF);  // 0F PWM13
	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(LEDSETOFF);  // 11 PWM15
	write_i2c_byte2(GRPPWMSET); // 12 GRPPWM;
	write_i2c_byte2(0x00);      // 13 GRPFREQ
	write_i2c_byte2(0xAA);      // 14 LEDOUT0
	write_i2c_byte2(0xAA);      // 14 LEDOUT1
	write_i2c_byte2(0xAA);      // 14 LEDOUT2
	write_i2c_byte2(0xAA);      // 14 LEDOUT3
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 complete\n\r" ) );
}

void set_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0xA2); // address 2 only PWM register write
	//
	// init tlc59116
	// 
	write_i2c_byte2(LEDSETOFF);  // 02 PWM0
	write_i2c_byte2(LEDSETOFF);  // 03 PWM1
/*
	write_i2c_byte2(LEDSETON);  // 04 PWM2
	write_i2c_byte2(LEDSETON);  // 05 PWM2
	write_i2c_byte2(LEDSETON);  // 06 PWM2
	write_i2c_byte2(LEDSETON);  // 07 PWM2
	write_i2c_byte2(LEDSETON);  // 08 PWM2
	write_i2c_byte2(LEDSETON);  // 09 PWM2
	write_i2c_byte2(LEDSETON);  // 0A PWM2
	write_i2c_byte2(LEDSETON);  // 0B PWM2
	write_i2c_byte2(LEDSETON);  // 0C PWM2
	write_i2c_byte2(LEDSETON);  // 0D PWM2
	write_i2c_byte2(LEDSETON);  // 0E PWM2
	write_i2c_byte2(LEDSETON);  // 0F PWM2
*/
	write_i2c_byte2(BRIGHTSET);  // 04 PWM2
	write_i2c_byte2(BRIGHTSET);  // 05 PWM3
	write_i2c_byte2(BRIGHTSET);  // 06 PWM4
	write_i2c_byte2(BRIGHTSET);  // 07 PWM5
	write_i2c_byte2(BRIGHTSET);  // 08 PWM6
	write_i2c_byte2(BRIGHTSET);  // 09 PWM7
	write_i2c_byte2(BRIGHTSET);  // 0A PWM8
	write_i2c_byte2(BRIGHTSET);  // 0B PWM9
	write_i2c_byte2(BRIGHTSET);  // 0C PWM10
	write_i2c_byte2(BRIGHTSET);  // 0D PWM11
	write_i2c_byte2(BRIGHTSET);  // 0E PWM12
	write_i2c_byte2(BRIGHTSET);  // 0F PWM13

	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(0x00);  // 11 PWM15
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 complete\n\r" ) );
}

void application_start( )
{


    wiced_core_init();

	//
    // setup leds & buzzer
    //
//	leds_buzzer_init();
//	xx_leds_buzzer_init();
	reset_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	init_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	set_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	int i = 0;
	while (1)
	{
		wiced_rtos_delay_milliseconds(1000);
		read_i2c_registers_tlc59116();
//		wiced_rtos_delay_milliseconds(5000);
//		if ((i++ % 5) ==0)
//			leds_buzzer_init();
	}
}
