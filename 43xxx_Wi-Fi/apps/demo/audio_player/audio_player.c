/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * Audio Player MQTT control app
 *
 */

#include <ctype.h>
#include <unistd.h>
#include <malloc.h>

#include "wiced.h"
#include "command_console.h"
#include "wifi/command_console_wifi.h"
#include "dct/command_console_dct.h"

#include "dns.h"

#include "bufmgr.h"
#include "wiced_log.h"
#include "wiced_audio.h"

#include "audio_player.h"
#include "audio_player_config.h"

#include "audio_player_display.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define AUDIO_PLAYER_DEFAULT_HTTP_PORT      (80)
#define AUDIO_PLAYER_DEFAULT_HTTPS_PORT     (443)

#define AUDIO_PLAYER_PLAYLIST_NAME          "/audio_playlist.txt"

#define TCP_DEFAULT_LISTEN_PORT             (19703)

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    int buflen;
    int maxlen;
    objhandle_t buf_handle;
    uint8_t buf[0];
} audio_buf_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

static void audio_player_set_effect(audio_player_t* player);

/******************************************************
 *               Variable Definitions
 ******************************************************/

audio_player_t* g_player;

/******************************************************
 *               Function Definitions
 ******************************************************/

wiced_result_t audio_player_msg_push(audio_player_t* player, AP_MSG_TYPE_T type, uint32_t arg1, uint32_t arg2)
{
    wiced_result_t result;
    ap_msg_t msg;

    msg.type = type;
    msg.arg1 = arg1;
    msg.arg2 = arg2;

    if ((result = wiced_rtos_push_to_queue(&player->msgq, &msg, AUDIO_PLAYER_MSGQ_PUSH_TIMEOUT_MS)) == WICED_SUCCESS)
    {
        wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_MSG_QUEUE);
    }
    else
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Error pushing AP msg %d\n", msg.type);
    }

    return result;
}


static int audio_player_log_output_handler(WICED_LOG_LEVEL_T level, char *logmsg)
{
    write(STDOUT_FILENO, logmsg, strlen(logmsg));

    return 0;
}


static wiced_result_t client_connected_callback(wiced_tcp_socket_t* socket, void* arg)
{
    audio_player_t* player = (audio_player_t*)arg;

    if (player != NULL && player->tag == AUDIO_PLAYER_TAG_VALID)
    {
        wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_CONNECT);
    }

    return WICED_SUCCESS;
}


static wiced_result_t client_disconnected_callback(wiced_tcp_socket_t* socket, void* arg)
{
    audio_player_t* player = (audio_player_t*)arg;

    if (player != NULL && player->tag == AUDIO_PLAYER_TAG_VALID)
    {
        player->disconnect_received = WICED_TRUE;
        wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_DISCONNECT);
    }

    return WICED_SUCCESS;
}


static wiced_result_t received_data_callback(wiced_tcp_socket_t* socket, void* arg)
{
    audio_player_t* player = (audio_player_t*)arg;

    if (player != NULL && player->tag == AUDIO_PLAYER_TAG_VALID)
    {
        wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_PACKET);
    }

    return WICED_SUCCESS;
}


void audio_player_delete_raw_socket(audio_player_t* player)
{
    if (player->socket_ptr)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_INFO, "Deleting socket\n");
        wiced_tcp_disconnect_with_timeout(player->socket_ptr, WICED_NO_WAIT);
        wiced_tcp_delete_socket(player->socket_ptr);
        player->socket_ptr = NULL;
    }

    player->disconnect_received = WICED_FALSE;
}


static wiced_result_t audio_client_buf_get_callback(audio_client_ref handle, void* userdata, audio_client_buf_t* ac_buf, uint32_t timeout_ms)
{
    audio_player_t* player = (audio_player_t*)userdata;
    objhandle_t buf_handle;
    audio_buf_t* buf;
    uint32_t buf_size;
    (void)handle;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || player->buf_pool == NULL)
    {
        return WICED_ERROR;
    }

    if (bufmgr_pool_alloc_buf(player->buf_pool, &buf_handle, (char **)&buf, &buf_size, timeout_ms) != BUFMGR_OK)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG4, "Unable to allocate audio packet\n");
        return WICED_ERROR;
    }

    memset(buf, 0, sizeof(audio_buf_t));

    ac_buf->opaque = buf_handle;
    ac_buf->buf    = buf->buf;
    ac_buf->buflen = player->dct_tables.dct_app->audio_buffer_size;
    ac_buf->curlen = 0;
    ac_buf->offset = 0;
    ac_buf->flags  = 0;

    return WICED_SUCCESS;
}


static wiced_result_t audio_client_buf_release_callback(audio_client_ref handle, void* userdata, audio_client_buf_t* buf)
{
    audio_player_t* player = (audio_player_t*)userdata;
    audio_render_buf_t render_buf;
    wiced_result_t result = WICED_SUCCESS;
    (void)handle;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || buf == NULL)
    {
        return WICED_ERROR;
    }

    if (player->audio)
    {
        /*
         * Pass this buffer on to audio render.
         */

        if (buf->buf && buf->curlen > 0)
        {
            render_buf.pts         = 0;
            render_buf.data_buf    = buf->buf;
            render_buf.data_offset = buf->offset;
            render_buf.data_length = buf->curlen;
            render_buf.opaque      = buf->opaque;

            result = audio_render_push(player->audio, &render_buf);

            if (result != WICED_SUCCESS)
            {
                /*
                 * Audio render didn't accept the buffer, make sure that we don't leak it.
                 */

                if (player->buf_pool && buf->opaque)
                {
                    bufmgr_pool_free_buf(player->buf_pool, (objhandle_t)buf->opaque);
                    buf->opaque = NULL;
                }
            }
        }
        else if (player->buf_pool && buf->opaque)
        {
            /*
             * The decoder is releasing this buffer without writing any data to it.
             */

            bufmgr_pool_free_buf(player->buf_pool, (objhandle_t)buf->opaque);
            buf->opaque = NULL;
        }

        if (buf->flags & AUDIO_CLIENT_BUF_FLAG_EOS)
        {
            /*
             * Tell audio render that there's no more audio coming for this stream.
             */

            result = audio_render_push(player->audio, NULL);
        }
    }
    else
    {
        /*
         * No audio render instance. Just release the buffer.
         */

        if (player->buf_pool != NULL && buf->opaque != NULL)
        {
            bufmgr_pool_free_buf(player->buf_pool, (objhandle_t)buf->opaque);
        }
    }

    return result;
}


static wiced_result_t audio_client_callback(audio_client_ref handle, void* userdata, AUDIO_CLIENT_EVENT_T event, void* arg)
{
    audio_player_t* player = (audio_player_t*)userdata;
    wiced_audio_config_t* config;
    wiced_result_t result = WICED_SUCCESS;
    (void)handle;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID)
    {
        return WICED_BADARG;
    }

    switch (event)
    {
        case AUDIO_CLIENT_EVENT_ERROR:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error from audio_client: %d\n", (int)arg);
            break;

        case AUDIO_CLIENT_EVENT_AUDIO_FORMAT:
            config = (wiced_audio_config_t*)arg;
            if (config == NULL)
            {
                return WICED_BADARG;
            }

            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "Audio format:\n");
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "    num channels: %u\n", config->channels);
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, " bits per sample: %u\n", config->bits_per_sample);
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "     sample rate: %lu\n", config->sample_rate);
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "      frame size: %u\n", config->frame_size);

            if (player->audio != NULL)
            {
                result = audio_render_configure(player->audio, config);
                if (result != WICED_SUCCESS)
                {
                    wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error returned from render configure: %d\n", result);
                }
                else
                {
                    if (player->effect_mode != AUDIO_EFFECT_MODE_NONE)
                        audio_player_set_effect(player);
                }
            }
            break;

        case AUDIO_CLIENT_EVENT_PLAYBACK_STARTED:
            break;

        case AUDIO_CLIENT_EVENT_PLAYBACK_EOS:
            audio_player_msg_push(player, AP_MSG_PLAYBACK_EOS, (uint32_t)arg, 0);
            break;

        case AUDIO_CLIENT_EVENT_DECODE_COMPLETE:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "Decode complete\n");
            break;

        case AUDIO_CLIENT_EVENT_DATA_THRESHOLD_HIGH:
            //wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Threshold high\n");
            break;

        case AUDIO_CLIENT_EVENT_DATA_THRESHOLD_LOW:
            //wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Threshold low\n");
            break;

        case AUDIO_CLIENT_EVENT_HTTP_COMPLETE:
            if (player->playlist_data)
            {
                free(player->playlist_data);
            }
            player->playlist_data = arg;

            if (player->playlist_entries)
            {
                free(player->playlist_entries);
                player->playlist_entries = NULL;
            }
            player->num_playlist_entries = 0;
            wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_PLAYLIST_LOADED);
            break;

        case AUDIO_CLIENT_EVENT_HTTP_REDIRECT:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "HTTP redirect to %s\n", (char*)arg);
            break;

        default:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Audio client event: %d\n", event);
            break;
    }

    return result;
}


wiced_result_t audio_render_buf_release(audio_render_buf_t* buf, void* userdata)
{
    audio_player_t* player = (audio_player_t*)userdata;

    /*
     * If we are in the process of shutting down, the player structure might be
     * tagged as invalid. So don't error out on that condition.
     */

    if (player == NULL)
    {
        return WICED_BADARG;
    }

    if (player->buf_pool != NULL && buf != NULL)
    {
        bufmgr_pool_free_buf(player->buf_pool, (objhandle_t)buf->opaque);
        buf->opaque = NULL;
    }

    return WICED_SUCCESS;
}


wiced_result_t audio_render_event(audio_render_ref handle, void* userdata, AUDIO_RENDER_EVENT_T event, void* arg)
{
    audio_player_t* player = (audio_player_t*)userdata;
    (void)handle;
    (void)arg;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID)
    {
        return WICED_BADARG;
    }

    switch (event)
    {
        case AUDIO_RENDER_EVENT_EOS:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_DEBUG0, "Audio render EOS event\n");
            audio_player_msg_push(player, AP_MSG_PLAYBACK_EOS, (uint32_t)arg, 0);
            break;

        default:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unrecognized audio render event: %d\n", event);
            break;
    }

    return WICED_SUCCESS;
}


static wiced_result_t audio_player_get_host_and_port(audio_player_t* player)
{
    const char* ptr;
    wiced_bool_t https;
    int copy_len;
    int len;

    player->port = 0;
    https = WICED_FALSE;

    /*
     * Skip over http:// or https://
     */

    ptr = player->server_uri;
    if ((ptr[0] == 'h' || ptr[0] == 'H') && (ptr[1] == 't' || ptr[1] == 'T') && (ptr[2] == 't' || ptr[2] == 'T') && (ptr[3] == 'p' || ptr[3] == 'P'))
    {
        ptr += 4;
        if (ptr[0] == 's' || ptr[0] == 'S')
        {
            https = WICED_TRUE;
            ptr++;
        }
        if (ptr[0] != ':' || ptr[1] != '/' || ptr[2] != '/')
        {
            return WICED_BADARG;
        }
        ptr += 3;
    }

    /*
     * Isolate the host part of the URI.
     */

    for (len = 0; ptr[len] != ':' && ptr[len] != '/' && ptr[len] != '\0'; )
    {
        len++;
    }

    if (ptr[len] == ':')
    {
        player->port = atoi(&ptr[len + 1]);
    }
    else
    {
        if (https == WICED_TRUE)
        {
            player->port = AUDIO_PLAYER_DEFAULT_HTTPS_PORT;
        }
        else
        {
            player->port = AUDIO_PLAYER_DEFAULT_HTTP_PORT;
        }
    }

    /*
     * And copy it over.
     */

    copy_len = len;
    if (copy_len > sizeof(player->hostname) - 1)
    {
        copy_len = sizeof(player->hostname) - 1;
    }
    memcpy(player->hostname, ptr, copy_len);
    player->hostname[copy_len] = '\0';

    return WICED_SUCCESS;
}


static void audio_player_load_playlist(audio_player_t* player)
{
    wiced_result_t      result;

    /*
     * Get and store the hostname and the port. We'll need that information later.
     */

    result = audio_player_get_host_and_port(player);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to parse server URI %s\n", player->server_uri);
        return;
    }

    if (player->port == AUDIO_PLAYER_DEFAULT_HTTP_PORT)
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "http://%s%s", player->hostname, AUDIO_PLAYER_PLAYLIST_NAME);
    }
    else if (player->port == AUDIO_PLAYER_DEFAULT_HTTPS_PORT)
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "https://%s%s", player->hostname, AUDIO_PLAYER_PLAYLIST_NAME);
    }
    else
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "%s:%d%s", player->hostname, player->port, AUDIO_PLAYER_PLAYLIST_NAME);
    }

    result = audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_LOAD_FILE, player->work_buffer);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error returned from load file\n");
    }
}


static void audio_player_output_playlist(audio_player_t* player)
{
    int i;

    if (player->num_playlist_entries == 0 || player->playlist_entries == NULL)
    {
        wiced_log_printf("No playlist loaded\n");
    }
    else
    {
        wiced_log_printf("Playlist from %s:%d\n", player->hostname, player->port);
        for (i = 0; i < player->num_playlist_entries; i++)
        {
            wiced_log_printf("%3lu %s\n", i, player->playlist_entries[i]);
        }
    }
}


static void audio_player_parse_playlist(audio_player_t* player)
{
    char* ptr;
    int count;
    int inspace;

    /*
     * Do we have data to parse?
     */

    if (player->playlist_data == NULL)
    {
        return;
    }

    /*
     * Count how many playlist entries we have converting all whitespace to nuls.
     */

    count   = 0;
    inspace = 1;
    for (ptr = player->playlist_data; *ptr != '\0'; ptr++)
    {
        if (isspace((int)*ptr))
        {
            *ptr = '\0';
            inspace = 1;
        }
        else if (inspace)
        {
            inspace = 0;
            count++;
        }
    }

    /*
     * Allocate the entries list.
     */

    if ((player->playlist_entries = calloc(count, sizeof(char*))) == NULL)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to allocate playlist entries\n");
        free(player->playlist_data);
        player->playlist_data = NULL;
        return;
    }
    player->num_playlist_entries = count;

    /*
     * And set up the individual entries.
     */

    count   = 0;
    inspace = 1;
    for (ptr = player->playlist_data; count < player->num_playlist_entries; ptr++)
    {
        if (*ptr == '\0')
        {
            inspace = 1;
        }
        else if (inspace)
        {
            inspace = 0;
            player->playlist_entries[count++] = ptr;
        }
    }

    /*
     * Print out the playlist.
     */

    audio_player_output_playlist(player);
}


static void audio_player_play_playlist_entry(audio_player_t* player)
{
    wiced_result_t result;

    /*
     * Valid playlist index?
     */

    if (player->play_index_current < 0 || player->play_index_current >= player->num_playlist_entries)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid playlist index %d\n", player->play_index_current);
        return;
    }

    /*
     * Construct the complete track URI.
     */

    if (player->port == AUDIO_PLAYER_DEFAULT_HTTP_PORT)
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "http://%s%s", player->hostname, player->playlist_entries[player->play_index_current]);
    }
    else if (player->port == AUDIO_PLAYER_DEFAULT_HTTPS_PORT)
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "https://%s%s", player->hostname, player->playlist_entries[player->play_index_current]);
    }
    else
    {
        snprintf(player->work_buffer, sizeof(player->work_buffer), "%s:%d%s", player->hostname, player->port, player->playlist_entries[player->play_index_current]);
    }
    player->work_buffer[sizeof(player->work_buffer) - 1] = '\0';

    /*
     * And ask the audio client to play it for us.
     */

    if (player->starting_offset_ms)
    {
        audio_client_play_offset_t play_offset;

        play_offset.uri       = player->work_buffer;
        play_offset.offset_ms = player->starting_offset_ms;

        result = audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_PLAY_OFFSET, &play_offset);
    }
    else
    {
        result = audio_client_ioctl(player->audio_client, (player->play_contiguous ? AUDIO_CLIENT_IOCTL_PLAY_CONTIGUOUS : AUDIO_CLIENT_IOCTL_PLAY), player->work_buffer);
    }
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to start playback (%d)\n", result);
    }
}


static void audio_player_set_effect(audio_player_t* player)
{
    wiced_result_t      result;

    if (player->effect_mode >= AUDIO_EFFECT_MODE_MAX)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Invalid parameter\n");
        return;
    }

    result = audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_EFFECT, (void*)((uint32_t)player->effect_mode));
    if(result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to set audio effect mode (%d)\n", result);
    }
}


static void audio_player_start_play_raw(audio_player_t* player)
{
    wiced_result_t result;

    wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Opening socket on port %d (%p)\n", TCP_DEFAULT_LISTEN_PORT, player->socket_ptr);

    result = wiced_tcp_create_socket(&player->socket, WICED_STA_INTERFACE);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to create socket\n");
        goto _error_out;
    }

    player->socket_ptr = &player->socket;
    result = wiced_tcp_register_callbacks(player->socket_ptr, client_connected_callback,
                                          received_data_callback, client_disconnected_callback, player);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error setting socket callbacks\n");
        goto _error_out;
    }

    result = wiced_tcp_listen(player->socket_ptr, TCP_DEFAULT_LISTEN_PORT);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error setting socket in listen mode\n");
        goto _error_out;
    }

    return;

  _error_out:
    audio_player_delete_raw_socket(player);
    player->play_raw_active = WICED_FALSE;
}


static void audio_player_end_play_raw(audio_player_t* player)
{
    audio_client_push_buf_t buf;

    memset(&buf, 0, sizeof(audio_client_push_buf_t));
    audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_PUSH_RAW, (void*)&buf);
    audio_player_delete_raw_socket(player);
    player->play_raw_active = WICED_FALSE;
}

static void audio_player_handle_packet(audio_player_t* player)
{
    wiced_packet_t*         packet;
    wiced_result_t          result;
    uint8_t*                data;
    uint16_t                data_length;
    uint16_t                available_data_length;
    audio_client_push_buf_t ac_buf;

    while (player->socket_ptr && player->play_raw_active)
    {
        /*
         * Get the next packet.
         */

        result = wiced_tcp_receive(player->socket_ptr, &packet, WICED_NO_WAIT);
        if (result != WICED_SUCCESS)
        {
            if (result != WICED_TCPIP_TIMEOUT && !player->disconnect_received)
            {
                wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Error receiving packet (%d)\n", result);
            }
            return;
        }

        wiced_packet_get_data(packet, 0, &data, &data_length, &available_data_length);

        ac_buf.buf       = data;
        ac_buf.buflen    = data_length;
        ac_buf.pushedlen = 0;

        do
        {
            result = audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_PUSH_RAW, &ac_buf);
            if (result != WICED_SUCCESS)
            {
                if (result == WICED_PENDING)
                {
                    wiced_rtos_delay_milliseconds(2);
                }
                else
                {
                    wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "audio_client: push raw failed with %d\n", result);
                    break;
                }
            }
        } while (result != WICED_SUCCESS);

        wiced_packet_delete(packet);
    }
}


static wiced_result_t audio_player_process_session_time(audio_player_t* player, uint32_t duration_ms)
{
    wiced_result_t rv;
    int32_t pll_ppm;

    if (duration_ms > 0)
    {
        pll_ppm = player->dct_tables.dct_app->pll_ppm_init;
        rv = audio_player_voice_clock_adj(player, &pll_ppm, duration_ms);

        if ((WICED_SUCCESS == rv) && (pll_ppm != player->dct_tables.dct_app->pll_ppm_init))
        {
            player->dct_tables.dct_app->pll_ppm_init = pll_ppm;

            /* save dct for new pll_ppm value */
            wiced_dct_write((void*)player->dct_tables.dct_app, DCT_APP_SECTION, 0, sizeof(audio_player_dct_t));

            /* Update the PLL adjustment for audio client */
            if (player->audio_client)
            {
                audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_SET_PLL_PPM_INIT, (void*)player->dct_tables.dct_app->pll_ppm_init);
            }

            /* Update the PLL adjustment for audio render if we're driving the audio output directly*/
            if (player->audio)
            {
                audio_render_command(player->audio, AUDIO_RENDER_CMD_SET_PLL_PPM_INIT, (void*)player->dct_tables.dct_app->pll_ppm_init);
            }

            /* voice confirmation */
            audio_player_play_voice(player, VOICE_AUDIO_PLAYER_CLOCK_TUNED, 0);
        }
    }

    return WICED_SUCCESS;
}


static void audio_player_process_msg_queue(audio_player_t* player)
{
    ap_msg_t msg;
    wiced_result_t result;

    result = wiced_rtos_pop_from_queue(&player->msgq, &msg, WICED_NO_WAIT);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Error reading ap message from queue\n");
        return;
    }

    switch (msg.type)
    {
        case AP_MSG_PLAYBACK_EOS:
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "Playback EOS\n");
            audio_player_process_session_time(player, msg.arg1);
            if (player->num_playlist_entries > 0)
            {
                if (++player->play_index_current > player->play_index_end)
                {
                    if (player->play_repeat && player->play_index_start >= 0)
                    {
                        player->play_index_current = player->play_index_start;
                    }
                    else
                    {
                        player->play_index_current = -1;
                    }
                }

                if (player->play_index_current >= 0)
                {
                    audio_player_play_playlist_entry(player);
                }
            }
            break;

        default:
            wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Unexpected msg %d\n", msg.type);
            break;
    }

    /*
     * If there's another message waiting, make sure that the proper event flag is set.
     */

    if (wiced_rtos_is_queue_empty(&player->msgq) != WICED_SUCCESS)
    {
        wiced_rtos_set_event_flags(&player->player_events, PLAYER_EVENT_MSG_QUEUE);
    }
}


static void audio_player_mainloop(audio_player_t* player)
{
    wiced_result_t      result;
    uint32_t            events;

    wiced_log_msg(WLF_AUDIO, WICED_LOG_NOTICE, "Begin audio player mainloop\n");

    /* we are ready to play */
    if (player->dct_tables.dct_app->pll_ppm_init == 0)
    {
        audio_player_play_voice(player, VOICE_AUDIO_PLAYER_CLOCK_NEEDS_TUNING, 0);
    }
    else
    {
        audio_player_play_voice(player, VOICE_AUDIO_PLAYER_IS_READY, 0);
    }

    while (player->tag == AUDIO_PLAYER_TAG_VALID)
    {
        events = 0;

        result = wiced_rtos_wait_for_event_flags(&player->player_events, PLAYER_ALL_EVENTS, &events, WICED_TRUE, WAIT_FOR_ANY_EVENT, WICED_WAIT_FOREVER);
        if (result != WICED_SUCCESS)
        {
            continue;
        }

        if (events & PLAYER_EVENT_SHUTDOWN)
        {
            wiced_log_msg(WLF_AUDIO, WICED_LOG_INFO, "mainloop received EVENT_SHUTDOWN\n");
            break;
        }

        if (events & PLAYER_EVENT_LOAD_PLAYLIST)
        {
            audio_player_load_playlist(player);
        }

        if (events & PLAYER_EVENT_PLAYLIST_LOADED)
        {
            audio_player_parse_playlist(player);
        }

        if (events & PLAYER_EVENT_OUTPUT_PLAYLIST)
        {
            audio_player_output_playlist(player);
        }

        if (events & PLAYER_EVENT_PLAY)
        {
            audio_player_play_playlist_entry(player);
        }

        if (events & PLAYER_EVENT_MSG_QUEUE)
        {
            audio_player_process_msg_queue(player);
        }

        if (events & PLAYER_EVENT_EFFECT)
        {
            audio_player_set_effect(player);
        }

        if (events & PLAYER_EVENT_PLAY_RAW)
        {
            audio_player_start_play_raw(player);
        }

        if (events & PLAYER_EVENT_PLAY_VOICE)
        {
            audio_player_play_voice(player, VOICE_AUDIO_PLAYER_IS_READY, 0);
        }

        if (events & PLAYER_EVENT_CONNECT)
        {
            result = wiced_tcp_accept(player->socket_ptr);
            if (result != WICED_SUCCESS)
            {
                wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Error accepting connection (%d)\n", result);
            }
        }

        if (events & PLAYER_EVENT_DISCONNECT)
        {
            audio_player_end_play_raw(player);
        }

        if (events & PLAYER_EVENT_PACKET)
        {
            audio_player_handle_packet(player);
        }

        if (events & PLAYER_EVENT_RELOAD_DCT_WIFI)
        {
            audio_player_config_reload_dct_wifi(&player->dct_tables);
        }

        if (events & PLAYER_EVENT_RELOAD_DCT_NETWORK)
        {
            audio_player_config_reload_dct_network(&player->dct_tables);
        }
    }   /* while */

    wiced_log_msg(WLF_AUDIO, WICED_LOG_NOTICE, "End audio player mainloop\n");
}


static void audio_player_shutdown(audio_player_t* player)
{
    /*
     * Shutdown the console.
     */

    audio_player_console_stop(player);

    wiced_rtos_deinit_event_flags(&player->player_events);
    wiced_rtos_deinit_queue(&player->msgq);

    player->tag = AUDIO_PLAYER_TAG_INVALID;

    if (player->audio_client)
    {
        audio_client_deinit(player->audio_client);
        player->audio_client = NULL;
    }

    if (player->audio)
    {
        audio_render_deinit(player->audio);
        player->audio = NULL;
    }

    if (player->playlist_data)
    {
        free(player->playlist_data);
        player->playlist_data = NULL;
    }

    if (player->playlist_entries)
    {
        free(player->playlist_entries);
        player->playlist_entries = NULL;
    }

    audio_player_config_deinit(&player->dct_tables);

    if (player->buf_pool != NULL)
    {
        bufmgr_pool_destroy(player->buf_pool);
        player->buf_pool = NULL;
    }

#ifdef AUDIO_PLAYER_ENABLE_MQTT
    audio_player_display_stop(&player->display);
    audio_player_mqtt_stop(&player->mqtt);
    audio_player_sntp_stop();
    audio_player_voice_deinit(player);
#endif

    free(player);
}


static audio_player_t* audio_player_init(void)
{
    audio_player_t*         player;
    audio_client_params_t   params;
    audio_render_params_t   render_params;
    wiced_result_t          result;
    uint32_t                tag;

    tag = AUDIO_PLAYER_TAG_VALID;

    /* Initialize the device */
    result = wiced_init();
    if (result != WICED_SUCCESS)
    {
        return NULL;
    }

     /* initialize audio */
    platform_init_audio();

    /*
     * Initialize the logging subsystem.
     */

    wiced_log_init(WICED_LOG_ERR, audio_player_log_output_handler, NULL);

    /*
     * Allocate the main player structure.
     */

    player = calloc_named("audio_player", 1, sizeof(audio_player_t));
    if (player == NULL)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to allocate player structure\n");
        return NULL;
    }

    /*
     * Create the command console.
     */

    result = audio_player_console_start(player);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error starting the command console\n");
        free(player);
        return NULL;
    }

    /*
     * Create our event flags.
     */

    result = wiced_rtos_init_event_flags(&player->player_events);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error initializing player event flags\n");
        tag = AUDIO_PLAYER_TAG_INVALID;
    }

    result = wiced_rtos_init_queue(&player->msgq, NULL, sizeof(ap_msg_t), AUDIO_PLAYER_MSG_QUEUE_SIZE);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Error initializing message queue\n");
        tag = AUDIO_PLAYER_TAG_INVALID;
    }

    /* read in our configurations */
    audio_player_config_init(&player->dct_tables);
    player->current_volume = player->dct_tables.dct_app->volume;

    /* print out our current configuration */
    audio_player_config_print_info(&player->dct_tables);

    /* Bring up the network interface */
    result = wiced_network_up_default(&player->dct_tables.dct_network->interface, NULL);
    if (result != WICED_SUCCESS)
    {
        /*
         * The network didn't initialize but we don't want to consider that a fatal error.
         * This allows the user to use the command console to update the network settings.
         */

        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Bringing up network interface failed\n");
    }

    /*
     * Fire off the audio client service.
     */

    memset(&params, 0, sizeof(audio_client_params_t));

    params.event_cb                    = audio_client_callback;
    params.userdata                    = player;
    params.interface                   = player->dct_tables.dct_network->interface;
    params.load_playlist_files         = WICED_TRUE;
    params.disable_hls_streaming       = WICED_FALSE; /* keep HLS (HTTP Live Streaming) support enabled */
    params.hls_max_entry_count         = 0;           /* let audio_client pick the minimum number of cached HLS playlist entries */

    params.data_buffer_num             = player->dct_tables.dct_app->http_buffer_num;
    params.audio_buffer_num            = player->dct_tables.dct_app->audio_buffer_num;
    params.audio_buffer_size           = player->dct_tables.dct_app->audio_buffer_size;
    params.audio_period_size           = player->dct_tables.dct_app->audio_period_size;
    params.data_buffer_preroll         = player->dct_tables.dct_app->http_buffer_preroll;
    params.no_length_disable_preroll   = player->dct_tables.dct_app->disable_preroll ? WICED_TRUE : WICED_FALSE;

    params.data_threshold_high         = player->dct_tables.dct_app->http_threshold_high;
    params.data_threshold_low          = player->dct_tables.dct_app->http_threshold_low;
    params.high_threshold_read_inhibit = player->dct_tables.dct_app->http_read_inhibit ? WICED_TRUE: WICED_FALSE;

    params.device_id                   = player->dct_tables.dct_app->audio_device_tx;
    params.volume                      = player->dct_tables.dct_app->volume;
    params.enable_playback             = player->dct_tables.dct_app->app_playback ? WICED_FALSE : WICED_TRUE;

    params.buffer_get                  = audio_client_buf_get_callback;
    params.buffer_release              = audio_client_buf_release_callback;

    params.pll_ppm_init                = player->dct_tables.dct_app->pll_ppm_init;

#ifdef AUDIO_PLAYER_ENABLE_VOICE
    params.enable_file_ops             = WICED_TRUE;
    params.file_ops.file_open          = audio_player_open_file;
    params.file_ops.file_read          = audio_player_read_file;
    params.file_ops.file_seek          = audio_player_seek_file;
    params.file_ops.file_close         = audio_player_close_file;
#endif

    if ((player->audio_client = audio_client_init(&params)) == NULL)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to initialize audio_client\n");
        tag = AUDIO_PLAYER_TAG_INVALID;
    }

    if (player->dct_tables.dct_app->app_playback)
    {
        /*
         * Create the audio buffer pool.
         */
        if (bufmgr_pool_create(&player->buf_pool, "audio_player_bufs", sizeof(audio_buf_t) + player->dct_tables.dct_app->audio_buffer_size,
            player->dct_tables.dct_app->audio_buffer_num, player->dct_tables.dct_app->audio_buffer_num, 0, sizeof(uint32_t)) != BUFMGR_OK)
        {
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "bufmgr_pool_create failed\n");
            tag = AUDIO_PLAYER_TAG_INVALID;
        }

        /*
         * Fire off the audio render component.
         */

        memset(&render_params, 0, sizeof(audio_render_params_t));
        render_params.buffer_nodes   = player->dct_tables.dct_app->audio_buffer_num;
        render_params.device_id      = player->dct_tables.dct_app->audio_device_tx;
        render_params.period_size    = player->dct_tables.dct_app->audio_period_size;
        render_params.userdata       = player;
        render_params.buf_release_cb = audio_render_buf_release;
        render_params.event_cb       = audio_render_event;
        render_params.pll_ppm_init   = player->dct_tables.dct_app->pll_ppm_init;

        player->audio = audio_render_init(&render_params);
        if (player->audio == NULL)
        {
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Unable to initialize audio_render\n");
            tag = AUDIO_PLAYER_TAG_INVALID;
        }
    }

#ifdef AUDIO_PLAYER_ENABLE_MQTT
    if (tag == AUDIO_PLAYER_TAG_VALID)
    {
        audio_player_mqtt_params_t mqtt_params;

        /* enable voice feedback */
        audio_player_voice_init(player);

        /* enable SNTP time control */
        audio_player_sntp_start(player->dct_tables.dct_app->sntp_server_ipaddr_01,
                                player->dct_tables.dct_app->sntp_server_ipaddr_02, player->dct_tables.dct_app->sntp_time_sync_ms);

        /* enable MQTT */
        memset(&mqtt_params, 0, sizeof(mqtt_params));
        mqtt_params.broker_ip    = player->dct_tables.dct_app->mqtt_broker_ipaddr;
        mqtt_params.topic_area   = player->dct_tables.dct_app->mqtt_topic_area;
        mqtt_params.topic_group  = player->dct_tables.dct_app->mqtt_topic_group;
        mqtt_params.topic_device = player->dct_tables.dct_app->mqtt_topic_device;
        mqtt_params.qos          = player->dct_tables.dct_app->mqtt_qos;
        audio_player_mqtt_start(&player->mqtt, &mqtt_params);

        /* enable display */
        audio_player_display_start(&player->display, player->dct_tables.dct_app->mqtt_topic_area,
                                   player->dct_tables.dct_app->mqtt_topic_group, player->dct_tables.dct_app->mqtt_topic_device, WICED_TRUE);
        audio_player_display_title(&player->display, "player is ready", NULL);
    }
#endif

    /* set our valid tag */
    player->tag = tag;

    return player;
}


void application_start(void)
{
    audio_player_t* player;

    /*
     * Main initialization.
     */

    if ((player = audio_player_init()) == NULL)
    {
        return;
    }
    g_player = player;

    if (player->tag != AUDIO_PLAYER_TAG_VALID)
    {
        /*
         * We didn't initialize successfully. Bail out here so that the console
         * will remain active. This lets the user correct any invalid configuration parameters.
         * Mark the structure as valid so the console command processing will work and
         * note that we are intentionally leaking the player structure memory here.
         */

        player->tag = AUDIO_PLAYER_TAG_VALID;
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Main application thread exiting...command console still active\n");
        return;
    }

    /*
     * Drop into our main loop.
     */

    audio_player_mainloop(player);

    /*
     * Cleanup and exit.
     */

    g_player = NULL;
    audio_player_shutdown(player);
    player = NULL;
}
