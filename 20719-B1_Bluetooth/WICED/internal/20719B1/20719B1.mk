# Define the base chip family name
BASE_NAME = 20719

# Define patch entries
NUM_PATCH_ENTRIES = 256
PATCH_ENTRY_SIZE  = 4

# Define 20719 common limits
# Begin address of ROM
IROM_BEGIN_ADDR		= 0x00000000
# Available ROM = 2M
IROM_LENGTH			= 0x00200000
# Begin address of RAM
SRAM_BEGIN_ADDR		= 0x00200000
# Available RAM = 448K
SRAM_LENGTH			= 0x00070000
#Begin Address of PATCH ROM
PROM_BEGIN_ADDR		= 0x00270400
#Length of PATCH ROM
PROM_LENGTH			= 0x0000FC00
# Length of patch table = 1024 bytes
PATCH_TABLE_LENGTH	= ($(NUM_PATCH_ENTRIES) * $(PATCH_ENTRY_SIZE))
# Begin address of AON.
AON_BEGIN_ADDR      = 0x280000
# Length of AON
AON_LENGTH          = 0x4000
# Begin address of flash0
FLASH0_BEGIN_ADDR	= 0x00500000
# Available flash = 1M
FLASH0_LENGTH		= 0x00100000
# This is a cortex m4
CHIP_CORE           = cortex-m4
# Core specific options for this chip
CORE_COMMON_OPTIONS = -mcpu=$(CHIP_CORE) -mfloat-abi=hard -mfpu=fpv4-sp-d16 -mthumb -mlittle-endian
ifeq ($(APP_XIP),1)
CORE_COMMON_OPTIONS += -mlong-calls
endif
