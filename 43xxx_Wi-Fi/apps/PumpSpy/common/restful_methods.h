
extern void clear_post_buffers();
extern wiced_result_t post_the_post(void);
extern wiced_result_t post_pump_cycles(uint64_t utcunixtime, uint64_t cycle_time, uint16_t current);
extern wiced_result_t post_ping(uint16_t idpings_data_type, uint64_t utcunixtime, float value);
extern wiced_bool_t post_outlet_alerts(uint16_t idalerts_type, uint64_t utcunixtime, uint16_t value);
extern wiced_result_t get_system_time();
extern wiced_result_t get_bearer_token();
