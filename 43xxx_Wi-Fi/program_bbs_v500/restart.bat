echo --------------------------
echo Resetting target
echo --------------------------
openocd-all-brcm-libftdi.exe -d -c "log_output openocd_log.txt" -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown >> openocd_log.txt 2>&1 && echo Target running
