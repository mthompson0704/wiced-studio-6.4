#
# Copyright 2016, PumpSpy
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of PumpSpy.
#

NAME := PumpOutlet_newCurrent

$(NAME)_SOURCES    := main.c 

$(NAME)_COMPONENTS := protocols/HTTP \
					  daemons/pumpspy_configuration \
					  daemons/ota_server 

GLOBAL_DEFINES += APPLICATION_STACK_SIZE=10000 
GLOBAL_DEFINES += APPLICATION_WATCHDOG_TIMEOUT_SECONDS=15

APPLICATION_DCT := config_mode_dct.c

#NETWORK_CONFIG_DCT_H := network_config_dct.h

#WIFI_CONFIG_DCT_H := wifi_config_dct.h

#Set factory reset application to be this same application.
FR_APP    := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
DCT_IMAGE := $(OUTPUT_DIR)/DCT.stripped.elf
#FR_APP    := $(SOURCE_ROOT)/build/snip.scan-ISM43362_M3G_L44-ThreadX-NetX/binary/snip.scan-ISM43362_M3G_L44-ThreadX-NetX.stripped.elf
#APP0      := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
#APP1      := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
#APP2      := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).stripped.elf
##APP0      := $(SOURCE_ROOT)/build/snip.scan-ISM43362_M3G_L44-ThreadX-NetX/binary/snip.scan-ISM43362_M3G_L44-ThreadX-NetX.stripped.elf
