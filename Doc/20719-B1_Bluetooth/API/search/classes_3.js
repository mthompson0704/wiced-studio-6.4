var searchData=
[
  ['desc_5finfo',['desc_info',['../structdesc__info.html',1,'']]],
  ['dma_5fctl0_5freg',['DMA_CTL0_REG',['../union_d_m_a___c_t_l0___r_e_g.html',1,'']]],
  ['dma_5fctl1_5freg',['DMA_CTL1_REG',['../union_d_m_a___c_t_l1___r_e_g.html',1,'']]],
  ['dma_5fctl2_5freg',['DMA_CTL2_REG',['../union_d_m_a___c_t_l2___r_e_g.html',1,'']]],
  ['dma_5flli',['DMA_LLI',['../struct_d_m_a___l_l_i.html',1,'']]],
  ['dma_5ftransfer_5frequest',['DMA_TRANSFER_REQUEST',['../struct_d_m_a___t_r_a_n_s_f_e_r___r_e_q_u_e_s_t.html',1,'']]]
];
