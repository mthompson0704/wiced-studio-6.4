@echo off 
@echo %time%
@echo --------------------------
@echo Downloading Bootloader ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase waf.bootloader-NoOS-ISM4343_wbm_L151_BBS2.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Bootloader download complete && "echo.exe" || echo "Error while programming bootloader - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading DCT ...
@echo --------------------------
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "verify_image_checksum DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
REM openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo DCT download complete && "echo.exe" || echo "Error while programming DCT - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading Application ...
@echo --------------------------
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "verify_image_checksum PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
REM openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Application download complete && "echo.exe" || echo "Error while programming Application - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Resetting target
@echo --------------------------
openocd-all-brcm-libftdi.exe -c "log_output openocd_log.txt" -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown >> openocd_log.txt 2>&1 && echo Target running
@echo %time%
@echo --------------------------
@echo Programming Complete 
@echo --------------------------
