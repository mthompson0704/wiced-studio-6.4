
#include <stdlib.h>
#include "wiced.h"
#include "waf_platform.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "control_leds.h"

extern void write_app_dct();
//
// Config button IRQ handler
//
//static void config_button_handler( void* arg )
void config_button_handler( void* arg )
{
    uint64_t server_time;
    uint64_t config_time;
//    WPRINT_APP_INFO (( "config button pressed \n"));
    config_button_passed = WICED_TRUE;
    config_mode = WICED_TRUE;
    config_mode = wiced_gpio_input_get( CONFIG_BUTTON ) ? WICED_FALSE : WICED_TRUE;
    if (config_mode == WICED_TRUE)
    {
        WPRINT_APP_INFO(("Config Mode Pressed\r\n"));
        //
        // measure button press time
        //
        wiced_time_get_utc_time_ms(&server_time);
        while (config_mode == WICED_TRUE)
        {
            config_mode = wiced_gpio_input_get( CONFIG_BUTTON ) ? WICED_FALSE : WICED_TRUE;
            wiced_time_get_utc_time_ms(&config_time);
    		if (((config_time - server_time) >= 2500) && ((config_time - server_time) < 7500))
    		{
    			WPRINT_APP_INFO(("State is Provisioning AP\r\n"));
    			control_leds(LED_RED_GREEN);
    			system_state = STATE_PROVISIONING;
    			dct.NewConfigurationFlag = 0x0F0F0F0F;
    		}
    		else if (((config_time - server_time) >= 7500) && ((config_time - server_time) < 10000))
    		{
    			WPRINT_APP_INFO(("State is OTA AP\r\n"));
    			control_leds(LED_GREEN);
    			system_state = STATE_OTA;
    			dct.NewConfigurationFlag = 0x0D0C0B0A;
    		}
    		else if (((config_time - server_time) >= 10000) && ((config_time - server_time) < 15000))
    		{
    			WPRINT_APP_INFO(("State is Test Mode AP\r\n"));
    			control_leds(LED_RED);
    			system_state = STATE_TEST_MODE;
    			dct.NewConfigurationFlag = 0x0B0B0B0B; //0xFFFFFFFF;
    		}
    		else if (((config_time - server_time) > 15000))
    		{
    			WPRINT_APP_INFO(("Going to factory reset \r\n"));
    			WPRINT_APP_INFO(("{\"button_press\" : 4 }"));
    			control_leds(LED_RED);
    			wiced_dct_restore_factory_reset( );
    			wiced_waf_app_set_boot( DCT_FR_APP_INDEX, PLATFORM_DEFAULT_LOAD );
    			WPRINT_APP_INFO(("Restarting... \r\n"));
    			wiced_framework_reboot();
    		}
        }
        WPRINT_APP_INFO(("Config Mode NOT Pressed\r\n"));
        //
        // take action on press time
        //
		if (((config_time - server_time) >= 2500) && ((config_time - server_time) < 7500))
		{
			WPRINT_APP_INFO(("State is Provisioning AP\r\n"));
			control_leds(LED_RED_GREEN);
			system_state = STATE_PROVISIONING;
			dct.NewConfigurationFlag = 0x0F0F0F0F;
			write_app_dct();
			wiced_framework_reboot();
		}
		else if (((config_time - server_time) >= 7500) && ((config_time - server_time) < 10000))
		{
			WPRINT_APP_INFO(("State is OTA AP\r\n"));
			control_leds(LED_GREEN);
			system_state = STATE_OTA;
			dct.NewConfigurationFlag = 0x0D0C0B0A;
			write_app_dct();
			wiced_framework_reboot();
		}
		else if (((config_time - server_time) >= 10000) && ((config_time - server_time) < 15000))
		{
			WPRINT_APP_INFO(("State is Test Mode AP\r\n"));
			control_leds(LED_RED);
			system_state = STATE_TEST_MODE;
			dct.NewConfigurationFlag = 0x0B0B0B0B; //0xFFFFFFFF;
			write_app_dct();
			wiced_framework_reboot();
		}
		else if (((config_time - server_time) > 15000))
		{
			WPRINT_APP_INFO(("Going to factory reset \r\n"));
			control_leds(LED_RED);
			wiced_dct_restore_factory_reset( );
			wiced_waf_app_set_boot( DCT_FR_APP_INDEX, PLATFORM_DEFAULT_LOAD );
			WPRINT_APP_INFO(("Restarting... \r\n"));
			wiced_framework_reboot();

		}
    }

}
