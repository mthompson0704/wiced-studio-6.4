var modules =
[
    [ "Bluetooth", "group__wicedbt.html", "group__wicedbt" ],
    [ "WICED trace utils", "group__wiced__utils.html", "group__wiced__utils" ],
    [ "Managing compiler diagnostics.", "group___compiler_diagnostics.html", "group___compiler_diagnostics" ],
    [ "Support for packed enumerations.", "group___packed_enum.html", "group___packed_enum" ],
    [ "WICED OTA Firmware Upgrade", "group__group__ota__wiced__firmware__upgrade.html", "group__group__ota__wiced__firmware__upgrade" ],
    [ "Hardware Drivers", "group___hardware_drivers.html", "group___hardware_drivers" ],
    [ "WICED result", "group___result.html", "group___result" ],
    [ "Crypto functions", "group__crypto.html", "group__crypto" ],
    [ "SPP Protocol Library API", "group__wiced__bt__spp__api__functions.html", "group__wiced__bt__spp__api__functions" ]
];