var group__keyscan =
[
    [ "KeyscanRegistration", "struct_keyscan_registration.html", [
      [ "next", "struct_keyscan_registration.html#ad97fb5f1b7ef36d5714eee2d0abf1980", null ],
      [ "userdata", "struct_keyscan_registration.html#afd0ffb02780e738d4c0a10ab833b7834", null ],
      [ "userfn", "struct_keyscan_registration.html#a526f7101f87fcafc7530269c7739d89c", null ]
    ] ],
    [ "KeyscanState", "struct_keyscan_state.html", [
      [ "keyscan_events", "struct_keyscan_state.html#a9e1e8741f71b1517e0f9dfc37cb65d96", null ],
      [ "keyscan_pollingKeyscanHw", "struct_keyscan_state.html#afe8d298afe62ffb80118bd4098a07b05", null ],
      [ "keyscanFirstReg", "struct_keyscan_state.html#aeabfd3018f6e6815aa2c82a769d9069b", null ],
      [ "keysPressedCount", "struct_keyscan_state.html#aedd6f5dfc0df9fb10099123da87d3cda", null ]
    ] ],
    [ "KeyscanQueue", "group__keyscan_queue.html", "group__keyscan_queue" ],
    [ "keyscan_eventsPending", "group__keyscan.html#gab27ee060363fca8c5034e9874bc14764", null ],
    [ "keyscan_getNextEvent", "group__keyscan.html#ga04fdf1632df1087531dd86c1ed631b56", null ],
    [ "keyscan_registerForEventNotification", "group__keyscan.html#ga51cf23bf590e2d4a7d7a3f9b1ccd9d74", null ],
    [ "keyscan_reset", "group__keyscan.html#ga0ee584e0729ee34108ac1ebf17bdeea9", null ],
    [ "keyscan_turnOff", "group__keyscan.html#gae92d85d9e762630eea1080d5cab62bcd", null ],
    [ "keyscan_turnOn", "group__keyscan.html#ga163c8d40ccee6b5f88c203814bbc0c2b", null ],
    [ "wiced_hal_keyscan_config_gpios", "group__keyscan.html#ga2d049bb622878f77a709a3351012b332", null ],
    [ "wiced_hal_keyscan_configure", "group__keyscan.html#ga4972fd502f668d66379d6763603f5386", null ],
    [ "wiced_hal_keyscan_enable_ghost_detection", "group__keyscan.html#ga803ab8c2f7803e973db44a49a64af4d6", null ],
    [ "wiced_hal_keyscan_init", "group__keyscan.html#ga32c36da481a346183e95ae6536fd40dd", null ],
    [ "wiced_hal_keyscan_is_any_key_pressed", "group__keyscan.html#ga76b1f9336c5e31cf01906937228beb29", null ],
    [ "wiced_hal_keyscan_register_for_event_notification", "group__keyscan.html#ga9e22a7ec51daee2ee97cc5ab4b9901e4", null ],
    [ "wiced_hal_keyscan_reset", "group__keyscan.html#ga52412adcc48aee452b8218f637a2a44d", null ],
    [ "wiced_hal_keyscan_turnOff", "group__keyscan.html#gaa4f910aaa329a2583e11b1410db99c11", null ]
];