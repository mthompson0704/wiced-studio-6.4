/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 * Defines board support package for BCM943907WCD1 board
 */
#include "stdio.h"
#include "string.h"
#include "platform.h"
#include "gpio_irq.h"
#include "platform_config.h"
#include "platform_peripheral.h"
#include "platform_mcu_peripheral.h"
#include "platform_ethernet.h"
#include "wwd_assert.h"
#include "platform/wwd_platform_interface.h"
#include "wiced_platform.h"
#include "platform_appscr4.h"
#include "platform_bluetooth.h"
#include "platform_mfi.h"
#include "wiced_filesystem.h"
#include "platform_button.h"
#include "gpio_button.h"
#include "waf_platform.h"
#include "platform.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Variables Definitions
 ******************************************************/

/* Platform pin mapping table. Used by WICED/platform/MCU/wiced_platform_common.c.*/
const platform_gpio_t platform_gpio_pins[] =
{
    //                                                  eS-Wifi Signal  L44       +10/L54       L170    Signal                  EVB     Notes
    /* JTAG */
    [WICED_GPIO_23         ]    = { PIN_GPIO_2 },       //TCK            5                      C16     GPIO_2_JTAG_TCK         JP19-1
    [WICED_GPIO_24         ]    = { PIN_GPIO_3 },       //TMS            4                      D6      GPIO_3_JTAG_TMS         JP18-1
    [WICED_GPIO_25         ]    = { PIN_GPIO_4 },       //TDI            6                      C6      GPIO_4_JTAG_TDI         JP14-1
    [WICED_GPIO_26         ]    = { PIN_GPIO_5 },       //TDO            7                      B9      GPIO_5_JTAG_TDO         JP17-1
    [WICED_GPIO_27         ]    = { PIN_GPIO_6 },       //TRSTN          8                      C5      GPIO_6_JTAG_TRST        JP13-1

    /* SPI */
    [WICED_GPIO_21         ]    = { PIN_SPI_0_MOSI },   //SPI_MOSI       9                      A14     SPI_0_MOSI              J3-18
    [WICED_GPIO_19         ]    = { PIN_SPI_0_MISO },   //SPI_MISO      10                      A11     SPI_0_MISO              J3-20
    [WICED_GPIO_20         ]    = { PIN_SPI_0_CLK },    //SPI_CLK       11                      A12     SPI_0_CLK               J3-22
    [WICED_GPIO_22         ]    = { PIN_SPI_0_CS },     //SPI_SSN       12                      A13     SPI_0_CS                J3-16
    [WICED_GPIO_3          ]    = { PIN_GPIO_7 },       //DATARDY       13                      B10     GPIO_7_WCPU_BOOT_MODE   J2-12

    /* COMM */
    [WICED_PERIPHERAL_PIN_6]    = { PIN_UART0_RTS },    //RTS           18                      A63     UART0_RTS_OUT           JP20-1
    [WICED_PERIPHERAL_PIN_5]    = { PIN_UART0_CTS },    //CTS           19                      A64     UART0_CTS_IN            JP21-1
    [WICED_PERIPHERAL_PIN_3]    = { PIN_UART0_RXD },    //RX            21                      B45     UART0_RXD_IN            JP23-1
    [WICED_PERIPHERAL_PIN_4]    = { PIN_UART0_TXD },    //TX            22                      B44     UART0_TXD_OUT           JP16-1

    /* GPIO */
    [WICED_GPIO_1          ]    = { PIN_GPIO_0 },       //GPIO0         23                      C7      GPIO_0                  J2-16
    [WICED_GPIO_2          ]    = { PIN_GPIO_1 },       //GPIO1         24                      A34     GPIO_1_GSPI_MODE        J2-14
    [WICED_GPIO_4          ]    = { PIN_GPIO_8 },       //GPIO3         25                      B12     GPIO_8_TAP_SELECT               Can't be used when JTAG selected
    [WICED_GPIO_5          ]    = { PIN_GPIO_9 },       //GPIO2         26                      A15     GPIO_9_USB_SELECT       J2-10
    [WICED_GPIO_6          ]    = { PIN_GPIO_10 },      //GPIO4         27                      D8      GPIO_10                 J28

    /* CFG */
    [WICED_PERIPHERAL_PIN_1]    = { PIN_RF_SW_CTRL_6 }, //CFG1          29                      B22     RF_SW_CTRL_6_UART1_RX_IN                    J4-19
    [WICED_PERIPHERAL_PIN_2]    = { PIN_RF_SW_CTRL_7 }, //CFG0          28                      B16     RF_SW_CTRL_7_RSRC_INIT_MODE_UART1_TX_OUT    J4-17

    /* RES GPIO */
    [WICED_GPIO_9          ]    = { PIN_GPIO_13 },      //GPIO13        32                      B34     GPIO_13_SDIO_MODE       J2-2
    [WICED_GPIO_8          ]    = { PIN_GPIO_12 },      //GPIO14        31                      C17     GPIO_12                 J2-4
    [WICED_GPIO_10         ]    = { PIN_GPIO_14 },      //GPIO15        30                      B35     GPIO_14                 J2-15

    /* +10 */
    [WICED_GPIO_12         ]    = { PIN_GPIO_16 },      //GPIOA                 45              B19     GPIO_16                 J2-11
    [WICED_GPIO_7          ]    = { PIN_GPIO_11 },      //GPIOB                 46              A21     GPIO_11_ACPU_BOOT_MODE  J2-6
    [WICED_PERIPHERAL_PIN_7]    = { PIN_RF_SW_CTRL_8 }, //GPIOD                 48              A23     RF_SW_CTRL_8_BT_SECI_IN                     J4-25
    [WICED_PERIPHERAL_PIN_8]    = { PIN_RF_SW_CTRL_9 }, //GPIOE                 49              C10     RF_SW_CTRL_9_HIB_LPO_SEL_BT_SECI_OUT        J4-23
    [WICED_GPIO_28         ]    = { PIN_I2C0_SDATA },   //GPIOI                 52              A51     I2C_0_SDA               J3-29
    [WICED_GPIO_29         ]    = { PIN_I2C0_CLK },     //GPIOH                 53              A58     I2C_0_SCL               J3-27

    /* Misc */
    [WICED_GPIO_11         ]    = { PIN_GPIO_15 },      //                                      B30     GPIO_15                 J2-13
    [WICED_GPIO_13         ]    = { PIN_PWM_0 },        //                                      C13     PWM_0                   J4-12
    [WICED_GPIO_14         ]    = { PIN_PWM_1 },        //                                      B15     PWM_1                   J4-10
    [WICED_GPIO_15         ]    = { PIN_PWM_2 },        //                                      A30     PWM_2                   J4-8
    [WICED_GPIO_16         ]    = { PIN_PWM_3 },        //                                      A29     PWM_3                   J4-6
    [WICED_GPIO_17         ]    = { PIN_PWM_4 },        //                                      A35     PWM_4                   J4-4
    [WICED_GPIO_18         ]    = { PIN_PWM_5 },        //                                      D7      PWM_5                   J4-2
    [WICED_GPIO_30         ]    = { PIN_I2S_MCLK0 },    //                                      B41     I2S0_MCK                J4-22
    [WICED_GPIO_31         ]    = { PIN_I2S_SCLK0 },    //                                      A59     I2S0_SCK_BCLK           J4-30
    [WICED_GPIO_32         ]    = { PIN_I2S_LRCLK0 },   //                                      A55     I2S0_WS_LRCLK           J4-26
    [WICED_GPIO_33         ]    = { PIN_I2S_SDATAI0 },  //                                      A54     I2S0_SD_IN              J4-18
    [WICED_GPIO_34         ]    = { PIN_I2S_SDATAO0 },  //                                      A55     I2S0_SD_OUT             J4-16
    [WICED_GPIO_35         ]    = { PIN_I2S_MCLK1 },    //                                      A61     I2S1_MCK                J3-17
    [WICED_GPIO_36         ]    = { PIN_I2S_SCLK1 },    //                                      B43     I2S1_SCK_BCLK           J3-23
    [WICED_GPIO_37         ]    = { PIN_I2S_LRCLK1 },   //                                      A53     I2S1_WS_LRCLK           J3-21
    [WICED_GPIO_38         ]    = { PIN_I2S_SDATAI1 },  //                                      B42     I2S1_SD_IN              J3-13
    [WICED_GPIO_39         ]    = { PIN_I2S_SDATAO1 },  //                                      A60     I2S1_SD_OUT             J3-11
    [WICED_PERIPHERAL_PIN_9]    = { PIN_I2C1_SDATA },   //                                      A52     I2C_1_SDA               J3-28
    [WICED_PERIPHERAL_PIN_10]    = { PIN_I2C1_CLK },    //                                      A57     I2C_1_SCL               J3-26
    [WICED_PERIPHERAL_PIN_11]    = { PIN_SPI_1_MISO },  //                                      B4      SPI_1_MISO              J3-5
    [WICED_PERIPHERAL_PIN_12]    = { PIN_SPI_1_CLK },   //                                      B8      SPI_1_CLK               J3-7
    [WICED_PERIPHERAL_PIN_13]    = { PIN_SPI_1_MOSI },  //                                      B37     SPI_1_MOSI              J3-3
    [WICED_PERIPHERAL_PIN_14]    = { PIN_SPI_1_CS },    //                                      B11     SPI_1_CS                J3-1
    [WICED_PERIPHERAL_PIN_15]    = { PIN_SDIO_CLK },    //                                      B29     SDIO_CLK                J1-5
    [WICED_PERIPHERAL_PIN_16]    = { PIN_SDIO_CMD },    //                                      A41     SDIO_CMD                J1-11
    [WICED_PERIPHERAL_PIN_17]    = { PIN_SDIO_DATA_0 }, //                                      B27     SDIO_DATA0              J1-3
    [WICED_PERIPHERAL_PIN_18]    = { PIN_SDIO_DATA_1 }, //                                      B26     SDIO_DATA1              J1-1
    [WICED_PERIPHERAL_PIN_19]    = { PIN_SDIO_DATA_2 }, //                                      A42     SDIO_DATA2              J1-15
    [WICED_PERIPHERAL_PIN_20]    = { PIN_SDIO_DATA_3 }, //                                      B31     SDIO_DATA3              J1-13
};


/* Ethernet configuration table. */
platform_ethernet_config_t platform_ethernet_config =
{
    .phy_addr      = 0x0,
    .phy_interface = PLATFORM_ETHERNET_PHY_MII,
    .wd_period_ms  = 1000,
    .speed_force   = PLATFORM_ETHERNET_SPEED_AUTO,
    .speed_adv     = PLATFORM_ETHERNET_SPEED_ADV(AUTO),
};


/* PWM peripherals. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_pwm_t platform_pwm_peripherals[] =
{
    [WICED_PWM_1]  = {PIN_GPIO_0,  PIN_FUNCTION_PWM0, },   /* or PIN_GPIO_8, PIN_GPIO_10, PIN_GPIO_12, PIN_GPIO_14, PIN_GPIO_16, PIN_PWM_0 */
    [WICED_PWM_2]  = {PIN_GPIO_1,  PIN_FUNCTION_PWM1, },   /* or PIN_GPIO_7, PIN_GPIO_9,  PIN_GPIO_11, PIN_GPIO_13, PIN_GPIO_15, PIN_PWM_1 */
    [WICED_PWM_3]  = {PIN_GPIO_8,  PIN_FUNCTION_PWM2, },   /* or PIN_GPIO_0, PIN_GPIO_10, PIN_GPIO_12, PIN_GPIO_14, PIN_GPIO_16, PIN_PWM_2 */
    [WICED_PWM_4]  = {PIN_GPIO_7,  PIN_FUNCTION_PWM3, },   /* or PIN_GPIO_1, PIN_GPIO_9,  PIN_GPIO_11, PIN_GPIO_13, PIN_GPIO_15, PIN_PWM_3 */
    [WICED_PWM_5]  = {PIN_GPIO_10, PIN_FUNCTION_PWM4, },   /* or PIN_GPIO_0, PIN_GPIO_8,  PIN_GPIO_12, PIN_GPIO_14, PIN_GPIO_16, PIN_PWM_4 */
    [WICED_PWM_6]  = {PIN_GPIO_9,  PIN_FUNCTION_PWM5, },   /* or PIN_GPIO_1, PIN_GPIO_7,  PIN_GPIO_11, PIN_GPIO_13, PIN_GPIO_15, PIN_PWM_5 */
};

const platform_spi_t platform_spi_peripherals[] =
{
    [WICED_SPI_1]  =
    {
        .port                    = BCM4390X_SPI_0,
        .pin_mosi                = &platform_gpio_pins[WICED_GPIO_21],
        .pin_miso                = &platform_gpio_pins[WICED_GPIO_19],
        .pin_clock               = &platform_gpio_pins[WICED_GPIO_20],
        .pin_cs                  = &platform_gpio_pins[WICED_GPIO_22],
        .driver                  = &spi_gsio_driver,
    },

    [WICED_SPI_2]  =
    {
        .port                    = BCM4390X_SPI_1,
        .driver                  = &spi_gsio_driver,
    },
};


/* I2C peripherals. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_i2c_t platform_i2c_peripherals[] =
{
    [WICED_I2C_1] =
    {
        .port                    = BCM4390X_I2C_0,
        .pin_sda                 = &platform_gpio_pins[WICED_GPIO_28],
        .pin_scl                 = &platform_gpio_pins[WICED_GPIO_29],
        .driver                  = &i2c_gsio_driver,
    },

    [WICED_I2C_2] =
    {
        .port                    = BCM4390X_I2C_1,
        .pin_sda                 = NULL,
        .pin_scl                 = NULL,
        .driver                  = &i2c_gsio_driver,
    },
};


/* UART peripherals and runtime drivers. Used by WICED/platform/MCU/wiced_platform_common.c */
const platform_uart_t platform_uart_peripherals[] =
{
    [WICED_UART_1] = /* ChipCommon Slow UART */
    {
        .port    = UART_SLOW,
        .rx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_1],
        .tx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_2],
        .cts_pin = NULL,
        .rts_pin = NULL,
        .src_clk = CLOCK_ALP,
    },
    [WICED_UART_2] = /* ChipCommon Fast UART */
    {
        .port    = UART_FAST,
        .rx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_3],
        .tx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_4],
        .cts_pin = &platform_gpio_pins[WICED_PERIPHERAL_PIN_5],
        .rts_pin = &platform_gpio_pins[WICED_PERIPHERAL_PIN_6],
        .src_clk = CLOCK_HT,
    },
    [WICED_UART_3] = /* GCI UART */
    {
        .port    = UART_GCI,
        .rx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_7],
        .tx_pin  = &platform_gpio_pins[WICED_PERIPHERAL_PIN_8],
        .cts_pin = NULL,
        .rts_pin = NULL,
        .src_clk = CLOCK_ALP,
    }
};
platform_uart_driver_t platform_uart_drivers[WICED_UART_MAX];


/* Second SPI flash. Exposed to the applications through include/wiced_platform.h */
#ifdef WICED_PLATFORM_INCLUDES_SPI_FLASH
const wiced_spi_device_t wiced_spi_flash =
{
    .port        = WICED_SPI_FLASH_PORT,
    .chip_select = WICED_SPI_FLASH_CS,
    .speed       = 5000000,
    .mode        = (SPI_CLOCK_RISING_EDGE | SPI_CLOCK_IDLE_HIGH | SPI_NO_DMA | SPI_MSB_FIRST),
    .bits        = 8
};
#endif

/* UART standard I/O configuration */
#ifndef WICED_DISABLE_STDIO
static const platform_uart_config_t stdio_config =
{
    .baud_rate    = 115200,
    .data_width   = DATA_WIDTH_8BIT,
    .parity       = NO_PARITY,
    .stop_bits    = STOP_BITS_1,
    .flow_control = FLOW_CONTROL_DISABLED,
};
#endif

const platform_i2s_port_info_t i2s_port_info[BCM4390X_I2S_MAX] =
{
    [BCM4390X_I2S_0] =
    {
        .port               = BCM4390X_I2S_0,
        .is_master          = 1,
        .irqn               = I2S0_ExtIRQn,
        .audio_pll_ch       = BCM4390X_AUDIO_PLL_MCLK1,
    },
    [BCM4390X_I2S_1] =
    {
        .port               = BCM4390X_I2S_1,
        .is_master          = 1,
        .irqn               = I2S1_ExtIRQn,
        .audio_pll_ch       = BCM4390X_AUDIO_PLL_MCLK2,
    },
};

const platform_i2s_t i2s_interfaces[WICED_I2S_MAX] =
{
    [WICED_I2S_1] =
    {
        .port_info          = &i2s_port_info[BCM4390X_I2S_0],
        .stream_direction   = PLATFORM_I2S_WRITE,
    },
    [WICED_I2S_2] =
    {
        .port_info          = &i2s_port_info[BCM4390X_I2S_0],
        .stream_direction   = PLATFORM_I2S_READ,
    },
    [WICED_I2S_3] =
    {
        .port_info          = &i2s_port_info[BCM4390X_I2S_1],
        .stream_direction   = PLATFORM_I2S_WRITE,
    },
    [WICED_I2S_4] =
    {
        .port_info          = &i2s_port_info[BCM4390X_I2S_1],
        .stream_direction   = PLATFORM_I2S_READ,
    },
};

const platform_hibernation_t hibernation_config =
{
    .clock              = PLATFORM_HIBERNATION_CLOCK_EXTERNAL,
    .hib_ext_clock_freq = 0, /* use default settings */
    .rc_code            = PLATFORM_HIB_WAKE_CTRL_REG_RCCODE,
};

/* Bluetooth control pins. Used by libraries/bluetooth/internal/bus/UART/bt_bus.c */
const platform_gpio_t* wiced_bt_control_pins[WICED_BT_PIN_MAX] =
{
    [WICED_BT_PIN_POWER]       = NULL,
    [WICED_BT_PIN_RESET]       = &platform_gpio_pins[WICED_GPIO_11],
    [WICED_BT_PIN_HOST_WAKE]   = &platform_gpio_pins[WICED_GPIO_10],
    [WICED_BT_PIN_DEVICE_WAKE] = &platform_gpio_pins[WICED_GPIO_2]
};

/* MFI-related variables */
const wiced_i2c_device_t auth_chip_i2c_device =
{
    .port          = AUTH_IC_I2C_PORT,
    .address       = 0x11,
    .address_width = I2C_ADDRESS_WIDTH_7BIT,
    .speed_mode    = I2C_STANDARD_SPEED_MODE,
};

const platform_mfi_auth_chip_t platform_auth_chip =
{
    .i2c_device = &auth_chip_i2c_device,
    .reset_pin  = WICED_GPIO_AUTH_RST
};


/** USB Disk Filesystem */

/* Block device for USB device */
wiced_block_device_t block_device_usb =
{
    .init_data            = NULL,
    .driver               = NULL,
    .device_specific_data = NULL,    /* store media handle provided by usb stack */
};

/* List of all filesystem devices on this platform - for interactive selection - e.g. console app */
const filesystem_list_t all_filesystem_devices[] =
{
        { &block_device_usb,     WICED_FILESYSTEM_HANDLE_FILEX_USBX, "USB" },
        { NULL, 0, NULL },
};

const gpio_button_t platform_gpio_buttons[] =
{
    [PLATFORM_BUTTON_1] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_BACK,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

    [PLATFORM_BUTTON_2] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_VOLUME_DOWN,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

    [PLATFORM_BUTTON_3] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_VOLUME_UP,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

    [PLATFORM_BUTTON_4] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_PAUSE_PLAY,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

    [PLATFORM_BUTTON_5] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_MULTI_FUNC,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

    [PLATFORM_BUTTON_6] =
    {
        .polarity   = WICED_ACTIVE_HIGH,
        .gpio       = WICED_BUTTON_FORWARD,
        .trigger    = IRQ_TRIGGER_BOTH_EDGES,
    },

};

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

platform_result_t platform_led_set_state(int led_index, int off_on )
{
    /* Note: The BCM943907WCD1 hardware does not have physical LEDs or buttons so the #defines are mapped to PWM 0,1,2,3 on the expansion header */
    /* THis needs support */
    return PLATFORM_BADARG;
}

void platform_init_external_devices( void )
{

#ifndef WICED_DISABLE_STDIO
    /* Initialise UART standard I/O */
    platform_stdio_init( &platform_uart_drivers[STDIO_UART], &platform_uart_peripherals[STDIO_UART], &stdio_config );
#endif
}

uint32_t  platform_get_button_press_time ( int button_index, int led_index, uint32_t max_time )
{
    int             button_gpio;
    uint32_t        button_press_timer = 0;
    int             led_state = 0;

    /* Initialize input */
    button_gpio     = platform_gpio_buttons[button_index].gpio;
    platform_gpio_init( &platform_gpio_pins[ button_gpio ], INPUT_PULL_UP );

    while ( (PLATFORM_BUTTON_PRESSED_STATE== platform_gpio_input_get(&platform_gpio_pins[ button_gpio ])) )
    {
        /* wait a bit */
        host_rtos_delay_milliseconds( PLATFORM_BUTTON_PRESS_CHECK_PERIOD );

        /* Toggle LED */
        platform_led_set_state(led_index, (led_state == 0) ? WICED_LED_OFF : WICED_LED_ON);
        led_state ^= 0x01;

        /* keep track of time */
        button_press_timer += PLATFORM_BUTTON_PRESS_CHECK_PERIOD;
        if ((max_time > 0) && (button_press_timer >= max_time))
        {
            break;
        }
    }

     /* turn off the LED */
    platform_led_set_state(led_index, WICED_LED_OFF );

    return button_press_timer;
}

uint32_t  platform_get_factory_reset_button_time ( uint32_t max_time )
{
    return platform_get_button_press_time ( PLATFORM_BUTTON_BACK, PLATFORM_RED_LED_INDEX, max_time );
}
