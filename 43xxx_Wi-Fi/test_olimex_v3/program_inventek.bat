@echo off 
@echo %time%
@echo %time% >> openocd_log.txt
@echo --------------------------
@echo Downloading Bootloader ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase waf.bootloader-NoOS-ISM4343_wbm_L151_BBS2.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Bootloader download complete && "echo.exe" || echo "Error while programming bootloader - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading DCT ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo DCT download complete && "echo.exe" || echo "Error while programming DCT - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading Application ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS2-ISM4343_WBM_L151_BBS2-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Application download complete && "echo.exe" || echo "Error while programming Application - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Resetting target
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown > openocd_log.txt 2>&1 && echo Target running
@echo %time%
@echo --------------------------
@echo Programming Complete 
@echo --------------------------

