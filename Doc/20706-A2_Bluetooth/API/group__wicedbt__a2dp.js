var group__wicedbt__a2dp =
[
    [ "A2DP SBC Support", "group__wicedbt__a2dp__sbc.html", "group__wicedbt__a2dp__sbc" ],
    [ "wiced_audio_sink_config_init", "group__wicedbt__a2dp.html#ga00d931eea740da7d5d867797b5048655", null ],
    [ "wiced_audio_sink_configure", "group__wicedbt__a2dp.html#ga9b28e8751a80b34db1b9c2123ba6a06f", null ],
    [ "wiced_audio_sink_mute", "group__wicedbt__a2dp.html#gaa913a3617fdd63966c9dd341c30a231d", null ],
    [ "wiced_audio_sink_reset", "group__wicedbt__a2dp.html#gaa3e55d6720eba4c34f3d54ee229220df", null ],
    [ "wiced_bt_a2d_bits_set", "group__wicedbt__a2dp.html#ga8ccbe103977bb96a5e441d382fdfd92c", null ],
    [ "wiced_bt_a2d_find_service", "group__wicedbt__a2dp.html#ga73a7d02bde825807202680eaa6e4c5c5", null ],
    [ "avdt_version", "group__wicedbt__a2dp.html#ga2d10e3be2b81d100443614e55b4b0c21", null ],
    [ "db_len", "group__wicedbt__a2dp.html#gad869c77e637e74544b7da4fc883c965e", null ],
    [ "features", "group__wicedbt__a2dp.html#ga05394e2501b222db77c3a39194523065", null ],
    [ "num_attr", "group__wicedbt__a2dp.html#ga15873975ac30bb80d6e4a1a47b14ca00", null ],
    [ "p_attrs", "group__wicedbt__a2dp.html#gaafb8710c45f74403ee3d7c12aa27f713", null ],
    [ "p_db", "group__wicedbt__a2dp.html#ga95655154f49d7fb2ce00b3dc127631f2", null ],
    [ "p_provider_name", "group__wicedbt__a2dp.html#gad9460235afcc5d705ff25de7b63cfb3d", null ],
    [ "p_service_name", "group__wicedbt__a2dp.html#gae2b770af86cc0cd81658a365df4915ea", null ],
    [ "provider_len", "group__wicedbt__a2dp.html#ga1141e4ebcbdd40401137776441e5a17b", null ],
    [ "service_len", "group__wicedbt__a2dp.html#ga412f90be47a00ccd253665c53b890f60", null ]
];