var group___r_t_c =
[
    [ "tRTC_LHL_ADC_RTC_CTL_REG", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html", [
      [ "bitmap", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#a442029d0f69c1c4ed0514b73a7ca20e9", null ],
      [ "lhl_adc_rtc_ctl_reg", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#adb6b3fe1400ff94224e891f8e9ada729", null ],
      [ "reserved1", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#afcfd9004b2604b08068988140cc7e658", null ],
      [ "reserved2", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#ae1b046f6cf511e50ff6424a126203560", null ],
      [ "rtcCounterEn", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#a35fee8609e5e7192753deb1de002571c", null ],
      [ "rtcResetCounter", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#ae6087de278ec94c8f08a711473b95012", null ],
      [ "rtcTerminalCntStatusEn", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#a96d733a4049359d7e19ec437444407cf", null ],
      [ "rtcTimerFuncEn", "uniont_r_t_c___l_h_l___a_d_c___r_t_c___c_t_l___r_e_g.html#a315e8c2c711318d107ca80415c1e7655", null ]
    ] ],
    [ "tRTC_REAL_TIME_CLOCK", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html", [
      [ "reg16map", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html#aaaf463839cb286a8e6c3c9caefec4408", null ],
      [ "reg32map", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html#a78b36347b708d32ce009b23d973ee2c2", null ],
      [ "rtc16", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html#ac5a7f7ab9f6e75a242ce24113d778b62", null ],
      [ "rtc32", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html#ae382428a3e298830d78e6352c056bea1", null ],
      [ "rtc64", "uniont_r_t_c___r_e_a_l___t_i_m_e___c_l_o_c_k.html#a7007a47d86be07ebbb2df38f8ef3e638", null ]
    ] ],
    [ "RtcTime", "struct_rtc_time.html", [
      [ "day", "struct_rtc_time.html#a07e870bd94eef1c1ec4d488263737af9", null ],
      [ "hour", "struct_rtc_time.html#a01a2c00bde7e0eaa814045a09ef7892d", null ],
      [ "minute", "struct_rtc_time.html#a8a57144c7b976c9588be93a7dbab67ec", null ],
      [ "month", "struct_rtc_time.html#a41d9300e5e8511f3b289ec7e57425b48", null ],
      [ "second", "struct_rtc_time.html#a45fcf9dc56226fa46e41da78c7869cdf", null ],
      [ "year", "struct_rtc_time.html#a15cf81be0bc3b33c7cc971ed1a815c74", null ]
    ] ],
    [ "RtcState", "struct_rtc_state.html", [
      [ "userSetRtcClockInSeconds", "struct_rtc_state.html#afc88e55983cc1997d47d3ce70d2f9af2", null ],
      [ "userSetRtcHWTimeStamp", "struct_rtc_state.html#a71a761de5f108548867cb39edcb82c93", null ]
    ] ]
];