********* Detection Failure *************
+------------------------------------------------------------------------------------------+
| The CYBT-343151-EVAL board was not detected. Verify that the device is connected,        |
| power-cycle if necessary to reboot into programming mode, and retry.                     |
|                                                                                          |
| Please see Appendix sections in the Quick Start Guide for common COM port problems.      |
+------------------------------------------------------------------------------------------+

