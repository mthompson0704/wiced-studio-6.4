/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file contains the HID Device Power Management logic
 */

#include "bt_types.h"
#include "string.h"
#include "hidd_int.h"

#if HID_DEV_PM_INCLUDED == TRUE

/*******************************************************************************
**
** Function         hidd_pm_init
**
** Description      This function is called when connection is established. It
**                  initializes the control block for power management.
**
** Returns          void
**
*******************************************************************************/

MAY_BE_STATIC const wiced_bt_hidd_pm_pwr_md_t pwr_modes[] =
{
    HID_DEV_BUSY_MODE_PARAMS,
    HID_DEV_IDLE_MODE_PARAMS,
    HID_DEV_SUSP_MODE_PARAMS
};

void hidd_pm_init( void )
{
    int i;

    hidd_cb.curr_pm.mode = HCI_MODE_ACTIVE ;
    hidd_cb.final_pm.mode = 0xff;

    for( i=0; i<3; i++ )
        memcpy( &hidd_cb.pm_params[i], &pwr_modes[i], sizeof( wiced_bt_hidd_pm_pwr_md_t ) ) ;

    hidd_cb.pm_ctrl_busy = FALSE;
}

/*******************************************************************************
**
** Function         hidd_pm_set_now
**
** Description      This drives the BTM for power management settings.
**
** Returns          TRUE if Success, FALSE otherwise
**
*******************************************************************************/
BOOLEAN hidd_pm_set_now( wiced_bt_hidd_pm_pwr_md_t *p_req_mode)
{
    wiced_bt_hidd_pm_pwr_md_t act_pm = { 0, 0, 0, 0, HCI_MODE_ACTIVE } ;
    UINT8 st = BTM_SUCCESS;

    /* Do nothing if already in required state or already performing a pm function */
    if( (hidd_cb.pm_ctrl_busy) ||
        ((p_req_mode->mode == hidd_cb.curr_pm.mode) && ( (p_req_mode->mode == HCI_MODE_ACTIVE) ||
            ((hidd_cb.curr_pm.interval >= p_req_mode->min) && (hidd_cb.curr_pm.interval <= p_req_mode->max)) )) )
    {
        hidd_cb.final_pm.mode = 0xff;
        return TRUE;
    }

    switch( p_req_mode->mode )
    {
    case HCI_MODE_ACTIVE:
        if( hidd_cb.curr_pm.mode == HCI_MODE_SNIFF )
        {
#if BTM_PWR_MGR_INCLUDED == TRUE
            st = BTM_SetPowerMode (BTM_PM_SET_ONLY_ID, hidd_cb.host_addr, (tBTM_PM_PWR_MD *) &act_pm);
#else
            st = BTM_CancelSniffMode (hidd_cb.host_addr);
#endif
            hidd_cb.pm_ctrl_busy = TRUE;
        }
        else if( hidd_cb.curr_pm.mode == HCI_MODE_PARK )
        {
#if BTM_PWR_MGR_INCLUDED == TRUE
            st = BTM_SetPowerMode (BTM_PM_SET_ONLY_ID, hidd_cb.host_addr, (tBTM_PM_PWR_MD *) &act_pm);
#else
            st = BTM_CancelParkMode (hidd_cb.host_addr);
#endif
            hidd_cb.pm_ctrl_busy = TRUE;
        }
        break;

    case HCI_MODE_SNIFF:
        if( hidd_cb.curr_pm.mode != HCI_MODE_ACTIVE ) /* Transition through active state required */
            hidd_pm_set_now (&act_pm);
        else
        {
#if BTM_PWR_MGR_INCLUDED == TRUE
            st = BTM_SetPowerMode (BTM_PM_SET_ONLY_ID, hidd_cb.host_addr, (tBTM_PM_PWR_MD *) p_req_mode);
#else
            st = BTM_SetSniffMode (hidd_cb.host_addr, p_req_mode->min, p_req_mode->max, p_req_mode->attempt, p_req_mode->timeout);
#endif
            hidd_cb.pm_ctrl_busy = TRUE;
        }
        break;

    case HCI_MODE_PARK:
        if( hidd_cb.curr_pm.mode != HCI_MODE_ACTIVE ) /* Transition through active state required */
            hidd_pm_set_now (&act_pm);
        else
        {
#if BTM_PWR_MGR_INCLUDED == TRUE
            st = BTM_SetPowerMode (BTM_PM_SET_ONLY_ID, hidd_cb.host_addr, (tBTM_PM_PWR_MD *) p_req_mode);
#else
            st = BTM_SetParkMode (hidd_cb.host_addr, p_req_mode->min, p_req_mode->max);
#endif
            hidd_cb.pm_ctrl_busy = TRUE;
        }
        break;

    default:
        break;
    }

    if( st == BTM_SUCCESS || st == BTM_CMD_STARTED )
        return TRUE;
    else
    {
        st += HCI_ERR_MAX_ERR ;
        if( hidd_cb.callback )
            hidd_cb.callback( WICED_BT_HIDD_EVT_PM_FAILED, hidd_cb.conn_substate, (wiced_bt_hidd_event_data_t *) &st ) ;
        return FALSE;
    }
}

/*******************************************************************************
**
** Function         hidd_pm_set_power_mode
**
** Description      This stores the power management setting and calls fn to set
**                  the power.
**
** Returns          TRUE if Success, FALSE otherwise
**
*******************************************************************************/
BOOLEAN hidd_pm_set_power_mode( wiced_bt_hidd_pm_pwr_md_t *p_req_mode)
{
    memcpy( &hidd_cb.final_pm, p_req_mode, sizeof( wiced_bt_hidd_pm_pwr_md_t ) ) ;
    return hidd_pm_set_now( p_req_mode ) ;
}


/*******************************************************************************
**
** Function         hidd_pm_proc_mode_change
**
** Description      This is the callback function, when power mode changes.
**
** Returns          void
**
*******************************************************************************/
void hidd_pm_proc_mode_change( UINT8 hci_status, UINT8 mode, UINT16 interval )
{
    wiced_bt_hidd_event_data_t event_data;

    if (!hidd_cb.reg_flag )
        return;

    hidd_cb.pm_ctrl_busy = FALSE;

    if( hci_status != HCI_SUCCESS )
    {
        if( hidd_cb.callback )
        {
            event_data.pm_err_code = hci_status;
            hidd_cb.callback( WICED_BT_HIDD_EVT_PM_FAILED, hidd_cb.conn_substate, (wiced_bt_hidd_event_data_t *) &event_data ) ;
        }
    }
    else
    {
        hidd_cb.curr_pm.mode = mode;
        hidd_cb.curr_pm.interval = interval;

        if( hidd_cb.final_pm.mode != 0xff )
        {
            /* If we haven't reached the final power mode, set it now */
            if( (hidd_cb.final_pm.mode != hidd_cb.curr_pm.mode) ||
                ( (hidd_cb.final_pm.mode != HCI_MODE_ACTIVE) &&
                  ((hidd_cb.curr_pm.interval < hidd_cb.final_pm.min) || (hidd_cb.curr_pm.interval > hidd_cb.final_pm.max))
                ) )
            {
                hidd_pm_set_now( &(hidd_cb.final_pm) ) ;
            }
            else
                hidd_cb.final_pm.mode = 0xff;
        }
        else
        {
            if( hidd_cb.curr_pm.mode == HCI_MODE_ACTIVE )
                hidd_pm_start();
        }

        if( hidd_cb.callback )
        {
            event_data.pm_interval = interval;
            hidd_cb.callback( WICED_BT_HIDD_EVT_MODE_CHG, mode, (wiced_bt_hidd_event_data_t *) &event_data) ;
        }
    }
}


/*******************************************************************************
**
** Function         hidd_pm_inact_timeout
**
** Description      Called when idle timer expires.
**
** Returns          void
**
*******************************************************************************/
void hidd_pm_inact_timeout (TIMER_LIST_ENT *p_tle)
{
    hidd_pm_set_power_mode ( &(hidd_cb.pm_params[WICED_BT_HIDD_IDLE_CONN_ST]));
    hidd_cb.conn_substate = WICED_BT_HIDD_IDLE_CONN_ST;
}

/*******************************************************************************
**
** Function         hidd_pm_start
**
** Description      Starts the power management function in a given state.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS hidd_pm_start( void )
{
    hidd_pm_set_power_mode ( &(hidd_cb.pm_params[WICED_BT_HIDD_BUSY_CONN_ST]) );

    hidd_cb.conn_substate = WICED_BT_HIDD_BUSY_CONN_ST;

    hidd_cb.idle_tle.param = (TIMER_PARAM_TYPE) hidd_pm_inact_timeout;
    btu_start_timer (&hidd_cb.idle_tle, BTU_TTYPE_USER_FUNC, HID_DEV_INACT_TIMEOUT);

    return( HID_SUCCESS );
}

/*******************************************************************************
**
** Function         hidd_pm_stop
**
** Description      Stops the idle timer.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS hidd_pm_stop( void )
{
    wiced_bt_hidd_pm_pwr_md_t p_md = { 0, 0, 0, 0, HCI_MODE_ACTIVE };

    hidd_pm_set_power_mode( &p_md );
    btu_stop_timer( &hidd_cb.idle_tle ) ;
    return( HID_SUCCESS );
}


/*******************************************************************************
**
** Function         hidd_pm_suspend_evt
**
** Description      Called when host suspends the device.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS hidd_pm_suspend_evt( void )
{
    if( hidd_cb.conn_substate == WICED_BT_HIDD_BUSY_CONN_ST )
        btu_stop_timer( &hidd_cb.idle_tle ) ;

    hidd_pm_set_power_mode ( &(hidd_cb.pm_params[WICED_BT_HIDD_SUSP_CONN_ST]) );
    hidd_cb.conn_substate = WICED_BT_HIDD_SUSP_CONN_ST;
    return( HID_SUCCESS );
}
#endif /* HID_DEV_PM_INCLUDED == TRUE */
