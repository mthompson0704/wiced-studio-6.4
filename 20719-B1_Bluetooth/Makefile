#
# Copyright 2014, Cypress Semiconductor
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Cypress Semiconductor.
#


MAKEFILE_TARGETS := help build download recover clean Release


include $(SOURCE_ROOT)wiced_toolchain_common.mk


.PHONY: $(MAKEFILE_TARGETS)

BASE_LOCATIONS  := BASErom BASEram BASEflash
SPAR_LOCATIONS  := SPARrom SPARram SPARflash
TOOLCHAINS      := RealView Wiced CodeSourcery
BUILD_TYPE_LIST := release

define USAGE_TEXT
Aborting due to invalid targets

Usage: make <target> [TRANSPORT=SPI] [download] [DEBUG=1|0] [VERBOSE=1] [UART=yyyy] [JOBS=x] [BT_DEVICE_ADDRESS=zzzzzzzzzzzz|random]

  <target>
    One each of the following mandatory [and optional] components separated by '-'
      * Application (Apps in sub-directories are referenced by subdir.appname)
      * Hardware Platform ($(filter-out common  include README.txt,$(notdir $(wildcard platforms/*))))
      * [BASE location] ($(BASE_LOCATIONS))
      * [SPAR location] ($(SPAR_LOCATIONS))
      * [Toolchain] ($(TOOLCHAINS))

  [download]
    Download firmware image to target platform.

  [build]
    Builds the firmware images for the selected platform.

  [DEBUG=1|0]
    Enable or disable debug code in application. When DEBUG=1, watchdog will be disabled,
    sleep will be disabled and the app may optionally wait in a while(1) for the debugger
    to connect

  [VERBOSE=1]
    Shows the commands as they are being executed

  [JOBS=x]
    Sets the maximum number of parallel build threads (default=4)

  [UART=yyyy]
    Use the uart specified here instead of trying to detect the WICED device.
    This is useful when you are working on multiple WICED devices simultaneously.

  [BT_DEVICE_ADDRESS=zzzzzzzzzzzz|random]
    Use the 48-bit Bluetooth address specified here instead of the default setting from
    Platform/*/*.btp file. The special string 'random' (without the quotes) will generate
    a random Bluetooth device address on every download.

  [OTA_FW_UPGRADE=1]
    Applications which want to upgrade firmware via OTA, have to be downloaded through installer using this option.

  [TRANSPORT]
    HCI transport interface (default is UART)

  Notes
    * Component names are case sensitive
    * 'Wiced', 'SPI', 'UART' and 'debug' are reserved component names
    * Component names MUST NOT include space or '-' characters
    * Building for release is assumed unless '-debug' is appended to the target
    * Application/target names must begin with an alphabetic character

  Example Usage
    Build for Release
      $> make demo.hello_sensor-CYW920719Q40EVB_01 build

    Build, Download and Run using the default programming interface
      $> make demo.hello_sensor-CYW920719Q40EVB_01 download

    Build, Download and Run using specific UART port, with a specific Bluetooth decice address
      $> make demo.hello_sensor-CYW920719Q40EVB_01 download UART=COMx BT_DEVICE_ADDRESS=20719AC0FFEE

    Clean output directory
      $> make clean

endef


define MAKE_TARGET_HELP

   Please use the targets in the Make Target window to build your application

endef

.PHONY: help

.DEFAULT_GOAL := help

bad_target_specified:
	$(info Invalid target specified)
	$(error $(USAGE_TEXT))


help:
	$(info $(USAGE_TEXT))

.PHONY: Release

Release:
	$(info $(MAKE_TARGET_HELP))

clean:
	$(QUIET)$(ECHO) Cleaning...
	$(QUIET)$(CLEAN_COMMAND)
	$(QUIET)$(RM) -rf .gdbinit
	$(QUIET)$(ECHO) Done


find_com_port:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Detecting device...
	$(QUIET)$(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/get_com_port/get_com_port.pl $(DETECTANDID_FULL_NAME) $(SOURCE_ROOT)platforms/$(PLATFORM_IDFILE) $(SOURCE_ROOT)com_port.txt $(PLATFORM_BAUDRATE) $(SOURCE_ROOT)baud_rate.txt > build/$(OUTPUT_NAME)/detect.log 2>&1 \
	&& $(ECHO) Device found \
	|| $(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/print_file/print_file.pl  $(SOURCE_ROOT)platforms/$(PLATFORM)/find_com_port_error.txt
	$(QUIET)$(ECHO_BLANK_LINE)

ifneq ($(DIR_BUILD_STRING),)

# Separate the build string into components
COMPONENTS := $(subst -, ,$(DIR_BUILD_STRING))
COMPONENTS := $(subst .,/,$(COMPONENTS))
BASE_LOC  ?= rom

#Find app and platform
PLATFORM_FULL :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)platforms/$(comp)),$(comp),)))
PLATFORM      :=$(notdir $(PLATFORM_FULL))
APP_FULL      :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)apps/$(comp)),$(comp),)))
COMMON_APP_DIR_QUALIFIER :=
ifeq ($(APP_FULL),)
APP_FULL      :=$(strip $(foreach comp,$(COMPONENTS),$(if $(wildcard $(SOURCE_ROOT)../common/apps/$(comp)),$(comp),)))
COMMON_APP_DIR_QUALIFIER := ../common/
endif
APP           :=$(notdir $(APP_FULL))

ifneq ($(filter $(COMPONENTS),$(BASE_LOCATIONS)),)
BASE_LOC      :=$(strip $(subst BASE,,$(filter $(COMPONENTS),$(BASE_LOCATIONS))))
endif
ifneq ($(filter $(COMPONENTS),$(SPAR_LOCATIONS)),)
SPAR_LOC      :=$(strip $(subst SPAR,,$(filter $(COMPONENTS),$(SPAR_LOCATIONS))))
endif

ifneq ($(filter $(COMPONENTS),$(TOOLCHAINS)),)
TOOLCHAIN     :=$(strip $(filter $(COMPONENTS),$(TOOLCHAINS)))
endif
BUILD_TYPE    :=$(strip $(filter $(COMPONENTS),$(BUILD_TYPE_LIST)))

ifneq ($(wildcard $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM).mk),)
PLATFORM_MK   := $(SOURCE_ROOT)platforms/$(PLATFORM_FULL)/$(PLATFORM).mk
else ifneq ($(wildcard $(SOURCE_ROOT)platforms/$(PLATFORM)/platform.mk),)
PLATFORM_MK   := $(SOURCE_ROOT)platforms/$(PLATFORM)/platform.mk
else
$(error Platform makefile not found: $(SOURCE_ROOT)platforms/platform.mk)
endif

include $(PLATFORM_MK)

ifeq ($(CHIP)$(CHIP_REV),)
$(error CHIP not defined in Platform makefile $(PLATFORM_MK))
endif

ifeq ($(BASE_LOC),)
$(error BASE Location not defined  in command-line target or in Platform makefile $(PLATFORM_MK))
endif

ifeq ($(SPAR_LOC),)
$(error SPAR Location not defined  in command-line target or in Platform makefile $(PLATFORM_MK))
endif

ifeq ($(TOOLCHAIN),)
$(error Toolchain not defined in command-line target or in Platform makefile $(PLATFORM_MK))
endif

ifeq ($(BUILD_TYPE),)
#default to release build
BUILD_TYPE := release
endif


JOBS ?=4
ifeq (,$(SUB_BUILD))
JOBSNO := $(if $(findstring 1,$(LINT)), , -j$(JOBS) )
endif

OUTPUT_NAME                := $(APP)-$(PLATFORM)-$(BASE_LOC)-$(SPAR_LOC)-$(TOOLCHAIN)-$(BUILD_TYPE)

BIN_OUT_DIR                := ../build/$(OUTPUT_NAME)

# Include SDK include directories
SDK_INC_FLAGS              :=../include/ ../../common/include/ ../include/platforms/$(PLATFORM)/ ../include/Drivers/ ../include/Drivers/$(CHIP)
ifeq ($(HOMEKIT),1)
SDK_INC_FLAGS              +=../../common/include/hap
endif

# Override the BD_ADDR parameter if provided on the command line.
ifeq ($(BT_DEVICE_ADDRESS),random)
BT_DEVICE_ADDRESS_OVERRIDE := -O DLConfigBD_ADDRBase:$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)r=int(rand(32768));printf "$(CHIP)$(CHIP_REV)0%04X",$(PERL_ESC_DOLLAR)r;')
else
ifneq ($(BT_DEVICE_ADDRESS),)
BT_DEVICE_ADDRESS_OVERRIDE := -O DLConfigBD_ADDRBase:$(BT_DEVICE_ADDRESS)
endif
endif

export BIN_OUT_DIR
export VERBOSE
export SDK_INC_FLAGS


ifeq ($(TOOLCHAIN),Wiced)
TC :=wiced
GCC_TOOL_DIR=$(call CONV_SLASHES,../../43xxx_Wi-Fi/tools/ARM_GNU/$(HOST_OS)/bin/)
else
ifeq ($(TOOLCHAIN),RealView)
TC :=rv
else
ifeq ($(TOOLCHAIN),CodeSourcery)
TC :=gcc
else
$(error Toolchain $(TOOLCHAIN) has no mapping to a name for ADK)
endif
endif
endif

ifeq ($(GCC_TOOL_DIR),)
$(error Toolchain not found - implement wildcard macro to find toolchain and define GCC_TOOL_DIR )
endif

export TC
export GCC_TOOL_DIR

BLD :=A_$(CHIP)$(CHIP_REV)
export BLD


TOOLSBIN=$(call CONV_SLASHES,../$(subst $(SOURCE_ROOT),,$(COMMON_TOOLS_PATH)))
export TOOLSBIN

BASE_IN :=$(BASE_LOC)
SPAR_IN :=$(SPAR_LOC)
export BASE_IN
export SPAR_IN


DIR := ../$(COMMON_APP_DIR_QUALIFIER)apps/$(APP_FULL)

export DIR

PLATFORM_CONFIGS := $(addprefix ../platforms/,$(PLATFORM_CONFIGS))
export PLATFORM_CONFIGS


ifeq ($(PLATFORM_NV),EEPROM)
ifneq ($(DIRECT_LOAD),1)
COMPRESSOR := $(call CONV_SLASHES,../../$(LZSS_FULL_PATH))
export COMPRESSOR
COMPRESSION_ENABLED ?= n
export COMPRESSION_ENABLED
endif
endif

ifneq ($(APP_STATIC_DATA),)
APP_STATIC_DATA_HEX = $(BIN_OUT_DIR)/$(OUTPUT_NAME)-app-static.hex
export APP_STATIC_DATA
export APP_STATIC_DATA_HEX
endif

ifeq ($(APP_XIP),1)
APP_XIP_DATA_HEX = $(BIN_OUT_DIR)/$(OUTPUT_NAME)-app-xip.hex
export APP_XIP
export APP_XIP_DATA_HEX
endif

export CONFIG_DEFINITION
export DIRECT_LOAD
export PLATFORM_FULL
export PLATFORM_NV
export DEBUG


PLATFORM_CGS_PATH := $(addprefix ../../platforms/,$(PLATFORM_CONFIGS))
export PLATFORM_CGS_PATH

# For SDK Version
ifneq ("$(wildcard $(SOURCE_ROOT)../version.txt)","")
WICED_SDK_VERSION  = $(subst ., , $(shell $(CAT) < $(call CONV_SLASHES,$(SOURCE_ROOT)../version.txt)))
WICED_SDK_MAJOR_VER    = $(word 2, $(subst _, ,$(word 3, $(WICED_SDK_VERSION))))
WICED_SDK_MINOR_VER    = $(word 4, $(WICED_SDK_VERSION))
WICED_SDK_REV_NUMBER   = $(word 5, $(WICED_SDK_VERSION))
WICED_SDK_BUILD_NUMBER = $(word 6, $(WICED_SDK_VERSION))
else
WICED_SDK_MAJOR_VER    = 000
WICED_SDK_MINOR_VER    = 000
WICED_SDK_REV_NUMBER   = 000
WICED_SDK_BUILD_NUMBER = 0000
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_MAJOR_VER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_MAJOR_VER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_MAJOR_VER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_MAJOR_VER=$(WICED_SDK_MAJOR_VER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_MINOR_VER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_MINOR_VER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_MINOR_VER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_MINOR_VER=$(WICED_SDK_MINOR_VER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_REV_NUMBER)\" ne \"000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_REV_NUMBER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_REV_NUMBER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_REV_NUMBER=$(WICED_SDK_REV_NUMBER)
endif

ifneq ($(shell $(call CONV_SLASHES,$(PERL)) -e "if(\"$(WICED_SDK_BUILD_NUMBER)\" ne \"0000\"){print 1}else{print 0}"),0)
C_FLAGS += -DWICED_SDK_BUILD_NUMBER=$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)temp=qq($(WICED_SDK_BUILD_NUMBER));$(PERL_ESC_DOLLAR)temp=~s/^0+//;print $(PERL_ESC_DOLLAR)temp')
else
C_FLAGS += -DWICED_SDK_BUILD_NUMBER=$(WICED_SDK_BUILD_NUMBER)
endif

# For Power Class
ifneq (,$(findstring Class2,$(PLATFORM)))
C_FLAGS += -DPOWER_CLASS=2
else
# enumeration, means class 1.5
C_FLAGS += -DPOWER_CLASS=1
endif

export C_FLAGS

ifeq ($(VERBOSE),1)
$(info ADK parameters:)
$(info GCC_TOOL_DIR="$(GCC_TOOL_DIR)")
$(info TC="$(TC)")
$(info DIR="$(DIR)")
$(info BLD="$(BLD)")
$(info TOOLSBIN="$(TOOLSBIN)")
$(info BASE_IN="$(BASE_IN)")
$(info SPAR_IN="$(SPAR_IN)")
$(info BIN_OUT_DIR="$(BIN_OUT_DIR)")
$(info OUTPUT_NAME="$(OUTPUT_NAME)")
$(info CGS_LIST="$(CGS_LIST)")
$(info CONFIG_DEFINITION="$(CONFIG_DEFINITION)")
$(info PLATFORM_CGS_PATH="$(PLATFORM_CGS_PATH)")
$(info APP_FULL="$(APP_FULL)")
$(info PATCH_ROOT_DIR="$(PATCH_ROOT_DIR)")
$(info PLATFORM_NV="$(PLATFORM_NV)")
$(info DEBUG="$(DEBUG)")
$(info DIRECT_LOAD="$(DIRECT_LOAD)")
$(info COMPRESSION_ENABLED="$(COMPRESSION_ENABLED)")
$(info COMPRESSOR="$(COMPRESSOR)")
endif

CONVERT_CGS = $(PLATFORM_DOWNLOAD_TARGET) $(PLATFORM_OTA_TARGET)

build: $(BUILD_STRING) $(CONVERT_CGS)

$(shell $(ECHO) $(PLATFORM_BAUDRATE) > baud_rate.txt)
ifneq ($(UART), )
UART:=$(shell $(ECHO) $(UART) > com_port.txt))
download: build download_using_chipload
else
CHIPLOAD_SKIP_RESET:= -NOHCIRESET
download: build find_com_port download_using_chipload
endif
recover:  build recover_using_chipload

$(BUILD_STRING):
	$(QUIET)$(MAKE) -C $(SOURCE_ROOT)WICED $(SILENT) $(JOBSNO) cgs
	$(QUIET)$(call MKDIR,$(SOURCE_ROOT)build/eclipse_debug/)
	$(QUIET)$(COPY) $(SOURCE_ROOT)build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.elf $(SOURCE_ROOT)build/eclipse_debug/last_built.elf

endif
