/*
 * Copyright 2018, Inventek Systems, LLC
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Inventek Systems;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Inventek Systems.
 */

#include "wiced_platform.h"
#include "wwd_constants.h"
#include "wwd_assert.h"

#include "wiced_rtos.h"
#include "wiced_audio.h"
#include "wwd_assert.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "wiced_result.h"

extern uint8_t ism_verbose;     //Verbose messages

#define SOM "\r\n"
#define EOP SOM
#define EOM SOM

/******************************************************
 *                      Macros
 ******************************************************/
//#define USE_10KHZ
//#define USE_400KHZ

/* Verify if WICED Platform API returns success.
 * Otherwise, returns the error code immediately.
 * Assert in DEBUG build.
 */
#define VERIFY_RETVAL( x ) \
do \
{ \
    wiced_result_t verify_result = (x); \
    if ( verify_result != WICED_SUCCESS ) \
    { \
        wiced_assert( "i2c bus error", 0!=0 ); \
        return verify_result; \
    } \
} while( 0 )

/* Macro for checking of bus is initialized */
#define IS_BUS_INITIALIZED( ) \
do \
{ \
    if ( bus_initialised == WICED_FALSE ) \
    { \
        wiced_assert( "bus uninitialized", 0!=0 ); \
        return WICED_ERROR; \
    } \
} while ( 0 )


/******************************************************
 *                    Structures
 ******************************************************/

wiced_i2c_device_t wiced_ism_device =
{

    .port          = WICED_I2C_1,
    .address       = 0x60,
    .address_width = I2C_ADDRESS_WIDTH_7BIT,
#if defined(USE_10KHZ)
    .speed_mode    = I2C_LOW_SPEED_MODE,
#elif defined(USE_400KHZ)
    .speed_mode    = I2C_HIGH_SPEED_MODE,
#else
    .speed_mode    = I2C_STANDARD_SPEED_MODE,
#endif //USE_10KHZ
};


/******************************************************
 *               Function Definitions
 ******************************************************/
static volatile wiced_bool_t bus_initialised = WICED_FALSE;

wiced_result_t ism_i2c_init( void )
{
    wiced_rtos_delay_milliseconds( 50 );
    wiced_result_t status;
#ifndef NOPROBE
    wiced_bool_t   device_found;
#endif

#if 0 //[RES]
    /*if (ism_verbose)*/ WPRINT_APP_INFO( ( "[%s:%d] Inited" EOP, __func__,__LINE__ ) );
#endif //[!RES]
    status = wiced_i2c_init( &wiced_ism_device );
    if ( status != WICED_SUCCESS )
    {
       // WPRINT_APP_INFO( ( "Initializing I2C interface failed with code: %d" EOP, status ) );
        return WICED_ERROR;
    }

    wiced_rtos_delay_milliseconds(100);

#ifndef NOPROBE
    device_found = wiced_i2c_probe_device( &wiced_ism_device, 3 );
    if ( device_found != WICED_TRUE )
    {
       // WPRINT_APP_INFO( ( "Probe ISM I2C interface failed with code: %d" EOP, status ) );
        return WICED_ERROR;
    }
#endif

    bus_initialised = WICED_TRUE;

    return WICED_SUCCESS;
}

wiced_result_t ism_i2c_deinit( void )
{
    if ( bus_initialised == WICED_FALSE )
    {
        return WICED_SUCCESS;
    }

    /*if (ism_verbose)*/ // WPRINT_APP_INFO( ( "[%s:%d] DeInited" EOP, __func__,__LINE__ ) );
    /* De-initialize I2C */
    VERIFY_RETVAL( wiced_i2c_deinit( &wiced_ism_device ) );

    bus_initialised = WICED_FALSE;

    return WICED_SUCCESS;
}

wiced_result_t ism_i2c_write( const uint8_t* data_out, uint32_t size )
{
    wiced_i2c_message_t message;

    IS_BUS_INITIALIZED();

#if 0 //[RES]
   /*if (ism_verbose)*/ WPRINT_APP_INFO( ( "[%s:%d] Write" EOP, __func__,__LINE__ ) );
#endif //[!RES]
   VERIFY_RETVAL( wiced_i2c_init_tx_message( &message, data_out, size, 3, 1 ) );

    wiced_i2c_transfer( &wiced_ism_device, &message, 1 );

    return WICED_SUCCESS;
}

wiced_result_t ism_i2c_read( uint8_t* data_in, uint32_t size, uint32_t timeout_ms )
{
    wiced_result_t      status = WICED_SUCCESS;
    wiced_i2c_message_t message;

    IS_BUS_INITIALIZED();

#if 0 //[RES}
    /*if (ism_verbose)*/ WPRINT_APP_INFO( ( "[%s:%d] Read" EOP, __func__,__LINE__ ) );
#endif //[!RES]
    VERIFY_RETVAL( wiced_i2c_init_rx_message( &message, data_in, size, 3, 1 ) );
	status = wiced_i2c_transfer( &wiced_ism_device, &message, 1 );

    return status;
}

