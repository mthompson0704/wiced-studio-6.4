/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "audio_client.h"
#include "audio_render.h"
#include "audio_client_private.h"
#include "audio_player_mqtt.h"
#include "audio_player_sntp.h"
#include "audio_player_display.h"

#ifdef AUDIO_PLAYER_ENABLE_VOICE
#include "wiced_filesystem.h"
#endif

/******************************************************
 *                     Macros
 ******************************************************/

#define PRINT_IP(addr)    (int)((addr.ip.v4 >> 24) & 0xFF), (int)((addr.ip.v4 >> 16) & 0xFF), (int)((addr.ip.v4 >> 8) & 0xFF), (int)(addr.ip.v4 & 0xFF)

/******************************************************
 *                    Constants
 ******************************************************/

#define AUDIO_PLAYER_TAG_VALID              0x51EDBA15
#define AUDIO_PLAYER_TAG_INVALID            0xDEADBEEF

#define AUDIO_PLAYER_MSG_QUEUE_SIZE         (10)
#define AUDIO_PLAYER_MSGQ_PUSH_TIMEOUT_MS   (100)

#define SERVER_URI_MAX_LEN                  (128)
#define WORK_BUFFER_LEN                     (1024)

#define AUDIO_EFFECT_MODE_NONE              (0)

#ifndef AUDIO_EFFECT_MODE_MAX
#define AUDIO_EFFECT_MODE_MAX               AUDIO_EFFECT_MODE_NONE
#endif

#define VOICE_AUDIO_PLAYER_CLOCK_NEEDS_TUNING         "file:///audio_player/audio_player_clock_needs_tuning.mp3"
#define VOICE_AUDIO_PLAYER_CLOCK_TUNED                "file:///audio_player/audio_player_clock_tuned.mp3"
#define VOICE_AUDIO_PLAYER_IS_READY                   "file:///audio_player/audio_player_is_ready.mp3"
#define VOICE_AUDIO_PLAYER_CANNOT_PLAY_LINK           "file:///audio_player/audio_player_cannot_play_link.mp3"
#define VOICE_AUDIO_PLAYER_CANNOT_CONNECT             "file:///audio_player/audio_player_cannot_connect.mp3"
#define VOICE_AUDIO_PLAYER_CONNECTED                  "file:///audio_player/audio_player_connected.mp3"
#define VOICE_AUDIO_PLAYER_DISCONNECTED               "file:///audio_player/audio_player_disconnected.mp3"
#define VOICE_AUDIO_PLAYER_WRONG_COMMAND              "file:///audio_player/audio_player_wrong_command.mp3"

/* for audio tuning */
#define VOICE_AUDIO_PLAYER_PLL_PPM_TUNING             "file:///audio_player/audio_player_time_tuning_cy.mp3"
#define VOICE_AUDIO_PLAYER_PLL_PPM_TUNING_DURATION_MS ((uint32_t)(180000))

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    AP_MSG_PLAYBACK_EOS,

    AP_MSG_MAX
} AP_MSG_TYPE_T;

typedef enum
{
    PLAYER_EVENT_SHUTDOWN               = (1 <<  0),
    PLAYER_EVENT_LOAD_PLAYLIST          = (1 <<  1),
    PLAYER_EVENT_PLAYLIST_LOADED        = (1 <<  2),
    PLAYER_EVENT_OUTPUT_PLAYLIST        = (1 <<  3),
    PLAYER_EVENT_PLAY                   = (1 <<  4),
    PLAYER_EVENT_MSG_QUEUE              = (1 <<  5),
    PLAYER_EVENT_EFFECT                 = (1 <<  6),
    PLAYER_EVENT_PLAY_RAW               = (1 <<  7),
    PLAYER_EVENT_CONNECT                = (1 <<  8),
    PLAYER_EVENT_DISCONNECT             = (1 <<  9),
    PLAYER_EVENT_PACKET                 = (1 << 10),
    PLAYER_EVENT_PLAY_VOICE             = (1 << 11),

    PLAYER_EVENT_RELOAD_DCT_WIFI        = (1 << 30),
    PLAYER_EVENT_RELOAD_DCT_NETWORK     = (1 << 31),
} PLAYER_EVENTS_T;

#define PLAYER_ALL_EVENTS       (-1)

/******************************************************
 *            Audio player  Type Definitions
 ******************************************************/

typedef struct
{
    AP_MSG_TYPE_T type;
    uint32_t arg1;
    uint32_t arg2;
} ap_msg_t;

typedef struct
{
    volatile wiced_bool_t   play_voice_active;

#ifdef AUDIO_PLAYER_ENABLE_VOICE
    wiced_bool_t            init;

    wiced_filesystem_t      filesys_handle;
    wiced_file_t            file_handle;
    wiced_file_t*           file_ptr;

    uint32_t                playback_tgt_time_ms;
#endif

} audio_player_voice_t;

typedef struct
{
    uint32_t                        tag;

    wiced_event_flags_t             player_events;
    wiced_queue_t                   msgq;

    audio_player_dct_collection_t   dct_tables;
    int                             current_volume;

    audio_client_ref                audio_client;
    audio_client_suspend_t*         suspend;

    char                            work_buffer[WORK_BUFFER_LEN];

    char                            server_uri[SERVER_URI_MAX_LEN];
    char                            hostname[SERVER_URI_MAX_LEN];
    int                             port;

    int                             play_index_start;
    int                             play_index_end;
    int                             play_index_current;
    wiced_bool_t                    play_repeat;
    wiced_bool_t                    play_contiguous;

    uint32_t                        starting_offset_ms;

    int                             num_playlist_entries;
    char**                          playlist_entries;
    char*                           playlist_data;

    /*
     * Decoded audio buffer pool.
     */

    objhandle_t                     buf_pool;

    audio_render_ref                audio;

    uint32_t                        effect_mode;

    /*
     * Play raw testing.
     */

    wiced_tcp_socket_t              socket;
    wiced_tcp_socket_t*             socket_ptr;
    wiced_bool_t                    play_raw_active;
    wiced_bool_t                    disconnect_received;

    /*
     * Play voice (system)
     */

    audio_player_voice_t            voice;

    /*
     * MQTT console frontend
     */

    audio_player_mqtt_t            *mqtt;

    /*
     * Display system
     */

    audio_player_display_t         display;

} audio_player_t;


/******************************************************
 *               Variable Definitions
 ******************************************************/

/*
 * That's only an extern variable because of the console
 * not allowing API users to provide their own context variable
 */
extern audio_player_t* g_player;

/******************************************************
 *               Function Declarations
 ******************************************************/

wiced_result_t audio_player_msg_push(audio_player_t* player, AP_MSG_TYPE_T type, uint32_t arg1, uint32_t arg2);
void audio_player_delete_raw_socket(audio_player_t* player);

int audio_player_console_command(int argc, char *argv[]);
wiced_result_t audio_player_console_start(audio_player_t* player);
wiced_result_t audio_player_console_stop(audio_player_t* player);

wiced_result_t audio_player_voice_init(audio_player_t* player);
wiced_result_t audio_player_voice_deinit(audio_player_t* player);
wiced_result_t audio_player_play_voice(audio_player_t* player, const char* voice_mp3, uint32_t duration_ms);
wiced_result_t audio_player_voice_clock_adj(audio_player_t* player, int32_t* pll_ppm, uint32_t duration_ms);

#ifdef AUDIO_PLAYER_ENABLE_VOICE
wiced_result_t audio_player_open_file(audio_client_ref handle, void* userdata, char* file_url, void** file_handle, uint32_t* file_size);
wiced_result_t audio_player_read_file(audio_client_ref handle, void* userdata, void* file_handle, uint8_t* buf, uint32_t buflen, uint32_t* bytes_read);
wiced_result_t audio_player_seek_file(audio_client_ref handle, void* userdata, void* file_handle, uint32_t file_position);
wiced_result_t audio_player_close_file(audio_client_ref handle, void* userdata, void* file_handle);
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
