#
# Copyright 2017, Inventek Systems. All Rights Reserved. This software, associated documentation
# and materials ("Software"), referenced and provided with this documentation is owned by Inventek
# Systems and is protected by and subject to worldwide patent protection (United States and foreign),
# United States copyright laws and international treaty provisions. Therefore, you may use this
# Software only as provided in the license agreement accompanying the software package from which
# you obtained this Software ("EULA"). If no EULA applies, Inventek Systems hereby grants you a
# personal, non-exclusive, non-transferable license to copy, modify, and compile the Software source
# code solely for use in connection with Inventek's integrated circuit products.
#
# Any reproduction, modification, translation, compilation, or representation of this Software except
# as specified above is prohibited without the express written permission of Inventek.
# Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE.
# 
# Inventek reserves the right to make changes to the Software without notice. Inventek does not
# assume any liability arising out of the application or use of the Software or any product or
# circuit described in the Software. Inventek does not authorize its products for use in any
# products where a malfunction or failure of the Inventek product may reasonably be expected to
# result in significant property damage, injury, or death ("High Risk Product"). By including
# Inventek's product in a High Risk product, the manufacturer of such system or application
# assumes all risk of such use and in doing so agrees to indemnify Inventek against all liability.
# Inventek Systems reserves the right to make changes without further notice to any products or
# data herein to improve reliability, function, or design. The information contained within is
# believed to be accurate and reliable. However Inventek does not assume any liability arising
# out of the application or use of this information, nor the application or use of any product
# or circuit described herein, neither does it convey any license under its patent rights nor
# the rights of others.
#

NAME := Platform_ISM43340

WLAN_CHIP            := 43340
WLAN_CHIP_REVISION   := B0
WLAN_CHIP_FAMILY     := 4334x
HOST_MCU_FAMILY      := STM32F4xx
#Please note the ISM43341 uses a STM32F405 doesn't have the cryto/hash processor 
HOST_MCU_VARIANT     := STM32F415
HOST_MCU_PART_NUMBER := STM32F405IGH6
BT_CHIP              := 43340
BT_CHIP_REVISION     := B0
BT_MODE              ?= HCI

# Set clock to 144MHz
GLOBAL_DEFINES       += SYSCLK144

#Inventek Compliant Firmware
WIFI_FIRMWARE_BIN := ../platforms/$(PLATFORM)/firmware/ISM43341B0-6.49.1.0.bin

$(NAME)_SOURCES += ISM4334x_bt_firmware_image_8dBm.c
 
PLATFORM_SUPPORTS_BUTTONS := 1

INTERNAL_MEMORY_RESOURCES = $(ALL_RESOURCES)

ifndef BUS
BUS := SDIO
endif

EXTRA_TARGET_MAKEFILES +=  $(MAKEFILES_PATH)/standard_platform_targets.mk

VALID_BUSES := SDIO SPI

ifeq ($(BUS),SDIO)
ifeq ($(MULTI_APP_WIFI_FIRMWARE),)
GLOBAL_DEFINES          += WWD_DIRECT_RESOURCES
else
# Setting some internal build parameters
WIFI_FIRMWARE           := $(MULTI_APP_WIFI_FIRMWARE)
WIFI_FIRMWARE_LOCATION 	:= WIFI_FIRMWARE_IN_MULTI_APP
GLOBAL_DEFINES          += WIFI_FIRMWARE_IN_MULTI_APP
endif
endif

#For testing 1-bit mode
#GLOBAL_DEFINES          += SDIO_1_BIT

# Global includes
GLOBAL_INCLUDES  += .
GLOBAL_INCLUDES  += $(SOURCE_ROOT)libraries/inputs/gpio_button

# Global defines
# HSE_VALUE = STM32 crystal frequency = 26MHz (needed to make UART work correctly)
GLOBAL_DEFINES += HSE_VALUE=26000000
#GLOBAL_DEFINES += $$(if $$(NO_CRLF_STDIO_REPLACEMENT),,CRLF_STDIO_REPLACEMENT)

GLOBAL_DEFINES += WICED_DCT_INCLUDE_BT_CONFIG
# FIXME: flag for applications to disable bluetooth low-power mode on a specified platform - DON'T copy this to other platforms!!!
GLOBAL_DEFINES += DISABLE_BLUETOOTH_LPM

# Components
$(NAME)_COMPONENTS += drivers/spi_flash \
                      inputs/gpio_button
# Source files
$(NAME)_SOURCES := platform.c


# WICED APPS
# APP0 and FILESYSTEM_IMAGE are reserved main app and resources file system
# FR_APP :=
# DCT_IMAGE :=
# OTA_APP :=
# FILESYSTEM_IMAGE :=
# WIFI_FIRMWARE :=
# APP0 :=
# APP1 :=
# APP2 :=

# WICED APPS LOOKUP TABLE
APPS_LUT_HEADER_LOC := 0x0000
APPS_START_SECTOR := 1

ifneq ($(APP),bootloader)
ifneq ($(MAIN_COMPONENT_PROCESSING),1)
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | IMPORTANT NOTES                                                                                     | )
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | Wi-Fi MAC Address                                                                                   | )
$(info |    The target Wi-Fi MAC address is defined in <WICED-SDK>/generated_mac_address.txt                 | )
$(info |    Ensure each target device has a unique address.                                                  | )
$(info +-----------------------------------------------------------------------------------------------------+ )
$(info | MCU & Wi-Fi Power Save                                                                              | )
$(info |    It is *critical* that applications using WICED Powersave API functions connect an accurate 32kHz | )
$(info |    reference clock to the sleep clock input pin of the WLAN chip. Please read the WICED Powersave   | )
$(info |    Application Note located in the documentation directory if you plan to use powersave features.   | )
$(info +-----------------------------------------------------------------------------------------------------+ )
endif
endif

#***** Inventek Systems *****
#This is Inventek Systems internal use only
ifneq ($(wildcard ./platforms/$(PLATFORM)/iwin/iwin.mk),)
#$(info Including ./platforms/$(PLATFORM)/iwin/iwin.mk )
include ./platforms/$(PLATFORM)/iwin/iwin.mk
endif #./platforms/$(PLATFORM)/iwin/iwin.mk
#****************************

