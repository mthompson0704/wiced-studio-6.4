/*
********************************************************************
* THIS INFORMATION IS PROPRIETARY TO
* Cypress Semiconductor.
*-------------------------------------------------------------------
*                                                                        
*           Copyright (c) 2014 Cypress Semiconductor.
*                      ALL RIGHTS RESERVED                              
*                                                                       
********************************************************************/
#ifndef __FAST_BOOT_PATCHES_H__
#define __FAST_BOOT_PATCHES_H__

#ifdef __cplusplus
extern "C" {
#endif

void hclk_96MHz_override_enable(void);
void hclk_96MHz_override_disable(void);
void fast_boot_patches_init(void);

#ifdef __cplusplus
}
#endif

#endif


