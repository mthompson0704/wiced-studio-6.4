/*
 * Copyright 2015, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                     Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
//
// struct for DCT configuration
//

typedef struct
{
    uint32_t NewConfigurationFlag;
    uint64_t DeviceID; //will be set to Wi-Fi MAC address
    char PumpSpyHostName[100];
    uint16_t PumpSpyHostPort;
    uint16_t startup_delay;
    int16_t default_primary_pump_current_limit;
    int16_t default_motor_current_limit;
    int8_t main_pump_init_index;
    int16_t main_pump_baseline_array[8];
    int8_t backup_pump_init_index;
    int16_t backup_pump_baseline_array[8];
} config_mode_app_dct_t;

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

#ifdef __cplusplus
} /*extern "C" */
#endif
