/*
 * Copyright (C) 2018, Cirrent Inc
 *
 * All use of this software must be covered by a license agreement signed by Cirrent Inc.
 *
 * DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
 * ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE.
 */

/*
 * Basic types which are used by the Cirrent Agent API
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#ifndef bool
typedef unsigned char bool;
#endif

#ifndef false
#define false 0
#endif

#ifndef true
#define true (!false)
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
