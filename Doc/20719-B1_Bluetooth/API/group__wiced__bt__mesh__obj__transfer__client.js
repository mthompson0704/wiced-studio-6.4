var group__wiced__bt__mesh__obj__transfer__client =
[
    [ "wiced_bt_mesh_obj_transfer_client_callback_t", "group__wiced__bt__mesh__obj__transfer__client.html#gaba890f767dade23ddef7b20a3b9b9fd8", null ],
    [ "wiced_bt_mesh_obj_transfer_client_data_callback_t", "group__wiced__bt__mesh__obj__transfer__client.html#gaaecb64301d485dad69d17a84bcfdae30", null ],
    [ "wiced_bt_mesh_model_obj_transfer_client_abort", "group__wiced__bt__mesh__obj__transfer__client.html#ga66ade77d960fe1ce449255138ea7b697", null ],
    [ "wiced_bt_mesh_model_obj_transfer_client_init", "group__wiced__bt__mesh__obj__transfer__client.html#ga75478675a52e84f94dfae14cd10c9470", null ],
    [ "wiced_bt_mesh_model_obj_transfer_client_message_handler", "group__wiced__bt__mesh__obj__transfer__client.html#gae17b0165f7043c946387114d61cf1015", null ],
    [ "wiced_bt_mesh_model_obj_transfer_client_phase_get", "group__wiced__bt__mesh__obj__transfer__client.html#ga68c7eb47978463bfcf8c969d9010d30f", null ],
    [ "wiced_bt_mesh_model_obj_transfer_client_start", "group__wiced__bt__mesh__obj__transfer__client.html#ga970703d3b0d31aa551498424091b055a", null ],
    [ "wiced_bt_mesh_object_transfer_client_get_info", "group__wiced__bt__mesh__obj__transfer__client.html#gab0c3f817a96b204677a6a550b10d93f4", null ],
    [ "wiced_bt_mesh_object_transfer_client_get_status", "group__wiced__bt__mesh__obj__transfer__client.html#ga4e05da95825b7fca2072e15874059bf0", null ]
];