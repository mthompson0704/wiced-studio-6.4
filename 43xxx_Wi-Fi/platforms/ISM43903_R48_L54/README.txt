--------------------------------------------
ISM943903_R48_L54 - README
--------------------------------------------

Provider    : Inventek Systems, LLC.
Website     : http://www.inventeksys.com
Description : Inventek's ISM43903_R48_L54 module mounted on a evaluation board


Module
  Mfr       : Inventek Systems
  P/N       : ISM43903-R48-L54
  MCU       : BCM943903 Cortex-R4 320MHz  (Apps Core)
  WLAN      : BCM943903 Cortex-R4 (WLAN Core)
  WLAN Antennas : Echted antennae or U.FL connectors

EVB Features:
  ARM 20-pin JTAG debug interface
  USB-JTAG debug interface
  USB-serial UART interface
  Power supply : USB and/or external +5v
  Reset button
  Module current monitor
  Sensors/Peripherals
     - 2 x Buttons
     - 2 x LEDs
     - 1 x Thermistor
     - 1 x 8Mbit serial flash
  28-pin Expansion header
