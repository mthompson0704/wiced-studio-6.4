/*
 * Author: Timothy WrightBrandt
 * Date: 3/1/2019
 * This program covers the UART, ADC read, as well as the I2C write functionality.
 * Use of this sample code is at your own risk.
 *
 *
 *
 */


#include "wiced.h"
#include <math.h>
#include <malloc.h>

#include "wiced_platform.h"
#include "platform.h"
#include "wwd_constants.h"
#include "wwd_assert.h"

#include "wiced_rtos.h"
#include "wiced_audio.h"
#include "wwd_assert.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdbool.h"
#include "wiced_result.h"
#include "float.h"

#include "..\..\..\libraries\isI2C\isI2C.h"


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
#define LED_DRIVER  0xC0         //address of TLC59116 LED DRIVER
#define MODE1       0x00         //Mode 1 register address
#define MODE2       0x01         //Mode 2 register address
#define PWM0        0x02         //PWM0 Brightness control LED0
#define PWM2        0x04         //PWM2 Brightness control LED2
#define PWM3        0x05         //PWM3 Brightness control LED3
#define PWM4        0x06         //PWM4 Brightness control LED4
#define PWM5        0x07         //PWM5 Brightness control LED5
#define PWM6        0x08         //PWM6 Brightness control LED6
#define PWM7        0x09         //PWM7 Brightness control LED7
#define PWM8        0x0A         //PWM8 Brightness control LED8
#define PWM9        0x0B         //PWM9 Brightness control LED9
#define PWM10       0x0C         //PWM10 Brightness control LED10
#define PWM11       0x0D         //PWM11 Brightness control LED11
#define PWM12       0x0E         //PWM12 Brightness control LED12
#define PWM13       0x0F         //PWM13 Brightness control LED13
#define PWM15       0x11         //PWM15 Buzzer output
#define GRPPWM      0x12         //Group duty cycle control register
#define LEDOUT0     0x14         //LED output state 0
#define LEDOUT1     0x15         //LED output state 1
#define LEDOUT2     0x16         //LED output state 2
#define LEDOUT3     0x17         //LEd output state 3
#define TWI_TX      0x84         //write data to TWI
#define MODE1REGSET 0x00         //Set MODE1 reg values
#define MODE2REGSET 0x80         //Set MODE2 reg values
#define OFF         0x00         //SET LED Brightness to zero
#define ON          0xFF         //Set full ON
#define BRIGHSET    0x5F         //SET Brightness of LEDs
#define YELLOW1     0x3F         //SET Color YELLOW green
#define YELLOW2     0x2F         //SET Color YELLOW red
#define BUZZER      0xFF         //Set buzzer to full volume.
#define GRPPWMSET   0xAF         //SET Group Duty Cycle
#define LEDSETON    0XAA         //SET LED output to full ON
#define LEDSETOFF   0x00         //SET LED output to full OFF
#define NUM_I2C_MESSAGE_RETRIES     3
#define DC_PUMP_CYCLE_TIME          6
#define TIMER_TIME (1000)       //Approximately 1 Sec. Adjust to calibrate
#define OV_DELAY 3              //3 second Delay for overvoltage event

#define WICED_I2C_START_FLAG                    (1U << 0)
#define WICED_I2C_REPEATED_START_FLAG           (1U << 1)
#define WICED_I2C_STOP_FLAG                     (1U << 2)

//#define DELAY_BETWEEN_SCANS       ( 5000 )

//#ifndef WICED_LED2
//#define WICED_LED2  WICED_LED1
//#endif


/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct
{
    wiced_semaphore_t   semaphore;      /* Semaphore used for signaling scan complete */
    uint32_t            result_count;   /* Count to measure the total scan results    */
} app_scan_data_t;

/******************************************************
 *               Static Function Declarations
 ******************************************************/
wiced_result_t scan_result_handler( wiced_scan_handler_result_t* malloced_scan_result );
void Buzzer_ON(void);
//void Buzzer_OFF(void);

/******************************************************
 *               Variable Definitions
 ******************************************************/
uint16_t STATE[5] = {0};  //[0] = AC power, [1] = Battery,  [2] = DC PUMP, [3] = Not assigned, [4] = Not assigned.
uint32_t  adc0_value = 0;
uint32_t  adc1_value = 0;
uint32_t  adc2_value = 0;
uint32_t  adc3_value = 0;
uint32_t prevAC = 0;
uint32_t prevBV = 0;
uint32_t prevACI = 0;


//uint16_t  adc11_value = 0;
//uint16_t  adc21_value = 0;
//uint16_t  adc31_value = 0;

uint8_t Counter = 0;   //1 second counter
uint8_t SecTimer = 0;   //1 second counter
uint8_t MinTimer = 0;  //1 minute counter
uint8_t HourTimer = 0;  //24 Hour counter
uint8_t deactivate_buzzer = 0;
uint8_t Battery_LOW = 0;  // do not let pump run with critically low battery
uint8_t NO_BAT = 0;       // No battery detected.
uint8_t REV_BAT = 0;      // Reverse polarity.
uint8_t signalFloat = 0;  // which Signal Float has triggered the pump.
uint8_t cooldownTimer = 0;// used for 10 second delay after pump deactivates
uint8_t testStep = 0;     //used to step through self test
uint8_t OVTic = 0;
static wiced_timer_t timerHandle;

wiced_bool_t button1_pressed;
wiced_bool_t button2_pressed;
wiced_bool_t button3_pressed;
wiced_bool_t button4_pressed;
wiced_bool_t button5_pressed;
wiced_bool_t AC_Present;
wiced_bool_t bat_Present;
wiced_bool_t charging_Bat;

wiced_bool_t pump_Active;
wiced_bool_t pump_Cooldown;

wiced_bool_t selfTestFlag;
wiced_bool_t RVPFlag;
wiced_bool_t OV_Flag;
wiced_bool_t led_On;

float batVolt = 0;

/*void*memset(void* ptr, int value, size_t num );*/

//static volatile wiced_bool_t bus_initialised = WICED_FALSE;
/******************************************************
 *               Function Definitions
 ******************************************************/
void LED_RED(void)
{
    uint8_t dout[5] = {0};
    memset(dout,0x00,5);
    uint32_t lngth =2;
    dout[0] = 0x00;
    dout[1] = 0x00;

    ism_i2c_init();
    //Mode Registers do not need to be set every time. They are set once in the init function.
//    dout[0] = MODE1;
//    dout[1] = MODE1REGSET;
//    ism_i2c_write( (uint8_t*)&dout, lngth );
//    dout[0] = MODE2;
//    dout[1] = MODE2REGSET;
//    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM2;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM4;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM6;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM8;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM12;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = GRPPWM;
    dout[1] = GRPPWMSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT0;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT1;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT2;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT3;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}
void LED_GREEN(void)
{
    uint8_t dout[5] = {0};
    memset(dout,0x00,5);
    uint32_t lngth =2;
    dout[0] = 0x00;
    dout[1] = 0x00;

    ism_i2c_init();
    //Mode Registers do not need to be set every time. They are set once in the init function.
//    dout[0] = MODE1;
//    dout[1] = MODE1REGSET;
//    ism_i2c_write( (uint8_t*)&dout, lngth );
//    dout[0] = MODE2;
//    dout[1] = MODE2REGSET;
//    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM3;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM5;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM7;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM9;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM11;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM13;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = GRPPWM;
    dout[1] = GRPPWMSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT0;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT1;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT2;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT3;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}

void LED_YELLOW(void)
{
    uint8_t dout[5] = {0};
    memset(dout,0x00,5);
    uint32_t lngth =2;
    dout[0] = 0x00;
    dout[1] = 0x00;

    ism_i2c_init();
    dout[0] = PWM3;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM5;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM7;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM9;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM11;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM13;
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = GRPPWM;
    dout[1] = GRPPWMSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT0;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT1;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT2;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT3;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM2;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = PWM4;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = PWM6;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = PWM8;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = PWM10;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = PWM12;
       dout[1] = BRIGHSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = GRPPWM;
       dout[1] = GRPPWMSET;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = LEDOUT0;
       dout[1] = LEDSETON;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = LEDOUT1;
       dout[1] = LEDSETON;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = LEDOUT2;
       dout[1] = LEDSETON;
       ism_i2c_write( (uint8_t*)&dout, lngth );
       dout[0] = LEDOUT3;
       dout[1] = LEDSETON;
       ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}
//void LED_OFF(void)
/* This function initializes the register in the LED driver IC and turns all outputs to OFF. */
void LED_INIT(void)
{
    uint8_t dout[5] = {0};
    memset(dout,0x00,5);
    uint32_t lngth =2;
    dout[0] = 0x00;
    dout[1] = 0x00;

    ism_i2c_init();
    dout[0] = MODE1;
    dout[1] = MODE1REGSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = MODE2;
    dout[1] = MODE2REGSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM2;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM3;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM4;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM5;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM6;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM7;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM8;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM9;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM11;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM12;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM13;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = GRPPWM;
    dout[1] = GRPPWMSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT0;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT1;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT2;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = LEDOUT3;
    dout[1] = LEDSETON;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}

void check_ac(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    uint32_t AC_Average = 0;
    int i;

    for(i=0;i<30;i++)
    {
    wiced_adc_take_sample(WICED_ADC_4, &adc3_value ); //This is the Battery Voltage sense line ADC1 on EVB
    AC_Average  += adc3_value;
    }
    AC_Average = AC_Average/30;
    prevAC = AC_Average;
    if(STATE[2] == 0x000)//meaning the pump is not running
    {
        if(charging_Bat)
        {
            if(AC_Average >= 3250)
            {
                AC_Present = WICED_TRUE;
                STATE[0] = 0x111; //We have AC if DC pump is not running turn on bat charge.
                ism_i2c_init();
                dout[0] = PWM7;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                dout[0] = PWM6;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                ism_i2c_deinit();
            }
            else if(AC_Average < 3250)
            {
                AC_Present = WICED_FALSE;
                STATE[0] = 0x000; //We DO NOT have AC. Bat charge set to off.
                Charger_OFF();
                ism_i2c_init();
                dout[0] = PWM7;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                dout[0] = PWM6;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                ism_i2c_deinit();

            }
        }
        else
        {
            if(AC_Average >= 600)
            {
                AC_Present = WICED_TRUE;
                STATE[0] = 0x111; //We have AC if DC pump is not running turn on bat charge.
                ism_i2c_init();
                dout[0] = PWM7;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                dout[0] = PWM6;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                ism_i2c_deinit();
            }
            else if(AC_Average < 600)
            {
                AC_Present = WICED_FALSE;
                STATE[0] = 0x000; //We DO NOT have AC. Bat charge set to off.
                Charger_OFF();
                ism_i2c_init();
                dout[0] = PWM7;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                dout[0] = PWM6;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                ism_i2c_deinit();

            }
        }
    }
}
void check_ac_Current(void)
{////SET STATE IN HERE FOR PUMP RUNNING FIRST TIME. Will NEED VALUES AT VERIOUS LEVELS OF CHARGE. AC?NO AC....
        uint8_t dout[5] = {0};
        uint32_t lngth =2;
        uint32_t Average = 0;
        int i;
        for(i=0;i<30;i++)
        {
        wiced_adc_take_sample(WICED_ADC_1, &adc0_value ); //This is the Battery Voltage sense line ADC1 on EVB
        Average  += adc0_value;
        }
        Average = Average/30;
        prevACI = Average;
       // wiced_adc_take_sample(WICED_ADC_1, Average ); //This is the AC Current sense line
       // WPRINT_APP_INFO(("AC Current Value!   %d,\r\r\n", adc0_value));
        if(Average >= 2500) //Set these to a variable that averages pump draw.
        {
            ism_i2c_init(); //TURN RED LED ON. Current draw to high.
            dout[0] = PWM9;
            dout[1] = OFF;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM8;
            dout[1] = BRIGHSET;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            ism_i2c_deinit();
           // WPRINT_APP_INFO(("Main Pump has ran:)  \r\n"));
        }
        else if(Average < 2500)
        {
            ism_i2c_init();//TURN GREEN LED ON.
            dout[0] = PWM9;
            dout[1] = BRIGHSET;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM8;
            dout[1] = OFF;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            ism_i2c_deinit();
            // WPRINT_APP_INFO(("Main Pump has NOT ran :)  \r\n"));
        }
}

float convertBatVoltage(uint32_t ADC_Value)
{
    float voltage = 0;

    voltage = 0.0047*ADC_Value - 1.6;
    return voltage;
}

void check_bat(void)
{
    uint8_t Count = 0;
    uint32_t BV_Average = 0;
    int i;
    RVPFlag = WICED_FALSE;
    bat_Present = WICED_FALSE;
   // Charger_OFF();  /Not here
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
   // wiced_rtos_delay_milliseconds(5000);
    for(i=0;i<30;i++)
    {
    wiced_adc_take_sample(WICED_ADC_2, &adc1_value ); //This is the Battery Voltage sense line ADC1 on EVB
    BV_Average  += adc1_value;
    }
    BV_Average = BV_Average/30;
    prevBV = BV_Average;
    batVolt = convertBatVoltage(BV_Average);

    if(!OV_Flag)
    {

        if(BV_Average > 3160) //need to find ADC Value with 13.45 Volts 3165
        {
            Charger_OFF();
            WPRINT_APP_INFO(("Over Voltage\r\r\n"));
            OV_Flag = WICED_TRUE;   //set over voltage flag
            bat_Present = WICED_TRUE;
            //wiced_rtos_delay_milliseconds(3000);  //wait 3 seconds to re-test Battery. (Changed to use timer)
    //        for(i=0;i<30;i++)
    //        {
    //        wiced_adc_take_sample(WICED_ADC_2, &adc1_value ); //This is the Battery Voltage sense line ADC1 on EVB
    //        BV_Average  += adc1_value;
    //        }

            if(BV_Average > 1425 && BV_Average <= 3160) //battery is present its just fully charged.
            {
                //GREEN LED Normal Voltage
                STATE[1] = 0x111; //STATE[1] = Battery voltage good. Can run DC pump.
                bat_Present = WICED_TRUE;
                if(STATE[2] == 0x000)
               // if(STATE[0] == 0x111 && STATE[2] == 0x000)//STATE[0] is the AC power.  STATE[2] is the DC pump ON/OFF
                {
                    //Charger_ON();//have to double check this with a battery present.
                }
                ism_i2c_init();
                dout[0] = PWM5;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                dout[0] = PWM4;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                Buzzer_OFF();
            }
        }
       // else if(BV_Average )
        // was 2600 to 3160 in Rev 3
        else if(BV_Average > 2682 && BV_Average <= 3160) //need to find ADC Value with 12.80 Volts  about 3060
        {
            //GREEN LED    Normal Voltage
            STATE[1] = 0x111; //STATE[1] = Battery voltage good. Can run DC pump.
            bat_Present = WICED_TRUE;
            if(STATE[2] == 0x000)
           // if(STATE[0] == 0x111 && STATE[2] == 0x000)//STATE[0] is the AC power.  STATE[2] is the DC pump ON/OFF
            {
                Charger_ON();
                //Charger_OFF();  //For testing
            }
            ism_i2c_init();
            dout[0] = PWM5;
            dout[1] = BRIGHSET;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM4;
            dout[1] = OFF;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            Buzzer_OFF();
            //ism_i2c_deinit();
        }
        //was 2515 to 2600 in Rev 3
        else if(BV_Average > 2624 && BV_Average <= 2682)
        {
            //YELLOW LED    Low Voltage  10.7V to 11.0V
            STATE[1] = 0x111;  //STATE[1] = Battery voltage good. Can run DC pump.
            bat_Present = WICED_TRUE;
            if(STATE[2] == 0x000)
           // if(STATE[0] == 0x111 && STATE[2] == 0x000)//STATE[0] is the AC power.  STATE[2] is the DC pump ON/OFF
            {
                Charger_ON();
                //Charger_OFF();//For testing
            }
            ism_i2c_init();
            dout[0] = PWM5;
            dout[1] = YELLOW1;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM4;
            dout[1] = YELLOW2;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            Buzzer_OFF();
           // ism_i2c_deinit();
        }
        //was 1900 to 2115 in Rev 3
        else if(BV_Average > 1534 && BV_Average <=2624)
        {
            //RED LED   Critical Voltage  6V to 10.7V
            STATE[1] = 0x111;  //STATE[1] = Battery voltage good. Can run DC pump.=
            bat_Present = WICED_TRUE;
            if(STATE[2] == 0x000)
           // if(STATE[0] == 0x111 && STATE[2] == 0x000)//STATE[0] is the AC power.  STATE[2] is the DC pump ON/OFF
            {
                Charger_ON();
                //Charger_OFF(); //For testing.
            }
            ism_i2c_init();
            dout[0] = PWM5;
            dout[1] = OFF;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM4;
            dout[1] = BRIGHSET;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            if(deactivate_buzzer == 0)
            {
                Buzzer_ON();
            }
            else
            {
                Buzzer_OFF();
            }
          //  ism_i2c_deinit();
        }
        else if(BV_Average > 1000 && BV_Average <= 1900)
        {
            STATE[1] = 0x000;  //In this state do not let the DC pump Run. This cutoff is 8 Volts.
            bat_Present = WICED_TRUE;
            if(STATE[2] == 0x000)
           // if(STATE[0] == 0x111 && STATE[2] == 0x000)//STATE[0] is the AC power.  STATE[2] is the DC pump ON/OFF
            {
                Charger_ON();
                //Charger_OFF(); //For testing
            }
            ism_i2c_init();
            dout[0] = PWM5;
            dout[1] = OFF;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            dout[0] = PWM4;
            dout[1] = BRIGHSET;
            ism_i2c_write( (uint8_t*)&dout, lngth );
            if(deactivate_buzzer == 0)
            {
                Buzzer_ON();
            }
            else
            {
                Buzzer_OFF();
            }
        }
        else if(BV_Average > 0 && BV_Average <= 1000)//need to flASH LED IN THIS STATE??
        {                                              //need to find the ADC value for no battery.
           // NO_BAT = 1;
            //Charger_ON();
            //Charger_OFF();  //For testing
            STATE[1] = 0x000;  //In this state do not let the DC pump Run.
            bat_Present = WICED_FALSE;
            ism_i2c_init();
            Buzzer_ON(); //Cannot silence this one.
           // wiced_rtos_delay_milliseconds(50);
            while(BV_Average <=  400)
            {
                for(i=0;i<30;i++)
                {
                wiced_adc_take_sample(WICED_ADC_2, &adc1_value ); //This is the Battery Voltage sense line ADC1 on EVB
                BV_Average  += adc1_value;
                }
                BV_Average = BV_Average/30;
                WPRINT_APP_INFO(("BATTERY V  %d,\r\r\n", BV_Average));
                if(BV_Average > 400 && BV_Average <=1000)
                {
                    WPRINT_APP_INFO(("Blinking LED NO BAT\r\r\n"));
                }
                else if(BV_Average < 400)
                {
                    WPRINT_APP_INFO(("Blinking LED RVP\r\r\n"));
                    RVPFlag = WICED_TRUE;
                }
                dout[0] = PWM5;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                ism_i2c_init();
                dout[0] = PWM4;
                dout[1] = BRIGHSET;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                wiced_rtos_delay_milliseconds(100);
                dout[0] = PWM4;
                dout[1] = OFF;
                ism_i2c_write( (uint8_t*)&dout, lngth );
                wiced_rtos_delay_milliseconds(100);
            }
            ism_i2c_deinit();
        }
    }

}

void  check_dc_pump(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    wiced_adc_take_sample(WICED_ADC_3, &adc2_value ); //This is DC current check
  //  WPRINT_APP_INFO(("DC CURRENT  %d,\r\r\n", adc2_value));
    if(adc2_value >= 500)  //Hard code current draw limit on the DC pump.
    {
        ism_i2c_init();
        dout[0] = PWM11;
        dout[1] = OFF;
        ism_i2c_write( (uint8_t*)&dout, lngth );
        dout[0] = PWM10;
        dout[1] = BRIGHSET;
        ism_i2c_write( (uint8_t*)&dout, lngth );
        ism_i2c_deinit();
        WPRINT_APP_INFO(("\r\nDC_CURRENT BAD :(  (Check Fuse)\r\n"));
    }
    else if(adc2_value < 500)
    {

        ism_i2c_init();
        dout[0] = PWM11;
        dout[1] = BRIGHSET;
        ism_i2c_write( (uint8_t*)&dout, lngth );
        dout[0] = PWM10;
        dout[1] = OFF;
        ism_i2c_write( (uint8_t*)&dout, lngth );
        ism_i2c_deinit();
        WPRINT_APP_INFO(("\r\nDC_CURRENT_GOOD :)  (Fuse Good)\r\n"));
    }
}

void check_float(void* arg)
{
    button2_pressed = wiced_gpio_input_get( WICED_BUTTON2 ) ? WICED_FALSE : WICED_TRUE; //Check if float is still high
    if(button2_pressed == WICED_TRUE)
    {
        STATE[4] = 0x111;//Pump float signaled pump to run
    }
}

void Float_Pump(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    STATE[4] = 0x000; //Pump float signaled pump to run.
    if(STATE[1] == 0x111)
    {
    wiced_gpio_output_high( WICED_PUMP );   //Turn DC pump on. start counter
    //signalFloat = 1; //has to stay with GPIO command to turn on pump
    Counter = 0;
    ism_i2c_init();
    dout[0] = PWM11;
    dout[1] = OFF; //PUMP LED GREEN OFF
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = BRIGHSET; //PUMP LED RED ON
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
    }
    else
    {
        //Do nothing.
    }


}

void Pump_OFF(void)
{
    STATE[2] = 0x000; //Allow battery charging again.
    wiced_gpio_output_low( WICED_PUMP );   //Turn DC pump OFF.
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    ism_i2c_init();
    dout[0] = PWM11;
    dout[1] = BRIGHSET;   //Green LED ON
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = OFF;        //Red LED OFF
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}

void H_Water_OFF(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    STATE[2] = 0x000;  //Allow battery charging again.
    wiced_gpio_output_low( WICED_PUMP ); //Turn DC pump OFF.
    ism_i2c_init();
    dout[0] = PWM11;
    dout[1] = BRIGHSET;   //Green LED ON
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = OFF;        //Red LED OFF
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM13;  //PUMP LED GREEN
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM12;  //PUMP LED RED
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    ism_i2c_deinit();
}


void high_water_check(void)
{

    button3_pressed = wiced_gpio_input_get( WICED_BUTTON3 ) ? WICED_FALSE : WICED_TRUE; //Check if float is still high
    if(button3_pressed == WICED_TRUE)
    {
        STATE[3] = 0x111;//High water signaled pump to run
    }

}
void High_Water_Pump(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    STATE[2] = 0x111;//Do not let battery charger during this time.
    STATE[3] = 0x000;//High water signaled pump to run

    if(STATE[1] == 0x111)
    {
    Charger_OFF();
    wiced_gpio_output_high( WICED_PUMP ); //Turn DC pump ON.
    signalFloat = 1; //has to stay with GPIO command to turn on pump
    Counter = 0;
    WPRINT_APP_INFO(( "Button 3 High Water Sound Alarm!!!!\r\n" ));

    ism_i2c_init();
    dout[0] = PWM11;
    dout[1] = OFF; //PUMP LED GREEN OFF
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM10;
    dout[1] = BRIGHSET; //PUMP LED RED ON
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM13;  //High Water LED GREEN
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth );
    dout[0] = PWM12;  //High Water LED RED
    dout[1] = BRIGHSET;
    ism_i2c_write( (uint8_t*)&dout, lngth );
       if(deactivate_buzzer == 0)  //Sound alarm. check spec is this one mute or no mute?
       {
           dout[0] = PWM15;
           dout[1] = BUZZER;
           ism_i2c_write( (uint8_t*)&dout, lngth );
       }
    ism_i2c_deinit();
    }
    else
    {
        //Do nothing.
    }
}

void Buzzer_OFF(void)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    dout[0] = PWM15;
    dout[1] = OFF;
    ism_i2c_write( (uint8_t*)&dout, lngth);
    ism_i2c_deinit();
}

void Buzzer_ON(void)   //need to check state for 24 hour silence
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
                        //  ism_i2c_init();//not needed if called in previous function.
                          dout[0] = PWM15;
                          dout[1] = 0xFF;
                          ism_i2c_write( (uint8_t*)&dout, lngth);
                          ism_i2c_deinit();
}

void Silence_Buzzer(void) //going to need to learn timers for this one to be a 24 hr timer.
{

    button1_pressed = wiced_gpio_input_get( WICED_BUTTON5 ) ? WICED_FALSE : WICED_TRUE;  /* The button has inverse logic */

           if ( button1_pressed == WICED_TRUE)
           {
               wiced_rtos_delay_milliseconds(10);
               button1_pressed = wiced_gpio_input_get( WICED_BUTTON5 ) ? WICED_FALSE : WICED_TRUE;  /* The button has inverse logic */
               if(button1_pressed == WICED_TRUE)
               {
                   //WPRINT_APP_INFO(( "Button 1 Buzzer set up button Pushed!!!!\r\n" ));

                   if( deactivate_buzzer == 1)
                    {
                       deactivate_buzzer = 0;
                       WPRINT_APP_INFO(( "Alarm Active!!!!\r\n" ));

                    }
                   else if (deactivate_buzzer == 0)
                   {
                       deactivate_buzzer = 1;
                       WPRINT_APP_INFO(( "Alarm Silenced!!!!\r\n" ));
                       ism_i2c_init();
                       Buzzer_OFF();////Set 24 hour timer here.
                   }
               }

           }

}

void Provision(void)
{
    if(led_On)
    {
        wiced_gpio_output_low( WICED_LED1 );
        led_On = WICED_FALSE;
    }

    else
    {
        wiced_gpio_output_high( WICED_LED1 );
        led_On = WICED_TRUE;
    }
}

/*
 * Callback function to handle scan results
 */
wiced_result_t scan_result_handler( wiced_scan_handler_result_t* malloced_scan_result )
{
    /* Validate the input arguments */
    wiced_assert("Bad args", malloced_scan_result != NULL);

    if ( malloced_scan_result != NULL )
    {
        app_scan_data_t* scan_data  = (app_scan_data_t*)malloced_scan_result->user_data;

        malloc_transfer_to_curr_thread( malloced_scan_result );

        if ( malloced_scan_result->status == WICED_SCAN_INCOMPLETE )
        {
            wiced_scan_result_t* record = &malloced_scan_result->ap_details;

            WPRINT_APP_INFO( ( "%3ld ", scan_data->result_count ) );
            print_scan_result(record);
            scan_data->result_count++;
        }
        else
        {
            wiced_rtos_set_semaphore( &scan_data->semaphore );
        }

        free( malloced_scan_result );
    }
    return WICED_SUCCESS;
}

void Charger_ON(void)
{
    if(bat_Present)
    {
     wiced_gpio_output_high( WICED_CHARGER );
     charging_Bat = WICED_TRUE;
    //wiced_gpio_output_low( WICED_CHARGER ); //Comment out after testing ADC reads. This keeps the battery charger OFF.
    }
}

void Charger_OFF(void)
{
    wiced_gpio_output_low( WICED_CHARGER );
    charging_Bat = WICED_FALSE;
}
void check_Pump_CooldownTimer(void)
{
    if(cooldownTimer >= 10)
    {
        pump_Cooldown = WICED_FALSE;
    }
}

void tictoc(void* arg)
{
    Counter ++;
    SecTimer++;
    if(SecTimer >= 60)
    {
        SecTimer = 0;
        MinTimer ++;
    }
   // HourTimer ++;
    cooldownTimer ++;
    if(MinTimer >= 60)
    {
        MinTimer = 0;
        if(HourTimer > 1440)
        {
            deactivate_buzzer = 0;
            HourTimer = 0;
        }
        else
        HourTimer ++;  //needs to count up to 1440
    }
    if(OV_Flag)
    {
        OVTic++;
        if(OVTic > 3)
        {
            OV_Flag = WICED_FALSE;
            OVTic = 0;
        }
    }
    else
    {
        OVTic = 0;
    }
}

void auto_Self_Test_Timer(void)
{
//    if((HourTimer % 48) == 0)
//    {
        if((MinTimer % 60) == 0)
        {
            if(SecTimer <20)
                selfTestFlag = WICED_TRUE;
        }
//    }
}
void auto_Self_Test(void)
{
    wiced_bool_t DC_CHECK = WICED_FALSE;

    WPRINT_APP_INFO(("\r\n\r\n48 Hour Self Test Initiated...\r\n"));
    check_bat(); //Initial battery check for reverse polarity

    if(RVPFlag) //print whether or not reverse polarity detected
         WPRINT_APP_INFO(("Reverse Polarity Detected.\r\n"));
    else
         WPRINT_APP_INFO(("No Reverse Polarity Detected.\r\n"));
    check_ac(); //checking for presence of AC power
    Charger_OFF();  //turn charger off before pump is run
    WPRINT_APP_INFO(("Charger Turned OFF...\r\n"));
    Float_Pump();   //run DC pump
    WPRINT_APP_INFO(("Pump Turned ON...\r\n"));
    while(Counter < 10) //loop to run pump for ten seconds
    {
        WPRINT_APP_INFO(("%d", Counter));
        WPRINT_APP_INFO(("\b"));
        if((Counter == 5)&&(DC_CHECK == WICED_FALSE))
        {
            check_dc_pump();    //in the middle of the pump run event, check DC current to assure fuse is not blown
            DC_CHECK = WICED_TRUE;  // set DC_CHECK flag high to only perform check once
        }
    }
    WPRINT_APP_INFO(("\r\nPump Cooldown...\r\n"));
    Pump_OFF(); //turn pump off after ten second loop
    wiced_rtos_delay_milliseconds(10000);   //ten second cool down delay
    check_bat();    //check battery again for voltage levels
    selfTestFlag = WICED_FALSE;

}
void clearTerminal(void)
{
    WPRINT_APP_INFO(("\033[2J"));
    WPRINT_APP_INFO(("\033[0;0H"));
}
void loop_Info(void)
{
    WPRINT_APP_INFO(("On-Time:\t%04d:%02d:%02d\r\n", HourTimer, MinTimer, SecTimer));
    WPRINT_APP_INFO(("PMP: %d, CD: %d\r\nCTR:%d\r\n", pump_Active, pump_Cooldown, Counter));
    WPRINT_APP_INFO(("AC Voltage ADC:   %d,\r\r\n", prevAC));
    WPRINT_APP_INFO(("AC Current ADC:   %d,\r\r\n", prevACI));
    if(AC_Present)
    {
        WPRINT_APP_INFO(("AC Present :)  \r\n"));
    }
    else
    {
        WPRINT_APP_INFO(("NO AC Present :(  \r\n"));
    }
    WPRINT_APP_INFO(("BV ADC:   %d,\r\r\n", prevBV));
    WPRINT_APP_INFO(("TOP BATTERY V:  %02.2f,\r\r\n", batVolt));
}


#define I2C_DELAY 10
extern void platform_init_i2c_sda( int is_input );
extern void platform_init_scl_sda_gpio(void);

void write_i2c_byte2(uint8_t data_byte)
{
	uint8_t bit_count = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	WPRINT_APP_INFO( ( "starting write_i2c_byte %02x\n\r", data_byte ) );
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		if ((data_byte & 0x80) > 0)
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit high \n\r"  ) );
			wiced_gpio_output_high( I2C_SDA );
		}
		else
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit low \n\r"  ) );
			wiced_gpio_output_low( I2C_SDA );
		}
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		data_byte = data_byte << 1;
	}
	// check the ack
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x nack\n\r", data_byte ) );
	}
	else
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x ack\n\r", data_byte ) );
	}
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
}

uint8_t  read_i2c_byte(wiced_bool_t last_byte)
{
	uint8_t bit_count = 0;
	uint8_t data_byte = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	data_byte = 0;
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		data_byte = data_byte << 1;
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit high\n\r" ) );
			data_byte = data_byte ^ 0x01;
		}
		else
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit low\n\r" ) );
		}
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
	}
	//
	// set SDA to output
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	if (last_byte)
	{
		//
		// nack the slave
		//
		WPRINT_APP_INFO( ( "nack tlc59116\n\r" ) );
		wiced_gpio_output_high( I2C_SDA );
	}
	else
	{
		//
		// ack the slave
		//
		WPRINT_APP_INFO( ( "ack tlc59116\n\r" ) );
		wiced_gpio_output_low( I2C_SDA );
	}
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	WPRINT_APP_INFO( ( "read_i2c_byte returning %02x\n\r", data_byte ) );
	return data_byte;
}


void start_i2c_bus(void)
{
	//
	//start
	//
	WPRINT_APP_INFO( ( "start_i2c_bus START\n\r" ) );
	wiced_gpio_output_low( I2C_SDA );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);

}
void stop_i2c_bus(void)
{
	//
	//stop
	//
	WPRINT_APP_INFO( ( "stop_i2c_bus STOP\n\r" ) );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SDA );

}

void read_i2c_registers_tlc59116(void )
{
    uint8_t buf[50] = {0};

	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// address
	//
	stop_i2c_bus();
	start_i2c_bus();
	//
	//write the i2c address 1 read
	//
	write_i2c_byte2(0xC1);
	//
	// read 32 bytes
	//
	int i = 0;
	for (i=0;i<31;i++)
	{ 
		buf[i] = read_i2c_byte(WICED_FALSE);
	}
	buf[i] = read_i2c_byte(WICED_TRUE);
	for (i=0; i<32;i++)
	{
		WPRINT_APP_INFO(("%02x ", buf[i]));
		if (i==15)
			WPRINT_APP_INFO(("\r\n"));
	}
	WPRINT_APP_INFO(("\r\n"));
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "read_i2c_registers_tlc59116 complete\n\r" ) );
}

void reset_i2c_tlc59116(void )
{
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xD6); // reset byte 1
	write_i2c_byte2(0xA5); // reset byte 1
	write_i2c_byte2(0x5A); // reset byte 1
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "reset_i2c_tlc59116 complete\n\r" ) );
}

void init_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// init tlc59116
	// 
	write_i2c_byte2(0x80);      // 00 MODE1 - defaults - turn on auto-inc
    write_i2c_byte2(0x80);      // 01 MODE2 - defaults no bits set - MODE2REGSET;
	write_i2c_byte2(LEDSETOFF);  // 02 PWM0
	write_i2c_byte2(LEDSETOFF);  // 03 PWM1
	write_i2c_byte2(LEDSETOFF);  // 04 PWM2
	write_i2c_byte2(LEDSETOFF);  // 05 PWM3
	write_i2c_byte2(LEDSETOFF);  // 06 PWM4
	write_i2c_byte2(LEDSETOFF);  // 07 PWM5
	write_i2c_byte2(LEDSETOFF);  // 08 PWM6
	write_i2c_byte2(LEDSETOFF);  // 09 PWM7
	write_i2c_byte2(LEDSETOFF);  // 0A PWM8
	write_i2c_byte2(LEDSETOFF);  // 0B PWM9
	write_i2c_byte2(LEDSETOFF);  // 0C PWM10
	write_i2c_byte2(LEDSETOFF);  // 0D PWM11
	write_i2c_byte2(LEDSETOFF);  // 0E PWM12
	write_i2c_byte2(LEDSETOFF);  // 0F PWM13
	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(LEDSETOFF);  // 11 PWM15
	write_i2c_byte2(GRPPWMSET); // 12 GRPPWM;
	write_i2c_byte2(0x00);      // 13 GRPFREQ
	write_i2c_byte2(0xAA);      // 14 LEDOUT0
	write_i2c_byte2(0xAA);      // 14 LEDOUT1
	write_i2c_byte2(0xAA);      // 14 LEDOUT2
	write_i2c_byte2(0xAA);      // 14 LEDOUT3
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 complete\n\r" ) );
}

void set_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0xA2); // address 2 only PWM register write
	//
	// init tlc59116
	// 
	write_i2c_byte2(LEDSETON);  // 02 PWM0
	write_i2c_byte2(LEDSETON);  // 03 PWM1
	write_i2c_byte2(LEDSETON);  // 04 PWM2
	write_i2c_byte2(LEDSETON);  // 05 PWM3
	write_i2c_byte2(LEDSETON);  // 06 PWM4
	write_i2c_byte2(LEDSETON);  // 07 PWM5
	write_i2c_byte2(LEDSETON);  // 08 PWM6
	write_i2c_byte2(LEDSETON);  // 09 PWM7
	write_i2c_byte2(LEDSETON);  // 0A PWM8
	write_i2c_byte2(LEDSETON);  // 0B PWM9
	write_i2c_byte2(LEDSETON);  // 0C PWM10
	write_i2c_byte2(LEDSETON);  // 0D PWM11
	write_i2c_byte2(LEDSETON);  // 0E PWM12
	write_i2c_byte2(LEDSETON);  // 0F PWM13
	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(LEDSETOFF);  // 11 PWM15
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 complete\n\r" ) );
}



//****************************MAIN APPLICATION*************************************

void application_start( )
{

    /* Initialize the WICED device */
    wiced_init();   //Initialize the WICED device.
    wiced_gpio_output_high( WICED_LED1 );
    led_On = WICED_TRUE;
    wiced_gpio_init(WICED_GPIO_8, OUTPUT_PUSH_PULL);
    wiced_gpio_init(WICED_GPIO_26, OUTPUT_PUSH_PULL);
    wiced_gpio_init(WICED_BUTTON5, INPUT_HIGH_IMPEDANCE);
    wiced_gpio_init(WICED_BUTTON3, INPUT_HIGH_IMPEDANCE);
    //INPUT_PULL_UP

    LED_INIT();     //Set registers in LED DRIVER. Turn all LEDs OFF
    wiced_adc_init( WICED_ADC_1, 5 );
    wiced_adc_init( WICED_ADC_2, 5 );
    wiced_adc_init( WICED_ADC_3, 5 );
    wiced_adc_init( WICED_ADC_4, 5 );

    /* Setup button interrupts */

   // wiced_gpio_input_irq_enable(WICED_BUTTON5, IRQ_TRIGGER_FALLING_EDGE, Silence_Buzzer, NULL);  //NOT able to get this one working
    wiced_gpio_input_irq_enable(WICED_BUTTON2, IRQ_TRIGGER_FALLING_EDGE, check_float, NULL);
    wiced_gpio_input_irq_enable(WICED_BUTTON3, IRQ_TRIGGER_FALLING_EDGE, high_water_check, NULL);
    //wiced_gpio_input_irq_enable(WICED_BUTTON4, IRQ_TRIGGER_FALLING_EDGE, Provision, NULL);
    check_bat();

    Charger_OFF();
    LED_GREEN(); //initial state all green LEDs
    wiced_rtos_delay_milliseconds(1000);
    LED_INIT();     //Set registers in LED DRIVER. Turn all LEDs OFF
    wiced_rtos_delay_milliseconds(1000);
    LED_RED();
    wiced_rtos_delay_milliseconds(2000);
   // LED_INIT();     //Set registers in LED DRIVER. Turn all LEDs OFF
    LED_YELLOW();
    wiced_rtos_init_timer(&timerHandle, TIMER_TIME, tictoc, NULL);
    wiced_rtos_start_timer(&timerHandle);
    pump_Active = WICED_FALSE;
    pump_Cooldown = WICED_FALSE;

    WPRINT_PLATFORM_INFO(("changing scl sda to gpio\r\n"));
    platform_init_scl_sda_gpio();

	read_i2c_registers_tlc59116();

    while ( 1 )
    {
/*
       // if(STATE[2] == 0x000)
       // wiced_rtos_delay_milliseconds(2000);//trying to slow things down a bit.ADC read points need to dissipate on the board
        //clearTerminal();
        loop_Info();
        if(!pump_Cooldown && !pump_Active)
        {
            check_bat();    //First determine if battery is properly connected.
        }
        //Provision();    //Second Check Provisioning button
        //WPRINT_APP_INFO(( "I'm working!!!!\r\n" ));
        check_ac();//if state[2] == 0000; then check
        check_ac_Current();
        Silence_Buzzer();

        if(STATE[3] == 0x111) //High water signaled pump to run.
        {
            pump_Active = WICED_TRUE;
            pump_Cooldown = WICED_TRUE;

            High_Water_Pump();
            WPRINT_APP_INFO(("HW ACTIVE\r\n\r\n"));
        }
        if(STATE[4] == 0x111) //Pump float signaled pump to run.
        {
            pump_Active = WICED_TRUE;
            pump_Cooldown = WICED_TRUE;

            Float_Pump();
            WPRINT_APP_INFO(("FLOAT ACTIVE\r\n\r\n"));
        }

        button2_pressed = wiced_gpio_input_get( WICED_BUTTON2 ) ? WICED_FALSE : WICED_TRUE; //Check if float is still high
        button3_pressed = wiced_gpio_input_get( WICED_BUTTON3 ) ? WICED_FALSE : WICED_TRUE; //Check if float is still high
        button4_pressed = wiced_gpio_input_get( WICED_BUTTON4 ) ? WICED_FALSE : WICED_TRUE; //Check if Provision Button is pressed
        if(button4_pressed)
            Provision();
        if(button2_pressed == WICED_TRUE || button3_pressed == WICED_TRUE)
        {
            Counter = 0;  //If either float is still high keep Counter at 0.
        }
        if(Counter > 10)
        {
            if(signalFloat == 0)
            {
                Pump_OFF();  //10 Seconds is up. Turn pump off.
                pump_Active = WICED_FALSE;
            }
            else
            {
                H_Water_OFF();  //10 Seconds is up. Turn pump off.
                pump_Active = WICED_FALSE;
                signalFloat = 0;
            }

        }
        if(pump_Active)
        {
            cooldownTimer = 0;
        }
        if(pump_Cooldown && !pump_Active)
        {
            WPRINT_APP_INFO(("COOLDOWN TIME: %d/10\r\n", cooldownTimer));
            check_Pump_CooldownTimer();

        }
        auto_Self_Test_Timer();
        if(!pump_Cooldown && !pump_Active && selfTestFlag)
        {
            auto_Self_Test();
        }
        clearTerminal();
    */
    }
}


