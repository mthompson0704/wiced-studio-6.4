
#include <stdlib.h>
#include "wiced.h"
#include "wiced_framework.h"
#include "wiced_ota_server.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "control_leds.h"
#include "get_deviceid.h"

static const wiced_ip_setting_t device_init_ip_settings =
{
	    INITIALISER_IPV4_ADDRESS( .ip_address, MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
	    INITIALISER_IPV4_ADDRESS( .netmask,    MAKE_IPV4_ADDRESS( 255,255,255,  0 ) ),
	    INITIALISER_IPV4_ADDRESS( .gateway,    MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
};

static const configuration_entry_t app_config[] =
{
    {"NewConfigurationFlag", DCT_OFFSET(config_mode_app_dct_t, NewConfigurationFlag), 4, CONFIG_UINT32_DATA },
    {"DeviceID", DCT_OFFSET(config_mode_app_dct_t, DeviceID), 8, CONFIG_UINT32_DATA },
    {"PumpSpyHostName", DCT_OFFSET(config_mode_app_dct_t, PumpSpyHostName), 100, CONFIG_STRING_DATA },
    {"PumpSpyHostName", DCT_OFFSET(config_mode_app_dct_t, PumpSpyHostPort), 2, CONFIG_UINT16_DATA },
    {"startup_delay", DCT_OFFSET(config_mode_app_dct_t, startup_delay), 2, CONFIG_UINT16_DATA },
    {0,0,0,0}
};

extern wiced_result_t pumpspy_configure_device( const configuration_entry_t* config );

void run_provisioning_ap()
{
	WPRINT_APP_INFO(("Provisioning Mode \r\n"));
	//
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// LEDs - red / geeen
	//
	control_leds(LED_RED_GREEN);
	configure_app_dct();
	configure_network_dct();
	configure_wifi_dct();

	//
	// run the config AP
	//
	WPRINT_APP_INFO(("turning on config AP \r\n"));
	//            result = wiced_configure_device( app_config );
	result = pumpspy_configure_device( app_config );
//	result = wiced_configure_device(app_config);
	if ( result != WICED_SUCCESS )
	{
		WPRINT_APP_INFO(( "Unable start config AP server. Error = [%d]\r\n", result ));
		if (result == WICED_OUT_OF_HEAP_SPACE)
			WPRINT_APP_INFO(( "Out of heap space.\r\n"));
		if (result == WICED_BADARG)
			WPRINT_APP_INFO(( "bad arg.\r\n"));
	   return;
	}
	read_app_dct();
	WPRINT_APP_INFO(("wiced_configure_device = %d\r\n", result));
	//
	// get deviceid
	//
	deviceid = get_deviceid();
	//
	// set the flag, and device id and write to DCT
	//
	dct.NewConfigurationFlag = 0x0A0B0C0D; //0xFFFFFFFF;
	dct.DeviceID = deviceid;
	write_app_dct();
	//
	// reboot
	//
	wiced_framework_reboot();
}

void run_ota_ap()
{
	//
	//
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// reset config flag
	//
	dct.NewConfigurationFlag = 0x0A0B0C0D;
	write_app_dct();
	//
	// turn off STA
	//
	WPRINT_APP_INFO(( "bring down  WCED_STA_INTERFACE\n" ));
	result = wiced_network_down(WICED_STA_INTERFACE);
	if ( result != WICED_SUCCESS )
	{
		WPRINT_APP_INFO(( "Unable bring down WICED_STA_INTERFACE. Error = [%d]\r\n", result ));
	}
	WPRINT_APP_INFO(( "Start WCED_AP_INTERFACE\n" ));
	wiced_network_up( WICED_AP_INTERFACE, WICED_USE_INTERNAL_DHCP_SERVER, &device_init_ip_settings );
	if ( result != WICED_SUCCESS )
	{
		WPRINT_APP_INFO(( "Unable bring down WICED_AP_INTERFACE. Error = [%d]\r\n", result ));
	}
	wiced_ota_server_start( WICED_AP_INTERFACE );
	while ( 1 )
	{
		wiced_rtos_delay_milliseconds( 100 );
	}
	//
	// reboot
	//
	WPRINT_APP_INFO(( "rebooting \n" ));
	wiced_framework_reboot();
}
