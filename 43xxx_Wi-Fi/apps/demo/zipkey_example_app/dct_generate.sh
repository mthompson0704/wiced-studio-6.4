#!/bin/sh

#
# Copyright (C) 2018, Cirrent Inc
#
# All use of this software must be covered by a license agreement signed by Cirrent Inc.
#
# DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
# ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE.
#

#
# This is the script which generates a DCT data source file for specific device credentials
#

if [ "$#" -ne 3 ] && ( [ "$#" -ne 4 ] || [ "$4" != "-t" ] ); then
    cat <<EOF >&2
Script generates initial zipkey configuration in DCT format
Usage: $0 <ACC ID> <DEVICE ID> <SECRET> [-t]
Options:
  -t = the credentials are temporary
EOF
    exit 1
fi

CLOUD_URL_BASE=https://dev.cirrentsystems.com/2016-01/config/0

ACC_ID=$1
DEV_ID=$2
SECRET=$3

if [ -n "$4" ]; then
    IS_TEMPORARY=1
else
    IS_TEMPORARY=0
fi

secret_length=${#SECRET}
API_KEY="$(expr substr $SECRET $((secret_length / 4 + 1))  $((secret_length - 2 * (secret_length / 4))))"

DCT_BLOB=$(curl -f -s -H 'x-response-type: binary' --user ${ACC_ID}_${DEV_ID}:${API_KEY} -X GET ${CLOUD_URL_BASE} | xxd -i)
if [ -z "$DCT_BLOB" ]; then
    echo "Cloud connection error" >&2
    exit 1
fi

DCT_FORMATTED_BLOB=''
for line in $DCT_BLOB; do
    DCT_FORMATTED_BLOB="${DCT_FORMATTED_BLOB}
          $line"
done

cat <<EOF
#include <wiced_framework.h>
#include "zipkey_app_dct.h"

DEFINE_APP_DCT(APP_DCT_T)
{
    .ca_config =
    {
        .internal_data =
        {${DCT_FORMATTED_BLOB}
        },
        .credentials =
        {
            .dev_id = "$DEV_ID",
            .acc_id = "$ACC_ID",
            .secret = "$SECRET",
            .is_temporary = 0x0$IS_TEMPORARY,
        }
    },
};
EOF
