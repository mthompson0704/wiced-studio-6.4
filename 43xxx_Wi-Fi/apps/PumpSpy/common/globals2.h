
#define POST_authorization_token \
    "POST /oauth/token HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Content-Type: application/x-www-form-urlencoded;charset=UTF-8 \r\n" \
	"Authorization: Basic SU9TOnNlY3JldA== \r\n" \
    "Content-Length: 70 \r\n" \
    "\r\n" \
	"grant_type=password&username=mthompson9414@wowway.com&password=MickL3o" \
    "\r\n"

#define GET_tm \
    "GET /tm HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

#define GET_new_firmware \
    "GET /new_firmware/%ld%ld HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

#define GET_new_firmware_download_block \
    "GET /new_firmware/%ld%ld/download/%ld HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

#define POST_STRING \
    "POST  %s HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Accept: */* \r\n"\
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "\r\n" \
    "%s" \
    "\r\n"

#define PUT_STRING \
    "PUT %s HTTP/1.1\r\n" \
    "Host: \r\n" \
    "Accept: */* \r\n"\
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "\r\n" \
    "%s" \
    "\r\n"

#define JSON_insertPumpOutletData \
    "[{\"deviceID\": %ld%ld, " \
    "\"recordNumber\": %ld, " \
    "\"utcunixTime\": %ld000," \
    "\"cycleDuration\": %d, " \
    "\"cycleCurrent\": %d }]"

#define JSON_insertPumpOutletAlerts \
    "[{\"idPumpAlertType\": %d , " \
    "\"deviceID\": %ld%ld, " \
    "\"recordNumber\": %ld, " \
    "\"utcunixTime\": %ld000," \
    "\"value\": %d }]" \
    "\r\n"

#define JSON_post_pings \
    "[{\"deviceid\": %ld%ld, " \
    "\"utcunixtime\": %ld000," \
    "\"idpings_data_type\": %d, " \
    "\"value\": %f }]"

#define JSON_post_new_firmware \
    "[{\"deviceid\": %ld%ld, " \
    "\"new_firmware_downloaded\": %d," \
    "\"new_firmware_download_date\": %ld000, " \
    "\"new_firmware_download_status_id\": %d }]"

#define PING_TIMER_LIMIT 120000
#define SET_CLOCK_TIMER_LIMIT 14400000
#define DNS_TIMER_LIMIT 300000
#define PUMP_RUN_TIME_LIMIT 60000
#define HIGH_WATER_DEBOUNCE_TIME 5000
#define NEW_FIRMWARE_TIMER_LIMIT 120000
#define RESET_24HR_TIME 86400000

#define BUFFER_LENGTH     (2048)
#define POST_SIZE 500
#define JSON_SIZE 500

#define LED_OFF 0
#define LED_RED 1
#define LED_GREEN 2
#define LED_RED_GREEN 3

#define HIGH_WATER_ALERT 1
#define AC_LOSS_ALERT 2
#define HIGH_CURRENT_ALERT 3
#define EXCESSIVE_CYCLE_DURATION_ALERT 4

#define PINGS_RSSI 1
#define PINGS_BaseCurrent 2
#define PINGS_WakeUp 3
#define PINGS_DailyReset 4

#define STATE_RUNNING 0
#define STATE_PROVISIONING 1
#define STATE_OTA 2
#define STATE_INITIALIZING 3
#define STATE_TEST_MODE 4

#define DOWNLOAD_STARTED 1
#define COMPLETE_AND_REBOOTING 2
#define DOWNLOAD_FAILED 3
#define DOWNLOAD_HALTED 4
#define DOWNLOAD_CORRUPT 5
#define FLASH_MEMORY_ERROR 6

#define MAXIMUM_ALLOWED_INTERVAL_BETWEEN_MONITOR_UPDATES (20000*MILLISECONDS)

extern int test_integer;

/*
extern const wiced_ip_setting_t device_init_ip_settings =
{
	    INITIALISER_IPV4_ADDRESS( .ip_address, MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
	    INITIALISER_IPV4_ADDRESS( .netmask,    MAKE_IPV4_ADDRESS( 255,255,255,  0 ) ),
	    INITIALISER_IPV4_ADDRESS( .gateway,    MAKE_IPV4_ADDRESS( 10,10,  115,  1 ) ),
};
*/

/*
extern static char bearer_token[];
extern char post[POST_SIZE];
extern char json[JSON_SIZE];
extern uint8_t buffer[BUFFER_LENGTH];
extern uint32_t new_firmware_size_in_bytes;
extern uint32_t new_firmware_size_in_blocks;
extern uint32_t new_firmware_block_size;
extern uint8_t new_firmware_block[256];
extern wiced_app_t app;
extern wiced_bool_t new_firmware_approved;
extern wiced_bool_t new_firmware_downloaded;
extern wiced_bool_t new_firmware_available;
extern wiced_ip_address_t ip_address;
extern wiced_result_t result;
extern wiced_bool_t high_water_detected;
extern wiced_bool_t config_mode;
extern wiced_bool_t ac_loss_detected;
extern uint64_t deviceid;
extern uint8_t system_state;

extern uint64_t pump_start_time;
extern uint64_t pump_end_time;
extern uint64_t pump_run_time;
extern uint16_t pump_run_current;
extern uint16_t pump_peak_current;
extern wiced_bool_t config_button_passed;
extern int main_thread_counter; // 5 * 12 = 60 seconds system monitor countdown
extern wiced_bool_t main_thread_counter_enabled;
//
//  DCT
//
extern config_mode_app_dct_t dct;
extern config_mode_app_dct_t *dct_ptr;
extern platform_dct_wifi_config_t dct_wifi_config;
extern platform_dct_wifi_config_t *dct_wifi_config_ptr;
extern platform_dct_network_config_t dct_network_config;
extern platform_dct_network_config_t *dct_network_config_ptr;
*/
