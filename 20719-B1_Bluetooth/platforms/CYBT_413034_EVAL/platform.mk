CHIP                       	    := 20719
CHIP_REV                   	    := B1
BASE_LOC                   	    ?= rom
SPAR_LOC                   	    ?= ram
TOOLCHAIN                  	    ?= Wiced
CONFIG_DEFINITION          	    =
PLATFORM_NV                	    ?= OCF
PLATFORM_APP_SPECIFIC_STATIC_DATA ?= 0x500800
PLATFORM_APP_SPECIFIC_STATIC_LEN  ?= 2048
PLATFORM_DIRECT_LOAD_BASE_ADDR 	:= 0x20A000
PLATFORM_BOOTP             	    := $(CHIP)_$(PLATFORM_NV).btp
PLATFORM_MINIDRIVER        	    := minidriver-20739B1-module-uart.hex
PLATFORM_CONFIGS           	    := CYW207x9B1.cgs
PLATFORM_IDFILE            	    := CYW207x9B1_IDFILE.txt
PLATFORM_BAUDRATE          	    := 115200
PLATFORM_DOWNLOAD_TARGET         = convert_cgs_to_hex
PLATFORM_OTA_TARGET              = create_ota_image
CHIPLOAD_CRC_VERIFY             := -CHECKCRC
CHIPLOAD_NO_READ_VERIFY         := -NOVERIFY
PLATFORM_APP_SPECIFIC_XIP_LEN  ?= 0x32000

export PLATFORM_APP_SPECIFIC_STATIC_DATA
export PLATFORM_APP_SPECIFIC_STATIC_LEN
export PLATFORM_APP_SPECIFIC_XIP_LEN
export PLATFORM_BOOTP

C_FLAGS += -DCYW$(CHIP)$(CHIP_REV)=1
C_FLAGS += -DCHIP=$(CHIP)

$(PLATFORM_DOWNLOAD_TARGET):
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)WICED/internal/$(CHIP)$(CHIP_REV)/*.hdf build/$(OUTPUT_NAME)/
ifneq ($(APP_STATIC_DATA),)
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM_FULL) $(BT_DEVICE_ADDRESS_OVERRIDE) -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-temp.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log)
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-app-static.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd
else ifneq ($(APP_XIP),)
	$(eval IXIP_END_CMD ?= $(PERL) -nle 'printf( "0x%08X", hex($$$$1) )  if m/(0x[[:alnum:]]+)([[:space:]]+)xip_area_end[[:space:]]=[[:space:]]./'                                                                  build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.list)
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM_FULL) -O ConfigDSLocation:$(call CONV_SLASHES,$(shell $(IXIP_END_CMD)))  $(BT_DEVICE_ADDRESS_OVERRIDE) -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-temp.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log)
	$(QUIET)$(call CONV_SLASHES,$(MERGE_INTEL_HEX_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-temp.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME)-app-xip.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd
else
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM_FULL) $(BT_DEVICE_ADDRESS_OVERRIDE) -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -H build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log)
endif


$(PLATFORM_OTA_TARGET):
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Creating OTA images...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)WICED/internal/$(CHIP)$(CHIP_REV)/*.hdf build/$(OUTPUT_NAME)/
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM_FULL) -O DLConfigFixedHeader:0 -O ConfigDSLocation:0 -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log)
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hcd
	$(QUIET)$(call CONV_SLASHES,$(PERL)) -e '$$s=-s "build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin";printf "OTA image footprint in NV is %d bytes",$$s;'
	$(QUIET)$(ECHO_BLANK_LINE)

download_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(eval CHIPLOAD_BAUDRATE:=$(shell $(CAT) < baud_rate.txt))
	$(QUIET)$(eval UART:=$(shell $(CAT) < com_port.txt))
ifeq ($(VERBOSE),1)
	$(QUIET)$(ECHO) UART: $(UART)  CHIPLOAD_BAUDRATE:  $(CHIPLOAD_BAUDRATE)  CHIPLOAD_SKIP_RESET $(CHIPLOAD_SKIP_RESET)
endif
	$(QUIET)$(if $(UART), \
        $(ECHO) Downloading application... && $(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE -PORT $(UART) -BAUDRATE $(CHIPLOAD_BAUDRATE) $(CHIPLOAD_SKIP_RESET) $(CHIPLOAD_REBAUD) $(CHIPLOAD_NO_READ_VERIFY) $(CHIPLOAD_CRC_VERIFY) -MINIDRIVER $(SOURCE_ROOT)platforms/$(PLATFORM_MINIDRIVER) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -LOGTO build/$(OUTPUT_NAME)/logto.log > build/$(OUTPUT_NAME)/download.log 2>&1 \
                        && $(ECHO) Download complete && $(ECHO_BLANK_LINE) && $(ECHO) $(QUOTES_FOR_ECHO)Application running.$(QUOTES_FOR_ECHO) \
			|| ($(call CONV_SLASHES,$(PERL)) $(TOOLS_ROOT)/print_file/print_file.pl  $(SOURCE_ROOT)platforms/$(PLATFORM)/download_error.txt && \
				$(ECHO) Download failed. This WICED platform of the SDK only supports download to 20719B1 device.))
