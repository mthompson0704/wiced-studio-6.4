
#include <stdlib.h>
#include "wiced.h"
#include "wiced_framework.h"
#include "wiced_management.h"
#include "config_mode_dct.h"
#include "globals.h"


char bearer_token[40];
char post[POST_SIZE];
char json[JSON_SIZE];
uint8_t buffer[BUFFER_LENGTH];
uint32_t new_firmware_size_in_bytes;
uint32_t new_firmware_size_in_blocks;
uint32_t new_firmware_block_size;
uint8_t new_firmware_block[256];
wiced_app_t app;
wiced_bool_t new_firmware_approved = WICED_FALSE;
wiced_bool_t new_firmware_downloaded = WICED_FALSE;
wiced_bool_t new_firmware_available = WICED_FALSE;
wiced_ip_address_t ip_address;
wiced_result_t result;
wiced_bool_t high_water_detected = WICED_FALSE;
wiced_bool_t config_mode = WICED_FALSE;
wiced_bool_t ac_loss_detected = WICED_FALSE;
uint64_t deviceid;
uint8_t system_state = STATE_INITIALIZING;

uint64_t pump_start_time = 0;
uint64_t pump_end_time = 1;
uint64_t pump_run_time = 0;
uint16_t pump_run_current = 0;
uint16_t pump_peak_current = 0;
wiced_bool_t config_button_passed = WICED_FALSE;
int main_thread_counter = 5; // 5 * 12 = 60 seconds system monitor countdown
wiced_bool_t main_thread_counter_enabled = WICED_TRUE;

//    wiced_utc_time_t server_time;
uint64_t server_time;
//    uint64_t config_time;
uint64_t ping_timer = 0;
uint64_t set_clock_timer = 0;
uint64_t high_water_timer = 0;
uint64_t new_firmware_timer = 0;
uint64_t new_parameters_timer = 0;
uint64_t reset_24hr_timer = 0;

uint32_t current_time = 0;
uint32_t last_time = 0;

wiced_bool_t wifi_error_condition = WICED_FALSE;
uint8_t wifi_error_counter = 0;

uint32_t new_firmware_block_number = 0;
wiced_bool_t new_firmware_download_in_progress = WICED_FALSE;
uint8_t new_firmware_donwload_block_error_count = 0;

//
//  DCT
//
config_mode_app_dct_t dct;
config_mode_app_dct_t *dct_ptr;
platform_dct_wifi_config_t dct_wifi_config;
platform_dct_wifi_config_t *dct_wifi_config_ptr;
platform_dct_network_config_t dct_network_config;
platform_dct_network_config_t *dct_network_config_ptr;


