/*
 * Copyright 2015, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 */

#include "config_mode_dct.h"
#include "wiced_framework.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

DEFINE_APP_DCT(config_mode_app_dct_t)
{
//    .NewConfigurationFlag = 0x0F0F0F0F,
    .NewConfigurationFlag = 0x0B0B0B0B,
    .DeviceID = 0xFFFFFFFFFFFFFFFF, //will be set to Wi-Fi MAC address
    .PumpSpyHostName = "www.pumpspy.com",
    .PumpSpyHostPort = 8081,
    .startup_delay = 0,
    .default_primary_pump_current_limit = -1,
    .default_motor_current_limit = -1,
    .main_pump_init_index = 0,
    .main_pump_baseline_array = {0},
    .backup_pump_init_index = 0,
    .backup_pump_baseline_array = {0}
};

/******************************************************
 *               Function Definitions
 ******************************************************/
