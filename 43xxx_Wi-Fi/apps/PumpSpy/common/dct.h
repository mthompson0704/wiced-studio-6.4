
extern void print_app_dct();
extern void read_app_dct();
extern void write_app_dct();
extern void configure_app_dct();
extern void print_wifi_dct();
extern void read_wifi_dct();
extern void write_wifi_dct();
extern void configure_wifi_dct();
extern wiced_bool_t check_wifi_dct();
extern void print_network_dct();
extern void read_network_dct();
extern void write_network_dct();
extern void configure_network_dct();
