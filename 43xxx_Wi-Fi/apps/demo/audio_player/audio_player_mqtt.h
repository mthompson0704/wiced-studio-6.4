/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif


#ifdef AUDIO_PLAYER_ENABLE_MQTT
#include "mqtt_api.h"
#endif

#define AUDIO_PLAYER_MQTT_BROKER_IPV4_DEFAULT      MAKE_IPV4_ADDRESS(192, 168, 1, 1) /* default */
#define AUDIO_PLAYER_MQTT_USE_ENCRYPTION           0
#define AUDIO_PLAYER_MQTTS_PASSPHRASE              ""

/* audio player MQTT topic uses the structure AREA/GROUP/DEVICE */
#define AUDIO_PLAYER_MQTT_TOPIC_AREA_DEFAULT       "area1"                           /* default */
#define AUDIO_PLAYER_MQTT_TOPIC_GROUP_DEFAULT      "group1"                          /* default */
#define AUDIO_PLAYER_MQTT_TOPIC_DEVICE_DEFAULT     "player1"                         /* default */
#define AUDIO_PLAYER_MQTT_TOPIC_SECTION_LEN        (128)
#define AUDIO_PLAYER_MQTT_TOPIC_MAX_LEN            (AUDIO_PLAYER_MQTT_TOPIC_SECTION_LEN*3+8)
#define AUDIO_PLAYER_MQTT_QOS                      ((uint8_t)(0))

/******************************************************
 *                     Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    wiced_ip_address_t  broker_ip;
    char*               topic_area;
    char*               topic_group;
    char*               topic_device;
    uint8_t             qos;
} audio_player_mqtt_params_t;

typedef struct
{
#ifdef AUDIO_PLAYER_ENABLE_MQTT
    wiced_mqtt_callback_t           mqtt_cb;
    wiced_mqtt_object_t             mqtt_obj;
#endif

    volatile uint8_t                is_connected;

} audio_player_mqtt_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

int audio_player_mqtt_start( audio_player_mqtt_t** mqtt, audio_player_mqtt_params_t* mqtt_params);

int audio_player_mqtt_stop(audio_player_mqtt_t** mqtt);


#ifdef __cplusplus
} /* extern "C" */
#endif
