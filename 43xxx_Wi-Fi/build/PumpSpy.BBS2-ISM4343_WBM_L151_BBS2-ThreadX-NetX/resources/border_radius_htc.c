#include "wiced_resource.h"

const char resources_styles_DIR_border_radius_htc_data[215] = ".curved {\r\n" \
"    -moz-border-radius:.5em;    /* Firefox */\r\n" \
"    -webkit-border-radius:.5em; /* Safari and chrome */\r\n" \
"    -khtml-border-radius:.5em;  /* Linux browsers */\r\n" \
"    border-radius:.5em;         /* CSS3 */\r\n" \
"}";
const resource_hnd_t resources_styles_DIR_border_radius_htc = { RESOURCE_IN_MEMORY, 214, { .mem = { resources_styles_DIR_border_radius_htc_data }}};
