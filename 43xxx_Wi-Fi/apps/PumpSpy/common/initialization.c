/*
 * initialization.c
 *
 *  Created on: Oct 20, 2018
 *      Author: mthom
 */

#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "config_button_handler.h"
#include "control_leds.h"
#include "get_deviceid.h"
#include "dns_lookup.h"
#include "restful_methods.h"

void random_delay()
{
	uint32_t dly = 0;
	//
	// configure the delay
	//

	if (dct.startup_delay != 0)
		srand(dct.startup_delay);
	uint32_t rnd = rand();
	dct.startup_delay = rnd;
	write_app_dct();
	//
	// delay based on dct.startup_delay
	//
	dly = (dct.startup_delay*5000)/0xFFFF;
	WPRINT_APP_INFO(("Delay_Starup %d delay_time %lu\r\n", dct.startup_delay, (unsigned long int)dly  ));
	wiced_rtos_delay_milliseconds( dly);
}

void run_common_startup_sequence()
{
	//
	// initialize the config button IRQ's
	// eval board buttons are normally high
	//
	//    result = wiced_gpio_input_irq_enable( HIGH_WATER, IRQ_TRIGGER_FALLING_EDGE, high_water_handler, NULL );
	//    WPRINT_APP_INFO( ( "high water irq FE %d\r\n", result ) );
	result = wiced_gpio_input_irq_enable( CONFIG_BUTTON, IRQ_TRIGGER_FALLING_EDGE, config_button_handler, NULL);
	WPRINT_APP_INFO( ( "config irq falling enable %d\r\n", result ) );
	//
	// Init the watch dog
	//
	result = platform_watchdog_init();
	WPRINT_APP_INFO( ( "watchdog init result %d\r\n", result ) );
	result = wiced_watchdog_kick();
	WPRINT_APP_INFO( ( "watchdog kick result %d\r\n", result ) );

	/*
	// display the external flash app segments
	//
	#define DCT_FR_APP_INDEX            ( 0 )
	#define DCT_DCT_IMAGE_INDEX         ( 1 )
	#define DCT_OTA_APP_INDEX           ( 2 )
	#define DCT_FILESYSTEM_IMAGE_INDEX  ( 3 )
	#define DCT_WIFI_FIRMWARE_INDEX     ( 4 )
	#define DCT_APP0_INDEX              ( 5 )
	#define DCT_APP1_INDEX              ( 6 )
	#define DCT_APP2_INDEX              ( 7 )
	*/

	wiced_app_t app2;
	uint32_t    current_size;
	int app_index;
	for (app_index = DCT_FR_APP_INDEX; app_index <= DCT_APP2_INDEX; app_index++)
	{
		if (wiced_framework_app_open( app_index, &app2 ) != WICED_SUCCESS)
		{
			printf("app [%d] open error\r\n", app_index);
		}
		if (wiced_framework_app_get_size( &app2, &current_size ) != WICED_SUCCESS)
		{
			printf("app [%d] get sized error\r\n", app_index);
		}
		printf("app [%d] size %ld\r\n", app_index, current_size);
		wiced_framework_app_close( &app2 );
	}


	//
	// LEDs - red
	//
	control_leds(LED_RED);
	//
	// get the deviceid from the mac address
	//
	deviceid = get_deviceid();
	//
	// read the dct's
	//
	read_app_dct();
	read_network_dct();
	read_wifi_dct();
	print_app_dct();
	print_wifi_dct();
	print_network_dct();
	//
	// Check the new configuration flag
	//
	WPRINT_APP_INFO(("New config flag %08lx\r\n", dct.NewConfigurationFlag));
	if (dct.NewConfigurationFlag == 0x0F0F0F0F)
	{
		WPRINT_APP_INFO(("New Configuration \r\n"));
		system_state = STATE_PROVISIONING;
	}
	else if (dct.NewConfigurationFlag == 0x0D0C0B0A)
	{
		WPRINT_APP_INFO(("OTA Configuration \r\n"));
		system_state = STATE_OTA;
	}
	else if (dct.NewConfigurationFlag == 0x0B0B0B0B)
	{
		WPRINT_APP_INFO(("TEST_MODE Configuration \r\n"));
		system_state = STATE_TEST_MODE;
	}
	else if (dct.NewConfigurationFlag == 0x0A0B0C0D)
		{
			WPRINT_APP_INFO(("STATE_INITIALIZING Configuration \r\n"));
			system_state = STATE_INITIALIZING;
		}
	else
	{
		dct.NewConfigurationFlag = 0x0B0B0B0B;
		write_app_dct();
		//
		// reset the DCT
		//1
		configure_app_dct();
		configure_network_dct();
		configure_wifi_dct();
		WPRINT_APP_INFO(("TEST_MODE Configuration \r\n"));
		system_state = STATE_TEST_MODE;

	}
	#ifdef REMOTEDEBUG
	//
	// *** test mode
	//	dct.NewConfigurationFlag = 0x0D0C0B0A; // OTA
	dct.NewConfigurationFlag = 0x0A0B0C0D; // Initializing
	write_app_dct();
	configure_app_dct();
	configure_network_dct();
	configure_wifi_dct();
	WPRINT_APP_INFO(("Initialization Configuration \r\n"));
	//	system_state = STATE_OTA;
	system_state = STATE_INITIALIZING;
	//
	// *** test mode
	#endif
	//
	// set up timers for the main thread counter
	//
	current_time = host_rtos_get_time();
	last_time = host_rtos_get_time();
	//
	// this is the beginning of application
	//
}

wiced_result_t run_common_initialization_sequence(float firmware_version)
{
	//
	// turn on system monitor
	//
	main_thread_counter_enabled = WICED_TRUE;
	//
	// 1) join the AP
	// 2) dns lookup
	// 3) bearer token 
	// 4) if all of those are successful then get time server 
	//   try a bumch of times
	//   if they all fail, then disconnect STA, then jump out 
	// Fall back to trying to join on the ping timer 

	//
	// Bring up the STA interface
	// for 5 times
	// start the STA
	// if result == success then move on
	// else
	// delay_randomly
	// counter ++
	// if counter >= 5 then reboot
	//
	system_state = STATE_RUNNING;
	wifi_error_condition = WICED_FALSE;
	WPRINT_APP_INFO(("brining up the STA interface\r\n"));
	control_leds(LED_RED);
	result = -1;
	result = wiced_network_up(WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL);
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("STA init ERROR result = %d\r\n", result));
		wifi_error_condition = WICED_TRUE;
#ifndef BBS2_SYSTEM
		//
		// randomly delay
		//
		random_delay();
		system_state = STATE_INITIALIZING;
		//
		// reboot if counter >= 5
		//
		if (wifi_error_counter++ >= 5)
		{
			wiced_framework_reboot();
		}
#endif
		return result;
	}

	//
	// reset delay_start
	//
	dct.startup_delay = 0;
	write_app_dct();
	//
	// get the deviceid from the mac address
	//
	deviceid = get_deviceid();
	//
	// read and print the app & network dct (don't think need to read wifi) configuration record
	//
	read_app_dct();
	print_app_dct();
	//
	// read and print the app dct configuration record
	//
	read_network_dct();
	print_network_dct();
	//
	// read and print the wifi dct configuration record
	//
	read_wifi_dct();
	print_wifi_dct();
	//
	// look up the DNS
	//
	result = dns_lookup();
	if (result != WICED_SUCCESS)
	{
		wiced_rtos_delay_milliseconds(500);
		result = dns_lookup();
	}
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("dns_lookup ERROR result = %d\r\n", result));
		wifi_error_condition = WICED_TRUE;
#ifndef BBS2_SYSTEM
		//
		// randomly delay
		//
		random_delay();
		system_state = STATE_INITIALIZING;
		//
		// reboot if counter >= 5
		//
		if (wifi_error_counter++ >= 5)
		{
			wiced_framework_reboot();
		}
#endif
		wiced_network_down(WICED_STA_INTERFACE);
		return result;
	}
	//
	// get the bearer token
	//
	result = get_bearer_token();
	if (result != WICED_SUCCESS)
	{
		wiced_rtos_delay_milliseconds(500);
		result = get_bearer_token();
	}
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("get_bearer_token ERROR result = %d\r\n", result));
		wifi_error_condition = WICED_TRUE;
#ifndef BBS2_SYSTEM
		//
		// randomly delay
		//
		random_delay();
		system_state = STATE_INITIALIZING;
		//
		// reboot if counter >= 5
		//
		if (wifi_error_counter++ >= 5)
		{
			wiced_framework_reboot();
		}
#endif
		wiced_network_down(WICED_STA_INTERFACE);
		return result;
	}
	//
	// get and set the time
	//
	result = get_system_time();
	if (result != WICED_SUCCESS)
	{
		wiced_rtos_delay_milliseconds(500);
		result = get_system_time();
	}
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("get_system_time ERROR result = %d\r\n", result));
		wifi_error_condition = WICED_TRUE;
#ifndef BBS2_SYSTEM
		//
		// randomly delay
		//
		random_delay();
		system_state = STATE_INITIALIZING;
		//
		// reboot if counter >= 5
		//
		if (wifi_error_counter++ >= 5)
		{
			wiced_framework_reboot();
		}
#endif
		wiced_network_down(WICED_STA_INTERFACE);
		return result;
	}
	//
	// set the timers
	//
	wiced_time_get_utc_time_ms  (&server_time);
	set_clock_timer = server_time;
	ping_timer = server_time;
	new_firmware_timer = server_time + 30000;
	new_parameters_timer = server_time + 60000;
	reset_24hr_timer = server_time;
	//
	// post the boot ping
	//
	result = post_ping(PINGS_WakeUp, ping_timer, firmware_version);
	WPRINT_APP_INFO(("Initialization Ping result = %d\r\n", result));
	if (result != WICED_SUCCESS)
	{
		wiced_rtos_delay_milliseconds(500);
		result = post_ping(PINGS_WakeUp, ping_timer, firmware_version);
	}
	if (result != WICED_SUCCESS)
	{
		WPRINT_APP_INFO(("post_pint ERROR result = %d\r\n", result));
		wifi_error_condition = WICED_TRUE;
#ifndef BBS2_SYSTEM
		//
		// randomly delay
		//
		random_delay();
		system_state = STATE_INITIALIZING;
		//
		// reboot if counter >= 5
		//
		if (wifi_error_counter++ >= 5)
		{
			wiced_framework_reboot();
		}
		//
		// set system state
		//
		system_state = STATE_INITIALIZING;
#endif
		wiced_network_down(WICED_STA_INTERFACE);
		return result;
	}
	//
	// POST /pump_outlet_alerts
	//
	WPRINT_APP_INFO(("Calling POST /outlet_alerts = \r\n"));
#ifdef OUTLET_SYSTEM
	post_outlet_alerts(AC_LOSS_ALERT, server_time, 0);
#endif
#ifdef BBS_SYSTEM
	post_outlet_alerts(Backup_Pump_Power_Fail, server_time, 0);
#endif
	//
	// set system state
	//
	wifi_error_counter = 0;
	control_leds(LED_GREEN);
	system_state = STATE_RUNNING;
	return WICED_SUCCESS;
}

