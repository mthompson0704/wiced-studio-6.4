
#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"
#include "restful_methods.h"
#include "dns_lookup.h"
#include "initialization.h"

extern wiced_result_t get_parameters();
extern float firmware_version;

void process_ping_timer()
{
	int32_t ap_signal_strength = 0;
	//
	// Ping timer check - 2 minutes
	//
	if ((server_time - ping_timer) > PING_TIMER_LIMIT)
	{
		//
		// POST /pings/baseline
		//

		ping_timer = server_time;
		if (wifi_error_condition == WICED_TRUE)
		{
			if ((memcmp(dct_wifi_config.stored_ap_list[0].details.SSID.value, "PumpSpy", 7) != 0) && (dct_wifi_config.stored_ap_list[0].security_key_length != 0))
				run_common_initialization_sequence(firmware_version);
		}
		if (wifi_error_condition != WICED_TRUE)
		{
		//					adc_baseline = set_baseline_current();
		//					result = post_ping(PINGS_BaseCurrent, ping_timer, (float)adc_baseline);
			ap_signal_strength = 0;
			wwd_wifi_get_rssi(&ap_signal_strength);
			WPRINT_APP_INFO(("rssi %d\r\n", (int)ap_signal_strength));
			result = post_ping(PINGS_RSSI, ping_timer, (float)ap_signal_strength);
			WPRINT_APP_INFO(("/pings result  %d\r\n", result));
			wiced_time_get_utc_time_ms  (&server_time);
			WPRINT_APP_INFO(("/pings POST time %d\r\n", (int)(server_time - ping_timer)));
			if (result != WICED_SUCCESS)
			{
				//
				// reboot - after trying 2x and waiting 2 minutes in between
				//
				WPRINT_APP_INFO(("/pings error_counter  %d\r\n", wifi_error_counter));
				wifi_error_condition = WICED_TRUE;
				if (wifi_error_counter++ >= 2)
					wiced_framework_reboot();
			}
		}
	}
}

void process_get_parameters_timer()
{

	//
	// check for get parameters timer - 2 minutes
	//
	if ((server_time - new_parameters_timer) > NEW_PARAMETERS_TIMER_LIMIT)
	{
		//
		// get parameters
		//
		new_parameters_timer = server_time;
		result = get_parameters();
	}
}

void process_daily_reset_timer()
{
	//
	// check for 24 or 72 hour reset timer
	//
#ifndef BBS2_SYSTEM
	if ((server_time - reset_24hr_timer) > RESET_24HR_TIME)
#else
	if ((server_time - reset_24hr_timer) > RESET_72HR_TIME)
#endif
	{
		result = post_ping(PINGS_DailyReset, ping_timer, (float)0.0);
		wiced_rtos_delay_milliseconds(5000);
		wiced_framework_reboot();
	}
}

void process_clock_timer()
{
	//
	// get_time timer check - 4 hours
	//
	if ((server_time - set_clock_timer) > SET_CLOCK_TIMER_LIMIT)
	{
		//
		// get and set the time
		//
		set_clock_timer = server_time;
		get_system_time();
		//
		// get the bearer token
		//
		get_bearer_token();
		//
		// get dns
		//
		dns_lookup();
	}
}
