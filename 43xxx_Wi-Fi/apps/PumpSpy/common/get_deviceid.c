/*
 * get_devicid.c
 *
 *  Created on: Dec 21, 2017
 *      Author: mthom
 */
#include <stdlib.h>
#include "wiced.h"

//
// Get the MAC address from Inventek Part and convert to decimal
// for the device_id
//
uint64_t get_deviceid()
{
	uint64_t deviceid = 0;
	wiced_mac_t ism_macaddr;

	//
	// get the mac address and make it the device id;
	//
	ism_macaddr.octet[0] = 0xFF;
	ism_macaddr.octet[1] = 0xFF;
	ism_macaddr.octet[2] = 0xFF;
	ism_macaddr.octet[3] = 0xFF;
	ism_macaddr.octet[4] = 0xFF;
	ism_macaddr.octet[5] = 0xFF;
	//
	// get the mac address from the Inventek Modules
	//
	wiced_wifi_get_mac_address( &ism_macaddr ); // retrieve mac address from WiFi Chip
	//
	// output mac address
	//
	WPRINT_APP_INFO(("MAC address: %02X:%02X:%02X:%02X:%02X:%02X\r\r\n",
			ism_macaddr.octet[0],
			ism_macaddr.octet[1],
			ism_macaddr.octet[2],
			ism_macaddr.octet[3],
			ism_macaddr.octet[4],
			ism_macaddr.octet[5]));
	//
	// set device id
	//
	deviceid = ism_macaddr.octet[0];
	deviceid = (deviceid << 8) + ism_macaddr.octet[1];
	deviceid = (deviceid << 8) + ism_macaddr.octet[2];
	deviceid = (deviceid << 8) + ism_macaddr.octet[3];
	deviceid = (deviceid << 8) + ism_macaddr.octet[4];
	deviceid = (deviceid << 8) + ism_macaddr.octet[5];
	WPRINT_APP_INFO(("deviceid: %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
	return deviceid;
}




