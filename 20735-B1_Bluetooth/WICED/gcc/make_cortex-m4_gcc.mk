# This contains the CM4 varient makefile customizations for GCC

# We link with the base elf files
LINK_ELFS 			:= $(addprefix --just-symbols=,$(ELF_LIST))
MAKE_ASSEMBLY       = $(FROMELF)  --disassemble $@ > $(@:.elf=.asm)
################################################################################
# General link flags
################################################################################
# Disable exceptions, datacompressor and some useless warnings
EXTRA_LD_FLAGS += -nostartfiles -nodefaultlibs -EL
EXTRA_LD_FLAGS += --cref --gc-sections

# Pre-processed
SPAR_LINK_LOAD_FILE = $(BIN_OUT_DIR)/$(CHIP)$(REVNUM)_$(SPAR_IN)_proc.ld

# Disable VFE, stdlibs and setup entry to spar_setup.
EXTRA_LD_FLAGS += -T $(SPAR_LINK_LOAD_FILE) -Map $(ELF_OUT:.elf=.list) -O2
EXTRA_LD_FLAGS += --entry=$(SPAR_CRT_SETUP) -z muldefs

ifneq ($(NEED_STD_LIBS), y)
   EXTRA_LD_FLAGS += -nostdlib
endif

################################################################################
# Code/data location
################################################################################
# This spar's CRT if it exists is responsible for moving last used ptr.
ifeq ($(DIRECT_LOAD), 1)
IRAM_BEGIN_CMD  := $(PERL) -nle 'printf( "0x%08X", hex($$1) )  if /^POST_INIT_SECTION_IN_SRAM=(0x[[:alnum:]]+)/'                                                                 $(ELF_LIST:.elf=.lst)
IRAM_LEN_CMD    := $(PERL) -nle 'printf( "0x%08X", ($(SRAM_BEGIN_ADDR) + $(SRAM_LENGTH) - $(PATCH_TABLE_LENGTH) - hex($$1)) )  if /^POST_INIT_SECTION_IN_SRAM=(0x[[:alnum:]]+)/' $(ELF_LIST:.elf=.lst)
else
IRAM_BEGIN_CMD  := $(PERL) -nle 'printf( "0x%08X", hex($$1) )  if /^FIRST_FREE_SECTION_IN_SRAM=(0x[[:alnum:]]+)/'                                                                 $(ELF_LIST:.elf=.lst)
IRAM_LEN_CMD    := $(PERL) -nle 'printf( "0x%08X", ($(SRAM_BEGIN_ADDR) + $(SRAM_LENGTH) - $(PATCH_TABLE_LENGTH) - hex($$1)) )  if /^FIRST_FREE_SECTION_IN_SRAM=(0x[[:alnum:]]+)/' $(ELF_LIST:.elf=.lst)
endif

IAON_BEGIN_CMD  := $(PERL) -nle 'printf( "0x%08X", hex($$1) )  if /^FIRST_FREE_SECTION_IN_AON=(0x[[:alnum:]]+)/'                                                                  $(ELF_LIST:.elf=.lst)
IAON_LEN_CMD    := $(PERL) -nle 'printf( "0x%08X", ($(AON_BEGIN_ADDR) + $(AON_LENGTH)  - hex($$1)) )  if /^FIRST_FREE_SECTION_IN_AON=(0x[[:alnum:]]+)/'                           $(ELF_LIST:.elf=.lst)

SPAR_LOC_PREREQ += $(ELF_LIST:.elf=.lst)

ifeq ($(SPAR_IN), flash)
# If SPAR is in flash, IRAM is already setup, need to setup IROM
# Note that we are rounding up to UINT32 closest to 4 bytes above current image just to be safe
  IROM_BEGIN_CMD := $(PERL) -nle 'printf( "0x%08X", hex($$1) + hex($$2) + 4 )  if /^[[:space:]]+Load[[:space:]]Region[[:space:]]CM3_Ver1[[:space:]].*Base.*[[:space:]](0x[[:alnum:]]+).*[[:space:]]Size\:[[:space:]]+(0x[[:alnum:]]+)/'                                            $(ELF_LIST:.elf=.lst)
  IROM_LEN_CMD   := $(PERL) -nle 'printf( "0x%08X", ($(FLASH0_BEGIN_ADDR) + $(FLASH0_LENGTH) - hex($$1) - hex($$2) - 4))  if /^[[:space:]]+Load[[:space:]]Region[[:space:]]CM3_Ver1[[:space:]].*Base.*[[:space:]](0x[[:alnum:]]+).*[[:space:]]Size\:[[:space:]]+(0x[[:alnum:]]+)/' $(ELF_LIST:.elf=.lst)


  IRAM_BEGIN_CMD := $(PERL) -nle 'printf( "0x%08X", hex($$1_ )   if /^[[:space:]]+Execution[[:space:]]Region[[:space:]]first_free_section_in_SRAM[[:space:]].*Base:[[:space:]]+(0x[[:alnum:]]+)/'                                                                 $(ELF_LIST:.elf=.lst)
  IRAM_LEN_CMD   := $(PERL) -nle 'printf( "0x%08X", ($(SRAM_BEGIN_ADDR) + $(SRAM_LENGTH) - $(PATCH_TABLE_LENGTH) - hex($$1)) )   if /^[[:space:]]+Execution[[:space:]]Region[[:space:]]first_free_section_in_SRAM[[:space:]].*Base:[[:space:]]+(0x[[:alnum:]]+)/' $(ELF_LIST:.elf=.lst)

  SPAR_LOC_PREREQ += $(ELF_LIST:.elf=.lst)
else
  IROM_BEGIN_CMD := $(ECHO) ""
  IROM_LEN_CMD := $(ECHO) ""
endif

IROM_BEGIN_CMD :=$(call PERL_FIX_QUOTES,$(IROM_BEGIN_CMD))
IROM_LEN_CMD   :=$(call PERL_FIX_QUOTES,$(IROM_LEN_CMD))
IRAM_BEGIN_CMD :=$(call PERL_FIX_QUOTES,$(IRAM_BEGIN_CMD))
IRAM_LEN_CMD   :=$(call PERL_FIX_QUOTES,$(IRAM_LEN_CMD))
IAON_BEGIN_CMD :=$(call PERL_FIX_QUOTES,$(IAON_BEGIN_CMD))
IAON_LEN_CMD   :=$(call PERL_FIX_QUOTES,$(IAON_LEN_CMD))


# Add overlay info to linker script if required.
ifneq ($(OVERLAY_SRC),)
  OVERLAY_FLAGS += OVERLAY_AREA_LENGTH=$(OVERLAY_AREA_LENGTH)
  APP_SRC += spar_overlay_manager.c
else
  OVERLAY_FLAGS += OVERLAY_AREAS=
endif

################################################################################
# General compiler flags
################################################################################
CPP_FLAGS      += -fno-exceptions -fno-rtti

# Common C flags
COMMON_FLAGS    =  -c -DCOMPILER_ARM $(CORE_COMMON_OPTIONS) -Os -g -Wa,-adhln -ffunction-sections \
					  -ffreestanding -DSPAR_CRT_SETUP=$(SPAR_CRT_SETUP) -DSPAR_APP_SETUP=$(SPAR_APP_SETUP)

# Add some more hacky flags
COMMON_FLAGS += -D__TARGET_CPU_CORTEX_M4 -D__ARMCC_VERSION=400677

# Add the common C flags for the chip
C_SPECIFIC_FLAGS   += -funsigned-char -fshort-wchar @$(PATCH_ROOT_DIR)/$(TC)/$(CHIP)$(REVNUM).cflag -DTOOLCHAIN_wiced=1

################################################################################
# General assembler flags
################################################################################
A_SPECIFIC_FLAGS +=

################################################################################
# Final linker, compiler and assembler options
################################################################################
C_FLAGS += $(C_SPECIFIC_FLAGS) $(COMMON_FLAGS) $(INCS_OPTS)
ASM_FLAGS += $(COMMON_FLAGS) $(A_SPECIFIC_FLAGS) $(INCS_OPTS)
LIBS_LINKER_OPTIONS = --start-group $(LIBS) --end-group
################################################################################
# Sections extraction, foo.elf -> foo.elf.text.hex etc., to make a CGS
################################################################################
ifneq ($(SPAR_IN), flash)
# enable compressed sections if compression is enabled.
ifeq ($(COMPRESSION_ENABLED),y)
  UNCOMPRESSED_SECTIONS +=
  COMPRESSED_SECTIONS += .text .rodata .data .setup .aon
else
# We will not extract sections for a flash image, else the only sections we want to extract
# Keep all sections uncompressed by default
  UNCOMPRESSED_SECTIONS +=  .text .rodata .data .setup .aon
  COMPRESSED_SECTIONS +=
endif
ifneq ($(OVERLAY_SRC),)
  # Add the overlaid sections to extract
  OVERLAID_SECTIONS += $(addprefix .,$(basename $(OVERLAY_SRC)))
endif
endif

ELF_OUT_LIST = $(ELF_OUT:.elf=.list)

