/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * Audio Player TIME control app
 *
 */

#include <ctype.h>
#include "wiced_log.h"

#include "audio_player_sntp.h"


/******************************************************
 *                     Macros
 ******************************************************/

#define PRINT_IP(addr)    (int)((addr.ip.v4 >> 24) & 0xFF), (int)((addr.ip.v4 >> 16) & 0xFF), (int)((addr.ip.v4 >> 8) & 0xFF), (int)(addr.ip.v4 & 0xFF)

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations : empty stub
 ******************************************************/

/*
 * SNTP is used to support MQTT only
 */

#ifndef AUDIO_PLAYER_ENABLE_MQTT

wiced_result_t audio_player_sntp_start(wiced_ip_address_t sntp_server1, wiced_ip_address_t sntp_server2, uint32_t interval_ms)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_sntp_stop()
{
    return WICED_SUCCESS;
}

/******************************************************
 *               Function Declarations : empty implementation
 ******************************************************/

#else

wiced_result_t audio_player_sntp_start(wiced_ip_address_t sntp_server1, wiced_ip_address_t sntp_server2, uint32_t interval_ms)
{
    /* the AP must provide an NTP server locally to guarantee accuracy */
    if ( sntp_server1.ip.v4 != 0 )
    {
        if (WICED_SUCCESS != sntp_set_server_ip_address(0, sntp_server1))
        {
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR,
                          "ERROR: cannot set NTP server (%d.%d.%d.%d) time accuracy will be degraded\n",
                          PRINT_IP(sntp_server1));
        }
        else
        {
            sntp_use_local_server_only(WICED_TRUE);
        }
    }
    else
    {
        sntp_clr_server_ip_address(0);
    }

    if ( sntp_server2.ip.v4 != 0 )
    {
        if (WICED_SUCCESS != sntp_set_server_ip_address(1, sntp_server2))
        {
            wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR,
                          "ERROR: cannot set NTP server (%d.%d.%d.%d) time accuracy will be degraded\n",
                          PRINT_IP(sntp_server2));
        }
        else
        {
            sntp_use_local_server_only(WICED_TRUE);
        }
    }
    else
    {
        sntp_clr_server_ip_address(1);
    }

    if (WICED_SUCCESS != sntp_start_auto_time_sync_nowait(interval_ms))
    {
        return WICED_ERROR;
    }

    return WICED_SUCCESS;
}


wiced_result_t audio_player_sntp_stop()
{
    sntp_stop_auto_time_sync();

    return WICED_SUCCESS;
}

#endif /*MQTT*/
