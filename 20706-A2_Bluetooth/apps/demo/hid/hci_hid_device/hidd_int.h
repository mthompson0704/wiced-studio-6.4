/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file contains HID DEVICE internal definitions
 */

#ifndef HIDD_INT_H
#define HIDD_INT_H

#include "wiced_bt_types.h"
#include "wiced_bt_hidd.h"
#include "wiced_bt_l2c.h"
#include "wiced_timer.h"
#include "wiced_bt_hid_defs.h"
#include "wiced_bt_trace.h"

#if 1
#define HIDD_TRACE_DBG                                 WICED_BT_TRACE
#else
#define HIDD_TRACE_DBG(...)
#endif

/* Define the possible events of the HID Device state machine.
*/
enum
{
    HOST_CONN_OPEN,
    HOST_CONN_CLOSE,
    HOST_CONN_LOST,
    HOST_CONN_FAIL,
    HID_API_CONNECT,
    HID_API_DISCONNECT,
    HID_API_SEND_DATA
};

/* Define the possible states of the HID Device.
*/
enum
{
    HID_DEV_ST_NO_CONN,
    HID_DEV_ST_CONNECTING,
    HID_DEV_ST_CONNECTED,
    HID_DEV_ST_DISC_ING
};

/* To remember the power mode and setting */
typedef struct curr_pm_setting
{
    uint8_t  mode;
    uint16_t interval;
} tHID_DEV_PM_CURR;

/* Define the HID Connection Block
*/
typedef struct hid_conn
{
#define HID_CONN_STATE_UNUSED           (0)
#define HID_CONN_STATE_CONNECTING_CTRL  (1)
#define HID_CONN_STATE_CONNECTING_INTR  (2)
#define HID_CONN_STATE_CONNECTED        (3)
#define HID_CONN_STATE_DISCONNECTING    (4)

    uint8_t           conn_state;

#define HID_CONN_FLAGS_IS_ORIG              (0x01)
#define HID_CONN_FLAGS_CONGESTED            (0x02)
#define HID_CONN_FLAGS_INACTIVE             (0x04)

    uint8_t           conn_flags;

    uint16_t          ctrl_cid;
    uint16_t          intr_cid;
    uint16_t          rem_mtu_size;
    uint16_t          disc_reason;                       /* Reason for disconnecting (for HID_HDEV_EVT_CLOSE) */
    wiced_timer_t     retry_timer;

} tHID_CONN;

/* Define the HID management control block.
*/
typedef struct hid_control_block
{
    BD_ADDR        host_addr;   /* BD-Addr of the host device */
    wiced_bool_t   host_known;  /* Mode */
    wiced_bool_t   virtual_cable;/* If the device is to behave as virtual cable */
    uint8_t        dev_state;   /* Device state if in HOST-KNOWN mode */
    uint8_t        conn_tries;  /* Remembers to the number of connection attempts while CONNECTING */
    uint8_t        sec_mask;
    uint16_t       get_rep_buf_sz;
    tHID_CONN      conn;        /* L2CAP channel info */

#if HID_DEV_PM_INCLUDED == TRUE
    TIMER_LIST_ENT idle_tle;    /* Timer used for inactivity timing */
    wiced_bt_hidd_pm_pwr_md_t pm_params[3]; /* Power management parameters for the three possible states */
    tHID_DEV_PM_CURR   curr_pm; /* Current power mode */
    wiced_bool_t        pm_ctrl_busy;/* A power mode transition is going on */
    uint8_t          conn_substate;
    wiced_bt_hidd_pm_pwr_md_t final_pm;/* To remember the power mode while a power mode change is ongoing */
#endif

    BOOLEAN        use_qos_flg; /* Qos information provided by application or not */
    wiced_bool_t   unplug_on;   /* Virtual unplug has been sent or received */
    wiced_bt_flow_spec_t      hci_qos;     /* Storage for QoS provided by application */

    /* L2CAP registration structures */
    wiced_bt_l2cap_appl_information_t l2cap_ctrl_appl_info;
    wiced_bt_l2cap_appl_information_t l2cap_intr_appl_info;

    wiced_bt_hidd_callback_t   *callback; /* Application callbacks */
    wiced_bool_t    reg_flag;
    uint8_t         trace_level;
} tHIDDEV_CB;

typedef struct snd_data_params
{
    wiced_bool_t ctrl_ch;   /* TRUE if control channel, FALSE if interrupt */
    uint8_t      trans_type;/* Transaction type */
    uint8_t      param;     /* Second byte after trans type */
    uint8_t      *p_data;   /* Data that comes after param */
    uint16_t     data_len;  /* Data length */
} tHID_SND_DATA_PARAMS; /* Is defined for hidd_conn_snd_data */

/* HID management function prototype
*/
typedef tHID_STATUS (tHIDD_MGMT_EVT_HDLR) (uint8_t, void *);

/* HID Globals
*/
#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************************************
** Main Control Block
*******************************************************************************/
HID_API extern tHIDDEV_CB  hidd_cb;

extern tHID_STATUS hidd_conn_reg (void);
extern void hidd_conn_dereg( void );
extern tHID_STATUS hidd_conn_initiate (void);
extern void hidd_conn_disconnect (void);
extern tHID_STATUS hidd_conn_snd_data (tHID_SND_DATA_PARAMS *p_data);

extern tHID_STATUS hidd_mgmt_process_evt( uint8_t event, void *data );
extern void hidd_proc_repage_timeout (uint32_t arg);

#if HID_DEV_PM_INCLUDED == TRUE
extern tHID_STATUS hidd_pm_start( void ) ;
extern tHID_STATUS hidd_pm_stop( void );
extern tHID_STATUS hidd_pm_activity_evt( void );
extern tHID_STATUS hidd_pm_suspend_evt( void );
extern tHID_STATUS hidd_pm_unsuspend_evt( void );
extern void hidd_pm_init( void );
extern BOOLEAN hidd_pm_set_power_mode( wiced_bt_hidd_pm_pwr_md_t *pm) ;

extern void hidd_pm_proc_mode_change( UINT8 hci_status, UINT8 mode, UINT16 interval );
#endif /* HID_DEV_PM_INCLUDED == TRUE */

#ifdef __cplusplus
}
#endif

#endif
