CHIP                       	    := 20735
CHIP_REV                   	    := B1
BASE_LOC                   	    ?= rom
SPAR_LOC                   	    ?= ram
TOOLCHAIN                  	    ?= Wiced
CONFIG_DEFINITION          	    = 
PLATFORM_NV                	    ?= SFLASH
PLATFORM_STORAGE_BASE_ADDR 	    := 0xFF000000
PLATFORM_APP_SPECIFIC_STATIC_DATA  ?= 0xFF000C00
PLATFORM_APP_SPECIFIC_STATIC_LEN   ?= 1024
# Placing @ 15kb offset from the end of RAM
PLATFORM_DIRECT_LOAD_BASE_ADDR  := 0x24C400
ifneq ($(DONT_WRITE_STATIC_SECTION), 1)
PLATFORM_BOOTP             	    := $(CHIP)_$(PLATFORM_NV).btp
else
PLATFORM_BOOTP             	    := ../apps/$(APP_FULL)/app.btp
endif
ifneq ($(OPUS_CELT_ENCODER), 1)
PLATFORM_CONFIGS           	    := CYW20735B1.cgs
else
PLATFORM_CONFIGS           	    := CYW20735B1_OPUS_CELT.cgs
endif
PLATFORM_MINIDRIVER        	    := minidriver-20735B1-uart.hex
PLATFORM_IDFILE            	    := CYW20735B1_IDFILE.txt
PLATFORM_BAUDRATE          	    := AUTO
PLATFORM_PREDOWNLOAD_CONFIG     := $(CHIP)_$(PLATFORM_NV).hcd
PLATFORM_DOWNLOAD_TARGET         = convert_cgs_to_hex
PLATFORM_OTA_TARGET              = create_ota_image
CHIPLOAD_CRC_VERIFY				:= -CHECKCRC
CHIPLOAD_NO_READ_VERIFY			:= -NOVERIFY
ifneq ($(RTS_DELAY), )
CHIPLOAD_RTS_DELAY 			= -RTS_DELAY $(RTS_DELAY)
else
CHIPLOAD_RTS_DELAY 			= -RTS_DELAY 0
endif

C_FLAGS += -DCYW$(CHIP)$(CHIP_REV)=1
C_FLAGS += -DCHIP=$(CHIP)

export PLATFORM_APP_SPECIFIC_STATIC_DATA
export PLATFORM_APP_SPECIFIC_STATIC_LEN

$(PLATFORM_DOWNLOAD_TARGET):
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting CGS to HEX...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)WICED/internal/$(CHIP)$(CHIP_REV)/*.hdf build/$(OUTPUT_NAME)/
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms $(BT_DEVICE_ADDRESS_OVERRIDE) -A $(PLATFORM_STORAGE_BASE_ADDR) -B $(SOURCE_ROOT)platforms/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -H build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hcd --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO) && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log)

$(PLATFORM_OTA_TARGET):
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Creating OTA images...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)WICED/internal/$(CHIP)$(CHIP_REV)/*.hdf build/$(OUTPUT_NAME)/
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM_FULL) -O DLConfigFixedHeader:0 -O ConfigDSLocation:0 -B $(SOURCE_ROOT)platforms/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex --cgs-files build/$(OUTPUT_NAME)/$(BLD)-$(APP)-$(BASE_LOC)-$(SPAR_LOC)-spar.cgs > build/$(OUTPUT_NAME)/cgs2hex.ota.log 2>&1 && $(ECHO) Conversion complete || ($(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.ota.log)
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_HCD_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hcd
	$(QUIET)$(call CONV_SLASHES,$(PERL)) -e '$$s=-s "build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin";printf "OTA image footprint in NV is %d bytes",$$s;'
	$(QUIET)$(ECHO_BLANK_LINE)

download_using_chipload:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(eval UART:=$(shell $(CAT) < com_port.txt))
	$(QUIET)$(if $(UART), \
		$(ECHO) Downloading application... \
			&& $(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) $(CHIPLOAD_REBAUD) $(CHIPLOAD_NO_READ_VERIFY) $(CHIPLOAD_CRC_VERIFY) -MINIDRIVER $(SOURCE_ROOT)platforms/$(PLATFORM_MINIDRIVER) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM_BOOTP) -CONFIG build/$(OUTPUT_NAME)/$(OUTPUT_NAME).hex -LOGTO build/$(OUTPUT_NAME)/logto.log $(CHIPLOAD_RTS_DELAY) > build/$(OUTPUT_NAME)/download.log 2>&1 \
			&& $(ECHO) Download complete && $(ECHO_BLANK_LINE) && $(ECHO) $(QUOTES_FOR_ECHO)Application running$(QUOTES_FOR_ECHO) \
			|| ($(ECHO) $(QUOTES_FOR_ECHO)****Download failed ****$(QUOTES_FOR_ECHO) && $(call CONV_SLASHES,$(PERL)) $(AFT) 4 build/$(OUTPUT_NAME)/download.log && \
				$(ECHO) Download failed. This WICED platform of the SDK only supports download to 20735B1 device.))
