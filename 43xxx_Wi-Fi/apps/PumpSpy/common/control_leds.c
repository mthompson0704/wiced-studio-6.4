#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"
//
// control the LEDs
//
/*
#ifdef NO_LEDS
void control_leds(int state)
{
	WPRINT_APP_INFO(("control_leds NO_LEDS\r\n"));
}

#elif
*/

#ifdef BBS2_SYSTEM
extern wiced_bool_t set_i2c_tlc59116(wiced_bool_t force_set);
#endif

void control_leds(int state)
{
//	WPRINT_APP_INFO(("control_leds OUTLET_SYSTEM BBS_SYSTEM o\r\n"));
	switch (state)
	{
		case LED_OFF:

#ifdef OUTLET_SYSTEM
			wiced_gpio_output_low( RED_LED );
			wiced_gpio_output_low( GREEN_LED );
#elif BBS2_SYSTEM
			set_i2c_tlc59116(WICED_TRUE);
#elif BBS_SYSTEM
			WPRINT_APP_INFO(("{\"button_press\" : 0 }"));
#endif

			break;

		case LED_RED:

#ifdef OUTLET_SYSTEM
			wiced_gpio_output_high( RED_LED );
			wiced_gpio_output_low( GREEN_LED );
#elif BBS2_SYSTEM
			set_i2c_tlc59116(WICED_TRUE);
#elif BBS_SYSTEM
			WPRINT_APP_INFO(("{\"button_press\" : 3 }"));
#endif

			break;

		case LED_GREEN:

#ifdef OUTLET_SYSTEM
			wiced_gpio_output_low( RED_LED );
			wiced_gpio_output_high( GREEN_LED );
#elif BBS2_SYSTEM
			set_i2c_tlc59116(WICED_TRUE);
#elif BBS_SYSTEM
			WPRINT_APP_INFO(("{\"button_press\" : 2 }"));
#endif

			break;

		case LED_RED_GREEN:

#ifdef OUTLET_SYSTEM
			wiced_gpio_output_high( RED_LED );
			wiced_gpio_output_high( GREEN_LED );
#elif BBS2_SYSTEM
			set_i2c_tlc59116(WICED_TRUE);
#elif BBS_SYSTEM
			WPRINT_APP_INFO(("{\"button_press\" : 1 }"));
#endif

			break;
	}
}

//#endif

