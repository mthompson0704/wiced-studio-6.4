--------------------------------------------
ISM43907-WM-L170 EVB - README
--------------------------------------------

Provider    : Inventek Systems
Website     : http://www.inventeksys.com/products-page/wifi-modules/serial-wifi/ism43907-l170/
Description : Inventek Systems ISM43907-WM-L170 mounted on a evaluation board

Module
  Mfr       : Inventek Systems
  P/N       : ISM43907-WM-L170
  MCU       : CYW943907 Cortex-R4 320MHz  (Apps Core)
  WLAN      : CYW943907 Cortex-R4 (WLAN Core)
  Bluetooth : None
  WLAN Antennas : Diversity with two printed antennae (and in-line switched AMP A-1JB connectors)
  
EVB Features
  High speed (50Mbyte/s) serial flash for program and data (8Mbyte capacity)
  USB-serial UART interface
  Reset button

--------------------------------------------
Board Revisions
--------------------------------------------

P205     ChipRevision : B1 Module : Inventek Systems (ISM43907-WM-L170)
