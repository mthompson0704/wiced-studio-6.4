var group__sco__api__functions =
[
    [ "wiced_bt_sco_accept_connection", "group__sco__api__functions.html#ga7c8ecc9a1333c1c5b4da60f1dd07d990", null ],
    [ "wiced_bt_sco_create_as_acceptor", "group__sco__api__functions.html#gaba857121768256e41d47bb56eab560b8", null ],
    [ "wiced_bt_sco_create_as_initiator", "group__sco__api__functions.html#gad3c0ea63212a9cca3b0b668d7e392288", null ],
    [ "wiced_bt_sco_output_stream", "group__sco__api__functions.html#ga127c7a31a1a50679013855db90a0f430", null ],
    [ "wiced_bt_sco_remove", "group__sco__api__functions.html#ga0fba642e9f6ed365e385457be01e4e15", null ],
    [ "wiced_bt_setup_voice_path", "group__sco__api__functions.html#gaec10b3c56469bce033c4b02f336712da", null ]
];