/*
 * Copyright 2019, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */


/** @file
 * Audio Player voice response/file support.
 */

#include "wiced.h"
#include "wiced_log.h"

#include "audio_player.h"

/******************************************************
 *                     Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

#ifdef AUDIO_PLAYER_ENABLE_VOICE
extern wiced_block_device_t block_device_sdmmc;
#endif

/******************************************************
 *               Function Declarations
 ******************************************************/

#ifdef AUDIO_PLAYER_ENABLE_VOICE

wiced_result_t audio_player_open_file(audio_client_ref handle, void* userdata, char* file_url, void** file_handle, uint32_t* file_size)
{
    audio_player_t* player = (audio_player_t*)userdata;
    wiced_dir_entry_details_t details;
    wiced_result_t result;
    char* ptr;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || file_url == NULL)
    {
        return WICED_ERROR;
    }

    /*
     * Do we already have a file open?
     */

    if (player->voice.file_ptr != NULL)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "File handle already open\n");
        return WICED_ERROR;
    }

    /*
     * Find the pointer to the start of the filename. The FILE scheme can start with
     * 'file://hostname/path' where hostname may be empty or 'file:/path'. For our
     * processing we will allow 'file:///' (empty hostname) or 'file:/' as valid URLs.
     */

    if (strlen(file_url) < 7)
    {
        return WICED_ERROR;
    }

    if (strncasecmp(file_url, "file://", 7) == 0)
    {
        ptr = &file_url[7];
    }
    else
    {
        ptr = &file_url[5];
    }

    /*
     * Get the size of the file.
     */

    wiced_log_msg(WLF_DEF, WICED_LOG_INFO, "Querying file: %s\n", ptr);
    result = wiced_filesystem_file_get_details(&player->voice.filesys_handle, ptr, &details);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Unable to access %s\n", file_url);
        return WICED_ERROR;
    }

    if (file_size)
    {
        *file_size = (uint32_t)details.size;
    }
    wiced_log_msg(WLF_DEF, WICED_LOG_INFO, "File size is %lu bytes\n", *file_size);

    /*
     * Open the file.
     */

    result = wiced_filesystem_file_open(&player->voice.filesys_handle, &player->voice.file_handle, ptr, WICED_FILESYSTEM_OPEN_FOR_READ);
    if (result != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_DEF, WICED_LOG_ERR, "Unable to open %s\n", file_url);
        return WICED_ERROR;
    }

    player->voice.file_ptr = &player->voice.file_handle;

    /*
     * Note: we don't use the file handle since we only allow one file at a time to be open.
     */

    *file_handle = NULL;

    return WICED_SUCCESS;
}


wiced_result_t audio_player_read_file(audio_client_ref handle, void* userdata, void* file_handle, uint8_t* buf, uint32_t buflen, uint32_t* bytes_read)
{
    audio_player_t* player = (audio_player_t*)userdata;
    uint64_t returned_bytes_count;
    uint64_t bytes_to_read;
    wiced_result_t result;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || !bytes_read)
    {
        return WICED_ERROR;
    }

    /*
     * The WICED filesystem (for FileX at least) returns an error when you try
     * and read past EOF. So we need to explicitly check for EOF before
     * trying to read so we can distinguish between EOF and a real error.
     */

    returned_bytes_count = 0;
    bytes_to_read        = buflen;

    if (!wiced_filesystem_file_end_reached(player->voice.file_ptr))
    {
        /*
         * Read the next chunk of the file and pass it back.
         */

        result = wiced_filesystem_file_read(player->voice.file_ptr, buf, bytes_to_read, &returned_bytes_count);
    }
    else
    {
        result = WICED_SUCCESS;
    }

    *bytes_read = (uint32_t)returned_bytes_count;

    return result;
}


wiced_result_t audio_player_seek_file(audio_client_ref handle, void* userdata, void* file_handle, uint32_t file_position)
{
    audio_player_t* player = (audio_player_t*)userdata;
    wiced_result_t result;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || player->voice.file_ptr == NULL)
    {
        return WICED_ERROR;
    }

    result = wiced_filesystem_file_seek(player->voice.file_ptr, file_position, WICED_FILESYSTEM_SEEK_SET);

    return result;
}


wiced_result_t audio_player_close_file(audio_client_ref handle, void* userdata, void* file_handle)
{
    audio_player_t* player = (audio_player_t*)userdata;
    wiced_result_t result;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || player->voice.file_ptr == NULL)
    {
        return WICED_ERROR;
    }

    result = wiced_filesystem_file_close(player->voice.file_ptr);

    player->voice.file_ptr          = NULL;
    player->voice.play_voice_active = WICED_FALSE;

    return result;
}


wiced_result_t audio_player_voice_init(audio_player_t* player)
{
    if (player == NULL)
    {
        return WICED_ERROR;
    }

    if (player->voice.init)
    {
        return WICED_SUCCESS;
    }

    if (wiced_filesystem_init() != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error initing filesystems\n");
        return WICED_ERROR;
    }

    if (wiced_filesystem_mount(&block_device_sdmmc, WICED_FILESYSTEM_HANDLE_FILEX, &player->voice.filesys_handle, "AudioPlayerWicedFS") != WICED_SUCCESS)
    {
        wiced_log_msg(WLF_AUDIO, WICED_LOG_ERR, "Error mounting filesystem\n");
        return WICED_ERROR;
    }

    player->voice.init              = WICED_TRUE;
    player->voice.play_voice_active = WICED_FALSE;

    return WICED_SUCCESS;
}


wiced_result_t audio_player_voice_deinit(audio_player_t* player)
{
    wiced_result_t result;

    if (player == NULL)
    {
        return WICED_ERROR;
    }

    if (!player->voice.init)
    {
        return WICED_SUCCESS;
    }

    player->voice.init = WICED_FALSE;
    if (player->voice.file_ptr)
    {
        wiced_filesystem_file_close(player->voice.file_ptr);
        player->voice.file_ptr = NULL;
    }

    result = wiced_filesystem_unmount(&player->voice.filesys_handle);

    return result;
}

wiced_result_t audio_player_play_voice(audio_player_t* player, const char* voice_mp3, uint32_t duration_ms)
{
    wiced_result_t result = WICED_ERROR;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || voice_mp3 == NULL)
    {
        return WICED_ERROR;
    }

    if (player->audio_client)
    {
        /*
         * Set the flag for voice playback and store the target duration time.
         */

        player->voice.play_voice_active    = WICED_TRUE;
        player->voice.playback_tgt_time_ms = duration_ms;

        /*
         * Send the play request to audio client.
         */

        result = audio_client_ioctl(player->audio_client, AUDIO_CLIENT_IOCTL_PLAY, (void*)voice_mp3);
        if (result != WICED_SUCCESS)
        {
            player->voice.play_voice_active    = WICED_FALSE;
            player->voice.playback_tgt_time_ms = 0;
        }
    }

    return result;
}

wiced_result_t audio_player_voice_clock_adj(audio_player_t* player, int32_t* pll_ppm, uint32_t duration_ms)
{
    wiced_result_t rv = WICED_ERROR;

    int32_t pll_ppm_adj;
    int32_t delta_time_ms;
    float adj = 0.0;

    if (player == NULL || player->tag != AUDIO_PLAYER_TAG_VALID || !player->voice.init || pll_ppm == NULL || duration_ms == 0)
    {
        return WICED_ERROR;
    }

    /* clock adj is one time only */
    if (player->voice.playback_tgt_time_ms != 0)
    {
        delta_time_ms = (int32_t)duration_ms - (int32_t)player->voice.playback_tgt_time_ms;

        if (delta_time_ms != 0)
        {
            adj = (  ( ((float)delta_time_ms * 1000000.0) + ((float)player->voice.playback_tgt_time_ms / 2.0) ) / (float)player->voice.playback_tgt_time_ms );

            pll_ppm_adj = (int32_t)adj;

            if (*pll_ppm == 0)
            {
                *pll_ppm = pll_ppm_adj;
            }
            else
            {
                *pll_ppm += pll_ppm_adj;
            }

            rv = WICED_SUCCESS;
        }

        /* reset clock adj request, need another playback command to do another tuning */
        player->voice.playback_tgt_time_ms = 0;
    }

    return rv;
}

#else

/*
 * EMPTY SUBS
 */
wiced_result_t audio_player_voice_init(audio_player_t* player)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_voice_deinit(audio_player_t* player)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_play_voice(audio_player_t* player, const char* voice_mp3, uint32_t duration_ms)
{
    return WICED_SUCCESS;
}

wiced_result_t audio_player_voice_clock_adj(audio_player_t* player, int32_t* pll_ppm, uint32_t duration_ms)
{
    return WICED_SUCCESS;
}
#endif
