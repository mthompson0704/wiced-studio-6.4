/*
 * Copyright (C) 2018, Cirrent Inc
 *
 * All use of this software must be covered by a license agreement signed by Cirrent Inc.
 *
 * DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
 * ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE.
 */

/*
 * The basic Cirrent Agent (CA) API
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "cirrent_types.h"

#define SOFTAP_IP_MAX_LEN 16
#define SOFTAP_SSID_MAX_LEN 33

#define DEV_ID_MAX_LEN 20
#define ACC_ID_MAX_LEN 20
#define SECRET_MAX_LEN 65

#define CA_DCT_INTERNAL_DATA_SIZE 5788

#define CA_PEM_CERT_AMAZON "-----BEGIN CERTIFICATE-----\n\
MIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF\n\
ADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6\n\
b24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL\n\
MAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv\n\
b3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj\n\
ca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM\n\
9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw\n\
IFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6\n\
VOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L\n\
93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm\n\
jgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC\n\
AYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA\n\
A4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI\n\
U5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs\n\
N+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv\n\
o/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU\n\
5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy\n\
rqXRfboQnoZsG4q5WTP468SQvvG5\n\
-----END CERTIFICATE-----"

#define CA_PEM_CERT_RADIUS "-----BEGIN CERTIFICATE-----\n\
MIID2TCCAsGgAwIBAgIJAKwVnFabvgthMA0GCSqGSIb3DQEBCwUAMIGCMQswCQYD\n\
VQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTESMBAGA1UEBwwJU2FuIE1hdGVv\n\
MRAwDgYDVQQKDAdDaXJyZW50MRswGQYDVQQLDBJjaXJyZW50c3lzdGVtcy5jb20x\n\
GzAZBgNVBAMMEmNpcnJlbnRzeXN0ZW1zLmNvbTAeFw0xNDEwMTgyMTQwMzRaFw0y\n\
NDEwMTUyMTQwMzRaMIGCMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5p\n\
YTESMBAGA1UEBwwJU2FuIE1hdGVvMRAwDgYDVQQKDAdDaXJyZW50MRswGQYDVQQL\n\
DBJjaXJyZW50c3lzdGVtcy5jb20xGzAZBgNVBAMMEmNpcnJlbnRzeXN0ZW1zLmNv\n\
bTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJkrymjoz9TZJGN4OZXj\n\
BkuXl0NjiPdnnZ5xYLaLYVyJj2RNlTtzS7PYEr+bkOo/h9Ffx82rJ+841XGNGcbj\n\
+umgCo0PAsRMQAXoJTZuQCU74EaJWmtJ5hw/fkmBUywZDXn/b6BN/OrSb7168OhP\n\
vY/uoS22SujOa54Yxftf5IEeGuVNSqnmFZM7Cq4/Ry6kqlOyS6d5zsEYU2TxlB7V\n\
OzOFOFBfe2NdkuixARV/L1g2Yr8ozzikqHhwtD1Tk+rih5Hmk8BaXI3XyECBNxe+\n\
iyaK9uxFJh/UljHLyJCkudRI2qlEzKgU9OBMphML8hx3ayObVP1rs7Zxf+eH97s7\n\
XI8CAwEAAaNQME4wHQYDVR0OBBYEFC8llytbOqPX0sRuUYvS0jjkSd1hMB8GA1Ud\n\
IwQYMBaAFC8llytbOqPX0sRuUYvS0jjkSd1hMAwGA1UdEwQFMAMBAf8wDQYJKoZI\n\
hvcNAQELBQADggEBAG2rdVgydQHqwWcOyTlg/0ocp4CtDTDd51K8K/zDNNLJ4kre\n\
lWmyfdgyOtVyQ0/L9kAC7WfmqrjFDmwAsAQpzklOl2qLD14xftP/qjE9yvCQa+Lj\n\
C40E036JfTLGg9Gms08rD71CFLsiFpq26TETzSDBvMro+izllERO5sL+o+mEWhVj\n\
GA3nMkHKebpZwzns/sBSDYgQ/FHxId9Sc8ovnaRDlIeFrAyujTGVJ2cpdMaM/INp\n\
Giy9dWnDnceO2LpwhPCYY9n6O8Bbbs3s+KooR5jF5vni0MSd4FYk2Bi+6+rWVs91\n\
9Y/2vzlGFjcajZJwJIlirofTAGCSM2TzwKiN1Aw=\n\
-----END CERTIFICATE-----"

#define CA_PEM_CERT ( CA_PEM_CERT_AMAZON "\n" CA_PEM_CERT_RADIUS )

struct ca_dct_credentials
{
    char dev_id[DEV_ID_MAX_LEN];
    char acc_id[ACC_ID_MAX_LEN];
    char secret[SECRET_MAX_LEN];
    bool is_temporary;
} __attribute__((aligned));

typedef struct ca_dct_credentials CA_DCT_CREDENTIALS_T;

typedef struct ca_dct_config
{
    char internal_data[CA_DCT_INTERNAL_DATA_SIZE];
    CA_DCT_CREDENTIALS_T credentials;
} CA_DCT_CONFIG_T;

typedef struct ca_start_config
{
    bool user_binding_enabled;
    bool onboarding_button_exists;
    bool no_log_print;
    uint32_t ca_dct_offset;
    uint32_t ca_dct_size;

    struct ca_softap_conf
    {
        bool softap_enabled;
        char ssid[SOFTAP_SSID_MAX_LEN];
        char server_ip[SOFTAP_IP_MAX_LEN];
        unsigned char server_port;
    } softap_config;

} CA_INIT_CONFIG_T;

int ca_start(const CA_INIT_CONFIG_T* config);
int ca_stop(void);

#ifdef __cplusplus
} /* extern "C" */
#endif
