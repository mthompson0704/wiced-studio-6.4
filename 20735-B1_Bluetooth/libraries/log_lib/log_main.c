/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * WICED Log implementation.
 *
 */
#include "wiced.h"
#include "wiced_bt_log.h"
#include "rtc.h"
#include "wiced_hal_sflash.h"
#include "wiced_bt_trace.h"

#if defined WICED_LOG

/******************************************************
 *                      Constants
 ******************************************************/
// following should be moved to the platform.h file.  For now please make sure that the same
// values are defined in the ota_fw_upgrade.c
#if defined(USE_256K_SECTOR_SIZE)
  #define FLASH_SECTOR_SIZE         (256*1024)
  #define FW_UPGRADE_FLASH_SIZE     (256*FLASH_SECTOR_SIZE)
#else
  #define FLASH_SECTOR_SIZE         (4*1024)
#ifndef SFLASH_SIZE_2M_BITS
  #define FW_UPGRADE_FLASH_SIZE     0x80000 // 512  kbyte/4M Bit Sflash for new tag boards
#else
  #define FW_UPGRADE_FLASH_SIZE     0x40000 // 256  kbyte/2M Bit Sflash for new tag boards
#endif
#endif

#define SFLASH_BASE                 0xFF000000

#define WICED_LOG_SIZE              (2 * FLASH_SECTOR_SIZE)
#define WICED_LOG_START_OFFSET      (FW_UPGRADE_FLASH_SIZE - (2 * FLASH_SECTOR_SIZE))
#define WICED_LOG_FIRST_BLOCK_LOC   WICED_LOG_START_OFFSET
#define WICED_LOG_SECOND_BLOCK_LOC  (WICED_LOG_START_OFFSET + FLASH_SECTOR_SIZE)

static void         log_add_entry(wiced_bt_log_entry_t *p_entry);
static wiced_bool_t log_read_entry(uint32_t addr, wiced_bt_log_entry_t *buf);
static uint32_t     log_find_first_empty_offset(void);

static uint8_t  log_enabled     = 0;
static uint32_t log_next_offset = 0;

extern UINT32 sfi_write(UINT32 addr, IN UINT8 *buffer, UINT32 len);
/*
 * Save a log entry
 */
void wiced_bt_log_enable(uint8_t enable)
{
    log_enabled = enable;
}

/*
 * Save a log entry
 */
void wiced_bt_log(uint8_t event, uint32_t param)
{
    wiced_bt_log_entry_t    log_entry;
    RtcTime                 rtctime;
    uint32_t                seconds;

    if (!log_enabled)
        return;

    rtc_getRTCTime(&rtctime);
    rtc_RtcTime2Sec(&rtctime, (UINT32 *)&seconds);

    log_entry.time_stamp[0] = seconds & 0xff;
    log_entry.time_stamp[1] = (seconds >> 8 ) & 0xff;
    log_entry.time_stamp[2] = (seconds >> 16 ) & 0xff;

    log_entry.event         = event;
    log_entry.param         = param;

    log_add_entry(&log_entry);
}

/*
 * Read the oldest entries from the log area
 */
uint32_t wiced_bt_log_get_first_address(void)
{
    uint32_t             offset = log_find_first_empty_offset();
    wiced_bt_log_entry_t temp;

    // if we are currently writing into the second block, the first block has start of the available log.
    if (offset < WICED_LOG_SECOND_BLOCK_LOC)
    {
        // we are writing into the first block.  The second block may have nothing or earlier entries.
        if (log_read_entry(WICED_LOG_SECOND_BLOCK_LOC, &temp))
        {
            // second sector should be full
            return (WICED_LOG_SECOND_BLOCK_LOC);
        }
    }
    // we are writing into the second block, or there is something in nothing in the second block.
    // read from the beginning of the first block.
    return (WICED_LOG_FIRST_BLOCK_LOC);
}

/*
 * Read log entries from the specified address
 */
uint32_t wiced_bt_log_read_next(uint32_t offset, uint16_t *p_size, uint8_t *buffer)
{
    uint16_t             bytes_read;
    wiced_bt_log_entry_t entry;
    uint16_t             max_size = *p_size;
    int      i;

    *p_size = 0;

    for (i = 0; ; i++)
    {
        // read the next entry
        if (log_read_entry(offset, &entry))
        {
            memcpy(buffer + *p_size, &entry, sizeof(wiced_bt_log_entry_t));
            *p_size += sizeof(wiced_bt_log_entry_t);

            // if we are at the end of the second block, start from the beginning of the first
            if (offset + sizeof(wiced_bt_log_entry_t) > WICED_LOG_SECOND_BLOCK_LOC + FLASH_SECTOR_SIZE)
            {
                offset = WICED_LOG_FIRST_BLOCK_LOC;
            }
            else
            {
                offset     += sizeof(wiced_bt_log_entry_t);
            }
            // if we are at the end of the first block, start with the second (this is protection against block not fitting
            // exact number of entries
            if ((offset < WICED_LOG_SECOND_BLOCK_LOC) && (offset + sizeof(wiced_bt_log_entry_t) > WICED_LOG_SECOND_BLOCK_LOC))
            {
                offset = WICED_LOG_SECOND_BLOCK_LOC;
            }

            if (*p_size + sizeof(wiced_bt_log_entry_t) > max_size)
            {
                WICED_BT_TRACE("done reading return:%d\n", *p_size);
                return (offset);
            }
        }
        else
        {
            // no more entries in the log
            WICED_BT_TRACE("no more entries offset:%x return:%d\n", offset, *p_size);
            return (offset);
        }
    }
    return 0;
}

/*
 * Find offset in the SFLASH where log entry can be written
 */
static uint32_t log_find_first_empty_offset(void)
{
    wiced_bt_log_entry_t temp;
    uint32_t             offset;
    int                  i;

    if (log_next_offset != 0)
    {
        return log_next_offset;
    }
    for (i = 0, offset = WICED_LOG_FIRST_BLOCK_LOC; i < FLASH_SECTOR_SIZE / sizeof(wiced_bt_log_entry_t); i++, offset += sizeof(wiced_bt_log_entry_t))
    {
        if (!log_read_entry(offset, &temp))
        {
            log_next_offset = offset;
            return (offset);
        }
    }
    for (i = 0, offset = WICED_LOG_SECOND_BLOCK_LOC; i < FLASH_SECTOR_SIZE / sizeof(wiced_bt_log_entry_t); i++, offset += sizeof(wiced_bt_log_entry_t))
    {
        if (!log_read_entry(offset, &temp))
        {
            log_next_offset = offset;
            return (offset);
        }
    }
    // if we could not find empty offset, start from the beginning
    WICED_BT_TRACE("Failed no free entry\n");
    return (WICED_LOG_START_OFFSET);
}

void log_add_entry(wiced_bt_log_entry_t *p_entry)
{
    wiced_bt_log_entry_t temp;
    uint32_t             offset = log_find_first_empty_offset();
    uint16_t             bytes_written, bytes_read;

    bytes_written = sfi_write(SFLASH_BASE + offset, (uint8_t *)p_entry, sizeof(wiced_bt_log_entry_t));

    log_next_offset = offset + sizeof(wiced_bt_log_entry_t);

    // switch to first block if the next write will not fit into the second block
    if ((log_next_offset + sizeof(wiced_bt_log_entry_t)) > (WICED_LOG_SECOND_BLOCK_LOC + FLASH_SECTOR_SIZE))
    {
        // if offset is the beginning of the first or second block, need to erase
        wiced_hal_sflash_erase(WICED_LOG_FIRST_BLOCK_LOC, FLASH_SECTOR_SIZE);
        log_next_offset = WICED_LOG_FIRST_BLOCK_LOC;
    }

    // in case one sector does not fit full number of entries
    else if ((log_next_offset <= WICED_LOG_SECOND_BLOCK_LOC) && (log_next_offset + sizeof(wiced_bt_log_entry_t)) > (WICED_LOG_SECOND_BLOCK_LOC))
    {
        if (log_read_entry(WICED_LOG_SECOND_BLOCK_LOC, &temp))
        {
            wiced_hal_sflash_erase(WICED_LOG_SECOND_BLOCK_LOC, FLASH_SECTOR_SIZE);
        }
        log_next_offset = WICED_LOG_SECOND_BLOCK_LOC;
    }
}

wiced_bool_t log_read_entry(uint32_t addr, wiced_bt_log_entry_t *buf)
{
    wiced_bool_t result;

    uint32_t bytes_read = wiced_hal_sflash_read(SFLASH_BASE + addr, sizeof(wiced_bt_log_entry_t), (uint8_t *)buf);
    if (bytes_read != sizeof(wiced_bt_log_entry_t))
    {
        WICED_BT_TRACE("Failed to read offset:0x%x len:%d of %d\n", addr, bytes_read, sizeof(wiced_bt_log_entry_t));
    }
    return ((buf->time_stamp[0] != 0xff) || (buf->time_stamp[1] != 0xff) || (buf->time_stamp[0] != 0xff));
}


#endif
