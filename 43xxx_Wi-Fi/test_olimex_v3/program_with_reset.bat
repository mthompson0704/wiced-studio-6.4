@echo off 
@echo %time%
@echo %time% >> openocd_log.txt
@echo --------------------------
@echo Downloading Bootloader ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase waf.bootloader-NoOS-ISM4343_wbm_L151_BBS2.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Bootloader download complete && "echo.exe" || echo "Error while programming bootloader - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading DCT ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo DCT download complete && "echo.exe" || echo "Error while programming DCT - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading Application ...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f stm32f4x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS2-ISM4343_WBM_L151_BBS2-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Application download complete && "echo.exe" || echo "Error while programming Application - check cables, power, and drivers "

@echo off 
@echo %time%
@echo --------------------------
@echo Downloading FR_APP PumpSpy.BBS-ISM43362_M3G_L44-ThreadX-NetX.stripped.elf at sector 1  address 4096...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f sflash_write.tcl -c "sflash_write_file PumpSpy.BBS2-ISM4343_WBM_L151_BBS2-ThreadX-NetX.stripped.elf 4096 ISM4343_WBM_L151_BBS2-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1
@echo %time%
@echo --------------------------
@echo Downloading DCT_IMAGE DCT.stripped.elf at sector 130 offset 532480 size 6...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f sflash_write.tcl -c "sflash_write_file DCT.stripped.elf 532480 ISM4343_WBM_L151_BBS2-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1
@echo %time%
@echo --------------------------
@echo Downloading apps lookup table in wiced_apps.mk ... APPS.bin @ 0x0000 size
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -f sflash_write.tcl -c "sflash_write_file APPS.bin 0x0000 ISM4343_WBM_L151_BBS2-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1

@echo %time%
@echo --------------------------
@echo Resetting target
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f4x.cfg -c init -c "reset run" -c shutdown > openocd_log.txt 2>&1 && echo Target running
@echo %time%
@echo --------------------------
@echo Programming Complete 
@echo --------------------------
