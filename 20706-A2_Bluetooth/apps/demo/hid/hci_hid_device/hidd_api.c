/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 *  This file contains the Device HID API entry points
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "bt_types.h"
#include "hidd_int.h"


/*******************************************************************************
**
** Function         wiced_bt_hidd_init
**
** Description      This function initializes the control block and trace variable
**
** Returns          void
**
*******************************************************************************/
void wiced_bt_hidd_init (void)
{
    memset(&hidd_cb, 0, sizeof(tHIDDEV_CB));

    if (wiced_init_timer(&hidd_cb.conn.retry_timer, hidd_proc_repage_timeout, 0,
            WICED_SECONDS_TIMER ) != WICED_SUCCESS)
    {
        WICED_BT_TRACE("Err: wiced_bt_hidd_init wiced_init_timer retry failed\n");
    }

#if defined(HID_INITIAL_TRACE_LEVEL)
    hidd_cb.trace_level = HID_INITIAL_TRACE_LEVEL;
#else
    hidd_cb.trace_level = BT_TRACE_LEVEL_NONE;
#endif
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_register
**
** Description      This function must be called at startup so the device receive
**                  HID related events and call other HID API Calls.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
wiced_bt_hidd_status_t wiced_bt_hidd_register(wiced_bt_hidd_reg_info_t *p_reg_info)
{
    tHID_STATUS st;
    BD_ADDR     bt_bd_any = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

    if( p_reg_info == NULL || p_reg_info->p_app_cback == NULL )
    {
        WICED_BT_TRACE("Err: hidd reg_info:%x app_cback:%x\n",
                p_reg_info, p_reg_info->p_app_cback);
        return HID_ERR_INVALID_PARAM;
    }

    if( hidd_cb.reg_flag )
    {
        WICED_BT_TRACE("Err: hidd already registered %d\n", hidd_cb.reg_flag);
        return HID_ERR_ALREADY_REGISTERED;
    }
    /* Check if the host address is provided */
    if( memcmp( p_reg_info->host_addr, bt_bd_any, BD_ADDR_LEN ) )
    {
        hidd_cb.host_known = WICED_TRUE;
        memcpy( hidd_cb.host_addr, p_reg_info->host_addr, BD_ADDR_LEN );
        hidd_cb.dev_state = HID_DEV_ST_NO_CONN;
    }
    else
    {
        hidd_cb.host_known = WICED_FALSE;
    }

    hidd_cb.virtual_cable = HID_DEV_VIRTUAL_CABLE;

    /* Copy QoS parameters if provided (used during l2cap channel configuration) */
    if( p_reg_info->p_qos_info )
    {
        hidd_cb.use_qos_flg = TRUE;

        /* Copy QoS configuration into l2cap registration structures */
        hidd_cb.l2cap_ctrl_appl_info.qos_present = TRUE;
        memcpy(&hidd_cb.l2cap_ctrl_appl_info.qos, &p_reg_info->p_qos_info->ctrl_ch, sizeof(wiced_bt_flow_spec_t));

        hidd_cb.l2cap_intr_appl_info.qos_present = TRUE;
        memcpy(&hidd_cb.l2cap_intr_appl_info.qos, &p_reg_info->p_qos_info->int_ch, sizeof(wiced_bt_flow_spec_t));

        memcpy(&hidd_cb.hci_qos, &p_reg_info->p_qos_info->hci, sizeof(wiced_bt_flow_spec_t));
    }
    else
    {
        hidd_cb.use_qos_flg = FALSE;
    }

    hidd_cb.callback = p_reg_info->p_app_cback;

    /* Register with L2CAP */
    if( (st = hidd_conn_reg()) != HID_SUCCESS )
    {
        return st;
    }

    hidd_cb.reg_flag = WICED_TRUE;
    hidd_cb.unplug_on = WICED_FALSE;

    HIDD_TRACE_DBG("wiced_bt_hidd_register successful\n");
    return HID_SUCCESS;
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_deregister
**
** Description      This function may be used to remove HID service records and
**                  deregister from L2CAP.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_deregister( void )
{
    if( !hidd_cb.reg_flag )
        return HID_ERR_NOT_REGISTERED;

    hidd_mgmt_process_evt( HID_API_DISCONNECT, NULL ); /* Disconnect first */

    /* Deregister with L2CAP */
    hidd_conn_dereg();

    /*
     * We initiated a disconnection, but deregister L2CAP, so set the device state to
     * "Not Connected" because the L2CAP disconnection messages will be dropped/ignored.
     */
    hidd_cb.dev_state = HID_DEV_ST_NO_CONN;
    hidd_cb.reg_flag = WICED_FALSE;
    return HID_SUCCESS;
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_connect
**
** Description      This function may be used to initiate a connection to the host..
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_connect( void )
{
    if( hidd_cb.reg_flag == WICED_FALSE )
        return HID_ERR_NOT_REGISTERED;

    return hidd_mgmt_process_evt( HID_API_CONNECT, NULL ); /* This will initiate connection */
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_disconnect
**
** Description      This function may be used to disconnect from the host
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_disconnect( void )
{
    if( hidd_cb.reg_flag == WICED_FALSE )
        return HID_ERR_NOT_REGISTERED;

    return hidd_mgmt_process_evt( HID_API_DISCONNECT, NULL ); /* This will initiate disconnection */
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_hand_shake
**
** Description      This function may be used to send HAND-SHAKE to host
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_hand_shake( uint8_t res_code )
{
    tHID_SND_DATA_PARAMS hsk_data;

    if( hidd_cb.reg_flag == WICED_FALSE )
        return HID_ERR_NOT_REGISTERED;

    hsk_data.trans_type = HID_TRANS_HANDSHAKE;
    hsk_data.ctrl_ch    = WICED_TRUE;
    hsk_data.param      = res_code;
    hsk_data.p_data     = NULL;
    hsk_data.data_len   = 0;

    return hidd_mgmt_process_evt( HID_API_SEND_DATA, &hsk_data );
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_virtual_unplug
**
** Description      This function may be used to send VIRTUAL-UNPLUG to host
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_virtual_unplug( void )
{
    tHID_STATUS st;
    tHID_SND_DATA_PARAMS vup_data;

    if( hidd_cb.reg_flag == WICED_FALSE )
        return HID_ERR_NOT_REGISTERED;

    vup_data.trans_type = HID_TRANS_CONTROL;
    vup_data.ctrl_ch    = WICED_TRUE;
    vup_data.param      = HID_PAR_CONTROL_VIRTUAL_CABLE_UNPLUG;
    vup_data.p_data     = NULL;
    vup_data.data_len   = 0;

    if( (st = hidd_mgmt_process_evt(HID_API_SEND_DATA, &vup_data)) == HID_SUCCESS )
    {
        hidd_cb.unplug_on = WICED_TRUE;
    }

    return st;
}

/*******************************************************************************
**
** Function         wiced_bt_hidd_send_data
**
** Description      This function may be used to send input reports to host
**
** Returns          tHID_STATUS
**
*******************************************************************************/
wiced_bt_hidd_status_t wiced_bt_hidd_send_data(wiced_bool_t control_ch, uint8_t rep_type,
                                               uint8_t *p_data, uint16_t data_len)
{
    tHID_SND_DATA_PARAMS  snd_data;

    if( !hidd_cb.reg_flag )
        return HID_ERR_NOT_REGISTERED;

    snd_data.trans_type = HID_TRANS_DATA;
    snd_data.ctrl_ch    = control_ch;
    snd_data.param      = rep_type;
    snd_data.p_data     = p_data;
    snd_data.data_len   = data_len;

    return hidd_mgmt_process_evt( HID_API_SEND_DATA, &snd_data );
}

#if HID_DEV_PM_INCLUDED == TRUE
/*******************************************************************************
**
** Function         wiced_bt_hidd_set_power_management_params
**
** Description      This function may be used to change power mgmt parameters.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS wiced_bt_hidd_set_power_management_params( UINT8 conn_substate, wiced_bt_hidd_pm_pwr_md_t pm_params )
{
    if( conn_substate > WICED_BT_HIDD_SUSP_CONN_ST )
        return (HID_ERR_INVALID_PARAM);

    memcpy( &hidd_cb.pm_params[conn_substate], &pm_params, sizeof( wiced_bt_hidd_pm_pwr_md_t ) );

    /* Set the power mode to new parameters if currently in that state */
    if( conn_substate == hidd_cb.conn_substate )
        hidd_pm_set_power_mode ( &(hidd_cb.pm_params[conn_substate]) );

    return (HID_SUCCESS);
}

#endif

