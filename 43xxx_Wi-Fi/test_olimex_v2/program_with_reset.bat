@echo off 
@echo %time%
@echo --------------------------
@echo Downloading Bootloader ...
@echo --------------------------
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "verify_image_checksum waf.bootloader-NoOS-ISM43362_M3G_L44_BBS.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase waf.bootloader-NoOS-ISM43362_M3G_L44_BBS.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase waf.bootloader-NoOS-ISM43362_M3G_L44_BBS.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Bootloader download complete && "echo.exe" || echo "Error while programming bootloader - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading DCT ...
@echo --------------------------
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "verify_image_checksum DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase DCT.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo DCT download complete && "echo.exe" || echo "Error while programming DCT - check cables, power, and drivers "
@echo %time%
@echo --------------------------
@echo Downloading Application ...
@echo --------------------------
rem openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "verify_image_checksum PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo No changes detected && "echo.exe" || openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Download complete && "echo.exe" || echo "**** OpenOCD failed - ensure you have installed the driver from the drivers directory, and that the debugger is not running **** In Linux this may be due to USB access permissions. In a virtual machine it may be due to USB passthrough settings. Check in the task list that another OpenOCD process is not running. Check that you have the correct target and JTAG device plugged in. ****"
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f stm32f2x-flash-app.cfg -c "flash write_image erase PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf" -c shutdown >> openocd_log.txt 2>&1 && echo Application download complete && "echo.exe" || echo "Error while programming Application - check cables, power, and drivers "
@echo off 
@echo %time%
@echo --------------------------
@echo Downloading FR_APP PumpSpy.BBS-ISM43362_M3G_L44-ThreadX-NetX.stripped.elf at sector 1  address 4096...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f sflash_write.tcl -c "sflash_write_file PumpSpy.BBS-ISM43362_M3G_L44_BBS-ThreadX-NetX.stripped.elf 4096 ISM43362_M3G_L44_BBS-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1
@echo %time%
@echo --------------------------
@echo Downloading DCT_IMAGE DCT.stripped.elf at sector 130 offset 532480 size 6...
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f sflash_write.tcl -c "sflash_write_file DCT.stripped.elf 532480 ISM43362_M3G_L44_BSS-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1
@echo %time%
@echo --------------------------
@echo Downloading apps lookup table in wiced_apps.mk ... APPS.bin @ 0x0000 size
@echo --------------------------
openocd-all-brcm-libftdi.exe -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -f sflash_write.tcl -c "sflash_write_file APPS.bin 0x0000 ISM43362_M3G_L44_BBS-SDIO 0 0" -c shutdown >> openocd_log.txt 2>&1
@echo %time%
@echo --------------------------
@echo Resetting target
@echo --------------------------
openocd-all-brcm-libftdi.exe -c "log_output build/openocd_log.txt" -f Olimex_ARM-USB-TINY-H.cfg -f stm32f2x.cfg -c init -c "reset run" -c shutdown >> openocd_log.txt 2>&1 && echo Target running
@echo %time%
@echo --------------------------
@echo Programming Complete 
@echo --------------------------
