/*
 * Copyright 2016-2021, PumpSpy
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of PumpSpy.
 */

#include <stdlib.h>
#include "wiced.h"
#include "http.h"
#include "string.h"
#include "math.h"
//#include "wwd_debug.h"
#include "wiced_framework.h"
#include "wiced_ota_server.h"
//#include "default_wifi_config_dct.h"
//#include "network_config_dct.h"
#include "platform.h"
//#include "aes.h"
#include "waf_platform.h"
//#include "wiced_management.h"

#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "get_deviceid.h"
#include "control_leds.h"
#include "config_button_handler.h"
#include "restful_methods.h"
#include "new_firmware.h"
#include "dns_lookup.h"
#include "initialization.h"
#include "timers.h"
#include "access_points.h"
#include "set_bor.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

//#define STOP_APP_HERE
//#define NO_LEDS
//#define ENABLE_SYSTEM_MONITOR
//#define TEST_AES_128
//#define REMOTEDEBUG

#define FIRMWARE_VERSION 5.00
#define FIRMWARE_VERSION_STRING "5.00 - BBS 2 - 4343 "




/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/** Runs device configuration (if required)
 *
 * @param[in] config  : an array of user configurable variables in configuration_entry_t format.
 *                      The array must be terminated with a "null" entry {0,0,0,0}
 *
 * @return    WICED_SUCCESS
 */
extern wiced_result_t pumpspy_configure_device( const configuration_entry_t* config );

/******************************************************
 *               Static Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

#define JSON_post_bbs_json \
    "{\"deviceid\": %ld%ld, " \
    "\"utcunixtime\": %ld000," \
    "\"json\": \"%s\" }"

#define GET_bbs_parameters \
    "GET /bbs_parameters/%ld%ld HTTP/1.1\r\n" \
    "Host: %s:%d \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"



wiced_result_t get_parameters()
{
//    char *p;
//    char *p2;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /bbs_parameters \r\n"));
    sprintf(post, GET_bbs_parameters, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
        	return WICED_ERROR;
		wiced_bool_t found_left_brace = WICED_FALSE;
		wiced_bool_t found_right_brace = WICED_FALSE;
		wiced_bool_t found_fifth_comma = WICED_FALSE;
		int8_t comma_count = 0;
		int i = 0;
		while (found_right_brace == WICED_FALSE)
		{
			if (found_left_brace == WICED_FALSE)
			{
				if (buffer[i] == '{')
				{
					found_left_brace = WICED_TRUE;
				    WPRINT_APP_INFO(("%c", buffer[i]));
				}
			}
			else
			{
				if ((buffer[i] == ',') && (found_fifth_comma != WICED_TRUE))
				{
					if (++comma_count > 5)
					{
						found_fifth_comma = WICED_TRUE;
						WPRINT_APP_INFO(("}{"));
						i++;
					}
				}
				if (buffer[i] != 'p')
					WPRINT_APP_INFO(("%c", buffer[i]));
				if (buffer[i] == '}')
					found_right_brace = WICED_TRUE;				
			}
			i++;
		}
		WPRINT_APP_INFO(("\r\n"));
//		WPRINT_APP_INFO(("{\"1\":300000,\"2\":11750,\"3\":11750,\"4\":11250,\"5\":15}"));
//		WPRINT_APP_INFO(("{\"6\":7000,\"7\":10000,\"8\":48,\"9\":20000}"));
    }
    //
    // it prints the json string - so it's sent to microchip
    //
    return result;
}


void test_mode()
{
//    wiced_bool_t test_mode_value_changed = WICED_TRUE;
    wiced_bool_t wifi_passed = WICED_TRUE;
    wiced_bool_t last_config_button_passed = WICED_FALSE;
    int battery_voltage = -1;
    int high_water = -1;
    int ac_current = -1;
    int dc_current = -1;
    int current = -1;
    int ac_dc = -1;
    uint8_t red_count = 0;
    wiced_bool_t red = WICED_TRUE;
    wiced_bool_t json_recieved = WICED_FALSE;
    int loop_count;
	//
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// reset Config Flag - boot to provisioning mode next time
	//
	if (dct.NewConfigurationFlag != 0x0F0F0F0F)
	{
		dct.NewConfigurationFlag = 0x0F0F0F0F;
		write_app_dct();
		//
		// reset the DCT
		//
		configure_app_dct();
		configure_network_dct();
		configure_wifi_dct();
	}
	//
	// loop here until power off
	//
	while (1)
	{
		//
		// display test parameters
		// Battery voltage, AC current, DC current, Config button check, Flash wifi led (green, red).
		//
	//	if (test_mode_value_changed == WICED_TRUE)
	//	{
			WPRINT_APP_INFO(("\033[2J\033[1;1H"));
			WPRINT_APP_INFO(( "\r\n\r\n" ));
			WPRINT_APP_INFO(( "DeviceID =        %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
			WPRINT_APP_INFO(( "Firmware Ver. =   %s\r\n", FIRMWARE_VERSION_STRING));
			WPRINT_APP_INFO(( "Wi-Fi Scan =      %d\r\n", wifi_passed ));
			WPRINT_APP_INFO(( "Wi-Fi Memory =    %d\r\n", check_wifi_dct() ));
			WPRINT_APP_INFO(( "Config Button =   %d\r\n", config_button_passed ));
		//
		// config button
		//
		if ((config_button_passed == WICED_TRUE) && (last_config_button_passed == WICED_FALSE))
		{
//			test_mode_value_changed = WICED_TRUE;
			last_config_button_passed = config_button_passed;
		}
		//
		// delay
		//
		loop_count = 0;
		while (loop_count++ < 10)
		{
			wiced_rtos_delay_milliseconds( 100 );
		}
	}
}

/******************************************************
 *               Main - application starts here
 ******************************************************/
//
// Power-on
//
void application_start( )
{
	uint64_t led_timer = 0;
    //
    // Init the wiced stack
    //
    wiced_init( );
    //
    // display app version
    //
    WPRINT_APP_INFO( ( "Starting Version %s \r\n", FIRMWARE_VERSION_STRING) );
    //
    // Run the Start-up
    //
    run_common_startup_sequence();
    //
    // configure the brown out reset
    //
	set_bor();
	//
    // stop app
    //
	#ifdef STOP_APP_HERE
	main_thread_counter_enabled = WICED_FALSE;
	while ( 1 )
	{
		wiced_rtos_delay_milliseconds( 1000 );
	}
	#endif
	//
	// main loop
	//
    while ( 1 )
    {
    	//
    	// increment the main_thread_counter every 12 seconds
    	// system_monitor decrements every 12 seconds
    	// if main loop stops, system_monitor will reboot when
    	// main_thread_counter <= 0
    	//
    	current_time = host_rtos_get_time();
//		WPRINT_APP_INFO(("current_time %ld\r\n", current_time));
    	if ((current_time - last_time) > 12000)
    	{
    		last_time = current_time;
			main_thread_counter = 5;
//			WPRINT_APP_INFO(("main_thread_counter %d\r\n", main_thread_counter));
    	}
        //
        // main state machine
        //
    	switch (system_state)
    	{
    	//
    	// start up the network
    	//
    	case STATE_INITIALIZING:
    		//
    		// run the initialization
    		//
    		run_common_initialization_sequence(FIRMWARE_VERSION);
    		break;
    	//
    	// STA up running normally
    	//
    	case STATE_RUNNING:

            //
            // turn on system monitor
            //
            main_thread_counter_enabled = WICED_TRUE;
            //
            // check network status
            //
            if (wiced_network_is_up(WICED_STA_INTERFACE) != WICED_TRUE)
            {
				control_leds(LED_RED);
            	system_state = STATE_INITIALIZING;
            }
            else
            {
				//
				// get the current system time
				//
				wiced_time_get_utc_time_ms  (&server_time);
            	//
            	// send leds
            	//

            	if ((server_time - led_timer) > 120000)
            	{
    				control_leds(LED_GREEN);
    				led_timer = server_time;
            	}
				//
				// handle the ping timer
				//
				process_ping_timer();
				//
				// handle parameters time
				//
				process_get_parameters_timer();
				//
				// handle the reset timer
				//
				process_daily_reset_timer();
				//
				// handle the OTA firmware
				//
				process_ota_firmware();
				//
				// handle the clock & dns reset
				//
				process_clock_timer();
            }
    		break;
    		//
    		// provisioning AP state
    		//
        	case STATE_PROVISIONING:
        		//
        		// run provisioning AP
        		//
        		run_provisioning_ap();
        		break;
    		//
    		// OTA state
    		//
        	case STATE_OTA:
        		//
        		// run the ota AP
        		run_ota_ap();
        		break;
    	case STATE_TEST_MODE:
    		//
    		// run test_mode
    		//
    		test_mode();
    		break;
    	}

//        wiced_rtos_delay_milliseconds(500);
    }
    //
    // de-init the wiced stack and end
    //
    wiced_deinit();
}


