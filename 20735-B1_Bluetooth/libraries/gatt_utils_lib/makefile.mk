#
# $ Copyright Cypress Semiconductor 
#

NAME := gatt_utils_lib

$(NAME)_SOURCES := gatt_utils_lib.c

########################################################################
################ DO NOT MODIFY FILE BELOW THIS LINE ####################
########################################################################
include $(LIBRARY_COMMON_MAKE)
