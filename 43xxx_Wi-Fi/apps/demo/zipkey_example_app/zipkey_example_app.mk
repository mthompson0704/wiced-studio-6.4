#
# Copyright (C) 2018, Cirrent Inc
#
# All use of this software must be covered by a license agreement signed by Cirrent Inc.
#
# DISCLAIMER. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OR CONDITION,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM
# ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE.
#

#
# Makefile for the Zipkey example application
#

NAME := zipkey_example_app

$(NAME)_COMPONENTS += daemons/Cirrent
$(NAME)_COMPONENTS += inputs/button_manager
$(NAME)_COMPONENTS += daemons/led_service

APPLICATION_DCT=zipkey_app_dct.c

$(NAME)_SOURCES += zipkey_example_app.c

VALID_OSNS_COMBOS  := ThreadX-NetX_Duo ThreadX-NetX
VALID_PLATFORMS    :=
VALID_PLATFORMS    += BCM943909WCD*
VALID_PLATFORMS    += BCM943907*
VALID_PLATFORMS    += CYW943907WAE*
VALID_PLATFORMS    += CY8CKIT_062
VALID_PLATFORMS    += CYW943907AEVAL1F*
VALID_PLATFORMS    += CYW954907AEVAL1F*
