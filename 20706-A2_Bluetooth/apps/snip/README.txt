Snip apps
+++++++++
These snippet applications demonstrate a single functionality of the BT software
or the Hardware Adaptation Layer (HAL) of the WICED board. See the applications
in the apps\snip\ folder.


Over the Air Firmware Upgrade application
    This snippet is a stub application that demonstrates how to link with the
    OTA FW Upgrade library.  See ota_firmware_upgrade\ota_firmware_upgrade.c.

Over the Air Firmware Upgrade peer application
    This is an application that demonstrates how to peform OTA firmware upgrade
    from a peer Windows or Android app communicating with the
    ota_firmware_upgrade snippet running on a WICED board.  See 
    ota_firmware_upgrade\peer_apps.

------------
BLE snippets
------------

AMS application
    This snippet shows how to use the Apple Media Service (AMS).  See
    ble\ams\ams.c.

ANCS application
    This snippet shows how to use the Apple Notification Center Service (ANCS).
    See ble\ancs\ancs.c.

ANC/ANS apps
    Sample apps for BT SIG Alert Notification profile 
    ANC: Client - see anc/anc.c and ANS: Service - see ans/anc.c

BAC/BAS apps
    Sample apps for BT SIG Battery Service profile 
    BAC: Client - see bac/bac.c and BAS: Service - see bas/bas.c 

HRC/HRS apps
    Sample apps for BT SIG Heart Rate profile 
    HRC: Client - see hrc/hrc.c, HRS: Server - see hrs/hrs.c

GATT DB app
    Sample application to dynamically configure GATT datatbase over WICED HCI. 
    See gatt_db/hci_control.c

Mesh sample apps
    Sample apps based on BT SIG Mesh protocol. Sample included for 
    various models including power on/off, level, battery, light controls, 
    power level, transition, location, property, time, scene, scheduler,  
    provision, sensor and time. Client and Server models apps are included 
    for each of the models. Windows and Android based peer apps are included
    for provisioning and control.

    Mesh applications are contained in subfolders under the mesh folder, and
    demonstrate implementions of the various BLE Mesh models, as indicated by folder
    name.
    - See the application .c file for each of these apps for more details on setup.
    - See the mesh\peerapps folder for the Windows application used to provision and
      control the mesh devices.
    - See the mesh\ClientControl folder for the demonstation host MCU Client Control
      app with custom mesh support that can communicate with the embedded HCI app
      over the WICED HCI interface.  Note this is a mesh-specific separate instance
      of the Client Control host MCU app, not to be confused with the generic Client
      Control app under apps\host.

-----------
BT snippets
-----------

Serial Port Profile application
    This snippet uses the WICED SPP profile library to establish and terminate
    SPP connections, and send and receive SPP data over BR/EDR.  See bt\spp\spp.c.

A2DP Sink application
    This snippet shows usage of the A2DP Sink profile.  See 
    bt\a2dp_sink\a2dp_sink_main.c.

iAP2 application
    Note: This application and library is available for Apple MFI licensees only.
    This snippet uses the WICED iAP2 library to send data to an iOS device using
    Apple's External Accessory iAP2 protocol. See bt\iap2\iap2.c

------------
HAL snippets
------------

ADC application
    This snippet demonstrates how to configure and use ADC in WICED boards to
    measure DC voltage on various DC input channels. See
    hal\adc\hal_adc_app.c.

GPIO application
    This snippet demonstrates how to use WICED GPIO APIs to configure GPIOs as
    Input (with interrupt enabled) or Output pins.  See
    hal\gpio\hal_gpio_app.c.

PUART application
    This snippet demonstrates how to use the WICED PUART driver interface to
    send and receive bytes or a stream of bytes over the UART hardware.  See
    hal\puart\hal_puart_app.c.

PWM application
    This snippet demonstrates how to configure and use PWM in WICED boards to
    control the LED flashing.  See hal\pwm\hal_pwm_app.c.


