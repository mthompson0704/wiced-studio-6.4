/*
 * Copyright 2018, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file contains the connection interface functions for HID profile
 * device role
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#include "hidd_int.h"
#include "wiced_memory.h"
#include "bt_types.h"

#if (defined(BT_TRACE_PROTOCOL) && (BT_TRACE_PROTOCOL == TRUE))
#include "trace_api.h"
#endif /* BT_TRACE_PROTOCOL == TRUE */

/********************************************************************************/
/*              L O C A L    F U N C T I O N     P R O T O T Y P E S            */
/********************************************************************************/
void hidd_l2cif_ctrl_connected(void *context, BD_ADDR bd_addr, uint16_t l2cap_cid,
        uint16_t peer_mtu);
void hidd_l2cif_intr_connected(void *context, BD_ADDR bd_addr, uint16_t l2cap_cid,
        uint16_t peer_mtu);
void hidd_l2cif_disconnect_ind (void *context, uint16_t l2cap_cid, wiced_bool_t ack_needed);
void hidd_l2cif_data_ind (void *context, uint16_t l2cap_cid, uint8_t *p_buff, uint16_t buf_len);
void hidd_l2cif_disconnect_cfm (void *context, uint16_t l2cap_cid, uint16_t result);
void hidd_l2cif_cong_ind (void *context, uint16_t l2cap_cid, wiced_bool_t congested);
typedef void (tBTM_CMPL_CB) (void *p1);
extern UINT8 BTM_SetQoS(BD_ADDR bd, wiced_bt_flow_spec_t *p_flow, tBTM_CMPL_CB *p_cb);
UINT16 btm_get_acl_disc_reason_code (void);

/*******************************************************************************
**
** Function         hidd_conn_reg
**
** Description      This function registers with L2CAP.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS hidd_conn_reg (void)
{
    HIDD_TRACE_DBG("hidd_conn_reg\n");

    /* Now, register with L2CAP for control and interrupt PSMs*/
    hidd_cb.l2cap_ctrl_appl_info.connected_cback                = hidd_l2cif_ctrl_connected;
    hidd_cb.l2cap_ctrl_appl_info.disconnect_indication_cback    = hidd_l2cif_disconnect_ind;
    hidd_cb.l2cap_ctrl_appl_info.disconnect_confirm_cback       = hidd_l2cif_disconnect_cfm;
    hidd_cb.l2cap_ctrl_appl_info.data_indication_cback          = hidd_l2cif_data_ind;
    hidd_cb.l2cap_ctrl_appl_info.congestion_status_cback        = hidd_l2cif_cong_ind;
    hidd_cb.l2cap_ctrl_appl_info.mtu                            = HID_DEV_MTU_SIZE;
    hidd_cb.l2cap_ctrl_appl_info.flush_timeout_present          = TRUE;
    hidd_cb.l2cap_ctrl_appl_info.flush_timeout                  = HID_DEV_FLUSH_TO;

    hidd_cb.l2cap_intr_appl_info.connected_cback                = hidd_l2cif_intr_connected;
    hidd_cb.l2cap_intr_appl_info.disconnect_indication_cback    = hidd_l2cif_disconnect_ind;
    hidd_cb.l2cap_intr_appl_info.disconnect_confirm_cback       = hidd_l2cif_disconnect_cfm;
    hidd_cb.l2cap_intr_appl_info.data_indication_cback          = hidd_l2cif_data_ind;
    hidd_cb.l2cap_intr_appl_info.congestion_status_cback        = hidd_l2cif_cong_ind;
    hidd_cb.l2cap_intr_appl_info.mtu                            = HID_DEV_MTU_SIZE;
    hidd_cb.l2cap_intr_appl_info.flush_timeout_present          = TRUE;
    hidd_cb.l2cap_intr_appl_info.flush_timeout                  = HID_DEV_FLUSH_TO;

    if (wiced_bt_l2cap_register(HID_PSM_CONTROL, &hidd_cb.l2cap_ctrl_appl_info, NULL) == 0)
    {
        WICED_BT_TRACE("Err: HIDD Control Registration failed\n");
        return( HID_ERR_L2CAP_FAILED );
    }

    if (wiced_bt_l2cap_register(HID_PSM_INTERRUPT, &hidd_cb.l2cap_intr_appl_info, NULL) == 0)
    {
        WICED_BT_TRACE("Err: HIDD Interrupt Registration failed\n");
        wiced_bt_l2cap_deregister( HID_PSM_CONTROL ) ;
        return( HID_ERR_L2CAP_FAILED );
    }

    hidd_cb.conn.conn_state = HID_CONN_STATE_UNUSED ;
    return( HID_SUCCESS );
}

/*******************************************************************************
**
** Function         hidd_conn_dereg
**
** Description      This function deregisters with L2CAP.
**
** Returns          void
**
*******************************************************************************/
void hidd_conn_dereg( void )
{
    wiced_bt_l2cap_deregister (HID_PSM_CONTROL);
    wiced_bt_l2cap_deregister (HID_PSM_INTERRUPT);
}

/*******************************************************************************
**
** Function         hid_conn_disconnect
**
** Description      This function disconnects a connection.
**
** Returns          TRUE if disconnect started, FALSE if already disconnected
**
*******************************************************************************/
void hidd_conn_disconnect ()
{
    tHID_CONN   *p_hcon = &hidd_cb.conn;
#if HID_DEV_PM_INCLUDED == TRUE
    wiced_bt_hidd_pm_pwr_md_t act_pm = { 0, 0, 0, 0, HCI_MODE_ACTIVE } ;
#endif

    HIDD_TRACE_DBG ("hidd_conn_disconnect\n");

#if HID_DEV_PM_INCLUDED == TRUE
    hidd_pm_stop(); /* This will stop the idle timer if running */

    /* Need to go to to active mode to be able to send disconnect */
    hidd_pm_set_power_mode( &act_pm );
#endif

    if ((p_hcon->ctrl_cid != 0) || (p_hcon->intr_cid != 0))
    {
        p_hcon->conn_state = HID_CONN_STATE_DISCONNECTING;
        /* Disconnect both control and interrupt channels */
        if (p_hcon->intr_cid)
            wiced_bt_l2cap_disconnect_req (p_hcon->intr_cid);

        if (p_hcon->ctrl_cid)
            wiced_bt_l2cap_disconnect_req (p_hcon->ctrl_cid);
    }
    else
    {
        p_hcon->conn_state = HID_CONN_STATE_UNUSED;
    }
}

/*******************************************************************************
**
** Function         hidd_conn_initiate
**
** Description      This function is called by the management to create a connection.
**
** Returns          void
**
*******************************************************************************/
tHID_STATUS hidd_conn_initiate (void)
{
    tHID_CONN   *p_hcon = &hidd_cb.conn;

#if HID_DEV_NORMALLY_CONN == TRUE
    if( p_hcon->conn_state != HID_CONN_STATE_UNUSED )
        return HID_ERR_CONN_IN_PROCESS ;
#endif

    HIDD_TRACE_DBG("hidd_conn_initiate\n");

    p_hcon->ctrl_cid = 0;
    p_hcon->intr_cid = 0;

    /* We are the originator of this connection */
    p_hcon->conn_flags = HID_CONN_FLAGS_IS_ORIG;

    /* Check if L2CAP started the connection process */
    if ((p_hcon->ctrl_cid = wiced_bt_l2cap_connect_req(HID_PSM_CONTROL,
            hidd_cb.host_addr, NULL)) == 0)
    {
        WICED_BT_TRACE("Err: HIDD - Originate failed\n");
        return (HID_ERR_L2CAP_FAILED);
    }
    else
    {
        /* Transition to the next appropriate state. */
        /* waiting for connection confirm on control channel. */
        p_hcon->conn_state = HID_CONN_STATE_CONNECTING_CTRL;

        return (HID_SUCCESS);
    }
}

/*******************************************************************************
**
** Function         hidd_l2c_connected
**
** Description      This function is called when both control and interrupt channels
**                  have been created.
**
** Returns          void
**
*******************************************************************************/
void hidd_l2c_connected( tHID_CONN    *p_hcon )
{
    p_hcon->conn_state = HID_CONN_STATE_CONNECTED;

    /* Set HCI QoS */
    if( hidd_cb.use_qos_flg )
        BTM_SetQoS( hidd_cb.host_addr, &(hidd_cb.hci_qos), NULL ) ;

    hidd_mgmt_process_evt( HOST_CONN_OPEN, hidd_cb.host_addr) ;
#if HID_DEV_PM_INCLUDED == TRUE
    hidd_pm_init();
    hidd_pm_start(); /* This kicks in the idle timer */
#endif
}

/*******************************************************************************
**
** Function         hidd_l2cif_ctrl_connected
**
** Description      HIDD control channel connected
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_ctrl_connected(void *context, BD_ADDR bd_addr, uint16_t l2cap_cid,
        uint16_t peer_mtu)
{
    tHID_CONN    *p_hcon = &hidd_cb.conn;

    HIDD_TRACE_DBG("hidd_l2cif_ctrl_connected (cid=0x%x, peer_mtu=%d)\n", l2cap_cid, peer_mtu);

    /* If host address provided during registration does not match, reject connection */
    if( hidd_cb.virtual_cable && hidd_cb.host_known &&
        memcmp( hidd_cb.host_addr, bd_addr, BD_ADDR_LEN) )
    {
        wiced_bt_l2cap_disconnect_req (l2cap_cid);
        return;
    }

    /* Check we are in the correct state for this (either we are initiating ctl or idle waiting
     * for remote to connect) */
    if ((p_hcon->conn_state != HID_CONN_STATE_CONNECTING_CTRL) &&
        (p_hcon->conn_state != HID_CONN_STATE_UNUSED))
    {
        WICED_BT_TRACE("Err: HIDD - Rcvd CTL L2CAP connection, wrong state: %d\n", p_hcon->conn_state);
        wiced_bt_l2cap_disconnect_req (l2cap_cid);
        return;
    }

    memcpy( hidd_cb.host_addr, bd_addr, BD_ADDR_LEN );
    hidd_cb.host_known = TRUE;
    p_hcon->ctrl_cid = l2cap_cid;
    p_hcon->rem_mtu_size = peer_mtu;

    /* If we are initiator, then initiate interrupt channel */
    if (p_hcon->conn_flags & HID_CONN_FLAGS_IS_ORIG)
    {
        if ((p_hcon->intr_cid = wiced_bt_l2cap_connect_req (HID_PSM_INTERRUPT,
                hidd_cb.host_addr, NULL)) == 0)
        {
            WICED_BT_TRACE("Err: HIDD - INTR Originate failed\n");
            hidd_conn_disconnect();
            return;
        }
    }

    /* Wait for interrupt channel to come up */
    p_hcon->conn_state = HID_CONN_STATE_CONNECTING_INTR;
}

/*******************************************************************************
**
** Function         hidd_l2cif_intr_connected
**
** Description      HIDD interrupt channel connected
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_intr_connected(void *context, BD_ADDR bd_addr, uint16_t l2cap_cid, uint16_t peer_mtu)
{
    tHID_CONN    *p_hcon = &hidd_cb.conn;

    HIDD_TRACE_DBG("hidd_l2cif_intr_connected cid=0x%x, peer_mtu=%d\n", l2cap_cid, peer_mtu);

    /* If host address provided during registration does not match, reject connection */
    if(hidd_cb.virtual_cable && hidd_cb.host_known &&
       memcmp( hidd_cb.host_addr, bd_addr, BD_ADDR_LEN))
    {
        WICED_BT_TRACE("Err: hidd_l2cif_intr_connected unknown HID Host. Disconnecting \n",
            p_hcon->ctrl_cid, p_hcon->conn_state);
        hidd_conn_disconnect();
        return;
    }

    /* Check we are in the correct state for this  */
    if ((p_hcon->ctrl_cid == 0) ||      /* Control channel must already be up */
        (p_hcon->conn_state != HID_CONN_STATE_CONNECTING_INTR))

    {
        WICED_BT_TRACE("Err: hidd_l2cif_intr_connected wrong state ctrl_cid=%d conn_state=%d\n",
            p_hcon->ctrl_cid, p_hcon->conn_state);
        hidd_conn_disconnect();
        return;
    }

    p_hcon->intr_cid   = l2cap_cid;
    p_hcon->rem_mtu_size = peer_mtu;

    hidd_l2c_connected( p_hcon );
}

/*******************************************************************************
**
** Function         hidd_l2cif_disconnect_ind
**
** Description      This function handles a disconnect event from L2CAP. If
**                  requested to, we ack the disconnect before dropping the CCB
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_disconnect_ind (void *context, uint16_t l2cap_cid, wiced_bool_t ack_needed)
{
    tHID_CONN    *p_hcon = &hidd_cb.conn;
    uint16_t     disc_res = HCI_PENDING ;
    uint8_t      evt = HOST_CONN_CLOSE;

    HIDD_TRACE_DBG("hidd_l2cif_disconnect_ind CID: 0x%x\n", l2cap_cid);

    /* Find CCB based on CID */
    if ((p_hcon->ctrl_cid != l2cap_cid) && (p_hcon->intr_cid != l2cap_cid))
    {
        WICED_BT_TRACE("Err: HIDD - Rcvd L2CAP disc, unknown CID: 0x%x\n", l2cap_cid);
        return;
    }

    if (ack_needed)
        wiced_bt_l2cap_disconnect_rsp(l2cap_cid);

    p_hcon->conn_state = HID_CONN_STATE_DISCONNECTING;

    if (l2cap_cid == p_hcon->ctrl_cid)
        p_hcon->ctrl_cid = 0;
    else
        p_hcon->intr_cid = 0;

    if ((p_hcon->ctrl_cid == 0) && (p_hcon->intr_cid == 0))
    {
        if (!ack_needed)
            disc_res = btm_get_acl_disc_reason_code();

        if( disc_res == HCI_ERR_CONNECTION_TOUT || disc_res == HCI_ERR_UNSPECIFIED )
            evt = HOST_CONN_LOST;

        p_hcon->conn_state = HID_CONN_STATE_UNUSED;
        p_hcon->conn_flags = 0;
        hidd_mgmt_process_evt( evt, &disc_res ) ;
#if HID_DEV_PM_INCLUDED == TRUE
        hidd_pm_stop();
#endif
    }
}


/*******************************************************************************
**
** Function         hid_l2cif_disconnect_cfm
**
** Description      This function handles a disconnect confirm event from L2CAP.
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_disconnect_cfm (void *context, uint16_t l2cap_cid, uint16_t result)
{
    uint16_t     disc_res = HCI_SUCCESS;
    tHID_CONN    *p_hcon = &hidd_cb.conn;

    HIDD_TRACE_DBG("hidd_l2cif_disconnect_cfm CID: 0x%x\n", l2cap_cid);

    /* Find CCB based on CID */
    if ((p_hcon->ctrl_cid != l2cap_cid) && (p_hcon->intr_cid != l2cap_cid))
    {
        WICED_BT_TRACE("Err: HIDD - Rcvd L2CAP disc cfm, unknown CID: 0x%x\n", l2cap_cid);
        return;
    }

    if (l2cap_cid == p_hcon->ctrl_cid)
        p_hcon->ctrl_cid = 0;
    else
        p_hcon->intr_cid = 0;

    if ((p_hcon->ctrl_cid == 0) && (p_hcon->intr_cid == 0))
    {
        p_hcon->conn_state = HID_CONN_STATE_UNUSED;
        p_hcon->conn_flags = 0;
        hidd_mgmt_process_evt( HOST_CONN_CLOSE, &disc_res ) ;
#if HID_DEV_PM_INCLUDED == TRUE
        hidd_pm_stop();
#endif
    }
}


/*******************************************************************************
**
** Function         hidd_l2cif_cong_ind
**
** Description      This function handles a congestion status event from L2CAP.
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_cong_ind (void *context, uint16_t l2cap_cid, wiced_bool_t congested)
{
    tHID_CONN    *p_hcon = &hidd_cb.conn;

    HIDD_TRACE_DBG("hidd_l2cif_cong_ind CID: 0x%x  Cong: %d\n", l2cap_cid, congested);

    /* Find CCB based on CID */
    if ((p_hcon->ctrl_cid != l2cap_cid) && (p_hcon->intr_cid != l2cap_cid))
    {
        WICED_BT_TRACE("Err: HIDD - Rcvd L2CAP congestion status, unknown CID: 0x%x\n", l2cap_cid);
        return;
    }


    if (congested)
        p_hcon->conn_flags |= HID_CONN_FLAGS_CONGESTED;
    else
    {
        p_hcon->conn_flags &= ~HID_CONN_FLAGS_CONGESTED;
    }

    hidd_cb.callback(WICED_BT_HIDD_ERR_CONGESTED, (p_hcon->ctrl_cid == l2cap_cid),
            (wiced_bt_hidd_event_data_t*)&congested);
}


/*******************************************************************************
**
** Function         hid_l2cif_data_ind
**
** Description      This function is called when data is received from L2CAP.
**                  if we are the originator of the connection, we are the SDP
**                  client, and the received message is queued up for the client.
**
**                  If we are the destination of the connection, we are the SDP
**                  server, so the message is passed to the server processing
**                  function.
**
** Returns          void
**
*******************************************************************************/
void hidd_l2cif_data_ind (void *context, uint16_t l2cap_cid, uint8_t *p_data, uint16_t buf_len)
{
    tHID_CONN                  *p_hcon = &hidd_cb.conn;
    uint8_t                    ttype, param, rep_type, idle_rate;
    wiced_bt_hidd_event_data_t hid_event_data;
    wiced_bool_t               suspend = WICED_FALSE;

    /* Find CCB based on CID */
    if ((p_hcon->ctrl_cid != l2cap_cid) && (p_hcon->intr_cid != l2cap_cid))
    {
        WICED_BT_TRACE("Err: hidd_l2cif_data_ind unknown CID: 0x%x\n", l2cap_cid);
        wiced_bt_free_buffer(p_data);
        return;
    }

    ttype    = HID_GET_TRANS_FROM_HDR(*p_data);
    param    = HID_GET_PARAM_FROM_HDR(*p_data);
    rep_type = param & HID_PAR_REP_TYPE_MASK;
    p_data++;
    buf_len--;

    switch (ttype)
    {
    case HID_TRANS_CONTROL:
        switch (param)
        {
        case HID_PAR_CONTROL_NOP:
        case HID_PAR_CONTROL_HARD_RESET:
        case HID_PAR_CONTROL_SOFT_RESET:
        case HID_PAR_CONTROL_EXIT_SUSPEND:
            break;

        case HID_PAR_CONTROL_SUSPEND:
            suspend = WICED_TRUE;
#if HID_DEV_PM_INCLUDED == TRUE
            hidd_pm_suspend_evt() ;
#endif
            break;

        case HID_PAR_CONTROL_VIRTUAL_CABLE_UNPLUG:
            hidd_mgmt_process_evt( HID_API_DISCONNECT, NULL ) ;
            hidd_cb.unplug_on = WICED_TRUE;
            break;

        default:
            wiced_bt_free_buffer(p_data);
            wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM ) ;
            return;
        }
        hidd_cb.callback( WICED_BT_HIDD_EVT_CONTROL, param, NULL ) ;
        break;

    case HID_TRANS_GET_REPORT:
        if (param & HID_PAR_GET_REP_BUFSIZE_FOLLOWS)
        {
            STREAM_TO_UINT8 (hid_event_data.get_rep.rep_id, p_data);
            STREAM_TO_UINT16 (hidd_cb.get_rep_buf_sz, p_data);
        }
        else
        {
            STREAM_TO_UINT8 (hid_event_data.get_rep.rep_id, p_data);
            hidd_cb.get_rep_buf_sz = 0;
        }
        hid_event_data.get_rep.rep_type = param & HID_PAR_REP_TYPE_MASK;
        hidd_cb.callback( WICED_BT_HIDD_EVT_GET_REPORT, param, &hid_event_data);
        break;

    case HID_TRANS_SET_REPORT:
        hid_event_data.data.len = buf_len;
        hid_event_data.data.p_data = p_data;
        hidd_cb.callback( WICED_BT_HIDD_EVT_SET_REPORT, rep_type, &hid_event_data );
        break;

    case HID_TRANS_DATA:
        hid_event_data.data.len = buf_len;
        hid_event_data.data.p_data = p_data;
        hidd_cb.callback( WICED_BT_HIDD_EVT_DATA, rep_type, &hid_event_data ) ;
        break;

    case HID_TRANS_DATAC:
        hid_event_data.data.len = buf_len;
        hid_event_data.data.p_data = p_data;
        hidd_cb.callback( WICED_BT_HIDD_EVT_DATC, rep_type, &hid_event_data );
        break;

    case HID_TRANS_GET_PROTOCOL:
        /* Get current protocol */
        hidd_cb.callback( WICED_BT_HIDD_EVT_GET_PROTO, 0, NULL ) ;
        break;

    case HID_TRANS_SET_PROTOCOL:
        hidd_cb.callback( WICED_BT_HIDD_EVT_SET_PROTO, (uint32_t) (param & HID_PAR_PROTOCOL_MASK), NULL ) ;
        break;

    /* for HID 1.0 device only */
    case HID_TRANS_GET_IDLE:
        hidd_cb.callback( WICED_BT_HIDD_EVT_GET_IDLE, 0, NULL ) ;
        break;

    /* for HID 1.0 device only */
    case HID_TRANS_SET_IDLE:
        STREAM_TO_UINT8 (idle_rate, p_data);
        hidd_cb.callback( WICED_BT_HIDD_EVT_SET_IDLE, idle_rate, NULL ) ;
        break;

    default:
        wiced_bt_free_buffer (p_data);
        wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_ERR_UNSUPPORTED_REQ ) ;
        return;
    }

#if HID_DEV_PM_INCLUDED == TRUE
    if( !suspend )
        hidd_pm_start();
#endif

	wiced_bt_free_buffer (p_data);
}

/*******************************************************************************
**
** Function         hidd_conn_snd_data
**
** Description      This function is called to send HIDD data.
**
** Returns          void
**
*******************************************************************************/
tHID_STATUS hidd_conn_snd_data (tHID_SND_DATA_PARAMS *p_param)
{
    tHID_CONN    *p_hcon = &hidd_cb.conn;
    uint8_t      *p_buf;
    uint16_t     buf_len;
    uint16_t     cid;
    uint16_t     rem_mtu;

    /* Safety check */
    if (p_param == NULL)
    {
        WICED_BT_TRACE("Err: hidd_conn_snd_data HID_ERR_INVALID_PARAM\n");
        return HID_ERR_INVALID_PARAM;
    }

    if (p_param->ctrl_ch && p_hcon->ctrl_cid == 0)
    {
        WICED_BT_TRACE("Err: hidd_conn_snd_data Control channel not opened!\n");
        return HID_ERR_NO_CONNECTION;
    }

    if (!p_param->ctrl_ch && p_hcon->intr_cid == 0)
    {
        WICED_BT_TRACE("Err: hidd_conn_snd_data Interrupt channel not opened!\n");
        return HID_ERR_NO_CONNECTION;
    }

    if (p_hcon->conn_flags & HID_CONN_FLAGS_CONGESTED)
    {
        return( HID_ERR_CONGESTED );
    }

    if( (p_param->trans_type == HID_TRANS_DATA) &&
         p_param->ctrl_ch && hidd_cb.get_rep_buf_sz )
    {
        rem_mtu = hidd_cb.get_rep_buf_sz;
        hidd_cb.get_rep_buf_sz = 0;
    }
    else
    {
        rem_mtu = p_hcon->rem_mtu_size;
    }

    /* If buf is null form the HID message with type and param alone */
    if ( p_param->p_data == NULL )
    {
        buf_len = HID_HDR_LEN;
        /* Allocate a Tx HID buffer */
        if((p_buf = (uint8_t *)wiced_bt_get_buffer(buf_len)) == NULL)
            return (HID_ERR_NO_RESOURCES);

        /* Write HID Header in buffer */
        *p_buf = HID_BUILD_HDR(p_param->trans_type, p_param->param);
    }
    /* There is data to send */
    else
    {
        if ((p_param->data_len > (rem_mtu - HID_HDR_LEN)))
        {
            WICED_BT_TRACE("Err: hidd_conn_snd_data Data packet too big for MTU!\n");
            return (HID_ERR_INVALID_PARAM);
        }

        WICED_BT_TRACE("HIDD Tx len:%d\n", p_param->data_len);
        buf_len = p_param->data_len + HID_HDR_LEN;

        /* Allocate a Tx HID buffer */
        if((p_buf = (uint8_t *)wiced_bt_get_buffer(buf_len)) == NULL)
            return (HID_ERR_NO_RESOURCES);

        /* Write HID Header in buffer */
        *p_buf = HID_BUILD_HDR(p_param->trans_type, p_param->param);

        /* Copy the data */
        memcpy(p_buf + HID_HDR_LEN, p_param->p_data, p_param->data_len);
    }

    cid = (p_param->ctrl_ch ? p_hcon->ctrl_cid : p_hcon->intr_cid);

    /* Send the buffer through L2CAP */
    if ((p_hcon->conn_flags & HID_CONN_FLAGS_CONGESTED) ||
        (!wiced_bt_l2cap_data_write(cid, p_buf, buf_len, L2CAP_NON_FLUSHABLE_PACKET)))
    {
        wiced_bt_free_buffer(p_buf);
        return (HID_ERR_CONGESTED);
    }

    wiced_bt_free_buffer(p_buf);

    return (HID_SUCCESS);
}

