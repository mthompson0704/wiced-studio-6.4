var group__sco__api__functions =
[
    [ "wiced_bt_sco_accept_connection", "group__sco__api__functions.html#ga7c8ecc9a1333c1c5b4da60f1dd07d990", null ],
    [ "wiced_bt_sco_create_as_acceptor", "group__sco__api__functions.html#gaba857121768256e41d47bb56eab560b8", null ],
    [ "wiced_bt_sco_create_as_initiator", "group__sco__api__functions.html#gad3c0ea63212a9cca3b0b668d7e392288", null ],
    [ "wiced_bt_sco_output_stream", "group__sco__api__functions.html#ga215e722f5c0d9e98cacfd2b003c69d22", null ],
    [ "wiced_bt_sco_remove", "group__sco__api__functions.html#ga0fba642e9f6ed365e385457be01e4e15", null ],
    [ "wiced_bt_sco_setup_voice_path", "group__sco__api__functions.html#gaaf913710ad791aa3c1f6509524921250", null ],
    [ "wiced_bt_sco_turn_off_pcm_clock", "group__sco__api__functions.html#gae8704d3d0ef9c0ad9d789b6b2db4eda8", null ]
];