var group__eventflags =
[
    [ "THREAD_PRIORITY_MAX", "group__eventflags.html#ga5cf3ec2df1719b7a35fbaf1ea29edd40", null ],
    [ "THREAD_PRIORITY_MIN", "group__eventflags.html#ga196e3293c94df476dd1dd0007013ad14", null ],
    [ "wiced_rtos_deinit_event_flags", "group__eventflags.html#ga689517e95093a9d76ac01635bff0f766", null ],
    [ "wiced_rtos_init_event_flags", "group__eventflags.html#gab3c048d1ada42601b7e0532a68d87692", null ],
    [ "wiced_rtos_set_event_flags", "group__eventflags.html#gaa43000da1e8402b4e427f13cb416c1a6", null ],
    [ "wiced_rtos_wait_for_event_flags", "group__eventflags.html#gae84b75998da2d72e3c03eeceb1a0e2e9", null ]
];