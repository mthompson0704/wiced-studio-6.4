var group__wiced__bt__hid =
[
    [ "HID Device Role (HIDD) over BR/EDR", "group__hidd__api__functions.html", "group__hidd__api__functions" ],
    [ "HID Host Role (HIDH) over BR/EDR", "group__wiced__bt__hidh__api__functions.html", "group__wiced__bt__hidh__api__functions" ],
    [ "HIDD Library API", "group__hidd__functions.html", "group__hidd__functions" ]
];