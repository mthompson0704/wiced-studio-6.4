var searchData=
[
  ['client',['Client',['../group__client__api__functions.html',1,'']]],
  ['clock',['Clock',['../group___c_p_u.html',1,'']]],
  ['client',['Client',['../group__gatt__client__api__functions.html',1,'']]],
  ['common',['Common',['../group__gatt__common__api__functions.html',1,'']]],
  ['callback_20functions',['Callback Functions',['../group__group__ota__fw__upgrade__cback__functions.html',1,'']]],
  ['callback_20functions',['Callback Functions',['../group__l2cap__callbacks.html',1,'']]],
  ['cype',['CyPE',['../group__wpl.html',1,'']]],
  ['cype_20functions',['CyPE Functions',['../group__wpl__api.html',1,'']]],
  ['cype_20power_20event',['CyPE Power Event',['../group__wpl__event.html',1,'']]]
];
