/*
 * Copyright 2016-2021, PumpSpy
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of PumpSpy.
 */

#include <stdlib.h>
#include "wiced.h"
#include "http_wiced.h"
#include "string.h"
#include "math.h"
//#include "wwd_debug.h"
#include "wiced_framework.h"
//#include "wiced_ota_server.h"
//#include "default_wifi_config_dct.h"
//#include "network_config_dct.h"
#include "platform.h"
//#include "aes.h"
#include "waf_platform.h"
//#include "wiced_management.h"

#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "get_deviceid.h"
#include "control_leds.h"
#include "config_button_handler.h"
#include "restful_methods.h"
#include "new_firmware.h"
#include "dns_lookup.h"
#include "initialization.h"
#include "timers.h"
#include "access_points.h"
//#include "set_bor.h"
#include "isI2c/isI2C.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

//#define STOP_APP_HERE
//#define NO_LEDS
//#define ENABLE_SYSTEM_MONITOR
//#define TEST_AES_128
//#define REMOTEDEBUG

// 
// TODO - globally
// it seems like DHCP takes longer, errors out more and provisioning times out
// 
#define FIRMWARE_VERSION 5.10
#define FIRMWARE_VERSION_STRING "5.10 - BBS 2 - 4343 "


#define LED_DRIVER  0xC0         //address of TLC59116 LED DRIVER
#define MODE1       0x00         //Mode 1 register address
#define MODE2       0x01         //Mode 2 register address
#define PWM0        0x02         //PWM0 Brightness control LED0
#define PWM2        0x04         //PWM2 Brightness control LED2
#define PWM3        0x05         //PWM3 Brightness control LED3
#define PWM4        0x06         //PWM4 Brightness control LED4
#define PWM5        0x07         //PWM5 Brightness control LED5
#define PWM6        0x08         //PWM6 Brightness control LED6
#define PWM7        0x09         //PWM7 Brightness control LED7
#define PWM8        0x0A         //PWM8 Brightness control LED8
#define PWM9        0x0B         //PWM9 Brightness control LED9
#define PWM10       0x0C         //PWM10 Brightness control LED10
#define PWM11       0x0D         //PWM11 Brightness control LED11
#define PWM12       0x0E         //PWM12 Brightness control LED12
#define PWM13       0x0F         //PWM13 Brightness control LED13
#define PWM15       0x11         //PWM15 Buzzer output
#define GRPPWM      0x12         //Group duty cycle control register
#define LEDOUT0     0x14         //LED output state 0
#define LEDOUT1     0x15         //LED output state 1
#define LEDOUT2     0x16         //LED output state 2
#define LEDOUT3     0x17         //LEd output state 3
#define TWI_TX      0x84         //write data to TWI
#define MODE1REGSET 0x00         //Set MODE1 reg values
#define MODE2REGSET 0x80         //Set MODE2 reg values
#define OFF         0x00         //SET LED Brightness to zero
#define ON          0xFF         //Set full ON
#define BRIGHSET    0x5F         //SET Brightness of LEDs
#define YELLOW1     0x3F         //SET Color YELLOW green
#define YELLOW2     0x2F         //SET Color YELLOW red
#define BUZZER      0xFF         //Set buzzer to full volume.
#define GRPPWMSET   0xAF         //SET Group Duty Cycle
#define LEDSETON    0XAA         //SET LED output to full ON
#define LEDSETOFF   0x00         //SET LED output to full OFF
#define NUM_I2C_MESSAGE_RETRIES     3
#define DC_PUMP_CYCLE_TIME          6
#define TIMER_TIME (1000)       //Approximately 1 Sec. Adjust to calibrate
#define OV_DELAY 3              //3 second Delay for overvoltage event

#define WICED_I2C_START_FLAG                    (1U << 0)
#define WICED_I2C_REPEATED_START_FLAG           (1U << 1)
#define WICED_I2C_STOP_FLAG                     (1U << 2)

uint8_t buf[50] = {0};
uint32_t length = 0;



/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/** Runs device configuration (if required)
 *
 * @param[in] config  : an array of user configurable variables in configuration_entry_t format.
 *                      The array must be terminated with a "null" entry {0,0,0,0}
 *
 * @return    WICED_SUCCESS
 */
extern wiced_result_t pumpspy_configure_device( const configuration_entry_t* config );


/******************************************************
 *               Static Variable Definitions
 ******************************************************/
//
// serial port control
//
#define RX_BUFFER_SIZE 250
#define TEST_STR "."

wiced_uart_config_t uart_config =
{
    .baud_rate    = 115200,
    .data_width   = DATA_WIDTH_8BIT,
    .parity       = NO_PARITY,
    .stop_bits    = STOP_BITS_1,
    .flow_control = FLOW_CONTROL_DISABLED,
};

wiced_ring_buffer_t rx_buffer;
uint8_t rx_data[RX_BUFFER_SIZE];
char json_input[RX_BUFFER_SIZE];


//
// ac sense
//
#define AC_POWER_OUT_LEVEL 1200
uint16_t ac_sense_level = 0;
wiced_bool_t ac_power_on = WICED_TRUE;

//
// ac pump motor
//
#define AC_MOTOR 1
#define AC_PUMP_CURRENT_RUN_LIMIT 2000
#define AC_PUMP_UNKNOWN          0x0000
#define AC_PUMP_IDLE             0x0001
#define AC_PUMP_RUNNING          0x0002
#define AC_PUMP_RUN_TOO_LONG     0x0004
#define AC_PUMP_OVER_CURRENT     0x0008
#define AC_PUMP_FAILURE          0x0010
uint16_t ac_pump_state = AC_PUMP_UNKNOWN;
uint32_t motor_run_timeout = 300000;
uint16_t ac_pump_current = 0;
//uint16_t motor_run_timeout = 300000; //	"1" : 3000, --> "motor_run_timeout" (V002)
uint16_t primary_pump_current_limit = 15000; //	"10" : 15000 --> primary_pump_current_limit
//
// dc pump & green float
//
#define DC_MOTOR 0
#define DC_MOTOR_SELF_TEST 2
#define DC_PUMP_CURRENT_RUN_LIMIT 500
#define DC_PUMP_UNKNOWN          0x0000
#define DC_PUMP_IDLE             0x0001
#define DC_PUMP_RUNNING          0x0002
#define DC_PUMP_RUN_TOO_LONG     0x0004
#define DC_PUMP_OVER_CURRENT     0x0008
#define DC_PUMP_COOL_DOWN        0x0010
#define DC_PUMP_SELF_TEST		 0x0020
uint16_t dc_pump_state = DC_PUMP_UNKNOWN;
uint32_t dc_motor_run_timeout = 300000;
uint16_t dc_pump_current = 0;
uint16_t dc_pump_current_raw = 0;
uint16_t battery_measure_delay = 150; //	"5" : 150, --> "battery_measure_delay" (V002)
uint16_t motor_current_limit = 20000; //	"9" : 200000 --> motor_current_limit in milliamps, ie 15000 = 15 amps/15000 milliamps.
wiced_bool_t g_float_current = WICED_TRUE;
wiced_bool_t g_float_last = WICED_TRUE;
wiced_bool_t g_float_falling = WICED_FALSE;
wiced_bool_t g_float_rising = WICED_FALSE;
uint64_t dc_pump_start_time = 0;
uint64_t dc_pump_end_time = 1;
uint64_t dc_pump_running_time;
//
// high water - red float
//
wiced_bool_t r_float_current = WICED_TRUE;
wiced_bool_t r_float_last = WICED_TRUE;
wiced_bool_t r_float_falling = WICED_FALSE;
wiced_bool_t r_float_rising = WICED_FALSE;
//wiced_bool_t high_water_detected = WICED_FALSE; // declared in globals.c
//
// battery sensing
//
#define BATTERY_VOLTAGE_UNKNOWN  0x0000
#define BATTERY_VOLTAGE_OK       0x0001
#define BATTERY_VOLTAGE_LOW      0x0002
#define BATTERY_VOLTAGE_CRITICAL 0x0004
#define BATTERY_VOLTAGE_PUMP_OFF 0x0008
uint16_t battery_state = BATTERY_VOLTAGE_UNKNOWN;
uint16_t battery_voltage_unloaded = 13550;
uint16_t battery_voltage_loaded = 13087;
uint16_t battery_level_ok = 12500; //	"2" : 12500, --> "battery_level_ok" 
uint16_t battery_level_low = 11000; //	"3" : 11000, --> "battery_level_low" 
uint16_t battery_level_critical = 10000; //	"4" : 10000, --> "battery_level_critical" 
uint16_t pump_off_voltage = 7000; //	“6” : 7000, --> “pump off voltage” (V002)
uint16_t pump_resume_voltage = 10000; //	“7” : 10000 --> “pump resume voltage” (V002)
//
//
//
wiced_bool_t alarm_muted = WICED_FALSE;
//
// self-test timer
//
uint64_t self_test_timer;
uint16_t hours_between_self_tests = 48; //	"8" : 48 --> hours between self tests
//
// json defines
//
#define JSON_post_bbs_json \
    "{\"deviceid\": %ld%ld, " \
    "\"utcunixtime\": %ld000," \
    "\"json\": \"%s\" }"

#define JSON_post_pump_run \
	"{\\\"motor\\\": %d, \\\"time\\\": %d ,\\\"mamp\\\": %d, \\\"battery_voltage\\\": %d, \\\"loaded\\\": %d}" 

#define JSON_post_ac_power \
	"{\\\"ac_power\\\": %d}" 

#define JSON_post_high_water \
	"{\\\"high_water\\\": %d}" 

#define JSON_post_motor_hc \
	"{\\\"motor_hc\\\": %d}" 

#define JSON_post_dc_highamps \
	"{\\\"dc_highamps\\\": %d}" 

#define JSON_post_motor_fail \
	"{\\\"motor_fail\\\": %d}" 

#define GET_bbs_parameters \
    "GET /bbs_parameters/%ld%ld HTTP/1.1\r\n" \
    "Host: %s:%d \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

/******************************************************
 *               Function Definitions
 ******************************************************/

//
// float switch IRQ handlers
//
/*
void r_float_falling_handler( void* arg )
{
    WPRINT_APP_INFO(("r_float falling\r\n"));
	r_float_falling = WICED_TRUE;
	r_float_rising = WICED_FALSE;
	uint64_t server_time;
	uint64_t r_float_time;
	wiced_bool_t r_float_b = wiced_gpio_input_get( R_FLOAT); // ? WICED_FALSE : WICED_TRUE;
    if (r_float_b == WICED_TRUE)
    {
        WPRINT_APP_INFO(("r_float_engaged\r\n"));
        //
        // measure button press time
        //
        wiced_time_get_utc_time_ms(&server_time);
        while (r_float_b == WICED_TRUE)
        {
            r_float_b = wiced_gpio_input_get( R_FLOAT ) ? WICED_FALSE : WICED_TRUE;
            wiced_time_get_utc_time_ms(&r_float_time);
		}
//        WPRINT_APP_INFO(("not r_float_engaged %ld \r\n", r_float_time-server_time));
	}
	else
	{
		WPRINT_APP_INFO(("r_float is high - nothing\r\n"));
	}
	
}
void r_float_rising_handler( void* arg )
{
    WPRINT_APP_INFO(("r_float risign\r\n"));
	r_float_falling = WICED_FALSE;
	r_float_rising = WICED_TRUE;
}
void g_float_falling_handler( void* arg )
{
    WPRINT_APP_INFO(("g_float falling\r\n"));
	g_float_falling = WICED_TRUE;
	g_float_rising = WICED_FALSE;
}
void g_float_rising_handler( void* arg )
{
    WPRINT_APP_INFO(("g_float risign\r\n"));
	g_float_falling = WICED_FALSE;
	g_float_rising = WICED_TRUE;
}
*/

void alarm_mute_handler( void* arg )
{
	//
	// set alarm_muted flag
	//
    WPRINT_APP_INFO(("alarm_mute falling\r\n"));
	alarm_muted = WICED_TRUE;
}

wiced_result_t get_parameters()
{
	//
	// *TODO 2
	// set up the base current parameters based on the 1st 10 runs 
	//
    char *p;
    char *pos;
	int i;
/*	
	PARAMETERS 
	{
	"1" : 3000, --> "motor_run_timeout" (V002)
	"2" : 12500, --> "battery_level_ok" 
	"3" : 11000, --> "battery_level_low" 
	"4" : 10000, --> "battery_level_critical" 
	"5" : 15, --> "battery_measure_delay" (V002)
	“6” : 7000, --> “pump off voltage” (V002)
	“7” : 10000 --> “pump resume voltage” (V002)
	"8" : 48 --> hours between self tests
	"9" : 200000 --> motor_current_limit in milliamps, ie 15000 = 15 amps/15000 milliamps.
	"10" : 15000 --> primary_pump_current_limit
	}

	{"p1":3000,"p2":11750,"p3":11750,"p4":11250,"p5":15,"p6":7000,"p7":10000,"p8":48,"p9":20000,"p10":15000}

*/
    clear_post_buffers();
    WPRINT_APP_INFO(("GET /bbs_parameters \r\n"));
    sprintf(post, GET_bbs_parameters, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//        WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
        if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
		{
        	return WICED_ERROR;
		}
		//
		//    scan buffer for "p1" motor_run_timeout 
		//
		pos = strstr((char*)buffer, "\"p1\":");
//		WPRINT_APP_INFO( ( "p1 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_run_timeout = i*100;
//			WPRINT_APP_INFO( ( "motor_run_timeout |%d| \r\n", (int)motor_run_timeout ) );
		}
		else
		{
			motor_run_timeout = 30000;
		}
		//
		//    scan buffer for "p2" battery_level_ok 
		//
		pos = strstr((char*)buffer, "\"p2\":");
//		WPRINT_APP_INFO( ( "p2 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_ok = i;
//			WPRINT_APP_INFO( ( "battery_level_ok |%d| \r\n", (int)battery_level_ok ) );
		}
		else
		{
			battery_level_ok = 11750;
		}
		//
		//    scan buffer for "p3" battery_level_low 
		//
		pos = strstr((char*)buffer, "\"p3\":");
//		WPRINT_APP_INFO( ( "p3 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_low = i;
//			WPRINT_APP_INFO( ( "battery_level_low |%d| \r\n", (int)battery_level_low ) );
		}
		else
		{
			battery_level_low = 11750;
		}
		//
		//    scan buffer for "p4" battery_level_critical
		//
		pos = strstr((char*)buffer, "\"p4\":");
//		WPRINT_APP_INFO( ( "p4 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_level_critical = i;
//			WPRINT_APP_INFO( ( "battery_level_critical |%d| \r\n", (int)battery_level_critical ) );
		}
		else
		{
			battery_level_critical = 11250;
		}
		//
		//    scan buffer for "p5" battery_measure_delay
		//
		pos = strstr((char*)buffer, "\"p5\":");
//		WPRINT_APP_INFO( ( "p5 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			battery_measure_delay = i*1000;
//			WPRINT_APP_INFO( ( "battery_measure_delay |%d| \r\n", (int)battery_measure_delay ) );
		}
		else
		{
			battery_measure_delay = 15000;
		}
		//
		//    scan buffer for "p6" pump_off_voltage
		//
		pos = strstr((char*)buffer, "\"p6\":");
//		WPRINT_APP_INFO( ( "p6 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			pump_off_voltage = i;
//			WPRINT_APP_INFO( ( "pump_off_voltage |%d| \r\n", (int)pump_off_voltage ) );
		}
		else
		{
			pump_off_voltage = 7000;
		}
		//
		//    scan buffer for "p7" pump_resume_voltage
		//
		pos = strstr((char*)buffer, "\"p7\":");
//		WPRINT_APP_INFO( ( "p7 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			pump_resume_voltage = i;
//			WPRINT_APP_INFO( ( "pump_resume_voltage |%d| \r\n", (int)pump_resume_voltage ) );
		}
		else
		{
			pump_resume_voltage = 10000;
		}
		//
		//    scan buffer for "p8" hours_between_self_tests
		//
		pos = strstr((char*)buffer, "\"p8\":");
//		WPRINT_APP_INFO( ( "p8 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			hours_between_self_tests = i;
//			WPRINT_APP_INFO( ( "hours_between_self_tests |%d| \r\n", (int)hours_between_self_tests ) );
		}
		else
		{
			hours_between_self_tests = 48;
		}
		//
		//    scan buffer for "p9" motor_current_limit - dc/backup pump
		//
		pos = strstr((char*)buffer, "\"p9\":");
//		WPRINT_APP_INFO( ( "p9 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 5;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_current_limit = i;
//			WPRINT_APP_INFO( ( "motor_current_limit |%d| \r\n", (int)motor_current_limit ) );
		}
		else
		{
			motor_current_limit = 20000;
		}
		//
		//    scan buffer for "p10" primary_pump_current_limit - ac
		//
		pos = strstr((char*)buffer, "\"p10\":");
//		WPRINT_APP_INFO( ( "p10 pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 6;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_current_limit = i;
//			WPRINT_APP_INFO( ( "primary_pump_current_limit |%d| \r\n", (int)primary_pump_current_limit ) );
		}
		else
		{
			primary_pump_current_limit = 15000;
		}
    }
    //
    // return
    //
    return result;
}

uint16_t get_ac_pump_current()
{
	// rev2 bd curve
	// 0aac=85, (dry)4aac=820
	// AmpsAC=4.6*adc - 389
	//
	// rev3 board curve
	// y = 7.8586x
	//
	uint16_t adc_sample = 0;
	uint32_t ac_pump_current_return = 0;
	//
	// read ac_sense a2d
	// average 30 data points
	//
	result = wiced_adc_take_sample( AC_CURRENT_SENSE, &adc_sample );
//	ac_pump_current_return = (uint16_t)((4.6*adc_sample)-389);
	ac_pump_current_return = (uint16_t)(7.9*adc_sample);
	//
	// return the value
	//
	return (ac_pump_current_return);

}

uint16_t get_ac_sense_level()
{
	//
	uint16_t adc_sample = 0;
	//
	// read a2d
	//
	result = wiced_adc_take_sample( AC_SENSE, &adc_sample );
	//
	// return the value
	//
	return (adc_sample);

}


uint16_t get_dc_pump_current()
{
	// 
	// add curent sense conversion 
	// 0adc = 0, (dry)4adc=40
	// AmpsDC=62.5*adc
	//
	// change curve for rev 3
	// AmpsDC = 6.5*adc - 19
	//
	uint16_t dc_pump_current_raw = 0;
	uint32_t dc_pump_current_return = 0;
	//
	// read ac_sense a2d
	// average 30 data points
	//
	result = wiced_adc_take_sample( CURRENT_SENSE, &dc_pump_current_raw );
//	dc_pump_current_return = (uint16_t)(62.5*dc_pump_current_raw);
	dc_pump_current_return = (uint16_t)((6.5*dc_pump_current_raw) - 19);
	//
	// return the value
	//
	return (dc_pump_current_return);
}

uint16_t measure_v_batt()
{
	//
	// insert the real conversion
	// 0vdc = 805 13vdc=3415
	// vac = 5*adc - 4009
	//
	// rev3 board
	// mVoltsAC = 4.6*adc - 2535
	//
	uint16_t raw_v_batt;
	uint16_t return_v_batt;
	//
	// measurev_sense
	// measure unloaded voltage - after 10 second cool down
	//
	result = wiced_adc_take_sample( V_SENSE, &raw_v_batt );
	//
	// look up real value
	//
//	return_v_batt = (uint16_t)(5*raw_v_batt) - 4009;
	return_v_batt = (uint16_t)((4.6*raw_v_batt) - 2535);
	//
	// return real value
	//
	return(return_v_batt);

}

void test_mode()
{
	//
	// *TODO 2 - improvement 
	// menu, user input, for e.g. turn on/off dc motor, turn on/off charger
	//
//    wiced_bool_t test_mode_value_changed = WICED_TRUE;
    wiced_bool_t wifi_passed = WICED_TRUE;
//    wiced_bool_t last_config_button_passed = WICED_FALSE;
	wiced_bool_t config_button_enabled = WICED_FALSE;
	wiced_bool_t r_float_enabled = WICED_FALSE;
	wiced_bool_t g_float_enabled = WICED_FALSE;
	wiced_bool_t alarm_mute_enabled = WICED_FALSE;
	uint16_t ac_current_sense = 0;
	uint16_t v_sense = 0;
	uint16_t current_sense = 0;
	uint16_t ac_sense = 0;
	wiced_bool_t charger_on_off = WICED_FALSE;
	wiced_bool_t relay_on_off = WICED_FALSE;
	wiced_bool_t led1_on_off = WICED_FALSE;
	int loop_count = 0;
	//
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// reset Config Flag - boot to provisioning mode next time
	//
	if (dct.NewConfigurationFlag != 0x0F0F0F0F)
	{
		dct.NewConfigurationFlag = 0x0F0F0F0F;
		write_app_dct();
		//
		// reset the DCT
		//
		configure_app_dct();
		configure_network_dct();
		configure_wifi_dct();
	}
	//
	// loop here until power off
	//
	while (1)
	{
		//
		// display test parameters
		// Battery voltage, AC current, DC current, Config button check, Flash wifi led (green, red).
		//
	//	if (test_mode_value_changed == WICED_TRUE)
	//	{
			WPRINT_APP_INFO(("\033[2J\033[1;1H"));
			WPRINT_APP_INFO(( "\r\n\r\n" ));
			WPRINT_APP_INFO(( "DeviceID           = %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
			WPRINT_APP_INFO(( "Firmware Ver.      = %s\r\n", FIRMWARE_VERSION_STRING));
			WPRINT_APP_INFO(( "Wi-Fi Scan         = %d\r\n", wifi_passed ));
			WPRINT_APP_INFO(( "Wi-Fi Memory       = %d\r\n", check_wifi_dct() ));
			WPRINT_APP_INFO(( "Config Button      = %d\r\n", config_button_enabled));
			WPRINT_APP_INFO(( "Red Float          = %d\r\n", r_float_enabled ));
			WPRINT_APP_INFO(( "Green Float        = %d\r\n", g_float_enabled ));
			WPRINT_APP_INFO(( "Alarm Mute         = %d\r\n", alarm_mute_enabled ));
			WPRINT_APP_INFO(( "AC_Current_Sense   = %d\r\n", ac_current_sense ));
			WPRINT_APP_INFO(( "V_Sense            = %d\r\n", v_sense ));
			WPRINT_APP_INFO(( "Current_Sense      = %d\r\n", current_sense ));
			WPRINT_APP_INFO(( "AC_Sense           = %d\r\n", ac_sense ));
			WPRINT_APP_INFO(( "Charger            = %d\r\n", charger_on_off ));
			WPRINT_APP_INFO(( "Relay              = %d\r\n", relay_on_off ));
			WPRINT_APP_INFO(( "LED1               = %d\r\n", led1_on_off ));			
			WPRINT_APP_INFO(( "loop_count         = %d\r\n", loop_count ));
		//
		// config button
		//
		/*
		if ((config_button_passed == WICED_TRUE) && (last_config_button_passed == WICED_FALSE))
		{
//			test_mode_value_changed = WICED_TRUE;
			last_config_button_passed = config_button_passed;
		}
		*/
	    config_button_enabled = wiced_gpio_input_get( CONFIG_BUTTON ) ? WICED_FALSE : WICED_TRUE;
		//
		// red float
		//
	    r_float_enabled = wiced_gpio_input_get( R_FLOAT ) ? WICED_FALSE : WICED_TRUE;
		//
		// green float
		//
	    g_float_enabled = wiced_gpio_input_get( G_FLOAT ) ? WICED_FALSE : WICED_TRUE;
		//
		// alarm mute
		//
	    alarm_mute_enabled = wiced_gpio_input_get( ALARM_MUTE ) ? WICED_FALSE : WICED_TRUE;
		//
		// ac_current_sense
		//
		ac_current_sense = get_ac_pump_current();
		//
		// v_sense
		//
		v_sense = measure_v_batt();
		//
		// current_sense
		//
		current_sense = get_dc_pump_current();
		//
		// ac_sense
		//
		ac_sense = get_ac_sense_level();
		//
		// charger
		//
//		result = wiced_adc_take_sample( CHARGER, &adc_sample0 );
//		charger = adc_sample0;
		charger_on_off = WICED_FALSE;
		//
		//  relay
		//
		if ((loop_count % 50) == 0)
		{
			if (relay_on_off == WICED_TRUE)
			{
				wiced_gpio_output_low(RELAY);
				relay_on_off = WICED_FALSE;
			}
			else
			{
				wiced_gpio_output_high(RELAY);
				relay_on_off = WICED_TRUE;
			}
		}
		//
		//  led1
		//
		if ((loop_count % 10) == 0)
		{
			if (led1_on_off == WICED_TRUE)
			{
				wiced_gpio_output_low(LED1);
				led1_on_off = WICED_FALSE;
			}
			else
			{
				wiced_gpio_output_high(LED1);
				led1_on_off = WICED_TRUE;
			}
		}
		//
		// delay
		//
//		loop_count = 0;
//		while (loop_count++ < 10)
//		{
			wiced_rtos_delay_milliseconds( 100 );
			loop_count++;
//			i++;
//		}
	}
}

void post_bbs_json(uint64_t utcunixtime)
{
	clear_post_buffers();
	WPRINT_APP_INFO( ( "POST /bbs_json \r\n"));
	sprintf(json, JSON_post_bbs_json, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), (long unsigned int)(utcunixtime/1000), json_input);
    WPRINT_APP_INFO(("POST /bbs_json |%s| len=%d\r\n", json, strlen(json)));
	sprintf(post, POST_STRING, "/bbs_json", dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(json), json);
//	    WPRINT_APP_INFO(("sprintf done\r\n"));
//	    WPRINT_APP_INFO(( post )); //This result looks correct
	post_the_post();
}

void check_ac_pump_high_current()
{
	//
	// check for high current
	//
	if (pump_peak_current > primary_pump_current_limit)
	{
		WPRINT_APP_INFO(("pump current too high - posting data\r\n"));

		sprintf(json_input, JSON_post_motor_hc, 1);
		post_bbs_json(pump_end_time);

		ac_pump_state |= AC_PUMP_OVER_CURRENT;

	}

}

void monitor_ac_pump()
{
	//
	// 
	//
	ac_pump_current = get_ac_pump_current();
	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("ac_pump_current %d\r\n", ac_pump_current));
	//
	// if ac pump idle or stopping  
	//
	if (ac_pump_current < AC_PUMP_CURRENT_RUN_LIMIT)
	{
//			WPRINT_APP_INFO(("pump idle\r\n"));

		if ((ac_pump_state & AC_PUMP_RUNNING) > 0)
//		if (pump_is_running == WICED_TRUE)
		{
			//
			// check if pump ran long enough
			//
			if ((pump_end_time - pump_start_time) >= 250)
			{
				WPRINT_APP_INFO(("pump ran %d time with %d current\r\n", (uint16_t)pump_end_time, pump_peak_current));
				//
				// check curent too high
				// 
				check_ac_pump_high_current();
				//
				// POST /bbs_json
				//
				// {\"motor\":1,\"time\":75,\"mamp\":7500,\"battery_voltage\":13550,\"loaded\":13087}" }

				sprintf(json_input, JSON_post_pump_run, AC_MOTOR, (unsigned int)(pump_end_time-pump_start_time)/100, pump_peak_current, (unsigned int) battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(pump_start_time);
			}
//			pump_is_running = WICED_FALSE;
			ac_pump_state = AC_PUMP_IDLE; 
		}
	}
	else
	{

		if ((ac_pump_state & AC_PUMP_RUNNING) == 0)
//		if (pump_is_running == WICED_FALSE)
		{
			result = wiced_time_get_utc_time_ms  (&pump_start_time);
//			    WPRINT_APP_INFO( ( "get start time result %d\r\n", result ) );
			pump_end_time = pump_start_time;
			pump_peak_current = 0;
//			pump_is_running = WICED_TRUE;
			ac_pump_state = ac_pump_state | AC_PUMP_RUNNING;
			WPRINT_APP_INFO(("pump started %d time with %d current %d\r\n", (uint16_t)pump_start_time, ac_pump_current, ac_pump_state));
		}
		else
		{
			//
			// measure run time and current
			//
			result = wiced_time_get_utc_time_ms  (&pump_end_time);
//			    WPRINT_APP_INFO( ( "get end time result %d\r\n", result ) );
			ac_pump_current = get_ac_pump_current();
			if ((server_time % 1000) == 0)
				WPRINT_APP_INFO(("AC Pump Running, %lu, %d, %d\r\n", (unsigned long)pump_end_time, ac_pump_current, ac_pump_state ));
			//
			// set peak current after 2 seconds
			//

			if ((pump_end_time - pump_start_time) >= 2000)
			{
				if (pump_peak_current < ac_pump_current)
					pump_peak_current = ac_pump_current;
			}
			else
			{
				pump_peak_current = 0;
			}
			//
			// check for running too long
			//
			if ((pump_end_time - pump_start_time) > motor_run_timeout)
			{
				WPRINT_APP_INFO(("pump running too long - posting data\r\n"));
				//
				// check curent too high
				// 
				check_ac_pump_high_current();
				//
				// POST /pump_outlet_cycles
				//
				sprintf(json_input, JSON_post_pump_run, AC_MOTOR, (unsigned int)(pump_end_time - pump_start_time)/100, pump_peak_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(pump_start_time);
//				post_pump_cycles(pump_end_time, (pump_end_time - pump_start_time), pump_peak_current);
				pump_start_time = pump_end_time;
				ac_pump_state = ac_pump_state | AC_PUMP_RUN_TOO_LONG;
			}
		}
	}
}

void turn_on_dc_pump()
{
	//
	// set flags
	//
	if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
		WPRINT_APP_INFO(("turning on dc pump self test\r\n"));
	else
		WPRINT_APP_INFO(("turning on dc pump green float\r\n"));
	dc_pump_state = dc_pump_state | DC_PUMP_RUNNING;
	g_float_falling = WICED_FALSE;
	result = wiced_time_get_utc_time_ms  (&dc_pump_start_time);
	dc_pump_running_time = dc_pump_start_time;
	//
	// turn on the pump 
	//
	wiced_gpio_output_high(RELAY);
}

int monitor_high_water()
{
	//
	//
	uint64_t high_water_event_time = 0;
//	wiced_bool_t b;

    r_float_current = wiced_gpio_input_get( R_FLOAT ); // ? WICED_FALSE : WICED_TRUE;
//	if ((server_time % 1000) == 0)
//		WPRINT_APP_INFO(("r_float_current %d r_float_last %d r_float_falling %d r_float_rising %d\r\n", r_float_current, r_float_last, r_float_falling, r_float_rising ));

	if ((r_float_current == WICED_FALSE) && (r_float_last == WICED_TRUE))
		r_float_falling = WICED_TRUE;
	else if ((r_float_current == WICED_TRUE) && (r_float_last == WICED_FALSE))
		r_float_rising = WICED_TRUE;
	r_float_last = r_float_current;

	if ((r_float_falling == WICED_TRUE) && (r_float_rising == WICED_FALSE))
	{
		//
		// some temporary filtering
		//
/*		for (int i=0; i<5; i++)
		{
			b = wiced_gpio_input_get( R_FLOAT );
			WPRINT_APP_INFO((" b = %d\r\n", b));
			if (b == WICED_TRUE)
			{
				r_float_falling = WICED_FALSE;
				return(0);
			}
			wiced_rtos_delay_milliseconds(100);
		}
*/		//
		// high water engaged
		//
		r_float_falling = WICED_FALSE;
		high_water_detected = WICED_TRUE;
		sprintf(json_input, JSON_post_high_water, 1);
		result = wiced_time_get_utc_time_ms  (&high_water_event_time);
		post_bbs_json(high_water_event_time);
		//
		// dc pump
		//
		if ((dc_pump_state & DC_PUMP_RUNNING ) == 0)
		{
			turn_on_dc_pump();
		}
	}
	else if ((r_float_falling == WICED_FALSE) && (r_float_rising == WICED_TRUE))
	{
		//
		// high water off
		//
		r_float_rising = WICED_FALSE;
		high_water_detected = WICED_FALSE;
		sprintf(json_input, JSON_post_high_water, 0);
		result = wiced_time_get_utc_time_ms  (&high_water_event_time);
		post_bbs_json(high_water_event_time);
	}
	return(0);
}

void check_dc_pump_high_current()
{
	//
	// TODO - this is going to happen over and over again
	// pump over current
	//
	if (dc_pump_current > motor_current_limit)
	{
		WPRINT_APP_INFO(("dcpump current over limit  - posting data\r\n"));

		sprintf(json_input, JSON_post_dc_highamps, 1);
		post_bbs_json(pump_end_time);

		dc_pump_state |= DC_PUMP_OVER_CURRENT;

	}

}

int monitor_dc_pump()
{
	//
	//
	uint8_t motor = 0;
//	wiced_bool_t b;
	//
	// green float
	//
    g_float_current = wiced_gpio_input_get( G_FLOAT ); //? WICED_FALSE : WICED_TRUE;
	if ((g_float_current == WICED_FALSE) && (g_float_last == WICED_TRUE))
		g_float_falling = WICED_TRUE;
	else if ((g_float_current == WICED_TRUE) && (g_float_last == WICED_FALSE))
		g_float_rising = WICED_TRUE;
	g_float_last = g_float_current;
	//
	// if falling and !rising 
	// turn on motor
	//
	if ((g_float_falling == WICED_TRUE) && (g_float_rising == WICED_FALSE))
	{
		//
		// some temporary filtering
		//
/*		for (int i=0; i<5; i++)
		{
			b = wiced_gpio_input_get( G_FLOAT );
			WPRINT_APP_INFO((" b = %d\r\n", b));
			wiced_rtos_delay_milliseconds(100);
			if (b == WICED_TRUE)
			{
				g_float_falling = WICED_FALSE;
				return(0);
			}
			wiced_rtos_delay_milliseconds(100);
		}
*/		turn_on_dc_pump();
		//
		// check if ac power on
		//
		sprintf(json_input, JSON_post_motor_fail, 1);
		post_bbs_json(dc_pump_start_time);
		ac_pump_state |= AC_PUMP_FAILURE;
	}
	else if ((g_float_falling == WICED_FALSE) && (g_float_rising == WICED_TRUE))
	{
		WPRINT_APP_INFO(("g_float_rising \r\n"));
		g_float_rising = WICED_FALSE;
	}
	else 
	{
		//
		// check if the dc motor is running
		//
		if ((dc_pump_state & DC_PUMP_RUNNING) > 0)
		{
			//
			// TODO -test this
			// change this with new board
			// check if float still engaged 
			//
			if (wiced_gpio_input_get( G_FLOAT ) == WICED_FALSE)
			{
				result = wiced_time_get_utc_time_ms  (&dc_pump_running_time);
			}
			//
			// current
			//
			dc_pump_current = get_dc_pump_current();
			//
			// loaded v_bat
			//
			battery_voltage_loaded = measure_v_batt();
			//
			// check how long pump has run
			//
			result = wiced_time_get_utc_time_ms  (&dc_pump_end_time);

			//
			// pump too long
			//
			if ((dc_pump_end_time - dc_pump_running_time) > motor_run_timeout)
			{
				WPRINT_APP_DEBUG(("dc pump running too long \r\n"));
				//
				// check high current
				//
				check_dc_pump_high_current();
				//
				// POST /bbs_jason & check flag 
				//
				if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
					motor = DC_MOTOR_SELF_TEST;
				else
					motor = DC_MOTOR;
				sprintf(json_input, JSON_post_pump_run, motor, (unsigned int)(dc_pump_end_time - dc_pump_running_time)/100, (unsigned int)dc_pump_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(dc_pump_start_time);
				dc_pump_start_time = dc_pump_running_time;

			}
			//
			// pump cycle complete
			//
			if ((dc_pump_end_time - dc_pump_running_time) > 10000)
			{
				WPRINT_APP_DEBUG(("turning off dc pump \r\n"));
				dc_pump_state = DC_PUMP_IDLE;
				//
				// turn of the pump 
				//
				wiced_gpio_output_low(RELAY);
				//
				// check high current
				//
				check_dc_pump_high_current();
				//
				// POST /bbs_jason & check flag 
				//
				if ((dc_pump_state & DC_PUMP_SELF_TEST) > 0)
					motor = DC_MOTOR_SELF_TEST;
				else
					motor = DC_MOTOR;
				sprintf(json_input, JSON_post_pump_run, motor, (unsigned int)(dc_pump_end_time - dc_pump_start_time)/100, (unsigned int)dc_pump_current, (unsigned int)battery_voltage_unloaded, (unsigned int)battery_voltage_loaded);
				post_bbs_json(dc_pump_start_time);

			}
			if ((server_time % 1000) == 0)
				WPRINT_APP_INFO(("DC Pump Running, %lu, %d, %d\r\n", (unsigned long)dc_pump_end_time, dc_pump_current, dc_pump_state ));
		}
	}
	return(0);
}

void monitor_ac_sense()
{
	uint64_t power_event_time = 0;

	ac_sense_level = get_ac_sense_level();
	if ((ac_sense_level < AC_POWER_OUT_LEVEL) && (ac_power_on == WICED_TRUE))
	{
		//
		// power out
		//
		ac_power_on = WICED_FALSE;
		sprintf(json_input, JSON_post_ac_power, 0);
		result = wiced_time_get_utc_time_ms  (&power_event_time);
		post_bbs_json(power_event_time);
		//
		// turn off charger
		//
		wiced_gpio_output_low(CHARGER);
	}
	else if ((ac_sense_level > AC_POWER_OUT_LEVEL) && (ac_power_on == WICED_FALSE))
	{
		//
		// power on
		//
		ac_power_on = WICED_TRUE;
		sprintf(json_input, JSON_post_ac_power, 1);
		result = wiced_time_get_utc_time_ms(&power_event_time);
		post_bbs_json(power_event_time);
		//
		// turn on charger
		//
		if ((battery_state & BATTERY_VOLTAGE_PUMP_OFF) == 0)
			wiced_gpio_output_high(CHARGER);
		else
			wiced_gpio_output_low(CHARGER);
	}

}


void monitor_v_batt()
{
	//
	// look up real value
	//
	if ((server_time - dc_pump_end_time) > battery_measure_delay)
		battery_voltage_unloaded = measure_v_batt();
	//
	// check unloaded v_batt state
	//
	if (battery_voltage_unloaded > battery_level_low)
	{
		battery_state = BATTERY_VOLTAGE_OK;
	}
	else if ((battery_voltage_unloaded <= battery_level_low) && (battery_voltage_unloaded > battery_level_critical))
	{
		battery_state = BATTERY_VOLTAGE_LOW;
	}
	else if ((battery_voltage_unloaded <= battery_level_critical))
	{
		battery_state = BATTERY_VOLTAGE_CRITICAL;
	}
	//
	// set pump run flags
	// *TODO
	// this isn't quite right - down ok, up no quite
	// 	
	if (battery_voltage_unloaded < pump_off_voltage)
	{
		battery_state |= BATTERY_VOLTAGE_PUMP_OFF;
	}
	else if ((battery_voltage_unloaded > pump_resume_voltage))
	{
		battery_state &= ~BATTERY_VOLTAGE_PUMP_OFF;
	}

	if ((server_time % 1000) == 0)
		WPRINT_APP_INFO(("battery_voltage_unloaded %d battery_state %04X\r\n", battery_voltage_unloaded, battery_state));

}

void check_self_test()
{
	// 
	// run every self_test_timer number of hours
	//
	// check the timer
	//
	if ((server_time - self_test_timer) > (hours_between_self_tests*60*60*1000))
	{
		WPRINT_APP_INFO(("running self test @ server_time %ld%ld self_test_timer %ld%ld \r\n", (unsigned long int)(server_time/10000000), (unsigned long int)(server_time%10000000),(unsigned long int)(self_test_timer/10000000), (unsigned long int)(self_test_timer%10000000) ));
		//
		// turn on motor for 10 seconds
		// set self-test flag
		//
		dc_pump_state |= DC_PUMP_SELF_TEST;
		turn_on_dc_pump();
		self_test_timer = server_time;
	}

}


void write_i2c_byte(uint8_t addr, uint8_t data)
{
    uint8_t dout[5] = {0};
    uint32_t lngth =2;
    dout[0] = addr;
    dout[1] = data;
    result = ism_i2c_write( (uint8_t*)&dout, lngth );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("write_i2c_byte error %d\r\n", result));
}

void leds_buzzer_init()
{

	WPRINT_APP_INFO(("leds_buzzer_init\r\n"));

	WPRINT_APP_INFO(("calling ism_i2c_init\r\n"));
	int i = 0;
    result = ism_i2c_init();
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));

	WPRINT_APP_INFO(("calling software reset\r\n"));
	i=0;
	buf[i++] = 0xD6;
	buf[i++] = 0xA5;
	buf[i++] = 0x5A;
	length = i;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c software reset error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("i2c software reset write success %d\r\n", result));


	write_i2c_byte(MODE1, MODE1REGSET);
	write_i2c_byte(MODE2, MODE2REGSET);	
	write_i2c_byte(PWM2, LEDSETON);	
	write_i2c_byte(PWM3, LEDSETON);
	write_i2c_byte(PWM4, LEDSETON);
	write_i2c_byte(PWM5, LEDSETON);
	write_i2c_byte(PWM6, LEDSETON);
	write_i2c_byte(PWM7, LEDSETON);
	write_i2c_byte(PWM8, LEDSETON);
	write_i2c_byte(PWM9, LEDSETON);
	write_i2c_byte(PWM10, LEDSETON);
	write_i2c_byte(PWM11, LEDSETON);
	write_i2c_byte(PWM12, LEDSETON);
	write_i2c_byte(PWM13, LEDSETON);
	write_i2c_byte(PWM15, LEDSETOFF);
	write_i2c_byte(GRPPWM, GRPPWMSET);
	write_i2c_byte(LEDOUT0, LEDSETON);
	write_i2c_byte(LEDOUT1, LEDSETON);
	write_i2c_byte(LEDOUT2, LEDSETON);
	write_i2c_byte(LEDOUT3, LEDSETON);

//    ism_i2c_deinit();
}

wiced_bool_t set_leds_buzzer()
{
	//
	// *TODO all
	// fix the blanking out
	//
	// n/c         - 0x02
	// n/c         - 0x03
	// led1 - WiFi - 0x04 - PWM2  - Red
	// led1 - WiFi - 0x05 - PWM3  - Green 
	// led2 - batt - 0x06 - PWM4  - Red
	// led2 - batt - 0x07 - PWM5  - Green
	// led3 - pwr  - 0x08 - PWM6  - Red
	// led3 - pwr  - 0x09 - PWM7  - Green
	// led4 - mpmp - 0x0A - PWM8  - Red
	// led4 - mpmp - 0x0B - PWM9  - Green
	// led5 - bpmp - 0x0C - PWM10 - Red
	// led5 - bpmp - 0x0D - PWM11 - Green
	// led6 - hwtr - 0x0E - PWM12 - Red
	// led6 - hwtr - 0x0F - PWM13 - Green
	// n/c         - 0x10 - PWM14        
	// buzzer      - 0x11 - PWM15
 	// 

	if ((server_time % 1000) != 0)
		return WICED_FALSE;

	WPRINT_APP_INFO(("set_leds_buzzer\r\n"));

    ism_i2c_init();


	//
	// wi-fi led
	//
	switch (system_state)
	{
		case STATE_INITIALIZING:
			write_i2c_byte(PWM2, LEDSETON); //red
			write_i2c_byte(PWM3, LEDSETOFF);
			break; 
		case STATE_RUNNING:
			write_i2c_byte(PWM2, LEDSETOFF); //green
			write_i2c_byte(PWM3, LEDSETON);
			break;
		case STATE_PROVISIONING:
			write_i2c_byte(PWM2, LEDSETON); //yellow
			write_i2c_byte(PWM3, LEDSETON);
			break; 
		case STATE_TEST_MODE:
			write_i2c_byte(PWM2, LEDSETON); //red
			write_i2c_byte(PWM3, LEDSETOFF);
			break;
		default:
			write_i2c_byte(PWM2, LEDSETON); //yellow
			write_i2c_byte(PWM3, LEDSETON);
			break; 
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	//
	// battery 
	//
	if (battery_state & BATTERY_VOLTAGE_OK)
	{
		write_i2c_byte(PWM4, LEDSETOFF); //green
		write_i2c_byte(PWM5, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	else if (battery_state & BATTERY_VOLTAGE_LOW)
	{
		write_i2c_byte(PWM4, LEDSETON); //yellow
		write_i2c_byte(PWM5, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	else
	{
		write_i2c_byte(PWM4, LEDSETON); //red
		write_i2c_byte(PWM5, LEDSETOFF);
		//
		// buzzer
		//
		if (alarm_muted)
			write_i2c_byte(PWM15, LEDSETOFF);
		else
			write_i2c_byte(PWM15, LEDSETON);
	}
	//
	// power
	//
	if (ac_loss_detected == WICED_TRUE)
	{
		write_i2c_byte(PWM6, LEDSETON); //red
		write_i2c_byte(PWM7, LEDSETOFF);
		//
		// buzzer
		//
		if (alarm_muted)
			write_i2c_byte(PWM15, LEDSETOFF);
		else
			write_i2c_byte(PWM15, LEDSETON);
	}
	{
		write_i2c_byte(PWM6, LEDSETOFF); //green
		write_i2c_byte(PWM7, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	//
	// main pump 
	//
	if (ac_pump_state == AC_PUMP_UNKNOWN)
	{
		write_i2c_byte(PWM8, LEDSETON); //yellow
		write_i2c_byte(PWM9, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	else if ((ac_pump_state & AC_PUMP_RUN_TOO_LONG) || (ac_pump_state & AC_PUMP_OVER_CURRENT))
	{
		write_i2c_byte(PWM8, LEDSETON); //red
		write_i2c_byte(PWM9, LEDSETOFF);
		//
		// buzzer
		//
		if (alarm_muted)
			write_i2c_byte(PWM15, LEDSETOFF);
		else
			write_i2c_byte(PWM15, LEDSETON);
	}
	else 
	{
		write_i2c_byte(PWM8, LEDSETOFF); //green
		write_i2c_byte(PWM9, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	//
	// backup pump
	//
	if (dc_pump_state == DC_PUMP_IDLE)
	{
		write_i2c_byte(PWM10, LEDSETOFF); //green
		write_i2c_byte(PWM11, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	else if ((dc_pump_state & DC_PUMP_RUN_TOO_LONG) || (dc_pump_state & DC_PUMP_OVER_CURRENT))
	{
		write_i2c_byte(PWM10, LEDSETON); //red
		write_i2c_byte(PWM11, LEDSETOFF);
		//
		// buzzer
		//
		if (alarm_muted)
			write_i2c_byte(PWM15, LEDSETOFF);
		else
			write_i2c_byte(PWM15, LEDSETON);
	}
	else
	{
		write_i2c_byte(PWM10, LEDSETOFF); //yellow
		write_i2c_byte(PWM11, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	
	//
	// high water
	//
	if (high_water_detected == WICED_TRUE)
	{
		write_i2c_byte(PWM12, LEDSETON); //red
		write_i2c_byte(PWM13, LEDSETOFF);
		//
		// buzzer
		//
		if (alarm_muted)
			write_i2c_byte(PWM15, LEDSETOFF);
		else
			write_i2c_byte(PWM15, LEDSETON);
	}
	else
	{
		write_i2c_byte(PWM12, LEDSETOFF); //green
		write_i2c_byte(PWM13, LEDSETON);
		//
		// buzzer
		//
		write_i2c_byte(PWM15, LEDSETOFF);
	}
	

	write_i2c_byte(GRPPWM, GRPPWMSET);
	write_i2c_byte(LEDOUT0, LEDSETON);
	write_i2c_byte(LEDOUT1, LEDSETON);
	write_i2c_byte(LEDOUT2, LEDSETON);
	write_i2c_byte(LEDOUT3, LEDSETON);

    ism_i2c_deinit();

	return WICED_TRUE;
}


void xx_leds_buzzer_init()
{
	/*
    result = ism_i2c_init();
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));

    buf[0] = MODE1;
    buf[1] = 0x80; //MODE1REGSET;
	length = 2;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("mode1 set error %d\r\n", result));
	else
		WPRINT_APP_INFO(("mode1 set success\r\n"));

    buf[0] = MODE2;
    buf[1] = MODE2REGSET;
	length = 2;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("mode2 set error %d\r\n", result));
	else
		WPRINT_APP_INFO(("mode2 set success\r\n"));

	buf[0] = 0x80 | GRPPWM; 
	buf[1] = GRPPWMSET;
	buf[2] = 0;
	buf[3] = 0xAA;
	buf[4] = 0xAA;
	buf[5] = 0xAA;
	buf[6] = 0xAA;
	length = 7;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("config set set error %d\r\n", result));
	else
		WPRINT_APP_INFO(("config set success\r\n"));
	memset(buf, 0, 50);
*/
	WPRINT_APP_INFO(("calling ism_i2c_init\r\n"));
	int i = 0;
    result = ism_i2c_init();
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));

	WPRINT_APP_INFO(("calling software reset\r\n"));
	i=0;
	buf[i++] = 0xD6;
	buf[i++] = 0xA5;
	buf[i++] = 0x5A;
	length = i;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c software reset error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("i2c software reset write success %d\r\n", result));

	WPRINT_APP_INFO(("calling ism_i2c_read of 25 registers\r\n"));
	memset(buf, 0, 50);
	i = 0;
	buf[i++] = 0x80; //0x8B;
	length = 0x1F; // 32;
    result = ism_i2c_read( (uint8_t*)&buf, length, 1000 );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c init read error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("i2c init read success %d\r\n", result));
	for (i=0; i<length;i++)
	{
		WPRINT_APP_INFO(("%02x ", buf[i]));
	}
	WPRINT_APP_INFO(("\r\n"));

	WPRINT_APP_INFO(("calling ism_i2c_write to 25 registers\r\n"));
	//
	// init tlc59116
	// 
	buf[0] = 0x80; // CONTROL = auto-inc all regs - addr 0
    buf[1] = 0x80; // 0x11; // MODE1 - defaults - turn on auto-inc // MODE1REGSET;
    buf[2] = 0x00; // MODE2 - defaults no bits set - MODE2REGSET;
	buf[3] = LEDSETON;  // PWM0
	buf[4] = LEDSETON;  // PWM1
	buf[5] = LEDSETON;  // PWM0
	buf[6] = LEDSETON;  // PWM1
	buf[7] = LEDSETON; // green
	buf[8] = LEDSETON; // pwm5
	buf[9] = LEDSETON; // green
	buf[10] = LEDSETON; // pwm7
	buf[11] = LEDSETON;  // yellow
	buf[12] = LEDSETON; // 
	buf[13] = LEDSETON; // yellow
	buf[14] = LEDSETON; //
	buf[15] = LEDSETON; // red
	buf[16] = LEDSETON; // 
	buf[17] = LEDSETON;
	buf[18] = LEDSETOFF; // pwm15
	buf[19] = 0xFF; //0xAF;  //GRPPWMSET;
	buf[20] = 0;
	buf[21] = 0xFF; //0xAA;
	buf[22] = 0xFF;
	buf[23] = 0xFF;
	buf[24] = 0xFF;
//	buf[25] = 0xD2; // sub addr
//	buf[26] = 0xD4; // sub addr
//	buf[27] = 0xD8; // sub addr
//	buf[28] = 0xD0; // allcalladr
	//
	// write to i2c
	//
	length = 25;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c init write error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("i2c init write success %d\r\n", result));


/*
	wiced_rtos_delay_milliseconds(100);

	i = 0;
	buf[i++] = 0xA2; // auto-inc - brightness only 
	buf[i++] = LEDSETON;  // PWM0
	buf[i++] = LEDSETON;  // PWM1
	buf[i++] = LEDSETON;  // PWM0
	buf[i++] = LEDSETON;  // PWM1
	buf[i++] = LEDSETON; // green
	buf[i++] = LEDSETON; // pwm5
	buf[i++] = LEDSETON; // green
	buf[i++] = LEDSETON; // pwm7
	buf[i++] = LEDSETON;  // yellow
	buf[i++] = LEDSETON; // 
	buf[i++] = LEDSETON; // yellow
	buf[i++] = LEDSETON; //
	buf[i++] = LEDSETON; // red
	buf[i++] = LEDSETON; // 
	buf[i++] = LEDSETON;
	buf[i++] = LEDSETOFF; // pwm15
	length = i;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c write error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("write success %d\r\n", result));
*/
}

wiced_bool_t xx_set_leds_buzzer()
{
	//
	// fix the blinking 
	//
	// n/c         - 0x02
	// n/c         - 0x03
	// led1 - WiFi - 0x04 - PWM2  - Red
	// led1 - WiFi - 0x05 - PWM3  - Green 
	// led2 - batt - 0x06 - PWM4  - Red
	// led2 - batt - 0x07 - PWM5  - Green
	// led3 - pwr  - 0x08 - PWM6  - Red
	// led3 - pwr  - 0x09 - PWM7  - Green
	// led4 - mpmp - 0x0A - PWM8  - Red
	// led4 - mpmp - 0x0B - PWM9  - Green
	// led5 - bpmp - 0x0C - PWM10 - Red
	// led5 - bpmp - 0x0D - PWM11 - Green
	// led6 - hwtr - 0x0E - PWM12 - Red
	// led6 - hwtr - 0x0F - PWM13 - Green
	// n/c         - 0x10 - PWM14        
	// buzzer      - 0x11 - PWM15
 	// 

	if ((server_time % 1000) != 0)
		return WICED_FALSE;

	memset(buf, 0, 50);

//    result = ism_i2c_init();
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("ism_i2c_init error %d\r\n", result));
	else
		WPRINT_APP_INFO(("ism_i2c_init success\r\n"));

	buf[0] = 0x80; // auto-inc all regs - addr 0 //MODE1;
    buf[1] = 0x80; // turn on auto-inc // MODE1REGSET;
    buf[2] = 0x00; // no bits set - MODE2REGSET;


//while(1)
//{
//	buf[0] = 0xA2; // control register, auto inc on, brightness only
	//
	// n/c
	//
	buf[3] = LEDSETOFF;  // PWM0
	buf[4] = LEDSETOFF;  // PWM1
	//
	// wi-fi led
	//
	switch (system_state)
	{
		case STATE_INITIALIZING:
			buf[5] = LEDSETON;   // red
			buf[6] = LEDSETOFF;  //
			break; 
		case STATE_RUNNING:
			buf[5] = LEDSETOFF;  // 
			buf[6] = LEDSETON;   // green
			break;
		case STATE_PROVISIONING:
			buf[5] = LEDSETON;  // yellow
			buf[6] = LEDSETON;  //
			break; 
		case STATE_TEST_MODE:
			buf[5] = LEDSETON;  // 
			buf[6] = LEDSETOFF;  // red
			break;
		default:
			buf[5] = LEDSETON;  // yellow
			buf[6] = LEDSETON;  //
			break; 
	}
	//
	// battery 
	//
	if (battery_state & BATTERY_VOLTAGE_OK)
	{
		buf[7] = LEDSETOFF; // green
		buf[8] = LEDSETON; // pwm5
	}
	else if (battery_state & BATTERY_VOLTAGE_LOW)
	{
		buf[7] = LEDSETON; // yellow
		buf[8] = LEDSETON; // 
	}
	else
	{
		buf[7] = LEDSETOFF; // red
		buf[8] = LEDSETON; // 
	}
	
	//
	// power
	//
	if (ac_loss_detected == WICED_TRUE)
	{
		buf[9] = LEDSETON; // red
		buf[10] = LEDSETOFF; // 
	}
	{
		buf[9] = LEDSETOFF; // green
		buf[10] = LEDSETON; // pwm7
	}
	//
	// main pump 
	//
	if (ac_pump_state == AC_PUMP_UNKNOWN)
	{
		buf[11] = LEDSETON;  // yellow
		buf[12] = LEDSETON; // 
	}
	else if ((ac_pump_state & AC_PUMP_RUN_TOO_LONG) || (ac_pump_state & AC_PUMP_OVER_CURRENT))
	{
		buf[11] = LEDSETON;  // red
		buf[12] = LEDSETOFF; // 
	}
	else 
	{
		buf[11] = LEDSETOFF;  // red
		buf[12] = LEDSETON; // 
	}
	//
	// backup pump
	//
	if (dc_pump_state == DC_PUMP_IDLE)
	{
		buf[13] = LEDSETON; // yellow
		buf[14] = LEDSETON; //
	}
	else if ((dc_pump_state & DC_PUMP_RUN_TOO_LONG) || (dc_pump_state & DC_PUMP_OVER_CURRENT))
	{
		buf[13] = LEDSETON; // red
		buf[14] = LEDSETOFF; // 
	}
	else
	{
		buf[13] = LEDSETOFF; // green
		buf[14] = LEDSETON; // 
	}
	
	//
	// high water
	//
	if (high_water_detected == WICED_TRUE)
	{
		buf[15] = LEDSETON; // red
		buf[16] = LEDSETOFF; // 
	}
	else
	{
		buf[15] = LEDSETOFF; // green
		buf[16] = LEDSETON; // 
	}
	
	//
	// n/c 
	//
	buf[17] = LEDSETOFF;
	//
	// buzzer
	//
	buf[18] = LEDSETOFF; // pwm15
	//
	//
	//
	buf[19] = 0xAF;  //GRPPWMSET;
	buf[20] = 0;
	buf[21] = 0xAA;
	buf[22] = 0xAA;
	buf[23] = 0xAA;
	buf[24] = 0xAA;
	buf[25] = 0xD2; // sub addr
	buf[26] = 0xD4; // sub addr
	buf[27] = 0xD8; // sub addr
	buf[28] = 0xD0; // allcalladr
	
	//
	// write to i2c
	//
	length = 29;
    result = ism_i2c_write( (uint8_t*)&buf, length );
	if (result != WICED_SUCCESS)
		WPRINT_APP_INFO(("i2c write error result %d\r\n", result));
	else
		WPRINT_APP_INFO(("write success %d\r\n", result));

//}
//	ism_i2c_deinit();
	return WICED_TRUE;
}

#define I2C_DELAY 10
extern void platform_init_i2c_sda( int is_input );


void write_i2c_byte2(uint8_t data_byte)
{
	uint8_t bit_count = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	WPRINT_APP_INFO( ( "starting write_i2c_byte %02x\n\r", data_byte ) );
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		if ((data_byte & 0x80) > 0)
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit high \n\r"  ) );
			wiced_gpio_output_high( I2C_SDA );
		}
		else
		{
//			WPRINT_APP_INFO( ( "write_i2c_byte bit low \n\r"  ) );
			wiced_gpio_output_low( I2C_SDA );
		}
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		data_byte = data_byte << 1;
	}
	// check the ack
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x nack\n\r", data_byte ) );
	}
	else
	{
		//this is an error
		WPRINT_APP_INFO( ( "write_i2c_byte %02x ack\n\r", data_byte ) );
	}
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
}

uint8_t  read_i2c_byte(wiced_bool_t last_byte)
{
	uint8_t bit_count = 0;
	uint8_t data_byte = 0;
	//
	// clock the byte out
	//
//	WPRINT_APP_INFO( ( "setting SDA to input\n\r" ) );
	platform_init_i2c_sda( 1 ); //1==input 0==output
	data_byte = 0;
	for (bit_count=0; bit_count < 8; bit_count++)
	{
		data_byte = data_byte << 1;
		wiced_gpio_output_high( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
		if (wiced_gpio_input_get( I2C_SDA ) == WICED_TRUE)
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit high\n\r" ) );
			data_byte = data_byte ^ 0x01;
		}
		else
		{
//			WPRINT_APP_INFO( ( "read_i2c_byte bit low\n\r" ) );
		}
		wiced_gpio_output_low( I2C_SCL );
		wiced_rtos_delay_microseconds(I2C_DELAY);
	}
	//
	// set SDA to output
	//
//	WPRINT_APP_INFO( ( "setting SDA to output\n\r" ) );
	platform_init_i2c_sda( 0 ); //1==input 0==output
	if (last_byte)
	{
		//
		// nack the slave
		//
		WPRINT_APP_INFO( ( "nack tlc59116\n\r" ) );
		wiced_gpio_output_high( I2C_SDA );
	}
	else
	{
		//
		// ack the slave
		//
		WPRINT_APP_INFO( ( "ack tlc59116\n\r" ) );
		wiced_gpio_output_low( I2C_SDA );
	}
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	WPRINT_APP_INFO( ( "read_i2c_byte returning %02x\n\r", data_byte ) );
	return data_byte;
}


void trigger_i2c_tlc59116(uint8_t control_register )
{
	//
	//start
	//
	WPRINT_APP_INFO( ( "trigger_i2c setting START\n\r" ) );
	wiced_gpio_output_low( I2C_SDA );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	//
	//write the address
	//
	write_i2c_byte2(0xC0); // address C0 not-write 0x80);
	//
	// control register
	//
	write_i2c_byte2(control_register);
	//
	//stop
	//
	/*
	wiced_rtos_delay_microseconds(5);
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(5);
	wiced_gpio_output_high( I2C_SDA );
	*/
	WPRINT_APP_INFO( ( "trigger_i2c_hdc1080 complete\n\r\n\r" ) );
}

void init_i2c_hdc1080(void )
{
	// configure the device
	WPRINT_APP_INFO( ( "init_i2c_hdc1080 started\n\r" ) );
	//
	// set the pointer to 0x02 - config register
	//
	trigger_i2c_tlc59116(0x02);
	//
	// MSB 0x10 (00010000) LSB 0x00 - both temp (14 bit) and humid (14bit res)
	//
	write_i2c_byte2(0x10); //
	write_i2c_byte2(0x00);
	//
	//stop
	//
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SDA );
	WPRINT_APP_INFO( ( "init_i2c_hdc1080 complete\n\r\n\r" ) );
}


void start_i2c_bus(void)
{
	//
	//start
	//
	WPRINT_APP_INFO( ( "start_i2c_bus START\n\r" ) );
	wiced_gpio_output_low( I2C_SDA );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_low( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);

}
void stop_i2c_bus(void)
{
	//
	//stop
	//
	WPRINT_APP_INFO( ( "stop_i2c_bus STOP\n\r" ) );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SCL );
	wiced_rtos_delay_microseconds(I2C_DELAY);
	wiced_gpio_output_high( I2C_SDA );

}

void read_i2c_registers_tlc59116(void )
{
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// address
	//
	stop_i2c_bus();
	start_i2c_bus();
	//
	//write the i2c address 1 read
	//
	write_i2c_byte2(0xC1);
	//
	// read 32 bytes
	//
	int i = 0;
	for (i=0;i<31;i++)
	{ 
		buf[i] = read_i2c_byte(WICED_FALSE);
	}
	buf[i] = read_i2c_byte(WICED_TRUE);
	for (i=0; i<32;i++)
	{
		WPRINT_APP_INFO(("%02x ", buf[i]));
		if (i==15)
			WPRINT_APP_INFO(("\r\n"));
	}
	WPRINT_APP_INFO(("\r\n"));
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "read_i2c_registers_tlc59116 complete\n\r" ) );
}

void reset_i2c_tlc59116(void )
{
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xD6); // reset byte 1
	write_i2c_byte2(0xA5); // reset byte 1
	write_i2c_byte2(0x5A); // reset byte 1
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "reset_i2c_tlc59116 complete\n\r" ) );
}

void init_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0x80); // address 0 write
	//
	// init tlc59116
	// 
	write_i2c_byte2(0x80);      // 00 MODE1 - defaults - turn on auto-inc
    write_i2c_byte2(0x80);      // 01 MODE2 - defaults no bits set - MODE2REGSET;
	write_i2c_byte2(LEDSETOFF);  // 02 PWM0
	write_i2c_byte2(LEDSETOFF);  // 03 PWM1
	write_i2c_byte2(LEDSETOFF);  // 04 PWM2
	write_i2c_byte2(LEDSETOFF);  // 05 PWM3
	write_i2c_byte2(LEDSETOFF);  // 06 PWM4
	write_i2c_byte2(LEDSETOFF);  // 07 PWM5
	write_i2c_byte2(LEDSETOFF);  // 08 PWM6
	write_i2c_byte2(LEDSETOFF);  // 09 PWM7
	write_i2c_byte2(LEDSETOFF);  // 0A PWM8
	write_i2c_byte2(LEDSETOFF);  // 0B PWM9
	write_i2c_byte2(LEDSETOFF);  // 0C PWM10
	write_i2c_byte2(LEDSETOFF);  // 0D PWM11
	write_i2c_byte2(LEDSETOFF);  // 0E PWM12
	write_i2c_byte2(LEDSETOFF);  // 0F PWM13
	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(LEDSETOFF);  // 11 PWM15
	write_i2c_byte2(GRPPWMSET); // 12 GRPPWM;
	write_i2c_byte2(0x00);      // 13 GRPFREQ
	write_i2c_byte2(0xAA);      // 14 LEDOUT0
	write_i2c_byte2(0xAA);      // 14 LEDOUT1
	write_i2c_byte2(0xAA);      // 14 LEDOUT2
	write_i2c_byte2(0xAA);      // 14 LEDOUT3
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "init_i2c_tlc59116 complete\n\r" ) );
}

void set_i2c_tlc59116(void)
{
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 started\n\r" ) );
	//
	// start
	//
	start_i2c_bus();
	//
	//write the i2c address
	//
	write_i2c_byte2(0xC0); // i2c address C0 );
	//
	// control register
	//
	write_i2c_byte2(0xA2); // address 2 only PWM register write
	//
	// init tlc59116
	// 
	write_i2c_byte2(LEDSETON);  // 02 PWM0
	write_i2c_byte2(LEDSETON);  // 03 PWM1
	write_i2c_byte2(LEDSETON);  // 04 PWM2
	write_i2c_byte2(LEDSETON);  // 05 PWM3
	write_i2c_byte2(LEDSETON);  // 06 PWM4
	write_i2c_byte2(LEDSETON);  // 07 PWM5
	write_i2c_byte2(LEDSETON);  // 08 PWM6
	write_i2c_byte2(LEDSETON);  // 09 PWM7
	write_i2c_byte2(LEDSETON);  // 0A PWM8
	write_i2c_byte2(LEDSETON);  // 0B PWM9
	write_i2c_byte2(LEDSETON);  // 0C PWM10
	write_i2c_byte2(LEDSETON);  // 0D PWM11
	write_i2c_byte2(LEDSETON);  // 0E PWM12
	write_i2c_byte2(LEDSETON);  // 0F PWM13
	write_i2c_byte2(LEDSETOFF);  // 10 PWM14
	write_i2c_byte2(LEDSETOFF);  // 11 PWM15
	//
	// stop
	//
	stop_i2c_bus();
	WPRINT_APP_INFO( ( "set_i2c_tlc59116 complete\n\r" ) );
}

/******************************************************
 *               Main - application starts here
 ******************************************************/
//
// Power-on
//
void application_start( )
{
    //
    // Init the wiced stack
    //
    wiced_init( );
	//
    // setup leds & buzzer
    //
//	leds_buzzer_init();
//	xx_leds_buzzer_init();
	reset_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	init_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	set_i2c_tlc59116();
	read_i2c_registers_tlc59116();
	int i = 0;
	while (1)
	{
		wiced_rtos_delay_milliseconds(1000);
//		read_i2c_registers_tlc59116();
//		wiced_rtos_delay_milliseconds(5000);
//		if ((i++ % 5) ==0)
//			leds_buzzer_init();
	}
    //
    // display app version
    //
    WPRINT_APP_INFO( ( "Starting Version %s \r\n", FIRMWARE_VERSION_STRING) );
    //
    // initialize the AD-C
    //
    result = wiced_adc_init( AC_CURRENT_SENSE, 480 );
    WPRINT_APP_INFO( ( "AC_CURRENT_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( V_SENSE, 480 );
    WPRINT_APP_INFO( ( "V_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( CURRENT_SENSE, 480 );
    WPRINT_APP_INFO( ( "CURRENT_SENSE init result %d\r\n", result ) );
    result = wiced_adc_init( AC_SENSE, 480 );
    WPRINT_APP_INFO( ( "AC_SENSE init result %d\r\n", result ) );
//    result = wiced_adc_init( CHARGER, 5 );
//    WPRINT_APP_INFO( ( "CHARGER init result %d\r\n", result ) );
    // Run the Start-up
    //
    run_common_startup_sequence();
    //
	// initialize the red green switch IRQ's
	//
//	result = wiced_gpio_input_irq_enable( R_FLOAT, IRQ_TRIGGER_FALLING_EDGE, r_float_falling_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq falling enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( R_FLOAT, IRQ_TRIGGER_RISING_EDGE, r_float_rising_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq risign enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( G_FLOAT, IRQ_TRIGGER_FALLING_EDGE, g_float_falling_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq falling enable %d\r\n", result ) );
//	result = wiced_gpio_input_irq_enable( G_FLOAT, IRQ_TRIGGER_RISING_EDGE, g_float_rising_handler, NULL);
//	WPRINT_APP_INFO( ( "r_float irq risign enable %d\r\n", result ) );
	result = wiced_gpio_input_irq_enable( ALARM_MUTE, IRQ_TRIGGER_FALLING_EDGE, alarm_mute_handler, NULL);
	WPRINT_APP_INFO( ( "alarm_mute irq falling enable %d\r\n", result ) );

	//
	// set up the self-test timer
	//
	self_test_timer = server_time;
    //
    // configure the brown out reset	//
	//
	// TODO	
	//set_bor();

	//
	// main loop
	//
    while ( 1 )
    {
    	//
    	// increment the main_thread_counter every 12 seconds
    	// system_monitor decrements every 12 seconds
    	// if main loop stops, system_monitor will reboot when
    	// main_thread_counter <= 0
    	//
    	current_time = host_rtos_get_time();
//		WPRINT_APP_INFO(("current_time %ld\r\n", current_time));
    	if ((current_time - last_time) > 12000)
    	{
    		last_time = current_time;
			main_thread_counter = 5;
//			WPRINT_APP_INFO(("main_thread_counter %d\r\n", main_thread_counter));
    	}
		//
		// set the LEDs
		//
		set_leds_buzzer();


        //
        // main state machine
        //
    	switch (system_state)
    	{
			//
			// start up the network
			//
			case STATE_INITIALIZING:
				//
				// run the initialization
				//
				run_common_initialization_sequence(FIRMWARE_VERSION);
				//
				// set up the self-test timer
				//
				self_test_timer = server_time;

				break;
			//
			// STA up running normally
			//
			case STATE_RUNNING:

				//
				// turn on system monitor
				//
				main_thread_counter_enabled = WICED_TRUE;
				//
				// check network status
				//
				if (wiced_network_is_up(WICED_STA_INTERFACE) != WICED_TRUE)
				{
					control_leds(LED_RED);
					system_state = STATE_INITIALIZING;
				}
				else
				{
					//
					// get the current system time
					//
					wiced_time_get_utc_time_ms  (&server_time);
	//
    // stop app
    //
	#ifdef STOP_APP_HERE
	main_thread_counter_enabled = WICED_FALSE;
	while ( 1 )
	{
		wiced_rtos_delay_milliseconds( 1000 );
	}
	#endif
					//
					// handle the ping timer
					//
					process_ping_timer();
					//
					// handle parameters time
					//
					process_get_parameters_timer();
					//
					// handle the reset timer
					//
					process_daily_reset_timer();
					//
					// handle the OTA firmware
					//
					process_ota_firmware();
					//
					// handle the clock & dns reset
					//
					process_clock_timer();
					//
					// look at the ac_pump
					//
					monitor_ac_pump();
					//
					// look at the dc_pump
					//
					monitor_dc_pump();
					//
					// look at ac sense
					//
					monitor_ac_sense();
					//
					// high water
					// 
					monitor_high_water();
					//
					// battery voltage
					//
					monitor_v_batt();
					//
					// self test timer check
					//
					check_self_test();
	            }
	    		break;
    		//
    		// provisioning AP state
    		//
        	case STATE_PROVISIONING:
        		//
        		// run provisioning AP
        		//
        		run_provisioning_ap();
        		break;
    		//
    		// OTA state
    		//
        	case STATE_OTA:
				WPRINT_APP_INFO(("OTA  \r\n"));
        		//
        		// run the ota AP
	      		run_ota_ap();
        		break;
			case STATE_TEST_MODE:
				//
				// run test_mode
				//
				test_mode();
				break;
    	}
	}
//        wiced_rtos_delay_milliseconds(500);
    //
    // de-init the wiced stack and end
    //
    wiced_deinit();
}


