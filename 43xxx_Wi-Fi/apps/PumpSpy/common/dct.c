
#include <stdlib.h>
#include "wiced.h"
#include "config_mode_dct.h"
#include "globals.h"



//
// Print the DCT APP record
//
void print_app_dct()
{
    WPRINT_APP_INFO( ( "\n" ) );
    WPRINT_APP_INFO( ( "Application DCT Configuration:\r\n" ) );
    WPRINT_APP_INFO( ( "NewConfigurationFlag                = %lx\r\n", (unsigned long int)dct.NewConfigurationFlag   ) );
    WPRINT_APP_INFO( ( "DeviceID                            = %lx%lx\r\n", (unsigned long int)(dct.DeviceID>>32), (unsigned long int)dct.DeviceID   ) );
    WPRINT_APP_INFO( ( "PumpSpyHostName                     = %s\r\n", dct.PumpSpyHostName   ) );
    WPRINT_APP_INFO( ( "string_var                          = %s \r\n", (char*)((config_mode_app_dct_t*)dct_ptr)->PumpSpyHostName ) );
    WPRINT_APP_INFO( ( "PumpSpyHostPort                     = %d\r\n", dct.PumpSpyHostPort   ) );
    WPRINT_APP_INFO( ( "startup_delay                       = %d\r\n", dct.startup_delay   ) );
    WPRINT_APP_INFO( ( "default_primary_pump_current_limit  = %d\r\n", dct.default_primary_pump_current_limit   ) );
    WPRINT_APP_INFO( ( "default_motor_current_limit         = %d\r\n", dct.default_motor_current_limit   ) );
}

void read_app_dct()
{
    //
    // read and lock into dct
    //
    wiced_dct_read_lock( (void**) &dct_ptr, WICED_FALSE, DCT_APP_SECTION, 0, sizeof(config_mode_app_dct_t));
    dct = *dct_ptr;
    //
    // unlock the DCT
    //
    wiced_dct_read_unlock( &dct, WICED_FALSE );
}

void write_app_dct()
{

//    dct.NewConfigurationFlag = 0xFFFFFFFF;
//    dct.DeviceID = deviceid;
        WPRINT_APP_INFO( ( "Writing Configuration\r\n" ) );
//        wiced_dct_read_lock( (void*) &dct, WICED_TRUE, DCT_APP_SECTION, 0, sizeof(config_mod_app_dct_t));
        wiced_dct_write( (const void*) &dct, DCT_APP_SECTION, 0, sizeof(config_mode_app_dct_t) );
//        wiced_dct_read_unlock( &dct, WICED_TRUE );
}

//
// configures the app dct
//

void configure_app_dct()
{
//
// set up app DCT
//
	WPRINT_APP_INFO(("setting up app DCT \r\n"));
//	dct.NewConfigurationFlag = 0x0A0B0C0D; //0xFFFFFFFF;
	dct.DeviceID = deviceid;
	memset(dct.PumpSpyHostName, 0, 100);
#ifdef LOCAL_SERVER
	memcpy(dct.PumpSpyHostName, "192.168.1.4", 11);
#endif
#ifdef API2_SERVER
	memcpy(dct.PumpSpyHostName, "173.241.229.43", 14);
#endif
#ifdef API3_SERVER
	memcpy(dct.PumpSpyHostName, "24.106.143.221", 14);
#endif
#ifdef API4_SERVER
	memcpy(dct.PumpSpyHostName, "208.40.73.158", 13);
#endif
#ifdef PRODUCTION_SERVER
	memcpy(dct.PumpSpyHostName, "www.pumpspy.com", 15);
#endif
#ifdef DEV_SERVER
	memcpy(dct.PumpSpyHostName, "dev.pumpspy.com", 15);
#endif
#ifdef TEST_SERVER
	memcpy(dct.PumpSpyHostName, "test.pumpspy.com", 16);
#endif
	dct.PumpSpyHostPort = 8081;
//	dct.startup_delay = 0;
	write_app_dct();
}

void print_wifi_dct()
{
    WPRINT_APP_INFO( ( "\r\n" ) );
    WPRINT_APP_INFO( ( "Wifi DCT Configuration:\r\n" ) );
    WPRINT_APP_INFO( ( "device_configured = %d\r\n", dct_wifi_config.device_configured ) );
    WPRINT_APP_INFO( ( "countrycode = %x\r\n", dct_wifi_config.country_code ) );
    WPRINT_APP_INFO( ( "WICED_COUNTRY_UNITED_STATES = %x\r\n", WICED_COUNTRY_UNITED_STATES ) );
    WPRINT_APP_INFO( ( "WICED_COUNTRY_UNITED_STATES_REV4 = %x\r\n", WICED_COUNTRY_UNITED_STATES_REV4 ) );
    WPRINT_APP_INFO( ( "WICED_COUNTRY_UNITED_STATES_NO_DFS = %x\r\n", WICED_COUNTRY_UNITED_STATES_NO_DFS ) );
    WPRINT_APP_INFO( ( "macaddress = %02X:%02X:%02X:%02X:%02X:%02X\r\n", dct_wifi_config.mac_address.octet[5], dct_wifi_config.mac_address.octet[4], dct_wifi_config.mac_address.octet[3], dct_wifi_config.mac_address.octet[2], dct_wifi_config.mac_address.octet[1], dct_wifi_config.mac_address.octet[0] ) );
    for (int i=0;i<1;i++)
    {
        WPRINT_APP_INFO( ( "stored ap list[%d].ssid = %s\r\n", i, dct_wifi_config.stored_ap_list[i].details.SSID.value ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].band = %d\r\n", i, dct_wifi_config.stored_ap_list[i].details.band ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].rssi = %d\r\n", i, dct_wifi_config.stored_ap_list[i].details.signal_strength ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].bssid = %02X:%02X:%02X:%02X:%02X:%02X\r\n", i, dct_wifi_config.stored_ap_list[i].details.BSSID.octet[5], dct_wifi_config.stored_ap_list[i].details.BSSID.octet[4], dct_wifi_config.stored_ap_list[i].details.BSSID.octet[3], dct_wifi_config.stored_ap_list[i].details.BSSID.octet[2], dct_wifi_config.stored_ap_list[i].details.BSSID.octet[1], dct_wifi_config.stored_ap_list[i].details.BSSID.octet[0] ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].bsstype = %d\r\n", i, dct_wifi_config.stored_ap_list[i].details.bss_type) );
        WPRINT_APP_INFO( ( "stored ap list[%d].channel = %d\r\n", i, dct_wifi_config.stored_ap_list[i].details.channel ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].maxdatarate = %ld\r\n", i, dct_wifi_config.stored_ap_list[i].details.max_data_rate) );
        WPRINT_APP_INFO( ( "stored ap list[%d].security = %d\r\n", i, dct_wifi_config.stored_ap_list[i].details.security ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].securitykey_length = %d\r\n", i, dct_wifi_config.stored_ap_list[i].security_key_length ) );
        WPRINT_APP_INFO( ( "stored ap list[%d].securitykey = %s\r\n", i, dct_wifi_config.stored_ap_list[i].security_key ) );
    }
    WPRINT_APP_INFO( ( "config_ap_settings.ssid = %s\r\n", dct_wifi_config.config_ap_settings.SSID.value ) );
    WPRINT_APP_INFO( ( "config_ap_settings.details_valid = %lx\r\n", dct_wifi_config.config_ap_settings.details_valid ) );
    WPRINT_APP_INFO( ( "config_ap_settings.security = %x\r\n", dct_wifi_config.config_ap_settings.security ) );
    WPRINT_APP_INFO( ( "config_ap_settings.security_key = %s\r\n", dct_wifi_config.config_ap_settings.security_key ) );
    WPRINT_APP_INFO( ( "config_ap_settings.security_key_length = %d\r\n", dct_wifi_config.config_ap_settings.security_key_length ) );
    WPRINT_APP_INFO( ( "config_ap_settings.channel = %d\r\n", dct_wifi_config.config_ap_settings.channel ) );

    WPRINT_APP_INFO( ( "soft_ap_settings.ssid = %s\r\n", dct_wifi_config.soft_ap_settings.SSID.value ) );
    WPRINT_APP_INFO( ( "soft_ap_settings.details_valid = %lx\r\n", dct_wifi_config.soft_ap_settings.details_valid ) );
    WPRINT_APP_INFO( ( "soft_ap_settings.security = %x\r\n", dct_wifi_config.soft_ap_settings.security ) );
    WPRINT_APP_INFO( ( "soft_ap_settings.security_key = %s\r\n", dct_wifi_config.soft_ap_settings.security_key ) );
    WPRINT_APP_INFO( ( "soft_ap_settings.security_key_length = %d\r\n", dct_wifi_config.soft_ap_settings.security_key_length ) );
    WPRINT_APP_INFO( ( "soft_ap_settings.channel = %d\r\n", dct_wifi_config.soft_ap_settings.channel ) );
}

void read_wifi_dct()
{
    //
    // read and lock wifi dct
    //
    wiced_dct_read_lock( (void**) &dct_wifi_config_ptr, WICED_FALSE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( platform_dct_wifi_config_t ) );
    dct_wifi_config = *dct_wifi_config_ptr;
    //
    // unlock the wifi DCT
    //
    wiced_dct_read_unlock( &dct_wifi_config, WICED_FALSE );
}

void write_wifi_dct()
{

    WPRINT_APP_INFO( ( "Writing wifi Configuration\r\n" ) );
    wiced_dct_write( (const void*) &dct_wifi_config, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );
}

//
// configures the wifi dct memory section
//

void configure_wifi_dct()
{
    //
    // set wi-fi is_configured, and other parameters needed
    //
    WPRINT_APP_INFO(("setting up wi-fi DCT \r\n"));
    //
    // country code and configured flag
    //
    dct_wifi_config.country_code = WICED_COUNTRY_UNITED_STATES;
    dct_wifi_config.device_configured = WICED_FALSE;
    //
    // stored ap list
    //
    dct_wifi_config.stored_ap_list[0].details.SSID.length = 7;
    memset(dct_wifi_config.stored_ap_list[0].details.SSID.value, 0, SSID_NAME_SIZE );
    memcpy(dct_wifi_config.stored_ap_list[0].details.SSID.value, "PumpSpy", 7);
//    dct_wifi_config.stored_ap_list[0].details.band = 1;
//    dct_wifi_config.stored_ap_list[0].details.signal_strength = 0;
//	dct_wifi_config.stored_ap_list[0].details.BSSID.octet[0] = 0; //00:00:00:00:00:00
//    dct_wifi_config.stored_ap_list[0].details.bss_type = 0;
//    dct_wifi_config.stored_ap_list[0].details.channel = 1;
//    dct_wifi_config.stored_ap_list[0].details.max_data_rate = 0;
    dct_wifi_config.stored_ap_list[0].details.security = WICED_SECURITY_OPEN;
    memset(dct_wifi_config.stored_ap_list[0].security_key, 0, SECURITY_KEY_SIZE);
    dct_wifi_config.stored_ap_list[0].security_key_length = 0;
#ifdef REMOTEDEBUG
    char passphrase[] = "EmmaAlexisIan07040704";
    dct_wifi_config.stored_ap_list[0].details.SSID.length = 7;
    memset(dct_wifi_config.stored_ap_list[0].details.SSID.value, 0, SSID_NAME_SIZE );
    memcpy(dct_wifi_config.stored_ap_list[0].details.SSID.value, "AP-T 5G", 7);
    dct_wifi_config.stored_ap_list[0].details.security = WICED_SECURITY_WPA2_AES_PSK;
    memcpy(dct_wifi_config.stored_ap_list[0].security_key, passphrase, 21);
    dct_wifi_config.stored_ap_list[0].security_key_length = 21;
#endif
    //
    // config ap
    //
    dct_wifi_config.config_ap_settings.SSID.length = 13;
    memset(dct_wifi_config.config_ap_settings.SSID.value, 0, SSID_NAME_SIZE );
    memcpy(dct_wifi_config.config_ap_settings.SSID.value, "PumpSpy", 7);
    dct_wifi_config.config_ap_settings.details_valid = CONFIG_VALIDITY_VALUE; // .details_valid = ca1bdf58
    dct_wifi_config.config_ap_settings.security = WICED_SECURITY_OPEN;
    memset(dct_wifi_config.config_ap_settings.security_key, 0, SECURITY_KEY_SIZE);
    dct_wifi_config.config_ap_settings.channel = 1; // CONFIG_AP_CHANNEL;
    //
    // soft ap
    //
    dct_wifi_config.soft_ap_settings.security_key_length = 0;
//    dct_wifi_config.soft_ap_settings.channel = 3;

    dct_wifi_config.soft_ap_settings.SSID.length = 13;
    memset(dct_wifi_config.soft_ap_settings.SSID.value, 0, SSID_NAME_SIZE );
    memcpy(dct_wifi_config.soft_ap_settings.SSID.value, "PumpSpy", 7);
    dct_wifi_config.soft_ap_settings.details_valid = CONFIG_VALIDITY_VALUE; // .details_valid = ca1bdf58
    dct_wifi_config.soft_ap_settings.security = WICED_SECURITY_OPEN;
    memset(dct_wifi_config.soft_ap_settings.security_key, 0, SECURITY_KEY_SIZE);
    dct_wifi_config.soft_ap_settings.security_key_length = 0;
//    dct_wifi_config.soft_ap_settings.channel = 3;

    write_wifi_dct();
}

wiced_bool_t check_wifi_dct()
{
    //
    // set wi-fi is_configured, and other parameters needed
    //
//    WPRINT_APP_INFO(("checking wi-fi DCT \r\n"));

//    dct_wifi_config.country_code = WICED_COUNTRY_UNITED_STATES;
//    dct_wifi_config.device_configured = WICED_FALSE;

//    dct_wifi_config.stored_ap_list[0].details.SSID.length = 13;
//    memset(dct_wifi_config.stored_ap_list[0].details.SSID.value, 0, SSID_NAME_SIZE );
//    memcpy(dct_wifi_config.stored_ap_list[0].details.SSID.value, "PumpSpyOutlet", 13);

    if (memcmp(dct_wifi_config.stored_ap_list[0].details.SSID.value, "PumpSpy", 7) != 0)
    {
        WPRINT_APP_INFO( ( "stored ap incorrect %s\r\n", dct_wifi_config.stored_ap_list[0].details.SSID.value ) );
    	return WICED_FALSE;
    }
//    dct_wifi_config.stored_ap_list[0].details.band = 1;
//    dct_wifi_config.stored_ap_list[0].details.signal_strength = 0;
//	dct_wifi_config.stored_ap_list[0].details.BSSID.octet[0] = 0; //00:00:00:00:00:00
//    dct_wifi_config.stored_ap_list[0].details.bss_type = 0;
//    dct_wifi_config.stored_ap_list[0].details.channel = 1;
//    dct_wifi_config.stored_ap_list[0].details.max_data_rate = 0;
//    dct_wifi_config.stored_ap_list[0].details.security = WICED_SECURITY_OPEN;
//    memset(dct_wifi_config.stored_ap_list[0].security_key, 0, SECURITY_KEY_SIZE);
//    dct_wifi_config.stored_ap_list[0].security_key_length = 0;

//    dct_wifi_config.config_ap_settings.SSID.length = 13;
//    memset(dct_wifi_config.config_ap_settings.SSID.value, 0, SSID_NAME_SIZE );
//    memcpy(dct_wifi_config.config_ap_settings.SSID.value, "PumpSpyOutlet", 13);
//    dct_wifi_config.config_ap_settings.details_valid = CONFIG_VALIDITY_VALUE; // .details_valid = ca1bdf58
//    dct_wifi_config.config_ap_settings.security = WICED_SECURITY_OPEN;
//    memset(dct_wifi_config.config_ap_settings.security_key, 0, SECURITY_KEY_SIZE);
//    dct_wifi_config.soft_ap_settings.security_key_length = 0;
//    dct_wifi_config.soft_ap_settings.channel = 3;

    if (memcmp(dct_wifi_config.config_ap_settings.SSID.value, "PumpSpy", 7) != 0)
    {
        WPRINT_APP_INFO( ( "config ap incorrect %s\r\n", dct_wifi_config.config_ap_settings.SSID.value ) );
    	return WICED_FALSE;
    }

//   dct_wifi_config.soft_ap_settings.SSID.length = 13;
//    memset(dct_wifi_config.soft_ap_settings.SSID.value, 0, SSID_NAME_SIZE );
//    memcpy(dct_wifi_config.soft_ap_settings.SSID.value, "PumpSpyOutlet", 13);
//    dct_wifi_config.soft_ap_settings.details_valid = CONFIG_VALIDITY_VALUE; // .details_valid = ca1bdf58
//    dct_wifi_config.soft_ap_settings.security = WICED_SECURITY_OPEN;
//    memset(dct_wifi_config.soft_ap_settings.security_key, 0, SECURITY_KEY_SIZE);
//    dct_wifi_config.soft_ap_settings.security_key_length = 0;
//    dct_wifi_config.soft_ap_settings.channel = 3;

    if (memcmp(dct_wifi_config.soft_ap_settings.SSID.value, "PumpSpy", 7) != 0)
    {
        WPRINT_APP_INFO( ( "soft ap incorrect %s\r\n", dct_wifi_config.soft_ap_settings.SSID.value) );
    	return WICED_FALSE;
    }

    return WICED_TRUE;

//    write_wifi_dct();
}

void print_network_dct()
{
    WPRINT_APP_INFO( ( "\r\n" ) );
    WPRINT_APP_INFO( ( "Network DCT Configuration:\r\n" ) );
    WPRINT_APP_INFO( ( "Host Name = %s\r\n", dct_network_config.hostname.value  ));
    WPRINT_APP_INFO( ( "interface = %d\r\n", dct_network_config.interface  ));
}

void read_network_dct()
{
    //
    // read and lock network dct
    //
    wiced_dct_read_lock( (void**) &dct_network_config_ptr, WICED_FALSE, DCT_NETWORK_CONFIG_SECTION, 0, sizeof(platform_dct_network_config_t) );
    dct_network_config = *dct_network_config_ptr;
    //
    // unlock the nework DCT
    //
    wiced_dct_read_unlock( &dct_network_config, WICED_FALSE );
}

void write_network_dct()
{
    //
    // write network dct
    //
	WPRINT_APP_INFO( ( "Writing network Configuration\r\n" ) );
    wiced_dct_write( (const void*) &dct_network_config_ptr, DCT_NETWORK_CONFIG_SECTION, 0, sizeof(platform_dct_network_config_t) );

}

//
// sets up the network dct section
//

void configure_network_dct()
{
    WPRINT_APP_INFO( ( "setting up Network DCT\r\n" ) );
	memset(dct_network_config.hostname.value, 0, 15);
	memcpy(dct_network_config.hostname.value, "PumpSpy", 7);
	dct_network_config.interface = 0;
	write_network_dct();
	wiced_network_set_hostname("PumpSpy");
}


