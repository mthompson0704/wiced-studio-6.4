#This Script will replace XIP_OBJS symbol with passed objects
# Example :
#             Suppose we have ld_file as below:
#                ...
#                XIP_OBJS(section_command)
#                ..
#            and given inputs are as below:
#            perl replace_xip_objects.pl file_name obj1 obj2 obj3 lib_obj1 lib_obj2
#
#            Output ld file for given input:
#               ...
#               obj1(section_command)
#               obj2(section_command)
#               obj3(section_command)
#               lib_obj1(section_command)
#               lib_obj2(section_command)
#               ...
#

#open the ld file
open(FILE, $ARGV[0])|| die "Failed to run $ARGV[0] \n";
#get ld file data
my @array = <FILE>;
close(FILE);

my $count_args = $#ARGV + 1;
my $replace_str = "";

#write output to the file
open(my $fh, '>', $ARGV[0]) or die "Could not open file $ARGV[0]";

foreach $_ (@array) {
    if ( $_ =~ m/XIP_OBJS/ )
    {
        for (my $i=1; $i<$count_args; $i++) {
            $replace_str .= $ARGV[$i];
            if ($i != $count_args-1)
            {
                $replace_str .= $';
            }
            $replace_str .= "\t";
        }
        $_ =~ s/XIP_OBJS/$replace_str/g;
    }
    print $fh $_;
}
close $fh;
exit;