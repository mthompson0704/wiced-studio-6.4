/*
 * Copyright 2016, PumpSpy
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of PumpSpy;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of PumpSpy.
 */

#include <stdlib.h>
#include "wiced.h"
#include "http_wiced.h"
#include "string.h"
#include "math.h"
//#include "wwd_debug.h"
#include "wiced_framework.h"
#include "wiced_ota_server.h"
//#include "default_wifi_config_dct.h"
//#include "network_config_dct.h"
#include "platform.h"
//#include "aes.h"
#include "waf_platform.h"
//#include "wiced_management.h"

#include "config_mode_dct.h"
#include "globals.h"
#include "dct.h"
#include "get_deviceid.h"
#include "control_leds.h"
#include "config_button_handler.h"
#include "restful_methods.h"
#include "new_firmware.h"
#include "dns_lookup.h"
#include "initialization.h"
#include "timers.h"
#include "access_points.h"
#include "set_bor.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

//#define STOP_APP_HERE
//#define NO_LEDS
//#define ENABLE_SYSTEM_MONITOR
//#define TEST_AES_128
//#define REMOTEDEBUG

#define FIRMWARE_VERSION 4.06
#define FIRMWARE_VERSION_STRING "4.06 - Outlet wiced 6.4 "
float firmware_version = 4.06;




/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/** Runs device configuration (if required)
 *
 * @param[in] config  : an array of user configurable variables in configuration_entry_t format.
 *                      The array must be terminated with a "null" entry {0,0,0,0}
 *
 * @return    WICED_SUCCESS
 */

/******************************************************
 *               Static Variable Definitions
 ******************************************************/



static int motor_current_limit;
static int motor_run_timeout;

/******************************************************
 *               Static IRQ Function Definitions
 ******************************************************/
//
// high water IRQ handler
//
/*
static void high_water_handler( void* arg )
{
    WPRINT_APP_INFO (( "high water\n"));
    high_water_detected = WICED_TRUE;
}
*/

/******************************************************
 *               Function Definitions
 ******************************************************/


//static void pump_monitor_thread(uint32_t arg)
void monitor_pump()
{
	//
	// this will be the threaded routine
	// will loop forever
	// set up the a2d
	//
    uint16_t adc_sample0 = 0;
    uint16_t adc_sample_buffer[80];
    uint16_t adc_average_buffer[80];
    uint16_t adc_sample_index = 0;
    uint64_t adc_sample_sum = 0;
    uint16_t adc_sample_average = 0;
    uint16_t adc_peak_average = 0;
    uint16_t adc_low_average = 0xFFFF;
//    uint16_t adc_diff_value = 0;
//    uint16_t adc_peak_diff_value = 0;
    int i;
    int j;
//    int k = 0;
//    wiced_bool_t pump_is_running = WICED_FALSE;

//    WPRINT_APP_INFO( ( "threading pump_momitor_thread %ld \r\n", arg ) );

//    result = wiced_time_set_utc_time_ms(&pump_start_time);
//    WPRINT_APP_INFO( ( "set time init result %d\r\n", result ) );

    //
    // init the A2d
    //
//    result = wiced_adc_init( PUMP_CURRENT, 3 );
//    WPRINT_APP_INFO( ( "PUMP CURRENT init result %d\r\n", result ) );
    //
    // take the raw data values and init the average buffer
    // this will take 8 mSec
	// sample interval = 200 uSec
	// 80 samples = 16 mSec - full AC wave time
    //
	for (adc_sample_index=0; adc_sample_index<80; adc_sample_index++)
	{
		result = wiced_adc_take_sample( PUMP_CURRENT, &adc_sample0 );
		adc_sample_buffer[adc_sample_index] = adc_sample0;
		adc_average_buffer[adc_sample_index] = 1700;
		wiced_rtos_delay_microseconds(200);
	}
    //
    // calculate the averages
    //
	for (adc_sample_index=0; adc_sample_index<80; adc_sample_index++)
    {
    	//
		// find the average of the last 10
		//
		i = adc_sample_index;
		adc_sample_sum = 0;
		for (j=0; j<10; j++)
		{
			adc_sample_sum += adc_sample_buffer[i];
			if (i == 0)
				i = 79;
			else
				i--;
		}
		adc_sample_average = adc_sample_sum / 10;
		adc_average_buffer[adc_sample_index] = adc_sample_average;
    }
	//
	// set the peak and low
	// scan the whole average buffer
	//
	adc_peak_average = 0;
	adc_low_average = 0xFFFF;

	for (adc_sample_index=0; adc_sample_index<80; adc_sample_index++)
	{
		if (adc_average_buffer[adc_sample_index] > adc_peak_average)
			adc_peak_average = adc_average_buffer[adc_sample_index];;
		if (adc_average_buffer[adc_sample_index] < adc_low_average)
			adc_low_average = adc_average_buffer[adc_sample_index];
	}
	//
	// calc the difference
	//
	pump_run_current = adc_peak_average - adc_low_average;
//    WPRINT_APP_INFO( ( "run %d peak %d low %d \r\n", pump_run_current, adc_peak_average, adc_low_average) );

}

#define GET_pump_outlet_parameters \
    "GET /pump_outlet_parameters/%ld%ld HTTP/1.1\r\n" \
    "Host: %s:%d \r\n" \
    "Content-Type: application/json;charset=UTF-8 \r\n" \
    "authorization: Bearer %s \r\n" \
    "Content-Length: %d \r\n" \
    "Connection: close\r\n" \
    "\r\n"

wiced_result_t get_parameters()
{
    char *p;
    char *pos;
    int i;

    clear_post_buffers();
    WPRINT_APP_INFO(("GET /pump_outlet_parameters \r\n"));
    sprintf(post, GET_pump_outlet_parameters, (unsigned long int)(dct.DeviceID/10000000), (unsigned long int)(dct.DeviceID%10000000), dct.PumpSpyHostName, dct.PumpSpyHostPort, bearer_token, strlen(GET_tm));
//    WPRINT_APP_INFO(( post ));
    result = post_the_post();
    if ( result == WICED_SUCCESS )
    {
//		WPRINT_APP_INFO( ( "Server returned\r\n%s", buffer ) );
		if (memcmp(buffer, "HTTP/1.1 200 OK", 15) != 0)
			return WICED_ERROR;
		//
		// parse the approved byte here
		/*
		{
			"motor_current_limit": 12000,
			"motor_run_timeout": 300
		}
		*/
		//
		//    scan buffer for motor_current_limit
		//
		pos = strstr((char*)buffer, "\"motor_current_limit\":");
//		WPRINT_APP_INFO( ( "pos = %d \r\n", (int)pos ) );
		if (pos != NULL)
		{
			pos += 22;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_current_limit = i;
//			WPRINT_APP_INFO( ( "motor_current_limit |%d| \r\n", motor_current_limit ) );
		}
		else
		{
			motor_current_limit = 10000;
		}
		//
		//    scan buffer for motor_current_limit
		//
		pos = strstr((char*)buffer, "\"motor_run_timeout\":");
		if (pos != NULL)
		{
			pos += 20;
			p = pos;
//			WPRINT_APP_INFO( ( "\r\npos points to |%s| \r\n", pos ) );
			i = 0;
			i  = strtol (pos, &p, 10);
			motor_run_timeout = i;
//			WPRINT_APP_INFO( ( "motor_run_timeout |%d| \r\n", motor_run_timeout ) );
		}
		else
		{
			motor_run_timeout = 300000;
		}
		//
		// return success
		//
		return WICED_SUCCESS;
	}
	else
	{
		WPRINT_APP_INFO( ( "GET /pump_outlet_parameters failed: %u\r\n", result ) );
		return WICED_ERROR;
	}
    //
    // it prints the json string - so it's sent to microchip
    //
    return result;
}


void test_mode()
{
    wiced_bool_t high_water_signal = WICED_FALSE;
    wiced_bool_t last_high_water_signal = WICED_FALSE;
    wiced_bool_t high_water_passed = WICED_FALSE;
    wiced_bool_t ac_loss_passed = WICED_FALSE;
    wiced_bool_t ac_current_passed = WICED_FALSE;
    wiced_bool_t test_mode_value_changed = WICED_TRUE;
    wiced_bool_t wifi_passed = WICED_TRUE;
    wiced_bool_t last_config_button_passed = WICED_FALSE;
    //
	// turn off system monitor
	//
	main_thread_counter_enabled = WICED_FALSE;
	//
	// reset Config Flag - boot to provisioning mode next time
	//
	if (dct.NewConfigurationFlag != 0x0F0F0F0F)
	{
		dct.NewConfigurationFlag = 0x0F0F0F0F;
		write_app_dct();
		//
		// reset the DCT
		//
		configure_app_dct();
		configure_network_dct();
		configure_wifi_dct();
	}
	//
	// display test parameters
	//
	if (test_mode_value_changed == WICED_TRUE)
	{
		WPRINT_APP_INFO(("\033[2J\033[1;1H"));
		WPRINT_APP_INFO(( "DeviceID =      %ld%ld\r\r\n", (unsigned long int)(deviceid/10000000), (unsigned long int)(deviceid%10000000)));
		WPRINT_APP_INFO(( "Firmware Ver. = %s\r\n", FIRMWARE_VERSION_STRING));
		WPRINT_APP_INFO(( "Wi-Fi Scan =    %d\r\n", wifi_passed ));
		WPRINT_APP_INFO(( "Wi-Fi Memory =  %d\r\n", check_wifi_dct() ));
		WPRINT_APP_INFO(( "High Water =    %d\r\n", high_water_passed ));
		WPRINT_APP_INFO(( "Config Button = %d\r\n", config_button_passed ));
		WPRINT_APP_INFO(( "Power Fail =    %d\r\n", ac_loss_passed ));
		WPRINT_APP_INFO(( "Current Sense = %d\r\n", ac_current_passed ));
		WPRINT_APP_INFO(( "Current =       %d, %f0.0\r\n", pump_run_current, (double)pump_run_current/215 ));
		WPRINT_APP_INFO(( "\r\n" ));
		test_mode_value_changed = WICED_FALSE;
	}
	// .75A - 130-230
	// 5.8A - 1200
	// 10.5 - 2000
	// HW Test
	//
	//
	// Wi-Fi Config & DCT Memory Test
	//
	//
	// check high water
	//
	last_high_water_signal = high_water_signal;
	high_water_signal = wiced_gpio_input_get( HIGH_WATER ) ? WICED_FALSE : WICED_TRUE;
	if (high_water_signal != last_high_water_signal)
	{
		high_water_passed = WICED_TRUE;
		test_mode_value_changed = WICED_TRUE;
	}
	//
	// check AC loss
	//
	ac_loss_detected = wiced_gpio_input_get( AC_LOSS ); // ? WICED_FALSE : WICED_TRUE;
	if (ac_loss_detected == WICED_TRUE)
	{
		ac_loss_passed = WICED_TRUE;
		test_mode_value_changed = WICED_TRUE;
	}
	//
	// pump
	//
	monitor_pump();
	if ((pump_run_current < 430) && (pump_run_current > 215))
	{
		if (ac_current_passed == WICED_FALSE)
		{
			test_mode_value_changed = WICED_TRUE;
		}
		ac_current_passed = WICED_TRUE;
	}
	else
	{
		if (ac_current_passed == WICED_TRUE)
		{
			test_mode_value_changed = WICED_TRUE;
		}
		ac_current_passed = WICED_FALSE;
	}
	//
	// config button
	//
	if ((config_button_passed == WICED_TRUE) && (last_config_button_passed == WICED_FALSE))
	{
		test_mode_value_changed = WICED_TRUE;
		last_config_button_passed = config_button_passed;
	}
	//
	// set the LEDs
	//
	if ((ac_current_passed == WICED_TRUE) && (config_button_passed == WICED_TRUE) &&
			(high_water_passed == WICED_TRUE) && (wifi_passed == WICED_TRUE) )
		control_leds(LED_GREEN);
	else
		control_leds(LED_RED);
	//
	// delay
	//
	wiced_rtos_delay_milliseconds( 100 );
}


/******************************************************
 *               Main - application starts here
 ******************************************************/
//
// Power-on
//
void application_start( )
{
    wiced_bool_t high_water_sent_to_server = WICED_FALSE;
    wiced_bool_t low_water_sent_to_server = WICED_TRUE;
    wiced_bool_t ac_loss_sent_to_server = WICED_FALSE;
//    wiced_bool_t ac_loss_recovery_sent_to_server = WICED_FALSE;
    wiced_bool_t high_water_signal = WICED_FALSE;
    wiced_bool_t last_high_water_signal = WICED_FALSE;
    wiced_bool_t pump_is_running = WICED_FALSE;
//    uint16_t adc_baseline = 0;
    //
    // Init the wiced stack
    //
    wiced_init( );
    //
    // display app version
    //
    WPRINT_APP_INFO( ( "Starting Version %s \r\n", FIRMWARE_VERSION_STRING) );
    //
    // initialize the AD-C
    //
    result = wiced_adc_init( PUMP_CURRENT, 480 );
    WPRINT_APP_INFO( ( "PUMP CURRENT init result %d\r\n", result ) );
    //
    // Run the Start-up
    //
    run_common_startup_sequence();
    //
    // configure the brown out reset
    //
	set_bor();
    //
    // stop app
    //
	#ifdef STOP_APP_HERE
	main_thread_counter_enabled = WICED_FALSE;
	while ( 1 )
	{
		wiced_rtos_delay_milliseconds( 1000 );
	}
	#endif
	//
	// main loop
	//
    while ( 1 )
    {

    	//
    	// increment the main_thread_counter every 12 seconds
    	// system_monitor decrements every 12 seconds
    	// if main loop stops, system_monitor will reboot when
    	// main_thread_counter <= 0
    	//
    	current_time = host_rtos_get_time();
//		WPRINT_APP_INFO(("current_time %ld\r\n", current_time));
    	if ((current_time - last_time) > 12000)
    	{
    		last_time = current_time;
			main_thread_counter = 5;
//			WPRINT_APP_INFO(("main_thread_counter %d\r\n", main_thread_counter));
    	}
        //
        // main state machine
        //
    	switch (system_state)
    	{
    	//
    	// start up the network
    	//
    	case STATE_INITIALIZING:
    		//
    		// run the initialization
    		//
    		run_common_initialization_sequence(FIRMWARE_VERSION);
    		break;
    	//
    	// STA up running normally
    	//
    	case STATE_RUNNING:
			//
			// configuration is complete
			// LEDs red off, green on
			//
//			control_leds(LED_GREEN);
            //
            // turn on system monitor
            //
            main_thread_counter_enabled = WICED_TRUE;
            //
            // check network status
            //
            if (wiced_network_is_up(WICED_STA_INTERFACE) != WICED_TRUE)
            {
				control_leds(LED_RED);
            	system_state = STATE_INITIALIZING;
            }
            else
            {
				//
				// get the current system time
				//
				wiced_time_get_utc_time_ms  (&server_time);
				//
				// handle the ping timer
				//
				process_ping_timer();
				//
				// handle parameters time
				//
				process_get_parameters_timer();
				//
				// handle the reset timer
				//
				process_daily_reset_timer();
				//
				// handle the OTA firmware
				//
				process_ota_firmware();
				//
				// handle the clock & dns reset
				//
				process_clock_timer();
				/*
				//
				// graph the a2d current every 10 seconds
				//
				if ((server_time - graph_timer) > 10000)
				{
					//
					// graph current - there are 20k samples done in a 2 second window
					//
					WPRINT_APP_INFO(("graphing timer \r\n"));
					graph_current();
					graph_timer = server_time;
				}
				*/
				//
				// check for ac loss
				//
				ac_loss_detected = wiced_gpio_input_get( AC_LOSS ); // ? WICED_FALSE : WICED_TRUE;
				if (ac_loss_detected == WICED_TRUE)
				{
					WPRINT_APP_INFO(( "AC Loss \r\n"));
					//
					// LEDs red
					//
					control_leds(LED_OFF);
					if (ac_loss_sent_to_server == WICED_FALSE)
					{
						//
						// POST /pump_outlet_alerts
						//
						wiced_time_get_utc_time_ms(&server_time);
						ac_loss_sent_to_server = post_outlet_alerts(AC_LOSS_ALERT, server_time, 1);
					}
				}
				//
				// check the pump run flage
				//
				monitor_pump();
				if ((server_time % 5000) == 0)
//					WPRINT_APP_INFO(("pump_has_run = %d - %d \r\n", 1, 1 ));
					WPRINT_APP_INFO(("pump_run_current = %d\r\n", pump_run_current ));
				if (pump_run_current < 125)
				{
		//			WPRINT_APP_INFO(("pump idle\r\n"));

					if (pump_is_running == WICED_TRUE)
					{
						//
						// pump has now stopped
						//
						pump_end_time = pump_end_time - pump_start_time;
						//
						// check if pump ran long enough
						//
						if (pump_end_time >= 250)
						{
							WPRINT_APP_INFO(("pump ran %d time with %d current\r\n", (uint16_t)pump_end_time, pump_peak_current));
							pump_run_time = pump_end_time;
							//
							// POST /pump_outlet_cycles
							//
							post_pump_cycles(pump_start_time, pump_run_time, pump_peak_current);
						}
						pump_is_running = WICED_FALSE;
					}
				}
				else
				{
		//			WPRINT_APP_INFO(("%d, %d, %d, %d, %d, %d, %d\r\n", adc_sample_index, adc_sample_buffer[adc_sample_index], adc_average_buffer[adc_sample_index], adc_sample_average, adc_peak_average, adc_low_average, adc_diff_value));
					if (pump_is_running == WICED_FALSE)
					{
						result = wiced_time_get_utc_time_ms  (&pump_start_time);
		//			    WPRINT_APP_INFO( ( "get start time result %d\r\n", result ) );
						pump_end_time = pump_start_time;
						pump_peak_current = 0;
						pump_is_running = WICED_TRUE;
						WPRINT_APP_INFO(("pump started %d time with %d current\r\n", (uint16_t)pump_start_time, pump_run_current));
					}
					else
					{
		//				WPRINT_APP_INFO(("pump running \r\n"));
						result = wiced_time_get_utc_time_ms  (&pump_end_time);
		//			    WPRINT_APP_INFO( ( "get end time result %d\r\n", result ) );
						//
						// set peak current after 2 seconds
						//
						if ((pump_end_time - pump_start_time) >= 2000)
						{
							if (pump_peak_current < pump_run_current)
								pump_peak_current = pump_run_current;
						}
						else
						{
							pump_peak_current = 0;
						}
						//
						// check for running too long
						//
						//motor_run_timeout = 60000;
						if ((pump_end_time - pump_start_time) > motor_run_timeout)
						{
							WPRINT_APP_INFO(("pump running too long - posting data\r\n"));
							//
							// POST /pump_outlet_cycles
							//
							post_pump_cycles(pump_end_time, (pump_end_time - pump_start_time), pump_peak_current);
							pump_start_time = pump_end_time;

						}
					}
				}
				//
				// Check High Water
				//
				last_high_water_signal = high_water_signal;
				high_water_signal = wiced_gpio_input_get( HIGH_WATER ) ? WICED_FALSE : WICED_TRUE;
				//
				// it's changed - reset the timers
				//
				if (high_water_signal != last_high_water_signal)
				{
					high_water_timer = server_time;
				}
				//
				// its the same, check the timers, and set high_water_detected if long enough
				//
				else
				{
					if ((server_time - high_water_timer) >= HIGH_WATER_DEBOUNCE_TIME)
					{
						if (high_water_signal == WICED_TRUE)
							high_water_detected = WICED_TRUE;
						else
							high_water_detected = WICED_FALSE;
					}

				}
				if (high_water_detected == WICED_TRUE)
				{
					//
					// LEDs red
					//
					control_leds(LED_RED);
					if (high_water_sent_to_server == WICED_FALSE)
					{
						//
						// POST /pump_outlet_alerts
						//
						low_water_sent_to_server = WICED_FALSE;
						wiced_time_get_utc_time_ms(&server_time);
						high_water_sent_to_server = post_outlet_alerts(HIGH_WATER_ALERT, server_time, 1);
					}
				}
				else
				{
					//
					// LEDs green
					//
					control_leds(LED_GREEN);
					if (low_water_sent_to_server == WICED_FALSE)
					{
						//
						// POST /pump_outlet_alerts - with value = 0
						//
						high_water_sent_to_server = WICED_FALSE;
						wiced_time_get_utc_time_ms(&server_time);
						low_water_sent_to_server = post_outlet_alerts(HIGH_WATER_ALERT, server_time, 0);
					}
				}
            }
    		break;
		//
		// provisioning AP state
		//
    	case STATE_PROVISIONING:
    		//
    		// run provisioning AP
    		//
    		run_provisioning_ap();
    		break;
		//
		// OTA state
		//
    	case STATE_OTA:
    		//
    		// run the ota AP
    		run_ota_ap();
    		break;
    	case STATE_TEST_MODE:
    		//
    		// run test_mode
    		//
    		test_mode();
    		break;
    	}

//        wiced_rtos_delay_milliseconds(500);
    }
    //
    // de-init the wiced stack and end
    //
    wiced_deinit();
}

