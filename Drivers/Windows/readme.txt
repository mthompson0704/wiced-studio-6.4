Windows drivers for FTDI
========================

Drivers in this folder are installed by WICED Studio installer. 

To manually install the "USB Serial Port Driver" and "USB Serial Converter Driver" on Windows OS, 
double click the DPInst exe for appropriate OS to install the drivers.